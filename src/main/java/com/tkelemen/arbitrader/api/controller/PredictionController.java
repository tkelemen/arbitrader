package com.tkelemen.arbitrader.api.controller;

import com.tkelemen.arbitrader.api.converter.PredictionConverter;
import com.tkelemen.arbitrader.api.model.Prediction;
import com.tkelemen.arbitrader.api.model.RawPredictionData;
import com.tkelemen.arbitrader.api.model.Resolution;
import com.tkelemen.arbitrader.db.repository.PredictionRepository;
import com.tkelemen.arbitrader.model.Exchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/prediction", produces = MediaType.APPLICATION_JSON_VALUE)
public class PredictionController {
    @Autowired
    private PredictionRepository predictionRepository;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List<RawPredictionData>> predictionList(@RequestParam("fromDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date fromDate,
                                                                  @RequestParam("toDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date toDate,
                                                                  @RequestParam(name = "fromExchanges", required = false) List<Exchange> fromExchanges,
                                                                  @RequestParam(name = "toExchanges", required = false) List<Exchange> toExchanges,
                                                                  @RequestParam("amounts") List<BigDecimal> amounts,
                                                                  @RequestParam(name = "resolution", required = false) Resolution resolution,
                                                                  @RequestParam(name = "fromCurrencies", required = false) List<String> fromCurrencies,
                                                                  @RequestParam(name = "transferCurrencies", required = false) List<String> transferCurrencies,
                                                                  @RequestParam(name = "toCurrencies", required = false) List<String> toCurrencies) {

        List<RawPredictionData> entities = predictionRepository.findByDateAndInitialAmount(fromDate, toDate, amounts);

        return new ResponseEntity<>(entities, HttpStatus.OK);
    }
}
