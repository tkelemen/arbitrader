package com.tkelemen.arbitrader.api.model;

import java.math.BigDecimal;
import java.util.Date;

public class RawPredictionData {
    private Date dateCreate;
    private String pair;
    private String initialExchange;
    private String finalExchange;
    private BigDecimal initialAmount;
    private BigDecimal totalProfit;
    private long count;

    public RawPredictionData(Date dateCreate, String pair, String initialExchange, String finalExchange, BigDecimal initialAmount, BigDecimal totalProfit, long count) {
        this.dateCreate = dateCreate;
        this.pair = pair;
        this.initialExchange = initialExchange;
        this.finalExchange = finalExchange;
        this.initialAmount = initialAmount;
        this.totalProfit = totalProfit;
        this.count = count;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public String getInitialExchange() {
        return initialExchange;
    }

    public void setInitialExchange(String initialExchange) {
        this.initialExchange = initialExchange;
    }

    public String getFinalExchange() {
        return finalExchange;
    }

    public void setFinalExchange(String finalExchange) {
        this.finalExchange = finalExchange;
    }

    public BigDecimal getInitialAmount() {
        return initialAmount;
    }

    public void setInitialAmount(BigDecimal initialAmount) {
        this.initialAmount = initialAmount;
    }

    public BigDecimal getTotalProfit() {
        return totalProfit;
    }

    public void setTotalProfit(BigDecimal totalProfit) {
        this.totalProfit = totalProfit;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
