package com.tkelemen.arbitrader.api.model;

import com.tkelemen.arbitrader.model.Exchange;

import java.math.BigDecimal;
import java.util.Date;

public class Prediction {
    private Date fromDate;
    private Date toDate;
    private Exchange fromExchange;
    private Exchange toExchange;
    private String fromCurrency;
    private String toCurrency;
    private String transferCurrency;
    private BigDecimal amount;
    private BigDecimal averageProfit;
    private BigDecimal totalProfit;
    private BigDecimal minProfit;
    private BigDecimal maxProfit;
    private long opportunityCount;

    public Prediction(Date fromDate, Date toDate, Exchange fromExchange, Exchange toExchange, String fromCurrency, String toCurrency, String transferCurrency, BigDecimal amount) {
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.fromExchange = fromExchange;
        this.toExchange = toExchange;
        this.fromCurrency = fromCurrency;
        this.toCurrency = toCurrency;
        this.transferCurrency = transferCurrency;
        this.amount = amount;
    }

    public BigDecimal getAverageProfit() {
        return averageProfit;
    }

    public void setAverageProfit(BigDecimal averageProfit) {
        this.averageProfit = averageProfit;
    }

    public BigDecimal getTotalProfit() {
        return totalProfit;
    }

    public void setTotalProfit(BigDecimal totalProfit) {
        this.totalProfit = totalProfit;
    }

    public BigDecimal getMinProfit() {
        return minProfit;
    }

    public void setMinProfit(BigDecimal minProfit) {
        this.minProfit = minProfit;
    }

    public BigDecimal getMaxProfit() {
        return maxProfit;
    }

    public void setMaxProfit(BigDecimal maxProfit) {
        this.maxProfit = maxProfit;
    }

    public long getOpportunityCount() {
        return opportunityCount;
    }

    public void setOpportunityCount(long opportunityCount) {
        this.opportunityCount = opportunityCount;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Exchange getFromExchange() {
        return fromExchange;
    }

    public void setFromExchange(Exchange fromExchange) {
        this.fromExchange = fromExchange;
    }

    public Exchange getToExchange() {
        return toExchange;
    }

    public void setToExchange(Exchange toExchange) {
        this.toExchange = toExchange;
    }

    public String getFromCurrency() {
        return fromCurrency;
    }

    public void setFromCurrency(String fromCurrency) {
        this.fromCurrency = fromCurrency;
    }

    public String getToCurrency() {
        return toCurrency;
    }

    public void setToCurrency(String toCurrency) {
        this.toCurrency = toCurrency;
    }

    public String getTransferCurrency() {
        return transferCurrency;
    }

    public void setTransferCurrency(String transferCurrency) {
        this.transferCurrency = transferCurrency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
