package com.tkelemen.arbitrader.api.model;

public enum Resolution {
    HOUR, DAY, WEEK, MONTH
}
