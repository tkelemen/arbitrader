package com.tkelemen.arbitrader.api.converter;

import com.tkelemen.arbitrader.api.model.Prediction;
import com.tkelemen.arbitrader.model.Exchange;
import com.tkelemen.arbitrader.util.CurrencyUtils;

public class PredictionConverter {
    public static Prediction fromEntity(com.tkelemen.arbitrader.db.entity.Prediction predictionEntity) {
        return new Prediction(
                predictionEntity.getDatetime(),
                predictionEntity.getDatetime(),
                Exchange.valueOf(predictionEntity.getInitialExchange()),
                Exchange.valueOf(predictionEntity.getFinalExchange()),
                CurrencyUtils.getQuote(predictionEntity.getPair()),
                CurrencyUtils.getQuote(predictionEntity.getFinalPair()),
                CurrencyUtils.getBase(predictionEntity.getPair()),
                predictionEntity.getInitialAmount()
        );
    }
}
