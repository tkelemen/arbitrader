package com.tkelemen.arbitrader;

import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.config.WalletConfig;
import com.tkelemen.arbitrader.model.Exchange;
import com.tkelemen.arbitrader.model.ExchangeOrder;
import com.tkelemen.arbitrader.service.arbitrage.ArbitrageManagerService;
import com.tkelemen.arbitrader.service.arbitrage.WithdrawService;
import com.tkelemen.arbitrader.service.conversion.CurrencyConversionService;
import com.tkelemen.arbitrader.service.mail.MailProcessingService;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class ArbitraderScheduler implements InitializingBean {
    private static final Logger LOGGER = LoggerFactory.getLogger(ArbitraderScheduler.class);

    @Autowired
    private ArbitrageManagerService arbitrageManagerService;

    @Autowired
    private WithdrawService withdrawService;

    @Autowired
    private MailProcessingService mailProcessingService;

    @Autowired
    private CurrencyConversionService currencyConversionService;

    @Autowired
    private WalletConfig walletConfig;

    @Autowired
    private ApplicationConfig applicationConfig;

    @Override
    public void afterPropertiesSet() throws Exception {
        LOGGER.info("Application config: " + applicationConfig);

        if (applicationConfig.isNotificationEnabled()) {
            arbitrageManagerService.initNotifications();
        }
        if (applicationConfig.isMarketWatchEnabled()) {
            arbitrageManagerService.subscribeForMarketData();
        }
        if (applicationConfig.isTradingEnabled()) {
            arbitrageManagerService.initTrading();
        }
    }

    @Scheduled(fixedRateString = "${scheduler.syncAccountBalancesRateMs}")
    private void syncAccountBalances() {
        try {
            if (applicationConfig.isPredictionsEnabled()) {
                arbitrageManagerService.syncAccountBalances();
            }
        } catch (Throwable e) {
            LOGGER.error("Error in scheduled task 'syncAccountBalances': " + e.getMessage(), e);
            LOGGER.info("Exception: " + ExceptionUtils.getStackTrace(e));
        }
    }

    @Scheduled(fixedRateString = "${scheduler.executeWithdrawsRateMs}")
    private void executeWithdraws() {
        try {
            if (!applicationConfig.isWithdrawEnabled()) {
                return;
            }

            withdrawService.executePendingWithdraws();
        } catch (Throwable e) {
            LOGGER.error("Error in scheduled task 'executeWithdraws': " + e.getMessage(), e);
            LOGGER.info("Exception: " + ExceptionUtils.getStackTrace(e));
        }
    }

    @Scheduled(fixedRateString = "${scheduler.processEmailsRateMs}")
    private void processEmails() {
        try {
            if (!applicationConfig.isEmailProcessingEnabled()) {
                return;
            }

            mailProcessingService.processMessages();
        } catch (Throwable e) {
            LOGGER.error("Error in scheduled task 'processEmails': " + e.getMessage(), e);
            LOGGER.info("Exception: " + ExceptionUtils.getStackTrace(e));
        }
    }

    @Scheduled(initialDelayString = "${scheduler.resynchronizeDelayMs}", fixedRateString = "${scheduler.resynchronizeRateMs}")
    private void resynchronize() {
        try {
            if (applicationConfig.isMarketWatchEnabled()) {
                arbitrageManagerService.syncOrderBooks();
            }

            // TODO: move somewhere
            System.out.println(currencyConversionService.getConversionPossibilities(Exchange.CEX, Exchange.CEX, "USD", "EUR", new BigDecimal(5000)));
            System.out.println(currencyConversionService.getConversionPossibilities(Exchange.CEX, Exchange.BINANCE, "USD", "EUR", new BigDecimal(5000)));
            System.out.println(currencyConversionService.getConversionPossibilities(Exchange.CEX, Exchange.KRAKEN, "USD", "EUR", new BigDecimal(5000)));
            System.out.println(currencyConversionService.getConversionPossibilities(Exchange.BINANCE, Exchange.BINANCE, "USDT", "EUR", new BigDecimal(5000)));
            System.out.println(currencyConversionService.getConversionPossibilities(Exchange.BINANCE, Exchange.BINANCE, "BUSD", "EUR", new BigDecimal(5000)));
            System.out.println(currencyConversionService.getConversionPossibilities(Exchange.BINANCE, Exchange.CEX, "BUSD", "EUR", new BigDecimal(5000)));
            System.out.println(currencyConversionService.getConversionPossibilities(Exchange.BINANCE, Exchange.CEX, "USDT", "EUR", new BigDecimal(5000)));
        } catch (Throwable e) {
            LOGGER.error("Error in scheduled task 'resynchronize': " + e.getMessage(), e);
            LOGGER.info("Exception: " + ExceptionUtils.getStackTrace(e));
        }
    }

    @Scheduled(initialDelayString = "${scheduler.findProfitableTradesDelayMs}", fixedRateString = "${scheduler.reconnectRateMs}")
    private void reconnect() {
        try {
            if (applicationConfig.isMarketWatchEnabled()) {
                arbitrageManagerService.reconnect();
            }
        } catch (Throwable e) {
            LOGGER.error("Error in scheduled task 'reconnect': " + e.getMessage(), e);
            LOGGER.info("Exception: " + ExceptionUtils.getStackTrace(e));
        }
    }

    @Scheduled(fixedRateString = "${scheduler.statusCheck}")
    private void statusCheck() {
        try {
            if (applicationConfig.isPredictionsEnabled()) {
                arbitrageManagerService.statusCheck();
            }
        } catch (Throwable e) {
            LOGGER.error("Error in scheduled task 'statusCheck': " + e.getMessage(), e);
            LOGGER.info("Exception: " + ExceptionUtils.getStackTrace(e));
        }
    }

    @Scheduled(initialDelayString = "${scheduler.findProfitableTradesDelayMs}", fixedRateString = "${scheduler.findProfitableTradesRateMs}")
    private void executeProfitableTrades() {
        try {
            if (applicationConfig.isPredictionsEnabled() && applicationConfig.isTradingEnabled()) {
                arbitrageManagerService.executeProfitableTrades();
            }
        } catch (Throwable e) {
            LOGGER.error("Error in scheduled task 'executeProfitableTrades': " + e.getMessage(), e);
            LOGGER.info("Exception: " + ExceptionUtils.getStackTrace(e));
        }
    }

    @Scheduled(initialDelayString = "${scheduler.executeMakerTradesDelayMs}", fixedRateString = "${scheduler.executeMakerTradesRateMs}")
    private void executeMakerTrades() {
        try {
            if (applicationConfig.isMakerTradingEnabled()) {
                arbitrageManagerService.processMakerOpportunities();
            }
        } catch (Throwable e) {
            LOGGER.error("Error in scheduled task 'executeMakerTrades': " + e.getMessage(), e);
            LOGGER.info("Exception: " + ExceptionUtils.getStackTrace(e));
        }
    }
}
