package com.tkelemen.arbitrader.util;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

public class CurrencyUtils {
    public static final int DEFAULT_PRECISION = 8;
    public static final String DEFAULT_QUOTE_TICKER = "BTC";

    public static String formatCrypto(BigDecimal amount, int precision) {
        return String.format(Locale.ROOT, "%." + precision + "f", amount);
    }

    public static String formatCrypto(BigDecimal amount) {
        return formatCrypto(amount, DEFAULT_PRECISION);
    }

    public static String getQuote(String pair) {
        return pair.split("/")[1];
    }

    public static String getBase(String pair) {
        return pair.split("/")[0];
    }

    public static String getPair(String base, String quote) {
        return base + "/" + quote;
    }

    public static Set<String> getFinalPairs(String initialPair) {
        final Set<String> finalPairs = new HashSet<>();
        finalPairs.add(initialPair);

        final String quote = CurrencyUtils.getQuote(initialPair);
        if ("USD".equals(quote)) {
            finalPairs.add(initialPair.replaceFirst("USD", "USDT"));
            finalPairs.add(initialPair.replaceFirst("USD", "BUSD"));
            finalPairs.add(initialPair.replaceFirst("USD", "USDC"));
        } else if ("USDT".equals(quote)) {
            finalPairs.add(initialPair.replaceFirst("USDT", "USD"));
            finalPairs.add(initialPair.replaceFirst("USDT", "BUSD"));
            finalPairs.add(initialPair.replaceFirst("USDT", "USDC"));
        } else if ("BUSD".equals(quote)) {
            finalPairs.add(initialPair.replaceFirst("BUSD", "USD"));
            finalPairs.add(initialPair.replaceFirst("BUSD", "USDT"));
            finalPairs.add(initialPair.replaceFirst("BUSD", "USDC"));
        } else if ("USDC".equals(quote)) {
            finalPairs.add(initialPair.replaceFirst("USDC", "USD"));
            finalPairs.add(initialPair.replaceFirst("USDC", "USDT"));
            finalPairs.add(initialPair.replaceFirst("USDC", "BUSD"));
        }

        return finalPairs;
    }
}
