package com.tkelemen.arbitrader.util;

import com.tkelemen.arbitrader.db.entity.Arbitrage;
import com.tkelemen.arbitrader.model.BuyResult;
import com.tkelemen.arbitrader.model.Opportunity;
import com.tkelemen.arbitrader.service.arbitrage.WithdrawService;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class TradeUtils {

    public static BigDecimal getAmountToWithdraw(Arbitrage arbitrage) {
        BigDecimal amount = arbitrage.getBoughtAmount();

        if (arbitrage.getBaseCurrency().equals(arbitrage.getBuyFeeCurrency())) {
            amount = amount.subtract(arbitrage.getBuyFee());
        }

        return amount;
    }

    public static BigDecimal getAmountToSell(Arbitrage arbitrage) {
        return getAmountToWithdraw(arbitrage).subtract(arbitrage.getWithdrawFee());
    }

    public static BigDecimal getAmountToSell(BuyResult buyResult) {
        return getAmountToWithdraw(buyResult).subtract(buyResult.getWithdrawFee());
    }

    public static BigDecimal getAmountToWithdraw(BuyResult buyResult) {
        BigDecimal amount = buyResult.getBoughtAmount();

        if (CurrencyUtils.getBase(buyResult.getPair()).equals(buyResult.getBuyFeeCurrency())) {
            amount = amount.subtract(buyResult.getBuyFee());
        }

        return amount;
    }

    public static BigDecimal getInitialExpenses(Arbitrage arbitrage) {
        BigDecimal amount = arbitrage.getInitialAmount();

        if (arbitrage.getInitialQuoteCurrency().equals(arbitrage.getBuyFeeCurrency())) {
            amount = amount.add(arbitrage.getBuyFee());
        }

        return amount;
    }

    public static BigDecimal getInitialExpenses(BuyResult buyResult) {
        BigDecimal amount = buyResult.getInitialAmount();

        if (CurrencyUtils.getQuote(buyResult.getPair()).equals(buyResult.getBuyFeeCurrency())) {
            amount = amount.add(buyResult.getBuyFee());
        }

        return amount;
    }

    public static BigDecimal getFinalAmount(Arbitrage arbitrage, boolean includeSaldo) {
        BigDecimal amount = arbitrage.getReceivedAmount();

        if (arbitrage.getFinalQuoteCurrency().equals(arbitrage.getSellFeeCurrency())) {
            amount = amount.subtract(arbitrage.getSellFee());
        }

        if(includeSaldo && arbitrage.getSellSaldoAmount() != null && BigDecimal.ZERO.compareTo(arbitrage.getSellSaldoAmount()) < 0) {
            amount = amount.add(arbitrage.getSellSaldoAmount().multiply(arbitrage.getInitialAmount().divide(arbitrage.getBoughtAmount(), 8, RoundingMode.HALF_UP)));
        }

        return amount;
    }

    public static BigDecimal getNetGain(Arbitrage arbitrage) {
        return getFinalAmount(arbitrage, false).subtract(getInitialExpenses(arbitrage));
    }

    public static BigDecimal getNetGainInclSaldo(Arbitrage arbitrage) {
        return getFinalAmount(arbitrage, true).subtract(getInitialExpenses(arbitrage));
    }

    public static BigDecimal getNetGainPercent(Arbitrage arbitrage) {
        return getFinalAmount(arbitrage, false).divide(getInitialExpenses(arbitrage), 8, RoundingMode.HALF_UP);
    }

    public static BigDecimal getNetGainPercentInclSaldo(Arbitrage arbitrage) {
        return getFinalAmount(arbitrage, true).divide(getInitialExpenses(arbitrage), 8, RoundingMode.HALF_UP);
    }
}
