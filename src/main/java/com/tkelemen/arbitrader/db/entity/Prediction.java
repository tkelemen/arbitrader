package com.tkelemen.arbitrader.db.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
public class Prediction implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;

    @Column(name = "iteration_id")
    private long iterationId;

    private Date datetime;

    private String pair;

    @Column(name = "final_pair")
    private String finalPair;

    @Column(name = "initial_amount")
    private BigDecimal initialAmount;

    @Column(name = "final_amount")
    private BigDecimal finalAmount;

    @Column(name = "transfer_amount")
    private BigDecimal transferAmount;

    @Column(name = "initial_exchange")
    private String initialExchange;

    @Column(name = "final_exchange")
    private String finalExchange;

    @Column(name = "buy_fee")
    private BigDecimal buyFee;

    @Column(name = "sell_fee")
    private BigDecimal sellFee;

    @Column(name = "withdraw_fee")
    private BigDecimal withdrawFee;

    @Column(name = "deposit_fee")
    private BigDecimal depositFee;

    @Column(name="insufficient_quote_funds")
    private Boolean insufficientQuoteFunds;

    @Column(name="insufficient_base_funds")
    private Boolean insufficientBaseFunds;

    @Column(name="best_gain")
    private Boolean bestGain;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIterationId() {
        return iterationId;
    }

    public void setIterationId(long iterationId) {
        this.iterationId = iterationId;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public String getFinalPair() {
        return finalPair;
    }

    public void setFinalPair(String finalPair) {
        this.finalPair = finalPair;
    }

    public BigDecimal getInitialAmount() {
        return initialAmount;
    }

    public void setInitialAmount(BigDecimal initialAmount) {
        this.initialAmount = initialAmount;
    }

    public BigDecimal getFinalAmount() {
        return finalAmount;
    }

    public void setFinalAmount(BigDecimal finalAmount) {
        this.finalAmount = finalAmount;
    }

    public BigDecimal getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(BigDecimal transferAmount) {
        this.transferAmount = transferAmount;
    }

    public String getInitialExchange() {
        return initialExchange;
    }

    public void setInitialExchange(String initialExchange) {
        this.initialExchange = initialExchange;
    }

    public String getFinalExchange() {
        return finalExchange;
    }

    public void setFinalExchange(String finalExchange) {
        this.finalExchange = finalExchange;
    }

    public BigDecimal getBuyFee() {
        return buyFee;
    }

    public void setBuyFee(BigDecimal buyFee) {
        this.buyFee = buyFee;
    }

    public BigDecimal getSellFee() {
        return sellFee;
    }

    public void setSellFee(BigDecimal sellFee) {
        this.sellFee = sellFee;
    }

    public BigDecimal getWithdrawFee() {
        return withdrawFee;
    }

    public void setWithdrawFee(BigDecimal withdrawFee) {
        this.withdrawFee = withdrawFee;
    }

    public BigDecimal getDepositFee() {
        return depositFee;
    }

    public void setDepositFee(BigDecimal depositFee) {
        this.depositFee = depositFee;
    }

    public boolean isInsufficientQuoteFunds() {
        return insufficientQuoteFunds;
    }

    public void setInsufficientQuoteFunds(boolean insufficientQuoteFunds) {
        this.insufficientQuoteFunds = insufficientQuoteFunds;
    }

    public boolean isInsufficientBaseFunds() {
        return insufficientBaseFunds;
    }

    public void setInsufficientBaseFunds(boolean insufficientBaseFunds) {
        this.insufficientBaseFunds = insufficientBaseFunds;
    }

    public boolean isBestGain() {
        return bestGain;
    }

    public void setBestGain(boolean bestGain) {
        this.bestGain = bestGain;
    }
}