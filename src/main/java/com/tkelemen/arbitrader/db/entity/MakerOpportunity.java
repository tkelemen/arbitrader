package com.tkelemen.arbitrader.db.entity;

import com.tkelemen.arbitrader.model.Exchange;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "maker_opportunity")
public class MakerOpportunity {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    private Date datetime;

    @Column(name = "iteration_id")
    private long iterationId;

    private String type;

    @Column(name = "trigger_exchange")
    @Enumerated(value = EnumType.STRING)
    private Exchange triggerExchange;

    @Column(name = "partner_exchange")
    @Enumerated(value = EnumType.STRING)
    private Exchange partnerExchange;

    @Column(name = "from_quote")
    private String fromQuote;

    @Column(name = "base")
    private String base;

    @Column(name = "to_quote")
    private String toQuote;

    @Column(name = "trigger_price")
    private BigDecimal triggerPrice;

    @Column(name = "from_quote_amount")
    private BigDecimal fromQuoteAmount;

    @Column(name = "base_amount")
    private BigDecimal baseAmount;

    @Column(name = "to_quote_amount")
    private BigDecimal toQuoteAmount;

    @Column(name = "trigger_highest_bid")
    private BigDecimal triggerHighestBid;

    @Column(name = "trigger_lowest_ask")
    private BigDecimal triggerLowestAsk;

    @Column(name = "spread")
    private BigDecimal spread;

    @Column(name = "partner_best_price")
    private BigDecimal partnerBestPrice;

    @Column(name = "liquidity_score")
    private BigDecimal liquidityScore;

    @Column(name = "buy_fee")
    private BigDecimal buyFee;

    @Column(name = "sell_fee")
    private BigDecimal sellFee;

    @Column(name = "withdraw_fee")
    private BigDecimal withdrawFee;

    @Column(name = "deposit_fee")
    private BigDecimal depositFee;

    @Column(name="has_insufficient_quote_funds")
    private Boolean hasInsufficientQuoteFunds;

    @Column(name="has_insufficient_base_funds")
    private Boolean hasInsufficientBaseFunds;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public long getIterationId() {
        return iterationId;
    }

    public void setIterationId(long iterationId) {
        this.iterationId = iterationId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Exchange getTriggerExchange() {
        return triggerExchange;
    }

    public void setTriggerExchange(Exchange triggerExchange) {
        this.triggerExchange = triggerExchange;
    }

    public Exchange getPartnerExchange() {
        return partnerExchange;
    }

    public void setPartnerExchange(Exchange partnerExchange) {
        this.partnerExchange = partnerExchange;
    }

    public String getFromQuote() {
        return fromQuote;
    }

    public void setFromQuote(String fromQuote) {
        this.fromQuote = fromQuote;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getToQuote() {
        return toQuote;
    }

    public void setToQuote(String toQuote) {
        this.toQuote = toQuote;
    }

    public BigDecimal getTriggerPrice() {
        return triggerPrice;
    }

    public void setTriggerPrice(BigDecimal triggerPrice) {
        this.triggerPrice = triggerPrice;
    }

    public BigDecimal getFromQuoteAmount() {
        return fromQuoteAmount;
    }

    public void setFromQuoteAmount(BigDecimal fromQuoteAmount) {
        this.fromQuoteAmount = fromQuoteAmount;
    }

    public BigDecimal getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(BigDecimal baseAmount) {
        this.baseAmount = baseAmount;
    }

    public BigDecimal getToQuoteAmount() {
        return toQuoteAmount;
    }

    public void setToQuoteAmount(BigDecimal toQuoteAmount) {
        this.toQuoteAmount = toQuoteAmount;
    }

    public BigDecimal getBuyFee() {
        return buyFee;
    }

    public void setBuyFee(BigDecimal buyFee) {
        this.buyFee = buyFee;
    }

    public BigDecimal getSellFee() {
        return sellFee;
    }

    public void setSellFee(BigDecimal sellFee) {
        this.sellFee = sellFee;
    }

    public BigDecimal getWithdrawFee() {
        return withdrawFee;
    }

    public void setWithdrawFee(BigDecimal withdrawFee) {
        this.withdrawFee = withdrawFee;
    }

    public BigDecimal getDepositFee() {
        return depositFee;
    }

    public void setDepositFee(BigDecimal depositFee) {
        this.depositFee = depositFee;
    }

    public Boolean getHasInsufficientQuoteFunds() {
        return hasInsufficientQuoteFunds;
    }

    public void setHasInsufficientQuoteFunds(Boolean insufficientQuoteFunds) {
        this.hasInsufficientQuoteFunds = insufficientQuoteFunds;
    }

    public Boolean getHasInsufficientBaseFunds() {
        return hasInsufficientBaseFunds;
    }

    public void setHasInsufficientBaseFunds(Boolean insufficientBaseFunds) {
        this.hasInsufficientBaseFunds = insufficientBaseFunds;
    }

    public BigDecimal getTriggerHighestBid() {
        return triggerHighestBid;
    }

    public void setTriggerHighestBid(BigDecimal triggerHighestBid) {
        this.triggerHighestBid = triggerHighestBid;
    }

    public BigDecimal getTriggerLowestAsk() {
        return triggerLowestAsk;
    }

    public void setTriggerLowestAsk(BigDecimal triggerLowestAsk) {
        this.triggerLowestAsk = triggerLowestAsk;
    }

    public BigDecimal getSpread() {
        return spread;
    }

    public void setSpread(BigDecimal spread) {
        this.spread = spread;
    }

    public BigDecimal getPartnerBestPrice() {
        return partnerBestPrice;
    }

    public void setPartnerBestPrice(BigDecimal partnerBestPrice) {
        this.partnerBestPrice = partnerBestPrice;
    }

    public BigDecimal getLiquidityScore() {
        return liquidityScore;
    }

    public void setLiquidityScore(BigDecimal liquidityScore) {
        this.liquidityScore = liquidityScore;
    }
}
