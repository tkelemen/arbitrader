package com.tkelemen.arbitrader.db.entity;

import com.tkelemen.arbitrader.model.Exchange;
import com.tkelemen.arbitrader.model.TransactionStatus;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "maker_trade")
public class MakerTrade {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private Date datetime;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "maker_trigger_order_id", nullable = false)
    private MakerTriggerOrder triggerOrder;

    @Column(name = "initial_exchange")
    @Enumerated(value = EnumType.STRING)
    private Exchange initialExchange;

    @Column(name = "final_exchange")
    @Enumerated(value = EnumType.STRING)
    private Exchange finalExchange;

    @Column(name = "from_quote")
    private String fromQuote;

    @Column(name = "base")
    private String base;

    @Column(name = "to_quote")
    private String toQuote;

    @Column(name = "buy_quote_amount")
    private BigDecimal buyQuoteAmount;

    @Column(name = "base_amount")
    private BigDecimal baseAmount;

    @Column(name = "sell_quote_amount")
    private BigDecimal sellQuoteAmount;

    @Column(name = "transfer_saldo_amount")
    private BigDecimal transferSaldoAmount;

    @Column(name = "trade_saldo_amount")
    private BigDecimal tradeSaldoAmount;

    @Column(name = "buy_fee")
    private BigDecimal buyFee;

    @Column(name = "buy_fee_currency")
    private String buyFeeCurrency;

    @Column(name = "sell_fee")
    private BigDecimal sellFee;

    @Column(name = "sell_fee_currency")
    private String sellFeeCurrency;

    @Column(name = "withdraw_fee")
    private BigDecimal withdrawFee;

    @Column(name = "deposit_fee")
    private BigDecimal depositFee;

    @Column(name = "initial_exchange_order_id")
    private String initialExchangeOrderId;

    @Column(name = "final_exchange_order_id")
    private String finalExchangeOrderId;

    @Column(name = "withdraw_status")
    @Enumerated(value = EnumType.STRING)
    private TransactionStatus withdrawStatus;

    @Column(name = "deposit_status")
    @Enumerated(value = EnumType.STRING)
    private TransactionStatus depositStatus;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public MakerTriggerOrder getTriggerOrder() {
        return triggerOrder;
    }

    public void setTriggerOrder(MakerTriggerOrder triggerOrder) {
        this.triggerOrder = triggerOrder;
    }

    public void setDepositStatus(TransactionStatus depositStatus) {
        this.depositStatus = depositStatus;
    }

    public TransactionStatus getDepositStatus() {
        return this.depositStatus;
    }

    public Exchange getInitialExchange() {
        return initialExchange;
    }

    public void setInitialExchange(Exchange initialExchange) {
        this.initialExchange = initialExchange;
    }

    public Exchange getFinalExchange() {
        return finalExchange;
    }

    public void setFinalExchange(Exchange finalExchange) {
        this.finalExchange = finalExchange;
    }

    public String getFromQuote() {
        return fromQuote;
    }

    public void setFromQuote(String fromQuote) {
        this.fromQuote = fromQuote;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getToQuote() {
        return toQuote;
    }

    public void setToQuote(String toQuote) {
        this.toQuote = toQuote;
    }

    public BigDecimal getBuyQuoteAmount() {
        return buyQuoteAmount;
    }

    public void setBuyQuoteAmount(BigDecimal buyQuoteAmount) {
        this.buyQuoteAmount = buyQuoteAmount;
    }

    public BigDecimal getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(BigDecimal baseAmount) {
        this.baseAmount = baseAmount;
    }

    public BigDecimal getSellQuoteAmount() {
        return sellQuoteAmount;
    }

    public void setSellQuoteAmount(BigDecimal sellQuoteAmount) {
        this.sellQuoteAmount = sellQuoteAmount;
    }

    public BigDecimal getTransferSaldoAmount() {
        return transferSaldoAmount;
    }

    public void setTransferSaldoAmount(BigDecimal transferSaldoAmount) {
        this.transferSaldoAmount = transferSaldoAmount;
    }

    public BigDecimal getTradeSaldoAmount() {
        return tradeSaldoAmount;
    }

    public void setTradeSaldoAmount(BigDecimal tradeSaldoAmount) {
        this.tradeSaldoAmount = tradeSaldoAmount;
    }

    public BigDecimal getBuyFee() {
        return buyFee;
    }

    public void setBuyFee(BigDecimal buyFee) {
        this.buyFee = buyFee;
    }

    public String getBuyFeeCurrency() {
        return buyFeeCurrency;
    }

    public void setBuyFeeCurrency(String buyFeeCurrency) {
        this.buyFeeCurrency = buyFeeCurrency;
    }

    public BigDecimal getSellFee() {
        return sellFee;
    }

    public void setSellFee(BigDecimal sellFee) {
        this.sellFee = sellFee;
    }

    public String getSellFeeCurrency() {
        return sellFeeCurrency;
    }

    public void setSellFeeCurrency(String sellFeeCurrency) {
        this.sellFeeCurrency = sellFeeCurrency;
    }

    public BigDecimal getWithdrawFee() {
        return withdrawFee;
    }

    public void setWithdrawFee(BigDecimal withdrawFee) {
        this.withdrawFee = withdrawFee;
    }

    public BigDecimal getDepositFee() {
        return depositFee;
    }

    public void setDepositFee(BigDecimal depositFee) {
        this.depositFee = depositFee;
    }

    public String getInitialExchangeOrderId() {
        return initialExchangeOrderId;
    }

    public void setInitialExchangeOrderId(String initialExchangeOrderId) {
        this.initialExchangeOrderId = initialExchangeOrderId;
    }

    public String getFinalExchangeOrderId() {
        return finalExchangeOrderId;
    }

    public void setFinalExchangeOrderId(String finalExchangeOrderId) {
        this.finalExchangeOrderId = finalExchangeOrderId;
    }

    public TransactionStatus getWithdrawStatus() {
        return withdrawStatus;
    }

    public void setWithdrawStatus(TransactionStatus withdrawStatus) {
        this.withdrawStatus = withdrawStatus;
    }
}
