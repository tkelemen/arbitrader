package com.tkelemen.arbitrader.db.entity;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
public class Arbitrage implements Serializable {
    public static final String STATUS_WITHDRAW_PENDING = "PENDING";
    public static final String STATUS_WITHDRAW_MANUAL = "MANUAL";
    public static final String STATUS_OK = "OK";
    public static final String STATUS_FAIL = "FAIL";
    public static final String TYPE_PROFIT = "PROFIT";
    public static final String TYPE_FUNDING = "FUNDING";
    public static final String TYPE_GENERAL_FUNDING = "GENERAL_FUNDING";

    public static final String BUY_FEE_INCLUDED_NO = "NO";
    public static final String BUY_FEE_INCLUDED_INITIAL = "INITIAL";
    public static final String BUY_FEE_INCLUDED_BOUGHT = "BOUGHT";
    public static final String BUY_FEE_INCLUDED_TRANSFER = "TRANSFER";
    public static final String SELL_FEE_INCLUDED_NO = "NO";
    public static final String SELL_FEE_INCLUDED_FINAL = "FINAL";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    //@OneToOne(cascade = {CascadeType.ALL})
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "prediction_id", nullable = false)
    private Prediction prediction;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "withdraw_id")
    private Withdraw withdraw;

    private Date datetime;

    @Column(name = "initial_quote_currency")
    private String initialQuoteCurrency;

    @Column(name = "base_currency")
    private String baseCurrency;

    @Column(name = "final_quote_currency")
    private String finalQuoteCurrency;

    @Column(name = "initial_amount")
    private BigDecimal initialAmount;

    @Column(name = "bought_amount")
    private BigDecimal boughtAmount;

    @Column(name = "sold_amount")
    private BigDecimal soldAmount;

    @Column(name = "final_amount")
    private BigDecimal receivedAmount;

    @Column(name = "transfer_saldo_amount")
    private BigDecimal transferSaldoAmount;

    @Column(name = "sell_saldo_amount")
    private BigDecimal sellSaldoAmount;

    @Column(name = "buy_fee")
    private BigDecimal buyFee;

    @Column(name = "buy_fee_currency")
    private String buyFeeCurrency;

    @Column(name = "sell_fee")
    private BigDecimal sellFee;

    @Column(name = "sell_fee_currency")
    private String sellFeeCurrency;

    @Column(name = "withdraw_fee")
    private BigDecimal withdrawFee;

    @Column(name = "deposit_fee")
    private BigDecimal depositFee;

    @Column(name = "buy_order_id")
    private String buyOrderId;

    @Column(name = "buy_order_id_additional")
    private String buyOrderIdAdditional;

    @Column(name = "sell_order_id")
    private String sellOrderId;

    @Column(name = "sell_order_id_additional")
    private String sellOrderIdAdditional;

    @Column(name = "buy_status")
    private String buyStatus;

    @Column(name = "sell_status")
    private String sellStatus;

    @Column(name = "withdraw_status")
    private String withdrawStatus;

    @Column(name = "threshold")
    private BigDecimal threshold;

    @Column(name = "type")
    private String type;

    @Column(name = "error")
    private String error;

    public long getId() {
        return id;
    }

    public String getInitialQuoteCurrency() {
        return initialQuoteCurrency;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Prediction getPrediction() {
        return prediction;
    }

    public void setPrediction(Prediction prediction) {
        this.prediction = prediction;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public void setInitialQuoteCurrency(String initialQuoteCurrency) {
        this.initialQuoteCurrency = initialQuoteCurrency;
    }

    public String getBaseCurrency() {
        return baseCurrency;
    }

    public void setBaseCurrency(String baseCurrency) {
        this.baseCurrency = baseCurrency;
    }

    public String getFinalQuoteCurrency() {
        return finalQuoteCurrency;
    }

    public void setFinalQuoteCurrency(String finalQuoteCurrency) {
        this.finalQuoteCurrency = finalQuoteCurrency;
    }

    public BigDecimal getInitialAmount() {
        return initialAmount;
    }

    public void setInitialAmount(BigDecimal initialAmount) {
        this.initialAmount = initialAmount;
    }

    public BigDecimal getBoughtAmount() {
        return boughtAmount;
    }

    public void setBoughtAmount(BigDecimal boughtAmount) {
        this.boughtAmount = boughtAmount;
    }

    public BigDecimal getSoldAmount() {
        return soldAmount;
    }

    public void setSoldAmount(BigDecimal soldAmount) {
        this.soldAmount = soldAmount;
    }

    public BigDecimal getReceivedAmount() {
        return receivedAmount;
    }

    public void setReceivedAmount(BigDecimal receivedAmount) {
        this.receivedAmount = receivedAmount;
    }

    public BigDecimal getBuyFee() {
        return buyFee;
    }

    public void setBuyFee(BigDecimal buyFee) {
        this.buyFee = buyFee;
    }

    public String getBuyFeeCurrency() {
        return buyFeeCurrency;
    }

    public void setBuyFeeCurrency(String buyFeeCurrency) {
        this.buyFeeCurrency = buyFeeCurrency;
    }

    public BigDecimal getSellFee() {
        return sellFee;
    }

    public void setSellFee(BigDecimal sellFee) {
        this.sellFee = sellFee;
    }

    public String getSellFeeCurrency() {
        return sellFeeCurrency;
    }

    public void setSellFeeCurrency(String sellFeeCurrency) {
        this.sellFeeCurrency = sellFeeCurrency;
    }

    public BigDecimal getWithdrawFee() {
        return withdrawFee;
    }

    public void setWithdrawFee(BigDecimal withdrawFee) {
        this.withdrawFee = withdrawFee;
    }

    public BigDecimal getDepositFee() {
        return depositFee;
    }

    public void setDepositFee(BigDecimal depositFee) {
        this.depositFee = depositFee;
    }

    public String getBuyOrderId() {
        return buyOrderId;
    }

    public void setBuyOrderId(String buyOrderId) {
        this.buyOrderId = buyOrderId;
    }

    public String getSellOrderId() {
        return sellOrderId;
    }

    public void setSellOrderId(String sellOrderId) {
        this.sellOrderId = sellOrderId;
    }

    public String getBuyOrderIdAdditional() {
        return buyOrderIdAdditional;
    }

    public void setBuyOrderIdAdditional(String buyOrderIdAdditional) {
        this.buyOrderIdAdditional = buyOrderIdAdditional;
    }

    public String getSellOrderIdAdditional() {
        return sellOrderIdAdditional;
    }

    public void setSellOrderIdAdditional(String sellOrderIdAdditional) {
        this.sellOrderIdAdditional = sellOrderIdAdditional;
    }

    public Withdraw getWithdraw() {
        return withdraw;
    }

    public void setWithdraw(Withdraw withdraw) {
        this.withdraw = withdraw;
    }

    public String getBuyStatus() {
        return buyStatus;
    }

    public void setBuyStatus(String buyStatus) {
        this.buyStatus = buyStatus;
    }

    public String getSellStatus() {
        return sellStatus;
    }

    public void setSellStatus(String sellStatus) {
        this.sellStatus = sellStatus;
    }

    public String getWithdrawStatus() {
        return withdrawStatus;
    }

    public void setWithdrawStatus(String withdrawStatus) {
        this.withdrawStatus = withdrawStatus;
    }

    public BigDecimal getThreshold() {
        return threshold;
    }

    public void setThreshold(BigDecimal threshold) {
        this.threshold = threshold;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getTransferSaldoAmount() {
        return transferSaldoAmount;
    }

    public Arbitrage setTransferSaldoAmount(BigDecimal transferSaldoAmount) {
        this.transferSaldoAmount = transferSaldoAmount;
        return this;
    }

    public BigDecimal getSellSaldoAmount() {
        return sellSaldoAmount;
    }

    public void setSellSaldoAmount(BigDecimal sellSaldoAmount) {
        this.sellSaldoAmount = sellSaldoAmount;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "Arbitrage{" +
                "id=" + id +
                ", prediction=" + prediction +
                ", datetime=" + datetime +
                ", initialAmount=" + initialAmount +
                ", boughtAmount=" + boughtAmount +
                ", soldAmount=" + soldAmount +
                ", receivedAmount=" + receivedAmount +
                ", transferSaldoAmount=" + transferSaldoAmount +
                ", sellSaldoAmount=" + transferSaldoAmount +
                ", buyFee=" + buyFee +
                ", buyFeeCurrency='" + buyFeeCurrency + '\'' +
                ", sellFee=" + sellFee +
                ", sellFeeCurrency='" + sellFeeCurrency + '\'' +
                ", withdrawFee=" + withdrawFee +
                ", depositFee=" + depositFee +
                ", buyOrderId='" + buyOrderId + '\'' +
                ", buyOrderIdAdditional='" + buyOrderIdAdditional + '\'' +
                ", sellOrderId='" + sellOrderId + '\'' +
                ", sellOrderIdAdditional='" + sellOrderIdAdditional + '\'' +
                ", type='" + type + '\'' +
                ", buyStatus='" + buyStatus + '\'' +
                ", sellStatus='" + sellStatus + '\'' +
                ", withdrawStatus='" + withdrawStatus + '\'' +
                ", error='" + error + '\'' +
                '}';
    }
}