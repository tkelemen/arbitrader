package com.tkelemen.arbitrader.db.entity;

import com.tkelemen.arbitrader.model.Exchange;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
public class Withdraw implements Serializable {
    // TODO: refactor to enum (simultaneously with WithdrawResult)
    public static final String STATUS_REQUESTED = "REQUESTED";
    public static final String STATUS_CONFIRMED = "CONFIRMED";
    public static final String STATUS_SENT = "SENT";
    public static final String STATUS_RECEIVED = "RECEIVED";
    public static final String STATUS_ERROR = "ERROR";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "datetime_init")
    private Date datetimeInit;

    @Column(name = "datetime_received")
    private Date datetimeReceived;

    @Column(name = "from_exchange")
    private String fromExchange;

    @Column(name = "to_exchange")
    private String toExchange;

    @Column(name = "recipient_address")
    private String recipientAddress;

    @Column(name = "recipient_tag")
    private String recipientTag;

    private String asset;

    private BigDecimal amount;

    @Column(name = "withdraw_fee")
    private BigDecimal withdrawFee;

    @Column(name = "deposit_fee")
    private BigDecimal depositFee;

    @Column(name = "withdraw_id")
    private String withdrawId;

    @Column(name = "deposit_id")
    private String depositId;

    @Column(name = "transaction_id")
    private String transactionId;

    @Column(name = "status")
    private String status;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDatetimeInit() {
        return datetimeInit;
    }

    public void setDatetimeInit(Date datetimeInit) {
        this.datetimeInit = datetimeInit;
    }

    public Date getDatetimeReceived() {
        return datetimeReceived;
    }

    public void setDatetimeReceived(Date datetimeReceived) {
        this.datetimeReceived = datetimeReceived;
    }

    public String getFromExchange() {
        return fromExchange;
    }

    public void setFromExchange(String fromExchange) {
        this.fromExchange = fromExchange;
    }

    public String getToExchange() {
        return toExchange;
    }

    public void setToExchange(String toExchange) {
        this.toExchange = toExchange;
    }

    public String getRecipientAddress() {
        return recipientAddress;
    }

    public void setRecipientAddress(String recipientAddress) {
        this.recipientAddress = recipientAddress;
    }

    public String getRecipientTag() {
        return recipientTag;
    }

    public void setRecipientTag(String recipientTag) {
        this.recipientTag = recipientTag;
    }

    public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getWithdrawFee() {
        return withdrawFee;
    }

    public void setWithdrawFee(BigDecimal withdrawFee) {
        this.withdrawFee = withdrawFee;
    }

    public BigDecimal getDepositFee() {
        return depositFee;
    }

    public void setDepositFee(BigDecimal depositFee) {
        this.depositFee = depositFee;
    }

    public String getWithdrawId() {
        return withdrawId;
    }

    public void setWithdrawId(String withdrawId) {
        this.withdrawId = withdrawId;
    }

    public String getDepositId() {
        return depositId;
    }

    public void setDepositId(String depositId) {
        this.depositId = depositId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}