package com.tkelemen.arbitrader.db.entity;

import com.tkelemen.arbitrader.model.ExchangeOrder;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "maker_trigger_order")
public class MakerTriggerOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "create_datetime")
    private Date createDatetime;

    @Column(name = "update_datetime")
    private Date updateDatetime;

    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name = "maker_opportunity_id", nullable = false)
    private MakerOpportunity opportunity;

    private String pair;

    private BigDecimal amount;

    private BigDecimal price;

    private BigDecimal executedAmount;

    @Column(name = "exchange_order_id")
    private String exchangeOrderId;

    @Enumerated(value = EnumType.STRING)
    private ExchangeOrder.Status status;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreateDatetime() {
        return createDatetime;
    }

    public void setCreateDatetime(Date createDatetime) {
        this.createDatetime = createDatetime;
    }

    public Date getUpdateDatetime() {
        return updateDatetime;
    }

    public void setUpdateDatetime(Date updateDatetime) {
        this.updateDatetime = updateDatetime;
    }

    public MakerOpportunity getOpportunity() {
        return opportunity;
    }

    public void setOpportunity(MakerOpportunity opportunity) {
        this.opportunity = opportunity;
    }

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getExecutedAmount() {
        return executedAmount;
    }

    public void setExecutedAmount(BigDecimal executedAmount) {
        this.executedAmount = executedAmount;
    }

    public String getExchangeOrderId() {
        return exchangeOrderId;
    }

    public void setExchangeOrderId(String exchangeOrderId) {
        this.exchangeOrderId = exchangeOrderId;
    }

    public ExchangeOrder.Status getStatus() {
        return status;
    }

    public void setStatus(ExchangeOrder.Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "MakerTriggerOrder{" +
                "id=" + id +
                ", createDatetime=" + createDatetime +
                ", updateDatetime=" + updateDatetime +
                ", opportunity=" + opportunity +
                ", pair='" + pair + '\'' +
                ", amount=" + amount +
                ", price=" + price +
                ", executedAmount=" + executedAmount +
                ", exchangeOrderId='" + exchangeOrderId + '\'' +
                ", status=" + status +
                '}';
    }
}
