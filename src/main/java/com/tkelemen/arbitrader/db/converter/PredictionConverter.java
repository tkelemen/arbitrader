package com.tkelemen.arbitrader.db.converter;

import com.tkelemen.arbitrader.db.entity.Prediction;
import com.tkelemen.arbitrader.model.Opportunity;

import java.util.Date;
import java.util.Set;

public class PredictionConverter {
    public static Prediction toEntity(Opportunity opportunity, long timestamp) {
        Prediction prediction = new Prediction();
        prediction.setDatetime(new Date(timestamp));
        prediction.setIterationId(timestamp);
        prediction.setInitialExchange(opportunity.getInitialExchange().toString());
        prediction.setInitialAmount(opportunity.getInitialAmount());
        prediction.setFinalExchange(opportunity.getFinalExchange().toString());
        prediction.setTransferAmount(opportunity.getTransferAmount());
        prediction.setFinalAmount(opportunity.getFinalAmount());
        prediction.setPair(opportunity.getPair());
        prediction.setFinalPair(opportunity.getFinalPair());
        prediction.setBuyFee(opportunity.getBuyFee());
        prediction.setSellFee(opportunity.getSellFee());
        prediction.setWithdrawFee(opportunity.getWithdrawFee());
        prediction.setDepositFee(opportunity.getDepositFee());

        Set<String> flags = opportunity.getFlags();

        if(flags.contains(Opportunity.FLAG_BEST_GAIN)) {
            prediction.setBestGain(true);
        }
        if(flags.contains(Opportunity.FLAG_INSUFFICIENT_BASE_FUNDS)) {
            prediction.setInsufficientBaseFunds(true);
        }
        if(flags.contains(Opportunity.FLAG_INSUFFICIENT_QUOTE_FUNDS)) {
            prediction.setInsufficientQuoteFunds(true);
        }

        return prediction;
    }
}
