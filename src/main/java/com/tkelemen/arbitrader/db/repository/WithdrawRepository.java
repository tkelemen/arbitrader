package com.tkelemen.arbitrader.db.repository;

import com.tkelemen.arbitrader.db.entity.Withdraw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;

public interface WithdrawRepository extends JpaRepository<Withdraw, Long> {
    @Query("SELECT w FROM Withdraw w WHERE w.asset = ?1 AND w.status = ?2 AND w.fromExchange = ?3 " +
            "AND w.recipientAddress = ?4 AND w.amount = ?5")
    Withdraw find(String asset, String status, String fromExchange, String recipientAddress, BigDecimal amount);
}
