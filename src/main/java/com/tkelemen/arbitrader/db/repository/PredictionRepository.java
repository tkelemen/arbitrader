package com.tkelemen.arbitrader.db.repository;

import com.tkelemen.arbitrader.api.model.RawPredictionData;
import com.tkelemen.arbitrader.db.entity.Prediction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface PredictionRepository extends JpaRepository<Prediction, Long> {

    @Query(
            value = "SELECT new com.tkelemen.arbitrader.api.model.RawPredictionData(cast(p.datetime as date), p.pair, p.initialExchange, p.finalExchange, p.initialAmount, sum(p.finalAmount-p.initialAmount), count(p)) " +
                    "FROM Prediction p " +
                    "WHERE p.datetime BETWEEN :fromDate AND :toDate " +
                    "AND p.initialAmount IN :initialAmounts " +
                    "GROUP BY cast(p.datetime as date), p.pair, p.initialExchange, p.finalExchange, p.initialAmount"
    )
    List<RawPredictionData> findByDateAndInitialAmount(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("initialAmounts") List<BigDecimal> initialAmounts);
}
