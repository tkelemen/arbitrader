package com.tkelemen.arbitrader.db.repository;

import com.tkelemen.arbitrader.db.entity.Arbitrage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface ArbitrageRepository extends JpaRepository<Arbitrage, Long> {
    List<Arbitrage> findByWithdrawStatus(String withdrawStatus);

    @Query("SELECT a FROM Arbitrage a WHERE (a.buyFee is null AND a.prediction.initialExchange = :exchange) OR (a.sellFee is null AND a.prediction.finalExchange = :exchange)")
    List<Arbitrage> findWithMissingDetails(@Param("exchange") String exchange);

    @Transactional
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query("UPDATE Arbitrage a SET a.withdrawStatus = 'MANUAL' WHERE a.withdrawStatus = 'PENDING' AND a.baseCurrency = :currency")
    int setManualWithdrawStatus(@Param("currency") String currency);
}
