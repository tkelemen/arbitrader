package com.tkelemen.arbitrader.db.repository;

import com.tkelemen.arbitrader.db.entity.MakerOpportunity;
import org.springframework.data.repository.CrudRepository;

public interface MakerOpportunityRepository extends CrudRepository<MakerOpportunity, Long> {
}
