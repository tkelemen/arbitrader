package com.tkelemen.arbitrader.db.repository;

import com.tkelemen.arbitrader.db.entity.MakerTrade;
import org.springframework.data.repository.CrudRepository;

public interface MakerTradeRepository extends CrudRepository<MakerTrade, Long> {
}
