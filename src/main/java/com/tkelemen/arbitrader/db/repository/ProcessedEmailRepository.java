package com.tkelemen.arbitrader.db.repository;

import com.tkelemen.arbitrader.db.entity.ProcessedEmail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProcessedEmailRepository extends JpaRepository<ProcessedEmail, Long> {
    ProcessedEmail findBySenderAndSubjectAndHash(String sender, String subject, String hash);
}
