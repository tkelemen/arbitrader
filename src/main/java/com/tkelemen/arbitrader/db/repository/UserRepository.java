package com.tkelemen.arbitrader.db.repository;

import com.tkelemen.arbitrader.db.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
