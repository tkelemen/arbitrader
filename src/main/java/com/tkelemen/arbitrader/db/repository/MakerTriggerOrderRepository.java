package com.tkelemen.arbitrader.db.repository;

import com.tkelemen.arbitrader.model.ExchangeOrder;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.tkelemen.arbitrader.db.entity.MakerTriggerOrder;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface MakerTriggerOrderRepository extends CrudRepository<MakerTriggerOrder, Long> {
    List<MakerTriggerOrder> findByStatus(ExchangeOrder.Status status);

    @Transactional
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query("UPDATE MakerTriggerOrder o SET o.status = :status WHERE o.id = :id")
    void setStatus(@Param("id") long id, @Param("status") ExchangeOrder.Status status);

    Optional<MakerTriggerOrder> findByExchangeOrderId(String exchangeOrderId);
}
