package com.tkelemen.arbitrader.service.mail.processor;

import com.tkelemen.arbitrader.model.ProcessResult;

import javax.mail.Message;

public interface MailProcessor<T extends ProcessResult> {
    T process(Message message) throws Exception;
}
