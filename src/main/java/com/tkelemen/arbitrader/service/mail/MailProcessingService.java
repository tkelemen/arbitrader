package com.tkelemen.arbitrader.service.mail;

import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.db.entity.ProcessedEmail;
import com.tkelemen.arbitrader.db.repository.ProcessedEmailRepository;
import com.tkelemen.arbitrader.model.WithdrawResult;
import com.tkelemen.arbitrader.service.mail.processor.CexDepositConfirmMailProcessor;
import com.tkelemen.arbitrader.service.mail.processor.CexWithdrawConfirmMailProcessor;
import com.tkelemen.arbitrader.service.mail.processor.CexWithdrawSentMailProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.search.SearchTerm;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Properties;

@Service
public class MailProcessingService {
    private static final Logger logger = LoggerFactory.getLogger(MailProcessingService.class);

    private static final int MESSAGE_COUNT_TO_PROCESS = 50;

    private ApplicationConfig config;

    private CexWithdrawConfirmMailProcessor cexWithdrawConfirmMailProcessor;
    private CexWithdrawSentMailProcessor cexWithdrawSentMailProcessor;
    private CexDepositConfirmMailProcessor cexDepositConfirmMailProcessor;


    private ProcessedEmailRepository processedEmailRepository;

    private MessageDigest md;

    @Autowired
    public MailProcessingService(
            ApplicationConfig config,
            CexWithdrawConfirmMailProcessor cexWithdrawConfirmMailProcessor,
            ProcessedEmailRepository processedEmailRepository
    ) {
        this.config = config;
        this.cexWithdrawConfirmMailProcessor = cexWithdrawConfirmMailProcessor;
        this.processedEmailRepository = processedEmailRepository;

        try {
            this.md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public void processMessages() {
        try {
            logger.info("Processing emails");

            String host = config.getMailHost();
            String user = config.getMailUser();

            Session session = Session.getDefaultInstance(new Properties(), null);
            Store store = session.getStore(config.getMailProtocol());
            store.connect(host, config.getMailPort(), user, config.getMailPassword());

            // TODO: make universal for all exchanges/folders
            String folder = "cex";
            Folder inbox = store.getFolder(folder);
            inbox.open(Folder.READ_WRITE);

            int totalCount = inbox.getMessageCount();
            int fromId = totalCount > MESSAGE_COUNT_TO_PROCESS ? totalCount - MESSAGE_COUNT_TO_PROCESS : 1;
            Message[] messages = inbox.getMessages(fromId, totalCount);

            if (messages.length == 0) {
                logger.info("No e-mails found.");
            }

            for (Message message : messages) {
                if (message.getFlags().contains(Flags.Flag.SEEN)) {
                    logger.info("Skipping seen message with subject: " + message.getSubject());
                    continue;
                }

                logger.info("Processing message with subject: " + message.getSubject());

                if (message.match(new SearchTerm() {
                    @Override
                    public boolean match(Message message) {
                        try {
                            return message.getSubject().contains("Action Needed — Withdrawal Confirmation") ||
                                    message.getSubject().contains("Crypto Withdrawal Confirmed") ||
                                    message.getSubject().contains("Ledger Withdrawal Confirmed");
                        } catch (MessagingException e) {
                            logger.error("Error while processing email: " + e);
                            return false;
                        }
                    }
                })) {
                    ProcessedEmail processedEmail = processedEmailRepository.findBySenderAndSubjectAndHash(message.getFrom()[0].toString(), message.getSubject(), getHash(message.getContent()));

                    if (processedEmail != null) {
                        logger.error("Message already processed [ from = " + processedEmail.getSender() + ", subject = " + processedEmail.getSubject() + ", hash = " + processedEmail.getHash());
                        continue;
                    } else {
                        processedEmail = new ProcessedEmail();
                    }

                    if (message.getSubject().contains("Action Needed — Withdrawal Confirmation")) {
                        WithdrawResult result = cexWithdrawConfirmMailProcessor.process(message);
                        processedEmail.setWithdraw(result.getWithdraw());
                    } else if (message.getSubject().contains("Crypto Withdrawal Confirmed") ||
                            message.getSubject().contains("Ledger Withdrawal Confirmed")) {
                        cexWithdrawSentMailProcessor.process(message);
                    }

                    processedEmail.setHost(host);
                    processedEmail.setUser(user);
                    processedEmail.setDateReceived(message.getReceivedDate());
                    processedEmail.setDateProcessed(new Date());
                    processedEmail.setFolder(folder);
                    processedEmail.setSender(message.getFrom()[0].toString());
                    processedEmail.setSubject(message.getSubject());
                    processedEmail.setHash(getHash(message.getContent()));

                    processedEmailRepository.save(processedEmail);

                    message.setFlag(Flags.Flag.SEEN, true);
                }
            }

            inbox.close(true);
            store.close();
        } catch (IOException | MessagingException e) {
            logger.error("Error while processing emails: " + e);
            e.printStackTrace();
        }
    }

    // TODO: move into utility class
    private String getHash(Object messageContent) {
        return DatatypeConverter.printHexBinary(md.digest(messageContent.toString().getBytes()));
    }
}
