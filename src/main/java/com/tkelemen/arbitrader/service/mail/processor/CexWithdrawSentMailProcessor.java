package com.tkelemen.arbitrader.service.mail.processor;

import com.tkelemen.arbitrader.model.ProcessResult;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import javax.mail.Message;
import javax.mail.MessagingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CexWithdrawSentMailProcessor implements MailProcessor {
    @Override
    public ProcessResult process(Message message) throws MessagingException {
/*
        Pattern subjectPattern = Pattern.compile("(Crypto|Ledger) Withdrawal Confirmed — 2021-(\\d{2})");
        Matcher subjectMatcher = subjectPattern.matcher(message.getSubject());


        Document mail = Jsoup.parse(message.getContent().toString());

        System.out.println("Processing email body: " + mail.text());

        Pattern pattern = Pattern.compile("Requested amount: ([A-Z]{3,4}) (\\d*\\.\\d+|\\d+\\.\\d*|\\d*)");
        Matcher matcher = pattern.matcher(mail.text());

        if(matcher.find() && matcher.group(1) != null && matcher.group(2) != null) {
            System.out.println("Requested amount: " + matcher.group(2) + " " + matcher.group(1));
            String ticker = matcher.group(1);

            Integer count = withdrawCounts.get(ticker);
            if (count == null) {
                count = 1;
            } else {
                count++;
            }

            withdrawCounts.put(ticker, count);
        } else {
            requestedAmountMissing.add(message.getSubject());
        }

        Pattern commissionPattern = Pattern.compile("Commission: ([A-Z]{3,4}) (\\d*\\.\\d+|\\d+\\.\\d*|\\d*)");
        Matcher commissionPatternMatcher = commissionPattern.matcher(mail.text());

        if(commissionPatternMatcher.find() && commissionPatternMatcher.group(1) != null && commissionPatternMatcher.group(2) != null) {
            System.out.println("Commission: " + commissionPatternMatcher.group(2) + " " + commissionPatternMatcher.group(1));
        } else {
            commissionMissing.add(message.getSubject());
        }

        Pattern transactionHashPattern = Pattern.compile("Transaction hash: ([a-zA-Z0-9]+)");
        Matcher transactionHashPatternMatcher = transactionHashPattern.matcher(mail.text());

        if(transactionHashPatternMatcher.find() && transactionHashPatternMatcher.group(1) != null) {
            System.out.println("Transaction hash: " + transactionHashPatternMatcher.group(1));
            transactionPresent.add(message.getSubject());
        }
        */

        return null;
    }
}
