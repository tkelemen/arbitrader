package com.tkelemen.arbitrader.service.mail.processor;

import com.tkelemen.arbitrader.model.Address;
import com.tkelemen.arbitrader.model.WithdrawResult;
import com.tkelemen.arbitrader.model.WithdrawStage;
import com.tkelemen.arbitrader.service.arbitrage.withdraw.CexWithdrawExecutorService;
import com.tkelemen.arbitrader.service.notification.NotificationService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.MessagingException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class CexWithdrawConfirmMailProcessor implements MailProcessor<WithdrawResult> {
    private static Logger logger = LoggerFactory.getLogger(CexWithdrawConfirmMailProcessor.class);

    private CexWithdrawExecutorService withdrawService;

    private NotificationService notificationService;

    @Autowired
    public CexWithdrawConfirmMailProcessor(CexWithdrawExecutorService withdrawService, NotificationService notificationService) {
        this.withdrawService = withdrawService;
        this.notificationService = notificationService;
    }

    @Override
    public WithdrawResult process(Message message) throws IOException, MessagingException {
        Document mail = Jsoup.parse(message.getContent().toString());

        String confirmUrl = mail.getElementsMatchingText("Confirm").attr("href");

        logger.info("Processing email body: " + mail.text());

        Pattern memoPattern = Pattern.compile("We have received a withdrawal request for (\\d*\\.\\d+|\\d+\\.\\d*) ([A-Z]{3,4}) to the wallet Address: ([a-zA-Z0-9]+) (Memo|Destination Tag): ([0-9]+)");
        Matcher memoMatcher = memoPattern.matcher(mail.text());

        Pattern pattern = Pattern.compile("We have received a withdrawal request for (\\d*\\.\\d+|\\d+\\.\\d*) ([A-Z]{3,4}) to the wallet Address: ([a-zA-Z0-9]+)");
        Matcher matcher = pattern.matcher(mail.text());

        BigDecimal amount;
        String asset;
        Address address;

        if (memoMatcher.find()) {
            amount = new BigDecimal(memoMatcher.group(1));
            asset = memoMatcher.group(2);
            String walletAddress = memoMatcher.group(3);
            String walletMemoTag = memoMatcher.group(5);

            address = Address.create(walletAddress, new BigInteger(walletMemoTag));
        } else if (matcher.find()) {
            amount = new BigDecimal(matcher.group(1));
            asset = matcher.group(2);
            String walletAddress = matcher.group(3);

            address = Address.create(walletAddress);
        } else {
            logger.error("Could not find withdraw details in email [ subject = " + message.getSubject() + " ]");

            return new WithdrawResult(WithdrawStage.CONFIRMED).setSuccess(false);
        }

        WithdrawResult result = withdrawService.confirmWithdraw(asset, amount, address, confirmUrl);

        logger.info("Withdraw confirm status " + (result.isSuccess() ? "OK" : "NOK"));

        if (result.isSuccess()) {
            notificationService.send("Withdraw confirmed [ from = CEX, asset = " + asset + ", amount = " + amount + ", destination wallet = " + address + " ]");
        } else {
            notificationService.send("Withdraw confirm error [ from = CEX, asset = " + asset + ", amount = " + amount + ", destination wallet = " + address + " ]");
        }

        return result;
    }
}
