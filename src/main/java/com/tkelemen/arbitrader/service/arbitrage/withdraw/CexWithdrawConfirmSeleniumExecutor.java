package com.tkelemen.arbitrader.service.arbitrage.withdraw;

import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.exception.SeleniumExecutorException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CexWithdrawConfirmSeleniumExecutor extends CexSeleniumExecutor {
    private static Logger logger = LoggerFactory.getLogger(CexWithdrawConfirmSeleniumExecutor.class);

    private String confirmUrl;
    private ApplicationConfig config;

    CexWithdrawConfirmSeleniumExecutor(ApplicationConfig config, String confirmUrl) {
        this.config = config;
        this.confirmUrl = confirmUrl;
    }

    @Override
    public void executeSteps() throws SeleniumExecutorException {
        driver.get(confirmUrl);

        WebElement header = driver.findElement(By.cssSelector("h1"));
        WebElement info = driver.findElement(By.cssSelector("h3"));

        if (!"Your transaction is being processed.".equals(header.getText()) ||
                !"Please wait...".equals(info.getText())) {
            logger.error("Withdraw confirm not successful");
            throw new SeleniumExecutorException("Could not confirm withdraw");
        }
    }

    @Override
    protected String getFinalScreenshotFileName() {
        return "withdraw_confirm";
    }

    @Override
    protected String getCookieProdSessionId() {
        return config.getCexCookieProdSessionId();
    }

    @Override
    protected String getCookieUserId() {
        return config.getCexCookieUserId();
    }

    @Override
    protected String getCookieSessionId() {
        return config.getCexCookieSessionId();
    }

}
