package com.tkelemen.arbitrader.service.arbitrage.taker;

import com.tkelemen.arbitrader.ArbitraderApplication;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.db.converter.PredictionConverter;
import com.tkelemen.arbitrader.db.entity.Arbitrage;
import com.tkelemen.arbitrader.db.entity.Prediction;
import com.tkelemen.arbitrader.db.entity.User;
import com.tkelemen.arbitrader.db.repository.ArbitrageRepository;
import com.tkelemen.arbitrader.db.repository.PredictionRepository;
import com.tkelemen.arbitrader.db.repository.UserRepository;
import com.tkelemen.arbitrader.model.*;
import com.tkelemen.arbitrader.model.enums.OrderType;
import com.tkelemen.arbitrader.service.arbitrage.funding.FundingService;
import com.tkelemen.arbitrader.service.market.FeeService;
import com.tkelemen.arbitrader.service.notification.NotificationService;
import com.tkelemen.arbitrader.service.trade.*;
import com.tkelemen.arbitrader.util.CurrencyUtils;
import com.tkelemen.arbitrader.util.TradeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.*;

@Service
public class TradeService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TradeService.class);

    private final PredictionRepository predictionRepository;
    private final ArbitrageRepository arbitrageRepository;
    private final NotificationService notificationService;
    private final FeeService feeService;
    private final ApplicationConfig config;
    private final ActiveOrders activeOrders;
    private final Map<Exchange, IExchangeService> exchangeServices;
    private Set<String> previousPredictions = new HashSet<>();
    private Set<Exchange> allowedExchanges = new HashSet<>();
    private User currentUser;
    private BigDecimal executeThreshold;
    private BigDecimal executeWithFundingThreshold;
    private BigDecimal executeFundingThreshold;

    private boolean tradingEnabled = true;

    private final Queue<Float> lastGains = new ArrayBlockingQueue<>(20);

    private long tradeStartTimestamp = 0L;

    @Autowired
    public TradeService(
            ExchangeManagerService exchangeManagerService,
            PredictionRepository predictionRepository,
            ArbitrageRepository arbitrageRepository,
            NotificationService notificationService,
            UserRepository userRepository,
            FeeService feeService,
            ApplicationConfig config,
            ActiveOrders activeOrders) {
        this.predictionRepository = predictionRepository;
        this.arbitrageRepository = arbitrageRepository;
        this.notificationService = notificationService;
        this.exchangeServices = exchangeManagerService.getExchangeServices();
        this.feeService = feeService;
        this.config = config;
        this.activeOrders = activeOrders;

        currentUser = userRepository.getOne(1L);

        allowedExchanges = new HashSet<>(config.getAllowedExchanges());
        executeFundingThreshold = config.getFundingThreshold();
        executeThreshold = config.getExecuteThreshold();
        executeWithFundingThreshold = config.getAfterFundingMinGain();
    }

    public void syncAccountBalances() {
        LOGGER.info("Syncing account balances");

        exchangeServices.forEach((exchange, exchangeService) -> {
            exchangeService.updateWallet();
            LOGGER.debug(exchange + " wallet after sync: " + exchangeService.getWallet());
        });
    }

    public void startProcessing() {
        syncAccountBalances();
    }

    public void processOpportunities(List<Opportunity> opportunities) {
        final long now = System.currentTimeMillis();

        final Map<String, Opportunity> newPredictions = new HashMap<>();
        final Map<String, Opportunity> bestGainForPairMap = new HashMap<>();
        boolean hasProfitableTrade = false;

        // TODO: refactor, move to PredictionService
        for (Opportunity opportunity : opportunities) {
            final String base = CurrencyUtils.getBase(opportunity.getPair());

            final Opportunity bestOpportunityForPair = bestGainForPairMap.get(base);

            final BigDecimal initialWalletQuoteAmount = exchangeServices.get(opportunity.getInitialExchange()).getWallet()
                    .getFreeAmount(CurrencyUtils.getQuote(opportunity.getPair()));
            final BigDecimal finalWalletBaseAmount = exchangeServices.get(opportunity.getFinalExchange()).getWallet()
                    .getFreeAmount(CurrencyUtils.getBase(opportunity.getFinalPair()));

            boolean hasSufficientFunds = true;

            if (initialWalletQuoteAmount.compareTo(opportunity.getInitialAmount()) < 0) {
                opportunity.addFlag(Opportunity.FLAG_INSUFFICIENT_QUOTE_FUNDS);
                hasSufficientFunds = false;
            }
            if (finalWalletBaseAmount.compareTo(opportunity.getTransferAmount()) < 0) {
                opportunity.addFlag(Opportunity.FLAG_INSUFFICIENT_BASE_FUNDS);
                hasSufficientFunds = false;
            }

            if (hasSufficientFunds &&
                    calculateGainMultiplier(opportunity) > executeThreshold.doubleValue() &&
                    (bestOpportunityForPair == null || calculateGain(bestOpportunityForPair) < calculateGain(opportunity))) {
                hasProfitableTrade = true;

                if (bestOpportunityForPair != null) {
                    bestOpportunityForPair.removeFlag(Opportunity.FLAG_BEST_GAIN);
                }

                opportunity.addFlag(Opportunity.FLAG_BEST_GAIN);
                bestGainForPairMap.put(base, opportunity);
            }

            newPredictions.put(getPredictionKey(opportunity), opportunity);
        }

        // TODO: refactor!!!
        for (Opportunity opportunity : opportunities) {
            final Prediction prediction = PredictionConverter.toEntity(opportunity, now);

            final double possibleGain = calculateGainMultiplier(opportunity);
            float currentGain = 0;
            boolean executed = false;

            if (hasProfitableTrade &&
                    possibleGain > executeThreshold.floatValue() &&
                    !opportunity.getFlags().contains(Opportunity.FLAG_INSUFFICIENT_QUOTE_FUNDS) &&
                    !opportunity.getFlags().contains(Opportunity.FLAG_INSUFFICIENT_BASE_FUNDS)) {
                final float gain = PredictionService.calculateOpportunityProfitMultiplier(opportunity, feeService);
                if(gain > 1) {
                    executed = executeProfitable(opportunity, prediction, false);
                    setCooldownPeriod(config.getCooldownAfterSuccessMs());
                } else {
                    currentGain = gain;
                    setCooldownPeriod(config.getCooldownAfterDesyncMs());
                    final String message = "Arbitrage not profitable anymore (gain " + gain + "), aborting: " + opportunity;
                    LOGGER.info(message);
                    LOGGER.info("Initial order book: " + opportunity.getInitialOrderBook());
                    LOGGER.info("Final order book: " + opportunity.getFinalOrderBook());
                    notificationService.send(message);
                }
            } else if (!opportunity.getFlags().contains(Opportunity.FLAG_INSUFFICIENT_BASE_FUNDS)) {
                if (possibleGain > executeWithFundingThreshold.floatValue()) {
                    // key: {FROM}{TO}{ASSET}{AMOUNT}
                    final Map<String, List<Opportunity>> reverseFundingOpportunities = PredictionService.getReverseFundingOpportunities(opportunities);

                    if (executeFundingForOpportunity(now, reverseFundingOpportunities, opportunity)) {
                        final float afterFundingGain = PredictionService.calculateOpportunityProfitMultiplier(opportunity, feeService);
                        if (afterFundingGain > 1) {
                            executed = executeProfitable(opportunity, prediction, true);
                        } else {
                            setCooldownPeriod(config.getCooldownAfterDesyncMs());
                            final String message = "Arbitrage not profitable anymore after funding (gain " + afterFundingGain + "), aborting: " + opportunity;
                            LOGGER.info(message);
                            LOGGER.info("Initial order book: " + opportunity.getInitialOrderBook());
                            LOGGER.info("Final order book: " + opportunity.getFinalOrderBook());
                            notificationService.send(message);
                        }
                    }
                }
            }

            if (!executed && canBeUsedForFunding(opportunity) && possibleGain > executeFundingThreshold.floatValue()) {
                final float gain = currentGain != 0 ? currentGain : PredictionService.calculateOpportunityProfitMultiplier(opportunity, feeService);
                if(gain < executeFundingThreshold.floatValue()) {
                    setCooldownPeriod(config.getCooldownAfterDesyncMs());
                    final String message = "General funding not profitable anymore (gain " + gain + "), aborting: " + opportunity;
                    LOGGER.info(message);
                    LOGGER.info("Initial order book: " + opportunity.getInitialOrderBook());
                    LOGGER.info("Final order book: " + opportunity.getFinalOrderBook());
                    notificationService.send(message);
                } else {
                    executed = executeGeneralFunding(opportunity, now);
                }
            }

            if (!previousPredictions.contains(getPredictionKey(opportunity)) && isAboveThreshold(opportunity)) {
                predictionRepository.save(prediction);
            }

            if(executed) {
                break;
            }
        }

        previousPredictions = newPredictions.keySet();

    }

    private boolean isAboveThreshold(Opportunity opportunity) {
        return opportunity.getFinalAmount().compareTo(opportunity.getInitialAmount().multiply(config.getMinWatchThreshold()))  > 0;
    }

    private boolean canBeUsedForFunding(Opportunity opportunity) {
        if (opportunity.getFlags().contains(Opportunity.FLAG_INSUFFICIENT_QUOTE_FUNDS) ||
                opportunity.getFlags().contains(Opportunity.FLAG_INSUFFICIENT_BASE_FUNDS)) {
            return false;
        }

        final String initialQuote = CurrencyUtils.getQuote(opportunity.getPair());
        final String finalQuote = CurrencyUtils.getQuote(opportunity.getFinalPair());

        if (!config.getFundingQuotes().contains(finalQuote)) {
            return false;
        }

        if (exchangeServices.get(opportunity.getInitialExchange()).getWallet().getFreeAmount(initialQuote).subtract(opportunity.getInitialAmount())
                .compareTo(config.getFundingMaxAmount()) <= 0) {
            return false;
        }

        return exchangeServices.get(opportunity.getFinalExchange()).getWallet().getFreeAmount(finalQuote).compareTo(config.getFundingMaxAmount()) < 0;
    }

    private boolean executeProfitable(Opportunity opportunity, Prediction prediction, boolean ignoreInsufficientQuoteFunds) {
        if ((!ignoreInsufficientQuoteFunds && opportunity.getFlags().contains(Opportunity.FLAG_INSUFFICIENT_QUOTE_FUNDS)) || opportunity.getFlags().contains(Opportunity.FLAG_INSUFFICIENT_BASE_FUNDS)) {
            return false;
        }

        try {
            if (!isAllowedTrade(opportunity)) {
                LOGGER.info("Trade with best gain not allowed: " + opportunity);
                return false;
            }

            LOGGER.info("!!! Executing arbitrage opportunity: " + opportunity);
            final Arbitrage arbitrage = executeTrade(opportunity, prediction);

            if (arbitrage == null) {
                LOGGER.info("!!! Arbitrage execution error, ignoring");
                return false;
            }

            arbitrage.setUser(currentUser);
            arbitrage.setThreshold(executeThreshold);
            arbitrage.setType(Arbitrage.TYPE_PROFIT);

            predictionRepository.save(prediction);
            arbitrageRepository.save(arbitrage);

            if (arbitrage.getReceivedAmount() != null && arbitrage.getInitialAmount() != null &&
                    BigDecimal.ZERO.compareTo(arbitrage.getInitialAmount()) < 0 && BigDecimal.ZERO.compareTo(arbitrage.getReceivedAmount()) < 0) {
                final float netGain = TradeUtils.getNetGainInclSaldo(arbitrage).floatValue();

                if (netGain < 0) {
                    LOGGER.info("Negative gain warning: " + netGain);
                    LOGGER.info("Setting cooldown period: " + config.getCooldownAfterFailureMs() + "ms");
                    setCooldownPeriod(config.getCooldownAfterFailureMs());

                    LOGGER.info("Negative gain warning: " + netGain);
                    LOGGER.info("Initial order book: " + opportunity.getInitialOrderBook());
                    LOGGER.info("Final order book: " + opportunity.getFinalOrderBook());
                } else {
                    //setCooldownPeriod(500);
                }

                recordGain(netGain);
            }

            if(!Arbitrage.STATUS_FAIL.equals(arbitrage.getBuyStatus()) && !Arbitrage.STATUS_FAIL.equals(arbitrage.getSellStatus())) {
                notificationService.send(getShortDescription(arbitrage));
            }

            LOGGER.info("!!! Arbitrage executed: " + arbitrage);

            if (getLastGainsSum() < -20) {
                LOGGER.info("Stopping trading because the loss of last 20 trades was bigger than 20");
                notificationService.send("Stopping trading because the loss of last 20 trades was bigger than 20");
                setTradingEnabled(false);
            }

            return true;
        } catch (Throwable e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage(), e);
            notificationService.send("ERROR: " + e);
            ArbitraderApplication.shutdown();
        }

        return false;
    }

    private boolean executeFundingForOpportunity(long timestamp, Map<String, List<Opportunity>> reverseFundingOpportunities, Opportunity opportunity) {
        final double possibleGain = calculateGainMultiplier(opportunity);

        if (possibleGain < executeWithFundingThreshold.doubleValue()) {
            return false;
        }

        if (!isAllowedTrade(opportunity)) {
            LOGGER.info("Trade with funding not allowed: " + opportunity);
            return false;
        }

        final String key = PredictionService.getFundingOpportunityKey(opportunity);

        LOGGER.debug("!!! Checking funding possibilities for arbitrage opportunity: " + opportunity);
        LOGGER.debug("!!! Possible gain: " + possibleGain);
        LOGGER.debug("!!! Funding opportunity key: " + key);

        if (reverseFundingOpportunities.containsKey(key)) {
            final List<Opportunity> fundingOpportunities = reverseFundingOpportunities.get(key);
            Opportunity fundingOpportunity = null;

            LOGGER.debug("!!! Found reverse funding opportunities: " + fundingOpportunities);

            final Iterator<Opportunity> fundingOpportunityIterator = fundingOpportunities.iterator();
            while (fundingOpportunityIterator.hasNext()) {
                fundingOpportunity = fundingOpportunityIterator.next();

                if (fundingOpportunity.getInitialAmount().compareTo(opportunity.getInitialAmount()) != 0) {
                    if (!fundingOpportunityIterator.hasNext()) {
                        fundingOpportunity = null;
                    }
                } else {
                    break;
                }
            }

            if (fundingOpportunity == null) {
                return false;
            }

            final double fundingLoss = calculateGainMultiplier(fundingOpportunity);

            LOGGER.debug(opportunity.getPair() + "-" + key + " " + possibleGain + "-" + fundingLoss + ":" + executeWithFundingThreshold.doubleValue());

            if ((possibleGain - (1 - fundingLoss)) > executeWithFundingThreshold.floatValue()) {
                try {
                    if (isAllowedTrade(fundingOpportunity)) {
                        LOGGER.info("!!! Executing arbitrage funding opportunity (" +
                                (possibleGain - (1 - fundingLoss)) + ": " + possibleGain + "-" + fundingLoss + "): " + fundingOpportunity +
                                ", based on possible profit opportunity: " + opportunity);

                        final Prediction fundingPrediction = PredictionConverter.toEntity(fundingOpportunity, timestamp);
                        final Arbitrage arbitrage = executeTrade(fundingOpportunity, fundingPrediction);

                        if (arbitrage != null) {
                            arbitrage.setUser(currentUser);
                            arbitrage.setThreshold(executeThreshold);
                            arbitrage.setType(Arbitrage.TYPE_FUNDING);
                            predictionRepository.save(fundingPrediction);
                            arbitrageRepository.save(arbitrage);

                            if (arbitrage.getReceivedAmount() != null && arbitrage.getInitialAmount() != null &&
                                    BigDecimal.ZERO.compareTo(arbitrage.getInitialAmount()) < 0 && BigDecimal.ZERO.compareTo(arbitrage.getReceivedAmount()) < 0) {
                                final float netGain = TradeUtils.getNetGainPercentInclSaldo(arbitrage).floatValue();

                                if (netGain * 1.01 < fundingLoss) {
                                    setCooldownPeriod(config.getCooldownAfterFailureMs());

                                    LOGGER.info("Excessive funding loss warning: actual=" + netGain + ", planned=" + fundingLoss + "");
                                    LOGGER.info("Initial order book: " + fundingOpportunity.getInitialOrderBook());
                                    LOGGER.info("Final order book: " + fundingOpportunity.getFinalOrderBook());
                                }

                                recordGain(netGain);
                            }

                            if(!Arbitrage.STATUS_FAIL.equals(arbitrage.getBuyStatus()) && !Arbitrage.STATUS_FAIL.equals(arbitrage.getSellStatus())) {
                                notificationService.send(getShortDescription(arbitrage));
                            }
                            LOGGER.info("!!! Arbitrage funding executed: " + arbitrage);

                            return true;
                        }
                    } else {
                        LOGGER.info("Funding trade not allowed: " + fundingOpportunity);
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                    notificationService.send("ERROR: " + e.toString());
                    ArbitraderApplication.shutdown();
                }
            }
        } else {
            LOGGER.debug("!!! No funding found: " + opportunity.getInitialAmount()
                    + " " + CurrencyUtils.getQuote(opportunity.getPair()) + " -> " + opportunity.getInitialExchange());
        }

        return false;
    }

    private boolean executeGeneralFunding(Opportunity fundingOpportunity, long timestamp) {
        final double fundingLoss = calculateGainMultiplier(fundingOpportunity);

        if (fundingLoss > executeFundingThreshold.floatValue()) {
            try {
                if (isAllowedTrade(fundingOpportunity)) {
                    LOGGER.info("!!! Executing general arbitrage funding opportunity (loss: " +
                            fundingLoss + "): " + fundingOpportunity);

                    final Prediction fundingPrediction = PredictionConverter.toEntity(fundingOpportunity, timestamp);
                    final Arbitrage arbitrage = executeTrade(fundingOpportunity, fundingPrediction);

                    if (arbitrage != null) {
                        arbitrage.setUser(currentUser);
                        arbitrage.setThreshold(executeThreshold);
                        arbitrage.setType(Arbitrage.TYPE_FUNDING);
                        predictionRepository.save(fundingPrediction);
                        arbitrageRepository.save(arbitrage);

                        if (arbitrage.getReceivedAmount() != null && arbitrage.getInitialAmount() != null &&
                            BigDecimal.ZERO.compareTo(arbitrage.getInitialAmount()) < 0 && BigDecimal.ZERO.compareTo(arbitrage.getReceivedAmount()) < 0) {
                            final float netGain = TradeUtils.getNetGainPercentInclSaldo(arbitrage).floatValue();

                            if (netGain * 1.01 < fundingLoss) {
                                setCooldownPeriod(config.getCooldownAfterFailureMs());

                                LOGGER.info("Excessive funding loss warning: actual=" + netGain + ", planned=" + fundingLoss + "");
                                LOGGER.info("Initial order book: " + fundingOpportunity.getInitialOrderBook());
                                LOGGER.info("Final order book: " + fundingOpportunity.getFinalOrderBook());
                            }

                            recordGain(netGain);
                        }

                        if(!Arbitrage.STATUS_FAIL.equals(arbitrage.getBuyStatus()) && !Arbitrage.STATUS_FAIL.equals(arbitrage.getSellStatus())) {
                            notificationService.send("General " + getShortDescription(arbitrage));
                        }
                        LOGGER.info("!!! Arbitrage general funding executed: " + arbitrage);

                        return true;
                    }
                } else {
                    LOGGER.info("General funding trade not allowed: " + fundingOpportunity);
                }
            } catch (Throwable e) {
                e.printStackTrace();
                if(e.getCause() != null) {
                    e.getCause().printStackTrace();
                }
                notificationService.send("ERROR: " + e.toString());
                ArbitraderApplication.shutdown();
            }
        }

        return false;
    }

    private boolean isAllowedTrade(Opportunity opportunity) {
        final String quote = CurrencyUtils.getQuote(opportunity.getPair());
        final String finalQuote = CurrencyUtils.getQuote(opportunity.getFinalPair());

        if(!config.getAllowedQuotesToTrade().contains(quote) || !config.getAllowedQuotesToTrade().contains(finalQuote)) {
            return false;
        }

        if (!tradingEnabled) {
            LOGGER.warn("Trading disabled, ignoring opportunity: " + opportunity);
            notificationService.send("Ignored opportunity: \n" + getShortDescription(opportunity));
            return false;
        }

        if(tradeStartTimestamp > 0 && tradeStartTimestamp > System.currentTimeMillis()) {
            final String message = "Cooldown period active (" + String.format("%.2f", (tradeStartTimestamp - System.currentTimeMillis())/1000f)+ "s left), ignoring opportunity: \n" + getShortDescription(opportunity);
            LOGGER.warn(message);
            notificationService.send(message);
            return false;
        }

        if(activeOrders.has(opportunity.getInitialExchange(), opportunity.getPair(), OrderType.SELL) ||
            activeOrders.has(opportunity.getFinalExchange(), opportunity.getFinalPair(), OrderType.BUY)) {
            final String message = "Trade ignored because our own maker order exists: " + getShortDescription(opportunity);
            LOGGER.warn(message);
            notificationService.send(message, NotificationService.Level.ERROR);
        }

        return true;
    }

    private void setCooldownPeriod(long durationMs) {
        this.tradeStartTimestamp = System.currentTimeMillis() + durationMs;
    }

    private Arbitrage executeTrade(Opportunity opportunity, Prediction prediction) {
        final IExchangeService initialExchangeService = exchangeServices.get(opportunity.getInitialExchange());
        final IExchangeService finalExchangeService = exchangeServices.get(opportunity.getFinalExchange());

        if (!initialExchangeService.hasSufficientFunds(CurrencyUtils.getQuote(opportunity.getPair()), opportunity.getInitialAmount())) {
            final String message = "Not enough funds on initial exchange " + opportunity.getInitialExchange() + " [ " + opportunity.getInitialAmount() + " " + CurrencyUtils.getQuote(opportunity.getPair()) + " needed ]";
            LOGGER.info(message);
            notificationService.send(message, NotificationService.Level.DEBUG);

            return null;
        }

        if (!finalExchangeService.hasSufficientFunds(CurrencyUtils.getBase(opportunity.getFinalPair()), opportunity.getTransferAmount())) {
            final String message = "Not enough funds on final exchange " + opportunity.getFinalExchange() + " [ " + opportunity.getTransferAmount() + " " + CurrencyUtils.getBase(opportunity.getFinalPair()) + " needed ]";
            LOGGER.info(message);
            notificationService.send(message, NotificationService.Level.DEBUG);

            return null;
        }

        final Arbitrage arbitrage = new Arbitrage();
        arbitrage.setPrediction(prediction);
        arbitrage.setFinalQuoteCurrency(CurrencyUtils.getQuote(opportunity.getFinalPair()));
        arbitrage.setInitialQuoteCurrency(CurrencyUtils.getQuote(opportunity.getPair()));
        arbitrage.setBaseCurrency(CurrencyUtils.getBase(opportunity.getPair()));
        arbitrage.setWithdrawStatus(Arbitrage.STATUS_WITHDRAW_PENDING);

        // these are just temporary placeholders, they will be rewritten later
        arbitrage.setInitialAmount(opportunity.getInitialAmount());
        arbitrage.setReceivedAmount(BigDecimal.ZERO);
        arbitrage.setBoughtAmount(BigDecimal.ZERO);
        arbitrage.setBuyFee(BigDecimal.ZERO);
        arbitrage.setSellFee(BigDecimal.ZERO);

        // TODO: is this correct?
        final BigDecimal amountToSell = initialExchangeService.getPredictedTransferredBaseAmountAfterBuy(opportunity);

        // TODO: maybe move to class level to reuse?
        ExecutorService tradeExecutorService = Executors.newFixedThreadPool(2);
        Future<BuyResult> buyResultFuture = tradeExecutorService.submit(new BuyAction(initialExchangeService, opportunity));
        Future<SellResult> sellResultFuture = tradeExecutorService.submit(new SellAction(finalExchangeService, opportunity, amountToSell));

        BigDecimal realAmountToSell = BigDecimal.ZERO;
        try {
            // BUY
            // TODO: add timeout
            final BuyResult buyResult = buyResultFuture.get();

            if(Arbitrage.STATUS_OK.equals(buyResult.getBuyStatus())) {
                arbitrage.setInitialAmount(buyResult.getInitialAmount());
                arbitrage.setBoughtAmount(buyResult.getBoughtAmount());
                arbitrage.setBuyFee(buyResult.getBuyFee());

                realAmountToSell = TradeUtils.getAmountToSell(buyResult);
            }
            arbitrage.setBuyStatus(buyResult.getBuyStatus());
            arbitrage.setBuyFeeCurrency(buyResult.getBuyFeeCurrency());
            arbitrage.setWithdrawFee(buyResult.getWithdrawFee());
            arbitrage.setBuyOrderId(buyResult.getOrderId());
            arbitrage.setBuyOrderIdAdditional(buyResult.getAdditionalOrderId());
        } catch (ExecutionException | InterruptedException e) {
            arbitrage.setBuyStatus(Arbitrage.STATUS_FAIL);
            LOGGER.error("Error during arbitrage buy execution: " + e.getMessage(), e);
            notificationService.send("ERROR: Error while executing arbitrage (buy): " + e);
        } finally {
            try {
                if (Arbitrage.STATUS_FAIL.equals(arbitrage.getBuyStatus())) {
                    notificationService.send("ERROR: Unable to buy funds for arbitrage: \n" + getShortDescription(arbitrage));
                }
            } catch (Exception e) {
                notificationService.send("ERROR: " + e.getMessage());
                LOGGER.error(e.toString());
            }
        }

        try {
            // SELL
            // TODO: add timeout
            final SellResult sellResult = sellResultFuture.get();

            if(Arbitrage.STATUS_OK.equals(sellResult.getStatus())) {
                arbitrage.setReceivedAmount(sellResult.getReceivedAmount());
                arbitrage.setSellFee(sellResult.getSellFee());
                arbitrage.setSellSaldoAmount(realAmountToSell.subtract(sellResult.getSoldAmount()));
            }

            arbitrage.setSellStatus(sellResult.getStatus());
            arbitrage.setSoldAmount(sellResult.getSoldAmount());
            arbitrage.setSellFeeCurrency(sellResult.getSellFeeCurrency());
            arbitrage.setSellOrderId(sellResult.getOrderId());
            arbitrage.setWithdrawStatus(Arbitrage.STATUS_WITHDRAW_PENDING);
        } catch (ExecutionException | InterruptedException e) {
            arbitrage.setSellStatus(Arbitrage.STATUS_FAIL);
            LOGGER.error("Error during arbitrage sell execution: " + e.getMessage(), e);
            notificationService.send("ERROR: Error while executing arbitrage (sell): " + e);
        } finally {
            try {
                if (Arbitrage.STATUS_FAIL.equals(arbitrage.getSellStatus())) {
                    notificationService.send("ERROR: Unable to execute sell for arbitrage: \n " + getShortDescription(arbitrage));
                }
            } catch (Exception e) {
                notificationService.send("ERROR: " + e.getMessage());
                LOGGER.error(e.toString());
            }
        }

        return arbitrage;
    }

    public static double calculateGain(Opportunity opportunity) {
        return opportunity.getFinalAmount().doubleValue() - opportunity.getInitialAmount().doubleValue() - opportunity.getBuyFee().doubleValue();
    }

    public static double calculateGainPercent(Opportunity opportunity) {
        return calculateGain(opportunity) / opportunity.getInitialAmount().doubleValue();
    }

    private static double calculateGainMultiplier(Opportunity opportunity) {
        return opportunity.getFinalAmount().doubleValue() / (opportunity.getInitialAmount().doubleValue() + opportunity.getBuyFee().doubleValue());
    }

    private String getPredictionKey(Opportunity opportunity) {
        return opportunity.getPair() + opportunity.getInitialExchange().toString() + opportunity.getFinalExchange().toString() + opportunity.getInitialAmount() + opportunity.getFlags();
    }

    public void setThreshold(BigDecimal threshold) {
        this.executeThreshold = threshold;
    }

    public void setExecuteWithFundingThreshold(BigDecimal threshold) {
        this.executeWithFundingThreshold = threshold;
    }

    public void setExecuteGeneralFundingThreshold(BigDecimal threshold) {
        this.executeFundingThreshold = threshold;
    }

    public void setTradingEnabled(boolean tradingEnabled) {
        this.tradingEnabled = tradingEnabled;
    }

    public String getStatusString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Threshold: ").append(executeThreshold).append("\n");
        sb.append("With funding threshold: ").append(executeWithFundingThreshold).append("\n");
        sb.append("General funding threshold: ").append(executeFundingThreshold).append("\n");
        sb.append("Trade enabled: ").append(tradingEnabled).append("\n");

        return sb.toString();
    }

    private String getShortDescription(Arbitrage arbitrage) {
        final StringBuilder sb = new StringBuilder();
        sb.append(Arbitrage.TYPE_PROFIT.equals(arbitrage.getType()) ? "Arbitrage\n" : "Funding\n");
        sb.append(arbitrage.getPrediction().getPair()).append(" -> ").append(arbitrage.getPrediction().getFinalPair()).append("\n");
        sb.append(arbitrage.getPrediction().getInitialExchange()).append(" -> ").append(arbitrage.getPrediction().getFinalExchange()).append("\n");
        sb.append("Initial amount: ").append(String.format("%.2f", TradeUtils.getInitialExpenses(arbitrage))).append("\n");

        final BigDecimal finalAmount = TradeUtils.getFinalAmount(arbitrage, false);
        final BigDecimal sellSaldo = arbitrage.getSellSaldoAmount();

        String saldoStr = "";
        if (BigDecimal.ZERO.compareTo(sellSaldo) != 0 && arbitrage.getBoughtAmount() != null) {
            BigDecimal saldo = sellSaldo.multiply(arbitrage.getInitialAmount().divide(arbitrage.getBoughtAmount(), 8, RoundingMode.HALF_UP));
            saldoStr = String.format(" (+ %.2f ≈ [%.2f])", saldo, finalAmount.add(saldo));
        }

        sb.append("Final amount: ").append(String.format("%.2f", finalAmount)).append(saldoStr).append("\n");
        sb.append("Gain of last ").append(getLastGainsCount()).append(" trades: ").append(getLastGainsSum()).append("\n");
        sb.append("ID: ").append(arbitrage.getId());

        return sb.toString();
    }


    private String getShortDescription(Opportunity opportunity) {
        final StringBuilder sb = new StringBuilder();
        sb.append(opportunity.getPair()).append(" -> ").append(opportunity.getFinalPair()).append("\n");
        sb.append(opportunity.getInitialExchange()).append(" -> ").append(opportunity.getFinalExchange()).append("\n");
        sb.append("Initial amount: ").append(String.format("%.2f", opportunity.getInitialAmount())).append("\n");
        sb.append("Final amount: ").append(String.format("%.2f", opportunity.getFinalAmount())).append("\n");

        return sb.toString();
    }

    private void recordGain(float gain) {
        if (((ArrayBlockingQueue) lastGains).remainingCapacity() == 0) {
            lastGains.poll();
        }

        lastGains.offer(gain);
    }


    private float getLastGainsSum() {
        float sum = 0;

        for (Float lastGain : lastGains) {
            sum += lastGain;
        }

        return sum;
    }

    private float getLastGainsCount() {
        return lastGains.size();
    }

    public String getLastGains() {
        return lastGains.toString();
    }

    static class BuyAction implements Callable<BuyResult> {
        private final IExchangeService exchangeService;
        private final Opportunity opportunity;

        public BuyAction(IExchangeService exchangeService, Opportunity opportunity) {
            this.exchangeService = exchangeService;
            this.opportunity = opportunity;
        }

        @Override
        public BuyResult call() throws Exception {
            return exchangeService.buy(opportunity.getPair(), opportunity.getInitialAmount(), true);
        }
    }

    static class SellAction implements Callable<SellResult> {
        private final IExchangeService exchangeService;
        private final Opportunity opportunity;
        private final BigDecimal amountToSell;

        public SellAction(IExchangeService exchangeService, Opportunity opportunity, BigDecimal amountToSell) {
            this.exchangeService = exchangeService;
            this.opportunity = opportunity;
            this.amountToSell = amountToSell;
        }
        @Override
        public SellResult call() throws Exception {
            return exchangeService.sell(opportunity.getFinalPair(), amountToSell);
        }
    }
}
