package com.tkelemen.arbitrader.service.arbitrage.withdraw;

import com.tkelemen.arbitrader.model.Address;
import com.tkelemen.arbitrader.model.WithdrawResult;
import com.tkelemen.arbitrader.service.trade.BinanceExchangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class BinanceWithdrawExecutorService implements WithdrawExecutor {
    private BinanceExchangeService exchangeService;

    @Autowired
    public BinanceWithdrawExecutorService(BinanceExchangeService binanceExchangeService) {
        this.exchangeService = binanceExchangeService;
    }

    @Override
    public WithdrawResult initiateWithdraw(String asset, BigDecimal amount, Address destination) {
        return exchangeService.withdraw(asset, amount, destination, getAddressName(asset, destination));
    }

    @Override
    public WithdrawResult confirmWithdraw(String asset, BigDecimal amount, Address destination, String confirmUrl) {
        // TODO: not applicable for Binance

        return null;
    }

    private String getAddressName(String asset, Address destination) {
        String prefix = destination.getExchange() != null ?
                destination.getExchange().name() : destination.getAddress().substring(destination.getAddress().length() - 4);
        return prefix + "_" + asset;
    }
}
