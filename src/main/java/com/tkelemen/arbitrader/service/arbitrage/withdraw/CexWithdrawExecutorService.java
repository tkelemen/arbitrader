package com.tkelemen.arbitrader.service.arbitrage.withdraw;

import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.db.entity.Withdraw;
import com.tkelemen.arbitrader.db.repository.WithdrawRepository;
import com.tkelemen.arbitrader.exception.AddressNotFoundException;
import com.tkelemen.arbitrader.model.*;
import com.tkelemen.arbitrader.service.market.WalletService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class CexWithdrawExecutorService implements WithdrawExecutor {
    private static Logger logger = LoggerFactory.getLogger(CexWithdrawExecutorService.class);

    private ApplicationConfig config;

    private WithdrawRepository withdrawRepository;

    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    private WalletService walletService;

    @Autowired
    public CexWithdrawExecutorService(ApplicationConfig config, WithdrawRepository withdrawRepository, WalletService walletService) {
        this.config = config;
        this.withdrawRepository = withdrawRepository;
        this.walletService = walletService;
    }

    @Override
    public WithdrawResult initiateWithdraw(String asset, BigDecimal amount, Address destination) {
        logger.info("Initiating withdraw from CEX [ asset = " + asset + ", amount = " + amount + ", destination = " + destination);

        WithdrawResult withdrawResult = new WithdrawResult(WithdrawStage.REQUESTED);

        try {
            String withdrawNetworkId = walletService.getAddress(Exchange.CEX, asset).getNetwork();

            executorService.submit(new CexWithdrawInitSeleniumExecutor(config, destination, asset, amount, withdrawNetworkId)).get();
            withdrawResult.setSuccess(true);
        } catch (InterruptedException | AddressNotFoundException e) {
            e.printStackTrace();
            logger.error(e.toString());
            withdrawResult.setSuccess(false);
            withdrawResult.setMessage(e.getMessage());
        } catch (ExecutionException e) {
            e.printStackTrace();
            logger.error(e.toString());
            withdrawResult.setSuccess(false);
            withdrawResult.setMessage(e.getCause().getMessage());
        }

        logger.info("Withdraw result: " + withdrawResult);

        return withdrawResult;
    }

    @Override
    public WithdrawResult confirmWithdraw(String asset, BigDecimal amount, Address destination, String confirmUrl) {
        logger.info("Confirming withdraw from CEX [ asset = " + asset + ", amount = " + amount + ", destination = " + destination);

        WithdrawResult withdrawResult = new WithdrawResult(WithdrawStage.CONFIRMED);

        Withdraw withdraw = withdrawRepository.find(asset, Withdraw.STATUS_REQUESTED, "CEX", destination.getAddress(), amount);

        if (withdraw == null) {
            logger.error("Withdraw to confirm not found [ exchange = CEX, asset = " + asset + ", amount = " + amount + ", destination = " + destination);
            withdrawResult.setSuccess(false);

            return withdrawResult;
        }

        try {
            SeleniumResult seleniumResult = executorService.submit(new CexWithdrawConfirmSeleniumExecutor(config, confirmUrl)).get();

            if (SeleniumResult.SUCCESS.equals(seleniumResult)) {
                withdraw.setStatus(Withdraw.STATUS_CONFIRMED);
                withdrawResult.setSuccess(true);
            } else {
                withdraw.setStatus(Withdraw.STATUS_ERROR);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
            logger.error(e.toString());
            withdrawResult.setSuccess(false);
            withdrawResult.setMessage(e.getMessage());
        } catch (ExecutionException e) {
            e.printStackTrace();
            logger.error(e.toString());
            withdrawResult.setSuccess(false);
            withdrawResult.setMessage(e.getCause().getMessage());
        }

        withdrawRepository.save(withdraw);

        withdrawResult.setWithdraw(withdraw);

        logger.info("Withdraw result: " + withdrawResult);

        return withdrawResult;
    }
}
