package com.tkelemen.arbitrader.service.arbitrage;

import com.tkelemen.arbitrader.model.Exchange;
import com.tkelemen.arbitrader.model.MakerOpportunityDTO;
import com.tkelemen.arbitrader.service.arbitrage.maker.MakerArbitrageExecutorService;
import com.tkelemen.arbitrader.service.arbitrage.maker.MakerArbitrageFinderService;
import com.tkelemen.arbitrader.service.arbitrage.taker.PredictionService;
import com.tkelemen.arbitrader.service.arbitrage.taker.TradeService;
import com.tkelemen.arbitrader.service.market.MarketDataService;
import com.tkelemen.arbitrader.service.market.OrderBookService;
import com.tkelemen.arbitrader.service.notification.NotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ArbitrageManagerService {
    private static final Logger logger = LoggerFactory.getLogger(ArbitrageManagerService.class);

    private final MarketDataService marketDataService;
    private final NotificationService notificationService;
    private final PredictionService predictionService;
    private final TradeService tradeService;
    private final MakerArbitrageFinderService makerArbitrageFinderService;
    private final MakerArbitrageExecutorService makerArbitrageExecutorService;

    @Autowired
    public ArbitrageManagerService(NotificationService notificationService,
                                   PredictionService predictionService,
                                   TradeService tradeService,
                                   MarketDataService marketDataService,
                                   MakerArbitrageFinderService makerArbitrageFinderService,
                                   MakerArbitrageExecutorService makerArbitrageExecutorService) {
        this.notificationService = notificationService;
        this.predictionService = predictionService;
        this.tradeService = tradeService;
        this.marketDataService = marketDataService;
        this.makerArbitrageFinderService = makerArbitrageFinderService;
        this.makerArbitrageExecutorService = makerArbitrageExecutorService;
    }

    public void initNotifications() {
        notificationService.subscribeForCommands(this);
    }

    public void subscribeForMarketData() {
        marketDataService.subscribeForMarketData();
    }

    public void initTrading() {
        tradeService.startProcessing();
    }

    public String getStatusString() {
        return marketDataService.getStatusString() + tradeService.getStatusString() + getMakerStatusString();
    }

    public void setThreshold(BigDecimal threshold) {
        tradeService.setThreshold(threshold);
    }

    public void setWithFundingThreshold(BigDecimal threshold) {
        tradeService.setExecuteWithFundingThreshold(threshold);
    }

    public void setGeneralFundingThreshold(BigDecimal threshold) {
        tradeService.setExecuteGeneralFundingThreshold(threshold);
    }

    public void setTradingEnabled(boolean tradingEnabled) {
        tradeService.setTradingEnabled(tradingEnabled);
    }

    public void syncAccountBalances() {
        tradeService.syncAccountBalances();
    }

    public void syncOrderBooks() {
        marketDataService.syncOrderBooks();
    }

    public void executeProfitableTrades() {
        tradeService.processOpportunities(predictionService.getPredictions());
    }

    public String getLastPredictionsString() {
        return predictionService.getLastPredictionsString();
    }

    public void reconnect() {
        logger.info("Reconnecting order book providers");

        this.subscribeForMarketData();
        this.makerArbitrageFinderService.fetchTradeVolumes();
    }

    public void statusCheck() {
        List<String> disconnectedList = marketDataService
                .getDisconnectedOrderBookProviders().stream().map(Enum::name).collect(Collectors.toList());

        if (!disconnectedList.isEmpty()) {
            notificationService.send("Warning: some providers disconnected [ " + String.join(", ", disconnectedList) + " ]");
        }

        for (Map.Entry<Exchange, OrderBookService> entry : marketDataService.getOrderBookServices().entrySet()) {
            Set<String> desiredSubscriptions = entry.getValue().getDesiredSubscriptions();
            desiredSubscriptions.removeAll(entry.getValue().getActiveSubscriptions());

            if(desiredSubscriptions.size() > 0) {
                notificationService.send("Warning: missing " + entry.getValue() + " subscriptions: " + String.join(",", desiredSubscriptions), NotificationService.Level.DEBUG);
            }
        }
    }

    public String getLastFundingPredictionsString() {
        return predictionService.getLastFundingPredictionsString();
    }

    public String getActiveSubscriptionsString() {
        StringBuilder sb = new StringBuilder();
        for(Map.Entry<Exchange, OrderBookService> entry : marketDataService.getOrderBookServices().entrySet()) {
            sb.append("--- ").append(entry.getValue()).append(": ").append(String.join(",", entry.getValue().getActiveSubscriptions())).append("\n");
        }

        return sb.toString();
    }

    public void processMakerOpportunities() {
        NavigableMap<Double, MakerOpportunityDTO> opportunities = makerArbitrageFinderService.getCurrentOpportunities();

        makerArbitrageExecutorService.processOpportunities(opportunities);
    }

    public void stopMakerTrading() {
        makerArbitrageExecutorService.stopMakerTrading();
    }

    public String getMakerStatusString() {
        return makerArbitrageFinderService.getStatusString();
    }

    public String getActiveMakerTriggerOrdersString() {
        return makerArbitrageExecutorService.getStatusString();
    }

    public void setMassMakerExecutionEnabled(boolean massMakerExecutionEnabled) {
        makerArbitrageExecutorService.setMassExecutionEnabled(massMakerExecutionEnabled);
    }
}
