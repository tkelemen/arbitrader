package com.tkelemen.arbitrader.service.arbitrage.taker;

import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.model.Exchange;
import com.tkelemen.arbitrader.model.Opportunity;
import com.tkelemen.arbitrader.model.OrderBook;
import com.tkelemen.arbitrader.service.market.*;
import com.tkelemen.arbitrader.util.CurrencyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
public class PredictionService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PredictionService.class);
    private static final BigDecimal PREDICTION_WATCH_THRESHOLD = BigDecimal.ONE;

    private final FeeService feeService;
    private final MarketDataService marketDataService;
    private final BigDecimal minWatchThreshold;
    private BigDecimal watchThreshold;
    private final Map<String, NavigableSet<BigDecimal>> initialQtyMap;
    private float lastIterationMaxGain = 1;
    private Map<String, List<String>> actualPredictionsString = new HashMap<>();
    private Map<String, List<String>> lastPredictionsString = new HashMap<>();
    private Map<String, List<String>> actualFundingPredictionsString = new HashMap<>();
    private Map<String, List<String>> lastFundingPredictionsString = new HashMap<>();
    private final Set<Exchange> internalArbitrageAllowedExchanges;
    private final Set<String> internalArbitrageAllowedPairs;

    @Autowired
    public PredictionService(FeeService feeService, ApplicationConfig config, MarketDataService marketDataService) {
        this.feeService = feeService;
        this.marketDataService = marketDataService;
        this.minWatchThreshold = config.getMinWatchThreshold();
        this.initialQtyMap = config.getAllowedQuoteVolumes();
        this.internalArbitrageAllowedExchanges = config.getInternalArbitrageAllowedExchanges();
        this.internalArbitrageAllowedPairs = config.getInternalArbitrageAllowedPairs();
        this.watchThreshold = PREDICTION_WATCH_THRESHOLD;
    }

    public List<Opportunity> getPredictions() {
        final long timeStart = System.currentTimeMillis();

        final Map<Exchange, Map<String, OrderBook>> orderBooks = new HashMap<>();
        final Set<String> initialPairs = new HashSet<>();

        marketDataService.getOrderBookServices().forEach((exchange, orderBookService) -> {
            orderBooks.put(exchange, orderBookService.getOrderBookSnapshots());
            initialPairs.addAll(orderBookService.getOrderBookSnapshots().keySet());
        });

        // TODO: move to class level
        final Map<String, Set<String>> allPairs = new HashMap<>();
        for (String initialPair : initialPairs) {
            allPairs.put(initialPair, CurrencyUtils.getFinalPairs(initialPair));
        }

        final List<Opportunity> opportunities = new ArrayList<>();

        LOGGER.info("Looking for profitable trades (threshold: " + watchThreshold.toPlainString() + ") ...");
        actualPredictionsString = new HashMap<>();
        actualFundingPredictionsString = new HashMap<>();

        lastIterationMaxGain = 1;

        for (Map.Entry<Exchange, Map<String, OrderBook>> initialExchangeBooks : orderBooks.entrySet()) {
            for (Map.Entry<Exchange, Map<String, OrderBook>> finalExchangeBooks : orderBooks.entrySet()) {
                final boolean isInternalTrade = initialExchangeBooks.getKey().equals(finalExchangeBooks.getKey());

                if(isInternalTrade && !internalArbitrageAllowedExchanges.contains(initialExchangeBooks.getKey())) {
                    continue;
                }

                for (String initialPair : allPairs.keySet()) {
                    final Set<String> finalPairs = allPairs.get(initialPair);

                    for (String finalPair : finalPairs) {
                        if(isInternalTrade && (!internalArbitrageAllowedPairs.contains(initialPair) ||
                                !internalArbitrageAllowedPairs.contains(finalPair))) {
                            continue;
                        }

                        if (initialExchangeBooks.getValue().containsKey(initialPair) || finalExchangeBooks.getValue().containsKey(finalPair)) {
                            final OrderBook initialBook = initialExchangeBooks.getValue().get(initialPair);
                            final OrderBook finalBook = finalExchangeBooks.getValue().get(finalPair);

                            if (initialBook == null || finalBook == null) {
                                continue;
                            }

                            for (BigDecimal initialQty : initialQtyMap.get(CurrencyUtils.getQuote(initialPair))) {
                                findTransferOpportunities(opportunities, initialBook, finalBook, initialQty);
                            }
                        }
                    }
                }
            }
        }

        lastPredictionsString = actualPredictionsString;
        lastFundingPredictionsString = actualFundingPredictionsString;

        watchThreshold = new BigDecimal(1 / lastIterationMaxGain);

        if (watchThreshold.compareTo(minWatchThreshold) > 0) {
            watchThreshold = minWatchThreshold;
        }

        if (opportunities.isEmpty()) {
            LOGGER.info("No opportunity found");
        } else {
            LOGGER.info("Found " + opportunities.size() + " opportunities");
        }

        opportunities.sort(new OpportunityGainComparator());

        LOGGER.info("Took: " + (System.currentTimeMillis() - timeStart) + " ms");

        return opportunities;
    }

    private void findTransferOpportunities(List<Opportunity> opportunities, OrderBook initialBook, OrderBook finalBook, BigDecimal initialQty) {
        final BigDecimal buyFeeMultiplier = feeService.getTradingFeeTaker(initialBook.getExchange());
        final BigDecimal sellFeeMultiplier = feeService.getTradingFeeTaker(finalBook.getExchange());

        final String initialPair = initialBook.getSymbol();
        final String finalPair = finalBook.getSymbol();

        BigDecimal withdrawFee = BigDecimal.ZERO;
        BigDecimal depositFee = BigDecimal.ZERO;

        if(initialBook.getExchange() != finalBook.getExchange()) {
            withdrawFee = feeService.getWithdrawFee(initialBook.getExchange(), CurrencyUtils.getBase(initialPair));
            depositFee = feeService.getDepositFee(finalBook.getExchange(), CurrencyUtils.getBase(finalPair));
        }

        final BigDecimal transferQty = OrderBookHelper.calculateBaseQuantity(initialBook, initialQty)
                .subtract(withdrawFee)
                .subtract(depositFee);

        final BigDecimal sellQty = OrderBookHelper.calculateQuoteQuantity(finalBook, transferQty);

        final BigDecimal buyFee = initialQty.multiply(buyFeeMultiplier);
        final BigDecimal sellFee = sellQty.multiply(sellFeeMultiplier);

        final BigDecimal initialExpenses = initialQty.add(buyFee);
        final BigDecimal finalQty = sellQty.subtract(sellFee);

        if (initialExpenses.multiply(watchThreshold).compareTo(finalQty) < 0) {
            final float gain = finalQty.floatValue() / initialExpenses.floatValue();
            final long now = System.currentTimeMillis();

            if(initialBook.getAskLevels().navigableKeySet().isEmpty() ||
                    finalBook.getBidLevels().navigableKeySet().isEmpty()) {
                return;
            }

            final String bestAsk = initialBook.getAskLevels().navigableKeySet().first();
            final String bestBid = finalBook.getBidLevels().descendingKeySet().first();
            final BigDecimal bestAskQty = initialBook.getAskLevelQuantity(bestAsk);
            final BigDecimal bestBidQty = finalBook.getBidLevelQuantity(bestBid);

            final long bestAskAge = now - initialBook.getAskLevelTime(bestAsk);
            final long bestBidAge = now - finalBook.getBidLevelTime(bestBid);
            final long initialOrderBookAge = now - initialBook.getLastUpdatedTimestamp();
            final long finalOrderBookAge = now - finalBook.getLastUpdatedTimestamp();

            final Opportunity opportunity = new Opportunity();
            opportunity.setPair(initialPair);
            opportunity.setFinalPair(finalPair);
            opportunity.setInitialExchange(initialBook.getExchange());
            opportunity.setInitialAmount(initialQty);
            opportunity.setBuyFee(buyFee);
            opportunity.setTransferAmount(transferQty);
            opportunity.setWithdrawFee(withdrawFee);
            opportunity.setDepositFee(depositFee);
            opportunity.setFinalAmount(finalQty);
            opportunity.setSellFee(sellFee);
            opportunity.setFinalExchange(finalBook.getExchange());
            opportunity.setInitialOrderBook(initialBook);
            opportunity.setFinalOrderBook(finalBook);

            opportunities.add(opportunity);

            final String message = initialPair +
                    (!initialPair.equals(finalPair) ? "->" + finalPair : "") +
                    " [ " + initialBook.getExchange() + " " + initialQty + " ] " +
                    "(+" + String.format("%.2f", buyFee) + ") " +
                    "(" + bestAskQty + "@" + bestAsk + ", " + bestAskAge + "ms ago | " + initialOrderBookAge + ")" +
                    " -> " + String.format("%.8f", transferQty.add(withdrawFee).add(depositFee)) +
                    " (-" + String.format("%.2f", withdrawFee) + ", -" + String.format("%.2f", depositFee) + ")" +
                    " -> " + String.format("%.2f", finalQty) +
                    " ( incl. " + String.format("%.2f", sellFee) + ") " + finalBook.getExchange() +
                    " (" + bestBidQty + "@" + bestBid + ", " + bestBidAge + "ms ago | " + finalOrderBookAge + ") " +
                    ": " + String.format("%.4f", ((gain) - 1) * 100) + "%";

            final String messageShort = initialPair +
                    (!initialPair.equals(finalPair) ? "->" + finalPair : "") +
                    " [ " + initialBook.getExchange() + " " + initialExpenses + " ] " +
                    " -> " + String.format("%.2f", finalQty) + " " + finalBook.getExchange() +
                    ": " + String.format("%.4f", ((finalQty.floatValue() / initialExpenses.floatValue()) - 1) * 100) + "%";

            final String key = initialPair + initialBook.getExchange() + finalBook.getExchange();

            if (gain > 1) {
                LOGGER.info(message);

                final List<String> predictions = actualPredictionsString.computeIfAbsent(key, a -> new ArrayList<>());
                predictions.add(messageShort);

                if (gain > lastIterationMaxGain) {
                    lastIterationMaxGain = gain;
                }
            } else {
                final List<String> predictions = actualFundingPredictionsString.computeIfAbsent(key, a -> new ArrayList<>());
                predictions.add(messageShort);
            }
        }
    }

    public static Map<String, List<Opportunity>> getReverseFundingOpportunities(List<Opportunity> opportunities) {
        final Map<String, List<Opportunity>> reverseFundingOpportunities = new HashMap<>();

        for (Opportunity opportunity : opportunities) {
            if (!opportunity.getFlags().contains(Opportunity.FLAG_INSUFFICIENT_QUOTE_FUNDS) &&
                    !opportunity.getFlags().contains(Opportunity.FLAG_INSUFFICIENT_BASE_FUNDS)) {
                final List<Opportunity> reverseOpportunities = reverseFundingOpportunities
                        .computeIfAbsent(getReverseFundingOpportunityKey(opportunity), a -> new ArrayList<>());
                reverseOpportunities.add(opportunity);
            }
        }

        // TODO: optimize
        for (Map.Entry<String, List<Opportunity>> entry : reverseFundingOpportunities.entrySet()) {
            final List<Opportunity> opportunityList = entry.getValue();
            opportunityList.sort(new OpportunityGainComparator());

            reverseFundingOpportunities.put(entry.getKey(), opportunityList);
        }

        return reverseFundingOpportunities;
    }


    private static String getReverseFundingOpportunityKey(Opportunity opportunity) {
        return opportunity.getFinalExchange().name() + CurrencyUtils.getQuote(opportunity.getFinalPair());
    }

    public static String getFundingOpportunityKey(Opportunity opportunity) {
        return opportunity.getInitialExchange().name() + CurrencyUtils.getQuote(opportunity.getPair());
    }

    public String getLastPredictionsString() {
        StringBuilder sb = new StringBuilder();

        for (List<String> entry : lastPredictionsString.values()) {
            if (entry.isEmpty()) {
                continue;
            }

            sb.append(entry.get(0)).append("\n");

            int size = entry.size();
            if (size > 1) {
                sb.append(entry.get(size - 1)).append("\n");
            }
        }

        return sb.toString();
    }

    public String getLastFundingPredictionsString() {
        StringBuilder sb = new StringBuilder();

        for (List<String> entry : lastFundingPredictionsString.values()) {
            if (entry.isEmpty()) {
                continue;
            }

            sb.append(entry.get(0)).append("\n");

            int size = entry.size();
            if (size > 1) {
                sb.append(entry.get(size - 1)).append("\n");
            }
        }

        return sb.toString();
    }

    public static float calculateOpportunityProfitMultiplier(Opportunity opportunity, FeeService feeService) {
        // TODO: this method looks almost the same as the first part of findTransferOpportunities, optimize them

        final OrderBook initialBook = opportunity.getInitialOrderBook();
        final OrderBook finalBook = opportunity.getFinalOrderBook();

        final BigDecimal buyFeeMultiplier = feeService.getTradingFeeTaker(initialBook.getExchange());
        final BigDecimal sellFeeMultiplier = feeService.getTradingFeeTaker(finalBook.getExchange());

        final String initialPair = initialBook.getSymbol();
        final String finalPair = finalBook.getSymbol();

        BigDecimal withdrawFee = BigDecimal.ZERO;
        BigDecimal depositFee = BigDecimal.ZERO;

        if(initialBook.getExchange() != finalBook.getExchange()) {
            withdrawFee = feeService.getWithdrawFee(initialBook.getExchange(), CurrencyUtils.getBase(initialPair));
            depositFee = feeService.getDepositFee(finalBook.getExchange(), CurrencyUtils.getBase(finalPair));
        }

        final BigDecimal transferQty = OrderBookHelper.calculateBaseQuantity(initialBook, opportunity.getInitialAmount())
            .subtract(withdrawFee)
            .subtract(depositFee);

        final BigDecimal sellQty = OrderBookHelper.calculateQuoteQuantity(finalBook, transferQty);

        final BigDecimal buyFee = opportunity.getInitialAmount().multiply(buyFeeMultiplier);
        final BigDecimal sellFee = sellQty.multiply(sellFeeMultiplier);

        final BigDecimal initialExpenses = opportunity.getInitialAmount().add(buyFee);
        final BigDecimal finalQty = sellQty.subtract(sellFee);

        return finalQty.floatValue() / initialExpenses.floatValue();
    }

    private static class OpportunityGainComparator implements Comparator<Opportunity> {
        @Override
        public int compare(Opportunity o1, Opportunity o2) {
            final double diff = TradeService.calculateGain(o2) - TradeService.calculateGain(o1);

            return diff == 0 ? 0 : (diff > 0 ? 1 : -1);
        }
    }
}
