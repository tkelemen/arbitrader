package com.tkelemen.arbitrader.service.arbitrage.funding;

import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.exception.FundingException;
import com.tkelemen.arbitrader.model.Exchange;
import com.tkelemen.arbitrader.service.trade.ExchangeManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class FundingServiceImpl implements FundingService {
    private Map<Exchange, Map<String, BigDecimal>> fundingAmounts = new ConcurrentHashMap<>();
    private Map<Exchange, Map<String, BigDecimal>> exchangeFundingLimits = new ConcurrentHashMap<>();

    private ApplicationConfig applicationConfig;
    private ExchangeManagerService exchangeManagerService;

    @Autowired
    public FundingServiceImpl(ApplicationConfig applicationConfig, ExchangeManagerService exchangeManagerService) {
        this.applicationConfig = applicationConfig;
        this.exchangeManagerService = exchangeManagerService;
    }

    @Override
    public void setExchangeFundingLimit(Exchange exchange, String currency, BigDecimal amount) {
        Map<String, BigDecimal> limitsForExchange = exchangeFundingLimits.get(exchange);

        if (limitsForExchange == null) {
            limitsForExchange = new HashMap<>();
        }


        BigDecimal limitForCurrency = limitsForExchange.get(currency);

        if (limitForCurrency == null) {
            limitForCurrency = amount;
        } else {
            limitForCurrency = limitForCurrency.add(amount);
        }

        limitsForExchange.put(currency, limitForCurrency);
        exchangeFundingLimits.put(exchange, limitsForExchange);
    }

    @Override
    public BigDecimal getCurrentFunding(Exchange exchange, String currency) {
        Map<String, BigDecimal> fundingsForExchange = fundingAmounts.get(exchange);

        if (fundingsForExchange == null) {
            return BigDecimal.ZERO;
        }

        BigDecimal fundingForCurrency = fundingsForExchange.get(currency);

        if (fundingForCurrency == null) {
            return BigDecimal.ZERO;
        }

        return fundingForCurrency;
    }

    @Override
    public BigDecimal getFundingLimit(Exchange exchange, String currency) {
        Map<String, BigDecimal> limitsForExchange = exchangeFundingLimits.get(exchange);

        if (limitsForExchange == null) {
            return BigDecimal.ZERO;
        }

        BigDecimal limitsForCurrency = limitsForExchange.get(currency);

        if (limitsForCurrency == null) {
            return BigDecimal.ZERO;
        }

        return limitsForCurrency;
    }

    @Override
    public boolean canBeFunded(Exchange exchange, String currency, BigDecimal amount) {
        BigDecimal currentFunding = getCurrentFunding(exchange, currency);
        BigDecimal fundingLimit = getFundingLimit(exchange, currency);

        return currentFunding.add(amount).compareTo(fundingLimit) <= 0;
    }

    @Override
    public synchronized void fund(Exchange exchange, String currency, BigDecimal amount) throws FundingException {
        if (!canBeFunded(exchange, currency, amount)) {
            throw new FundingException("Funding exchange " + exchange + " with amount " + amount + " " + currency + " would exceed the funding limit");
        }

        Map<String, BigDecimal> fundingsForExchange = fundingAmounts.get(exchange);

        if (fundingsForExchange == null) {
            fundingsForExchange = new HashMap<>();
        }

        BigDecimal fundingForCurrency = fundingsForExchange.get(currency);

        if (fundingForCurrency == null) {
            fundingForCurrency = amount;
        } else {
            fundingForCurrency = fundingForCurrency.add(amount);
        }

        fundingsForExchange.put(currency, fundingForCurrency);
        fundingAmounts.put(exchange, fundingsForExchange);
    }

    @Override
    public synchronized void repay(Exchange exchange, String currency, BigDecimal amount) throws FundingException {
        Map<String, BigDecimal> fundingsForExchange = fundingAmounts.get(exchange);

        if (fundingsForExchange == null) {
            throw new FundingException("Could not repay funding for exchange " + exchange +
                    ", none was found [ currency = " + currency + ", amount = " + amount + " ]");
        }

        BigDecimal fundingForCurrency = fundingsForExchange.get(currency);

        if (fundingForCurrency == null) {
            throw new FundingException("Could not repay funding for exchange " + exchange +
                    " and currency currency, none was found [ amount = " + amount + " ]");
        } else {
            fundingForCurrency = fundingForCurrency.subtract(amount);

            if (fundingForCurrency.compareTo(BigDecimal.ZERO) < 0) {
                fundingForCurrency = BigDecimal.ZERO;
            }
        }

        fundingsForExchange.put(currency, fundingForCurrency);
        fundingAmounts.put(exchange, fundingsForExchange);
    }
}
