package com.tkelemen.arbitrader.service.arbitrage.withdraw;

import com.tkelemen.arbitrader.exception.WithdrawException;
import com.tkelemen.arbitrader.model.Address;
import com.tkelemen.arbitrader.model.WithdrawResult;

import java.math.BigDecimal;

public interface WithdrawExecutor {
    WithdrawResult initiateWithdraw(String asset, BigDecimal amount, Address destination) throws WithdrawException;

    WithdrawResult confirmWithdraw(String asset, BigDecimal amount, Address destination, String confirmUrl);
}
