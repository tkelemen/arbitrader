package com.tkelemen.arbitrader.service.arbitrage.withdraw;

import com.eatthepath.otp.TimeBasedOneTimePasswordGenerator;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.exception.SeleniumExecutorException;
import com.tkelemen.arbitrader.model.Address;
import org.apache.commons.codec.binary.Base32;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.SecretKey;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;

public class CexWithdrawInitSeleniumExecutor extends CexSeleniumExecutor {
    private static Logger logger = LoggerFactory.getLogger(CexWithdrawInitSeleniumExecutor.class);

    private ApplicationConfig config;
    private Address destination;
    private String asset;
    private BigDecimal amount;
    private String network;

    CexWithdrawInitSeleniumExecutor(ApplicationConfig config, Address destination, String asset, BigDecimal amount, String network) {
        this.config = config;
        this.destination = destination;
        this.asset = asset;
        this.amount = amount;
        this.network = network;
    }

    @Override
    public void executeSteps() throws SeleniumExecutorException {
        WebDriverWait wait = new WebDriverWait(driver, 10);

        String url = "https://cex.io/trade/withdrawal#/currency/" + asset + "/instrument_id/" + network + "?back_to=%2Ffinance";

        logger.info("Opening URL " + url);
        driver.get(url);

        if (destination.hasTag()) {
            WebElement addressField = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("input[name=destination]")));
            addressField.sendKeys(destination.getAddress());

            try {
                WebElement tagField = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("input[name=destinationTag]")));
                tagField.sendKeys(destination.getTag().toString());
            } catch (TimeoutException e) {
                WebElement memoField = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("input[name=memo]")));
                memoField.sendKeys(destination.getTag().toString());
            }
        } else {
            WebElement addressField = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("input[name=address]")));
            addressField.sendKeys(destination.getAddress());
        }

        WebElement amountField = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("input[name=amount]")));
        amountField.sendKeys(amount.toPlainString());

        driver.findElement(By.cssSelector("input[name=checkbox]")).click();
        driver.findElement(By.cssSelector("button[submitter]")).click();

        WebElement twoFaField = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("input[name=code]")));
        twoFaField.sendKeys(get2FAPassword());

        driver.findElement(By.cssSelector("form[name=googleForm] button")).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("p.text-successful")));

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[@class='text-successful' and contains(text(),'CHECK YOUR EMAIL')]")));

        } catch (TimeoutException e) {
            throw new SeleniumExecutorException("Withdrawal initialization error: unable to verify success");
        }
    }

    @Override
    protected String getFinalScreenshotFileName() {
        return "withdraw_init";
    }

    @Override
    protected String getCookieSessionId() {
        return config.getCexCookieSessionId();
    }

    @Override
    protected String getCookieProdSessionId() {
        return config.getCexCookieProdSessionId();
    }

    @Override
    protected String getCookieUserId() {
        return config.getCexCookieUserId();
    }

    private String get2FAPassword() {
        try {
            final TimeBasedOneTimePasswordGenerator totp = new TimeBasedOneTimePasswordGenerator();
            Base32 base32 = new Base32();

            Key key = new SecretKey() {
                @Override
                public String getAlgorithm() {
                    return TimeBasedOneTimePasswordGenerator.TOTP_ALGORITHM_HMAC_SHA1;
                }

                @Override
                public String getFormat() {
                    return null;
                }

                @Override
                public byte[] getEncoded() {
                    return base32.decode(config.getCex2FaSecret());
                }
            };

            Instant now = Instant.now();

            return String.format("%06d", totp.generateOneTimePassword(key, now));
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            logger.error("Error while getting 2FA password: " + e.toString());

            return null;
        }
    }
}
