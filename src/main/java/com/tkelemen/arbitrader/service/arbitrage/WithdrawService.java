package com.tkelemen.arbitrader.service.arbitrage;

import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.db.entity.Arbitrage;
import com.tkelemen.arbitrader.db.entity.Withdraw;
import com.tkelemen.arbitrader.db.repository.ArbitrageRepository;
import com.tkelemen.arbitrader.db.repository.WithdrawRepository;
import com.tkelemen.arbitrader.exception.AddressNotFoundException;
import com.tkelemen.arbitrader.exception.WithdrawException;
import com.tkelemen.arbitrader.model.*;
import com.tkelemen.arbitrader.service.market.FeeService;
import com.tkelemen.arbitrader.service.market.OrderBookHelper;
import com.tkelemen.arbitrader.service.market.WalletService;
import com.tkelemen.arbitrader.service.notification.NotificationService;
import com.tkelemen.arbitrader.service.trade.ExchangeManagerService;
import com.tkelemen.arbitrader.util.CurrencyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class WithdrawService {
    private static final long WITHDRAW_DELAY_SECONDS = 60;

    private static final Logger LOGGER = LoggerFactory.getLogger(WithdrawService.class);

    private final ApplicationConfig config;
    private final ArbitrageRepository arbitrageRepository;
    private final WithdrawRepository withdrawRepository;
    private final FeeService feeService;
    private final NotificationService notificationService;
    private final WalletService walletService;
    private final ExchangeManagerService exchangeManagerService;

    @Autowired
    public WithdrawService(ApplicationConfig config,
                           ArbitrageRepository arbitrageRepository,
                           ExchangeManagerService exchangeManagerService,
                           FeeService feeService,
                           NotificationService notificationService,
                           WalletService walletService,
                           WithdrawRepository withdrawRepository) {
        this.config = config;
        this.arbitrageRepository = arbitrageRepository;
        this.exchangeManagerService = exchangeManagerService;
        this.feeService = feeService;
        this.notificationService = notificationService;
        this.walletService = walletService;
        this.withdrawRepository = withdrawRepository;
    }

    public void executePendingWithdraws() {
        final List<Arbitrage> pendingWithdrawArbitrageList = arbitrageRepository.findByWithdrawStatus(Arbitrage.STATUS_WITHDRAW_PENDING);

        final Map<Exchange, Map<String, PendingWithdraw>> pendingWithdrawMap = new HashMap<>();
        for (Arbitrage arbitrage : pendingWithdrawArbitrageList) {
            try {
                final Exchange exchange = Exchange.valueOf(arbitrage.getPrediction().getInitialExchange());
                Map<String, PendingWithdraw> tickerMap = pendingWithdrawMap.computeIfAbsent(exchange, a -> new HashMap<>());

                String ticker = CurrencyUtils.getBase(arbitrage.getPrediction().getPair());
                PendingWithdraw pendingWithdraw = tickerMap.computeIfAbsent(ticker, a -> new PendingWithdraw(ticker));

                pendingWithdraw.add(arbitrage);
                tickerMap.put(ticker, pendingWithdraw);
                pendingWithdrawMap.put(exchange, tickerMap);
            } catch (WithdrawException e) {
                LOGGER.error(e.getMessage());
            }
        }

        for (Exchange initialExchange : pendingWithdrawMap.keySet()) {
            if (!config.getWithdrawEnabledExchanges().contains(initialExchange)) {
                LOGGER.info("Withdraw from " + initialExchange + " disabled, skipping");
                continue;
            }

            for (String asset : pendingWithdrawMap.get(initialExchange).keySet()) {
                PendingWithdraw pendingWithdraw = pendingWithdrawMap.get(initialExchange).get(asset);

                for (Exchange destinationExchange : pendingWithdraw.getDestinations()) {
                    String withdrawStatus = null;
                    Withdraw withdraw = new Withdraw();
                    BigDecimal withdrawFee = feeService.getWithdrawFee(initialExchange, asset);
                    BigDecimal amount = pendingWithdraw.getAmount(destinationExchange);

                    // TODO: do it through config
                    if ("NEO".equals(asset) || "ONT".equals(asset)) {
                        amount = amount.setScale(0, RoundingMode.DOWN);
                    }

                    List<Arbitrage> arbitrages = pendingWithdraw.getArbitrages(destinationExchange);

                    try {
                        Address address = walletService.getAddress(destinationExchange, asset);

                        LOGGER.info("Withdrawing " + amount + " " + asset + " to " + destinationExchange + " [ " + address + " ]");

                        if (!isEligibleToWithdraw(arbitrages, destinationExchange, asset, amount)) {
                            continue;
                        }

                        WithdrawResult withdrawResult = exchangeManagerService.getWithdrawExecutor(initialExchange).initiateWithdraw(asset, amount, address);

                        LOGGER.info("Withdraw status: " + (withdrawResult.isSuccess() ? "OK" : "NOK") + " [ " + withdrawResult.toString() + " ]");

                        if (!withdrawResult.isSuccess()) {
                            throw new WithdrawException(withdrawResult.getMessage());
                        }

                        withdraw.setAmount(amount);
                        withdraw.setAsset(asset);
                        withdraw.setDatetimeInit(new Date());
                        withdraw.setFromExchange(initialExchange.name());
                        withdraw.setToExchange(destinationExchange.name());
                        withdraw.setRecipientAddress(address.getAddress());
                        if (address.getTag() != null) {
                            withdraw.setRecipientTag(address.getTag().toString());
                        }
                        withdraw.setWithdrawId(withdrawResult.getWithdrawId());
                        withdraw.setWithdrawFee(withdrawFee);
                        withdraw.setStatus(Withdraw.STATUS_REQUESTED);

                        withdrawStatus = Arbitrage.STATUS_OK;

                        withdrawRepository.save(withdraw);

                        BigDecimal withdrawFeePart = withdrawFee.divide(new BigDecimal(arbitrages.size()), 8, RoundingMode.HALF_UP);
                        for (Arbitrage arbitrage : arbitrages) {
                            arbitrage.setWithdraw(withdraw);
                            arbitrage.setWithdrawFee(withdrawFeePart);
                            arbitrage.setWithdrawStatus(withdrawStatus);
                            arbitrage.setTransferSaldoAmount(withdrawFee.subtract(withdrawFeePart));

                            arbitrageRepository.save(arbitrage);
                        }

                        String arbitrageIdList = arbitrages.stream().map(arbitrage -> String.valueOf(arbitrage.getId())).collect(Collectors.joining(","));
                        notificationService.send("Withdraw initiated: from = " + initialExchange + ", to = " + destinationExchange + ", ticker = " + asset + ", amount = " + amount + ", arbitrages = " + arbitrageIdList);
                    } catch (AddressNotFoundException e) {
                        LOGGER.error("Could not find " + asset + " address for exchange " + destinationExchange + ", manual withdraw required");
                        notificationService.send("Manual withdraw required: from = " + initialExchange + ", to = " + destinationExchange + ", ticker = " + asset + ", amount = " + amount);
                    } catch (Exception e) {
                        e.printStackTrace();
                        LOGGER.error("Transfer not successful: " + e.toString());
                        notificationService.send("Manual withdraw required: from = " + initialExchange + ", to = " + destinationExchange + ", ticker = " + asset + ", amount = " + amount);
                    }
                }
            }
        }
    }

    private boolean isEligibleToWithdraw(List<Arbitrage> arbitrages, Exchange toExchange, String asset, BigDecimal amount) {
        if (hasTooLowBaseQuantity(toExchange, asset)) {
            return true;
        }

        // last trade should be older than WITHDRAW_DELAY_SECONDS
        if (arbitrages.get(arbitrages.size() - 1).getDatetime().after(Date.from(Instant.now().minusSeconds(WITHDRAW_DELAY_SECONDS)))) {
            LOGGER.info("Last " + asset + " arbitrage to " + toExchange + " not older than " + WITHDRAW_DELAY_SECONDS + " seconds, postponing withdraw for now");
            return false;
        }

        return true;
    }

    private boolean hasTooLowBaseQuantity(Exchange exchange, String asset) {
        for (String quote : config.getAllowedQuotesToTrade()) {
            OrderBook destinationOrderBook = exchangeManagerService.getOrderBookService(exchange).getOrderBookSnapshots().get(CurrencyUtils.getPair(asset, quote));

            if (destinationOrderBook == null) {
                continue;
            }

            NavigableSet<BigDecimal> volumes = config.getAllowedQuoteVolumes().get(quote);
            if (volumes == null || volumes.isEmpty()) {
                continue;
            }

            Wallet wallet = exchangeManagerService.getExchangeService(exchange).getWallet();

            if (volumes.first().compareTo(OrderBookHelper.calculateQuoteQuantity(destinationOrderBook, wallet.getFreeAmount(asset))) > 0) {
                return true;
            }
        }

        return false;
    }
}
