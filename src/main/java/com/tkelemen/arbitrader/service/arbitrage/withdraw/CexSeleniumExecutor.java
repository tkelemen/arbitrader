package com.tkelemen.arbitrader.service.arbitrage.withdraw;

import com.tkelemen.arbitrader.exception.SeleniumExecutorException;
import com.tkelemen.arbitrader.exception.WithdrawException;
import com.tkelemen.arbitrader.model.SeleniumResult;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.Instant;
import java.util.Date;
import java.util.concurrent.Callable;

abstract class CexSeleniumExecutor implements Callable<SeleniumResult> {
    private static Logger logger = LoggerFactory.getLogger(CexSeleniumExecutor.class);

    protected String initialUrl = "https://cex.io/";
    protected WebDriver driver;

    public SeleniumResult call() throws SeleniumExecutorException {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--window-size=1920,1200", "--ignore-certificate-errors", "--headless");
        driver = new ChromeDriver(options);

        String userAgent = (String) ((JavascriptExecutor) driver).executeScript("return navigator.userAgent;");

        driver.close();

        options.addArguments("--user-agent=" + userAgent.replaceAll("HeadlessChrome", "Chrome"));

        driver = new ChromeDriver(options);

        logger.info("ChromeDriver name: " + userAgent);

        try {
            driver.get(initialUrl);
            //Cookie userIdCookie = new Cookie("user_id", getCookieUserId(), "cex.io", "/", Date.from(Instant.now().plusSeconds(6000)), false, false);
            //driver.manage().addCookie(userIdCookie);
            Cookie sessionCookie = driver.manage().getCookieNamed("cex-session");
            driver.manage().deleteCookieNamed("cex-session");
            driver.manage().addCookie(new Cookie("cex-session", getCookieSessionId(), ".cex.io", "/", null, true, true));
            //Cookie prodSessionCookie = new Cookie("cex-prod-session", getCookieProdSessionId(), ".cex.io", "/", null, true, true);
            //driver.manage().addCookie(prodSessionCookie);

            executeSteps();
        } catch (WebDriverException e) {
            logger.error("Selenium executor WebDriver error: " + e.toString());
            throw new SeleniumExecutorException("Selenium executor exception: " + e.toString());
        } catch (Exception e) {
            logger.error("Selenium executor error: " + e.toString());
            throw new SeleniumExecutorException("Selenium executor exception: " + e.toString());
        } finally {
            String screenshotPath = System.getProperty("java.io.tmpdir") + File.separator + getFinalScreenshotFileName() + "_" + System.currentTimeMillis() + ".png";

            logger.info("Selenium job finished, saving screenshot to: " + screenshotPath);
            takeScreenShot(driver, screenshotPath);
            driver.quit();
        }

        return SeleniumResult.SUCCESS;
    }

    private void takeScreenShot(WebDriver webdriver, String destinationPath) {
        try {
            TakesScreenshot scrShot = ((TakesScreenshot) webdriver);

            File srcFile = scrShot.getScreenshotAs(OutputType.FILE);

            Files.move(srcFile.toPath(), new File(destinationPath).toPath());
        } catch (IOException e) {
            logger.error("Could not save screenshot: " + e.toString());
        }
    }

    protected abstract void executeSteps() throws SeleniumExecutorException;

    protected abstract String getFinalScreenshotFileName();

    protected abstract String getCookieSessionId();

    protected abstract String getCookieProdSessionId();

    protected abstract String getCookieUserId();
}
