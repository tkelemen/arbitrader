package com.tkelemen.arbitrader.service.arbitrage.maker;

import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.model.*;
import com.tkelemen.arbitrader.service.market.FeeService;
import com.tkelemen.arbitrader.service.market.MarketDataService;
import com.tkelemen.arbitrader.service.market.OrderBookHelper;
import com.tkelemen.arbitrader.service.trade.ExchangeManagerService;
import com.tkelemen.arbitrader.service.trade.IExchangeService;
import com.tkelemen.arbitrader.util.CurrencyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListMap;

@Service
public class MakerArbitrageFinderService {
    private final static Logger LOGGER = LoggerFactory.getLogger(MakerArbitrageFinderService.class);

    private final MarketDataService marketDataService;
    private final FeeService feeService;
    private final Map<String, List<BigDecimal>> initialQtyMap = new HashMap<>();
    private final Map<Exchange, IExchangeService> exchangeServices;
    private NavigableMap<Double, MakerOpportunityDTO> bestOpportunities;
    private final Map<Exchange, Map<String, BigDecimal>> exchangePairVolumes = new HashMap<>();
    private final ApplicationConfig config;

    @Autowired
    public MakerArbitrageFinderService(MarketDataService marketDataService,
                                       FeeService feeService,
                                       ExchangeManagerService exchangeManagerService,
                                       ApplicationConfig config) {
        this.marketDataService = marketDataService;
        this.feeService = feeService;
        this.exchangeServices = exchangeManagerService.getExchangeServices();
        this.config = config;

        fetchTradeVolumes();
        //printWithdrawFeeComparison();

        // TODO: move into config
        initialQtyMap.put("USD", List.of(new BigDecimal(50)));
        initialQtyMap.put("USDT", List.of(new BigDecimal(50)));
        initialQtyMap.put("BUSD", List.of(new BigDecimal(50)));
        initialQtyMap.put("GBP", List.of(new BigDecimal(50)));
        initialQtyMap.put("AUD", List.of(new BigDecimal(50)));
        initialQtyMap.put("EUR", List.of(new BigDecimal(50)));
    }

    public void fetchTradeVolumes() {
        // TODO: refresh regularly
        try {
            for (Exchange exchange : exchangeServices.keySet()) {
                final Map<String, BigDecimal> volumeMap = exchangeServices.get(exchange).getTradeVolumesByAssetPair();
                exchangePairVolumes.put(exchange, volumeMap);

                //printTradingVolumes(exchange, volumeMap);
            }
        } catch (Exception e) {
            LOGGER.error("Error while getting trade volumes: " + e.getMessage(), e);
        }
    }

    private void printTradingVolumes(Exchange exchange, Map<String, BigDecimal> volumeMap) {
        TreeMap<BigDecimal, String> sortedVolumes = new TreeMap<>();
        for(Map.Entry<String, BigDecimal> volume : volumeMap.entrySet()) {
            sortedVolumes.put(volume.getValue(), volume.getKey());
        }
        System.out.println("Trading volumes on " + exchange + ": " + sortedVolumes.descendingMap());
    }

    public void printWithdrawFeeComparison() {
        for (Exchange exchange : exchangeServices.keySet()) {
            Map<String, BigDecimal> lastPriceMap = exchangeServices.get(exchange).getLastPricesByAssetPair();
            TreeMap<BigDecimal, String> sortedWithdrawFeeToPriceRatios = new TreeMap<>();
            BigDecimal[] amounts = new BigDecimal[]{new BigDecimal(100), new BigDecimal(300), new BigDecimal(500), new BigDecimal(1000)};
            for (BigDecimal amount : amounts) {
                for (Map.Entry<String, BigDecimal> price : lastPriceMap.entrySet()) {
                    if (price.getValue() == null) {
                        continue;
                    }
                    BigDecimal withdrawFee = feeService.getWithdrawFee(exchange, CurrencyUtils.getBase(price.getKey()));
                    if (withdrawFee == null) {
                        continue;
                    }
                    BigDecimal boughtAmount = amount.divide(price.getValue(), 8, RoundingMode.HALF_UP);
                    BigDecimal feePercent = withdrawFee.divide(boughtAmount, 4, RoundingMode.HALF_UP);
                    sortedWithdrawFeeToPriceRatios.put(feePercent, price.getKey());
                }
                System.out.println("Cheapest withdrawals (" + amount + ") on " + exchange + ": " + sortedWithdrawFeeToPriceRatios);
            }
        }
    }

    public NavigableMap<Double, MakerOpportunityDTO> getCurrentOpportunities() {
        final Map<Exchange, Wallet> exchangeWallets = new HashMap<>();
        for (Exchange exchange : exchangeServices.keySet()) {
            exchangeWallets.put(exchange, exchangeServices.get(exchange).getWallet());
        }

        bestOpportunities = new ConcurrentSkipListMap<>();

        final Map<Exchange, Map<String, OrderBook>> orderBooks = new HashMap<>();
        final Set<String> initialPairs = new HashSet<>();

        marketDataService.getOrderBookServices().forEach((exchange, orderBookService) -> {
            orderBooks.put(exchange, orderBookService.getOrderBookSnapshots());
            initialPairs.addAll(orderBookService.getOrderBookSnapshots().keySet());
        });

        final Map<String, Set<String>> allPairs = new HashMap<>();
        for (String initialPair : initialPairs) {
            allPairs.put(initialPair, CurrencyUtils.getFinalPairs(initialPair));
        }

        findProfitableOpportunities(exchangeWallets, orderBooks, allPairs);

        printBestOpportunities();

        return bestOpportunities;
    }

    private void findProfitableOpportunities(Map<Exchange, Wallet> exchangeWallets, Map<Exchange, Map<String, OrderBook>> orderBooks, Map<String, Set<String>> allPairs) {
        for (Map.Entry<Exchange, Map<String, OrderBook>> initialOrderBooks : orderBooks.entrySet()) {
            for (Map.Entry<Exchange, Map<String, OrderBook>> finalOrderBooks : orderBooks.entrySet()) {
                if (initialOrderBooks.getKey() == finalOrderBooks.getKey()) {
                    continue;
                }

                final Map<String, OrderBook> initialExchangeBooks = initialOrderBooks.getValue();
                final Map<String, OrderBook> finalExchangeBooks = finalOrderBooks.getValue();

                for (String initialPair : allPairs.keySet()) {
                    final Set<String> finalPairs = allPairs.get(initialPair);

                    for (String finalPair : finalPairs) {
                        if (initialExchangeBooks.containsKey(initialPair) || finalExchangeBooks.containsKey(finalPair)) {
                            final OrderBook initialBook = initialExchangeBooks.get(initialPair);
                            final OrderBook finalBook = finalExchangeBooks.get(finalPair);

                            if (initialBook == null || finalBook == null) {
                                continue;
                            }

                            final List<BigDecimal> initialQtyList = initialQtyMap.get(CurrencyUtils.getQuote(initialPair));

                            if (initialQtyList == null) {
                                continue;
                            }

                            for (BigDecimal initialQty : initialQtyList) {
                                findOpportunities(initialBook, finalBook, initialQty, exchangeWallets, System.currentTimeMillis());
                            }
                        }
                    }
                }
            }
        }
    }

    private void printBestOpportunities() {
        for(Map.Entry<Double, MakerOpportunityDTO> entry : bestOpportunities.descendingMap().entrySet())  {
            LOGGER.info(getOpportunityString(entry.getValue()));
        }
    }

    private BigDecimal getLiquidityScore(BigDecimal volume) {
        if(volume == null) {
            return BigDecimal.ZERO;
        }

        return volume.divide(new BigDecimal(10000000), 8, RoundingMode.DOWN);
    }

    private void findOpportunities(OrderBook triggerBook, OrderBook partnerBook, BigDecimal initialQty, Map<Exchange, Wallet> exchangeWallets, long iterationId) {
        if (triggerBook.getBidLevels().isEmpty() || triggerBook.getAskLevels().isEmpty() ||
                partnerBook.getBidLevels().isEmpty() || partnerBook.getAskLevels().isEmpty()) {
            return;
        }

        final Exchange triggerExchange = triggerBook.getExchange();
        final Exchange partnerExchange = partnerBook.getExchange();

        final BigDecimal triggerExchangeFeeMultiplier = feeService.getTradingFeeMaker(triggerBook.getExchange());
        final BigDecimal partnerExchangeFeeMultiplier = feeService.getTradingFeeMaker(partnerBook.getExchange());

        final String triggerPair = triggerBook.getSymbol();
        final String partnerPair = partnerBook.getSymbol();

        final BigDecimal liquidityScore = getLiquidityScore(exchangePairVolumes.get(triggerBook.getExchange()).get(triggerPair));

        final BigDecimal triggerExchangeWithdrawFee = feeService.getWithdrawFee(triggerBook.getExchange(), CurrencyUtils.getBase(triggerPair));
        final BigDecimal partnerExchangeWithdrawFee = feeService.getWithdrawFee(partnerBook.getExchange(), CurrencyUtils.getBase(partnerPair));

        final BigDecimal lowestAsk = new BigDecimal(triggerBook.getAskLevels().firstEntry().getKey());
        final BigDecimal highestBid = new BigDecimal(triggerBook.getBidLevels().lastEntry().getKey());

        final int triggerExchangePricePrecision = exchangeServices.get(triggerBook.getExchange()).getPairPricePrecision(triggerPair);

        final BigDecimal orderAsk = getNextLowerPrice(lowestAsk, triggerExchangePricePrecision);
        final BigDecimal orderBid = getNextHigherPrice(highestBid, triggerExchangePricePrecision);

        final float spreadSizePercent = (((orderAsk.floatValue() / orderBid.floatValue()) - 1) * 100);

        if (spreadSizePercent > config.getMakerWatchSpread().floatValue()) {
            float buyLowQty = initialQty.floatValue() / orderBid.floatValue();
            final BigDecimal blTransferQty = new BigDecimal(buyLowQty - triggerExchangeWithdrawFee.floatValue());
            final BigDecimal blSellQty = OrderBookHelper.calculateQuoteQuantity(partnerBook, blTransferQty);

            float blGainPercent = ((blSellQty.floatValue() / initialQty.floatValue()) - 1) * 100;

            if (blSellQty.compareTo(initialQty.multiply(config.getMakerWatchGain())) > 0) {
                final BigDecimal adjustedGainScore = new BigDecimal(blGainPercent).multiply(liquidityScore);

                final String fromQuote = CurrencyUtils.getQuote(triggerPair);
                final String base = CurrencyUtils.getBase(triggerPair);

                final MakerOpportunityDTO opportunity = new MakerOpportunityDTO();
                opportunity.setIterationId(iterationId);
                opportunity.setType(MakerOpportunityType.BL);
                opportunity.setTriggerExchange(triggerExchange);
                opportunity.setPartnerExchange(partnerExchange);
                opportunity.setBase(base);
                opportunity.setFromQuote(fromQuote);
                opportunity.setToQuote(CurrencyUtils.getQuote(partnerPair));
                opportunity.setTriggerPrice(orderBid);
                opportunity.setFromQuoteAmount(initialQty);
                opportunity.setBaseAmount(new BigDecimal(buyLowQty));
                opportunity.setToQuoteAmount(blSellQty);
                opportunity.setTriggerHighestBid(highestBid);
                opportunity.setTriggerLowestAsk(lowestAsk);
                opportunity.setPartnerBestPrice(new BigDecimal(partnerBook.getBidLevels().lastKey()));
                opportunity.setSpread(new BigDecimal(spreadSizePercent));
                opportunity.setLiquidityScore(liquidityScore);
                opportunity.setBuyFee(initialQty.multiply(triggerExchangeFeeMultiplier));
                opportunity.setSellFee(blSellQty.multiply(partnerExchangeFeeMultiplier));
                opportunity.setDatetime(new Date());
                opportunity.setWithdrawFee(triggerExchangeWithdrawFee);

                final BigDecimal freeQuoteAmount = exchangeWallets.get(triggerExchange).getTotalAmount(fromQuote);
                final BigDecimal freeBaseAmount = exchangeWallets.get(partnerExchange).getTotalAmount(base);

                boolean hasInsufficientQuoteFunds = freeQuoteAmount.compareTo(initialQty) < 0;
                boolean hasInsufficientBaseFunds = freeBaseAmount.compareTo(blTransferQty) < 0;

                opportunity.setHasInsufficientBaseFunds(hasInsufficientBaseFunds);
                opportunity.setHasInsufficientQuoteFunds(hasInsufficientQuoteFunds);

                bestOpportunities.put(adjustedGainScore.doubleValue(), opportunity);
            }

            float sellHighQty = (initialQty.floatValue() / orderAsk.floatValue());
            final BigDecimal shTransferQty = new BigDecimal(sellHighQty + partnerExchangeWithdrawFee.floatValue());
            final BigDecimal shBuyQty = OrderBookHelper.calculateBuyQuoteQuantity(partnerBook, shTransferQty);

            float shGainPercent = ((initialQty.floatValue() / shBuyQty.floatValue()) - 1) * 100;

            if (shBuyQty.multiply(config.getMakerWatchGain()).compareTo(initialQty) < 0) {
                final BigDecimal adjustedGainScore = new BigDecimal(shGainPercent).multiply(liquidityScore);

                final String fromQuote = CurrencyUtils.getQuote(partnerPair);
                final String base = CurrencyUtils.getBase(triggerPair);

                final MakerOpportunityDTO opportunity = new MakerOpportunityDTO();
                opportunity.setIterationId(iterationId);
                opportunity.setType(MakerOpportunityType.SH);
                opportunity.setTriggerExchange(triggerExchange);
                opportunity.setPartnerExchange(partnerExchange);
                opportunity.setBase(base);
                opportunity.setFromQuote(CurrencyUtils.getQuote(partnerPair));
                opportunity.setToQuote(CurrencyUtils.getQuote(triggerPair));
                opportunity.setTriggerPrice(orderAsk);
                opportunity.setFromQuoteAmount(shBuyQty);
                opportunity.setBaseAmount(shTransferQty);
                opportunity.setToQuoteAmount(initialQty);
                opportunity.setTriggerHighestBid(highestBid);
                opportunity.setTriggerLowestAsk(lowestAsk);
                opportunity.setPartnerBestPrice(new BigDecimal(partnerBook.getAskLevels().firstKey()));
                opportunity.setSpread(new BigDecimal(spreadSizePercent));
                opportunity.setLiquidityScore(liquidityScore);
                opportunity.setBuyFee(shBuyQty.multiply(partnerExchangeFeeMultiplier));
                opportunity.setSellFee(initialQty.multiply(triggerExchangeFeeMultiplier));
                opportunity.setDatetime(new Date());
                opportunity.setWithdrawFee(partnerExchangeWithdrawFee);

                final BigDecimal freeQuoteAmount = exchangeWallets.get(partnerExchange).getTotalAmount(fromQuote);
                final BigDecimal freeBaseAmount = exchangeWallets.get(triggerExchange).getTotalAmount(base);

                boolean hasInsufficientBaseFunds = freeBaseAmount.compareTo(shTransferQty) < 0;
                boolean hasInsufficientQuoteFunds = freeQuoteAmount.compareTo(initialQty) < 0;

                opportunity.setHasInsufficientBaseFunds(hasInsufficientBaseFunds);
                opportunity.setHasInsufficientQuoteFunds(hasInsufficientQuoteFunds);

                bestOpportunities.put(adjustedGainScore.doubleValue(), opportunity);
            }
        }
    }

    private static String getOpportunityString(MakerOpportunityDTO opportunity) {
        final String arrow = MakerOpportunityType.BL.equals(opportunity.getType()) ? " -> " : " <- ";
        final String bigArrow = MakerOpportunityType.BL.equals(opportunity.getType()) ? " =====> " : " <===== ";
        final BigDecimal initialAmount = MakerOpportunityType.BL.equals(opportunity.getType()) ? opportunity.getFromQuoteAmount() : opportunity.getToQuoteAmount();
        final String triggerPair = CurrencyUtils.getPair(opportunity.getBase(), MakerOpportunityType.BL.equals(opportunity.getType())
                ? opportunity.getFromQuote()
                : opportunity.getToQuote());
        final String partnerPair = CurrencyUtils.getPair(opportunity.getBase(), MakerOpportunityType.BL.equals(opportunity.getType())
                ? opportunity.getToQuote()
                : opportunity.getFromQuote());
        final BigDecimal oppositeQty = MakerOpportunityType.BL.equals(opportunity.getType()) ? opportunity.getToQuoteAmount() : opportunity.getFromQuoteAmount();
        float gainPercent = MakerOpportunityType.BL.equals(opportunity.getType())
                ? ((opportunity.getToQuoteAmount().floatValue() / opportunity.getFromQuoteAmount().floatValue()) - 1) * 100
                : ((opportunity.getFromQuoteAmount().floatValue() / opportunity.getToQuoteAmount().floatValue()) - 1) * 100;
        final BigDecimal adjustedGainScore = new BigDecimal(gainPercent).multiply(opportunity.getLiquidityScore());

        return opportunity.getType() + ": " + String.format("%10s", opportunity.getTriggerExchange()) +
                arrow + String.format("%-10s", opportunity.getPartnerExchange()) + " " + initialAmount + " " +
                String.format("%10s", triggerPair) + " -> " + String.format("%-10s", partnerPair) + ": " +
                String.format("%14.8f", opportunity.getTriggerHighestBid()) + " < " + String.format("%5.2f", opportunity.getSpread()) + "% > " +
                String.format("%-14.8f", opportunity.getTriggerLowestAsk()) + bigArrow + String.format("%6.2f", oppositeQty) +
                " (" + String.format("%14.8f", opportunity.getPartnerBestPrice()) + ") : " +
                String.format("%5.2f", gainPercent) + " " + String.format("%9.8f", opportunity.getLiquidityScore()) + ">>>" + adjustedGainScore;
    }

    private BigDecimal getNextLowerPrice(BigDecimal price, int precision) {
        return price.subtract(BigDecimal.ONE.divide(BigDecimal.TEN.pow(precision), precision, RoundingMode.HALF_UP));
    }

    private BigDecimal getNextHigherPrice(BigDecimal price, int precision) {
        return price.add(BigDecimal.ONE.divide(BigDecimal.TEN.pow(precision), precision, RoundingMode.HALF_UP));
    }

    public String getStatusString() {
        return "Maker trading enabled: " + config.isMakerTradingEnabled() + "\n" +
                "Maker spread watch threshold: " + config.getMakerWatchSpread() + "\n" +
                "Maker min. gain threshold: " + config.getMakerWatchGain() + "\n";
    }
}
