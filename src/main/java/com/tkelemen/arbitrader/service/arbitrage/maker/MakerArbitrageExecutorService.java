package com.tkelemen.arbitrader.service.arbitrage.maker;

import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.db.entity.MakerOpportunity;
import com.tkelemen.arbitrader.db.entity.MakerTrade;
import com.tkelemen.arbitrader.db.entity.MakerTriggerOrder;
import com.tkelemen.arbitrader.db.repository.MakerOpportunityRepository;
import com.tkelemen.arbitrader.db.repository.MakerTradeRepository;
import com.tkelemen.arbitrader.db.repository.MakerTriggerOrderRepository;
import com.tkelemen.arbitrader.exception.ExchangeApiException;
import com.tkelemen.arbitrader.exception.TradeException;
import com.tkelemen.arbitrader.model.*;
import com.tkelemen.arbitrader.model.enums.OrderType;
import com.tkelemen.arbitrader.service.notification.NotificationService;
import com.tkelemen.arbitrader.service.trade.ExchangeManagerService;
import com.tkelemen.arbitrader.service.trade.IExchangeService;
import com.tkelemen.arbitrader.service.trade.IMassCancelCreateCapable;
import com.tkelemen.arbitrader.util.CurrencyUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
public class MakerArbitrageExecutorService {
    private final static Logger LOGGER = LoggerFactory.getLogger(MakerArbitrageExecutorService.class);

    private final Map<Exchange, Map<String, MakerTriggerOrderDTO>> activeTriggers = new ConcurrentHashMap<>();
    private final ModelMapper modelMapper = new ModelMapper();
    private final MakerOpportunityRepository makerOpportunityRepository;
    private final MakerTriggerOrderRepository makerTriggerOrderRepository;
    private final Map<Exchange, IExchangeService> exchangeServices;
    private final NotificationService notificationService;
    private final ActiveOrders activeOrders;
    private final MakerTradeRepository makerTradeRepository;
    private final ApplicationConfig config;
    private long executionCount = 0;
    private boolean massExecutionEnabled = true;

    @Autowired
    public MakerArbitrageExecutorService(MakerOpportunityRepository makerOpportunityRepository,
                                         MakerTriggerOrderRepository makerTriggerOrderRepository,
                                         ExchangeManagerService exchangeManagerService,
                                         NotificationService notificationService,
                                         ActiveOrders activeOrders,
                                         MakerTradeRepository makerTradeRepository,
                                         ApplicationConfig config) {
        this.makerOpportunityRepository = makerOpportunityRepository;
        this.makerTriggerOrderRepository = makerTriggerOrderRepository;
        this.exchangeServices = exchangeManagerService.getExchangeServices();
        this.notificationService = notificationService;
        this.activeOrders = activeOrders;
        this.makerTradeRepository = makerTradeRepository;
        this.config = config;
    }

    public void processOpportunities(NavigableMap<Double, MakerOpportunityDTO> opportunities) {
        executionCount++;

        final long processingStartTimestamp = System.currentTimeMillis();
        final String iterationId = opportunities.size() > 0 ? String.valueOf(opportunities.firstEntry().getValue().getIterationId()) : "N/A";
        LOGGER.info("Processing " + opportunities.size() + " MAKER opportunities START (iteration " + iterationId + ")");

        final Map<Exchange, List<MakerTriggerOrderDTO>> ordersToCancel = new HashMap<>();
        /*
         *  TODO: execute the other side of the orders which have been triggered during the wait period but
         *  the trigger signal was not captured through Websocket update for some reason */
        // TODO: put the interval into config
        if(executionCount % Math.ceil((double) 5000 / config.getExecuteMakerTradesRateMs()) == 0) {
            ordersToCancel.putAll(
                    handleAsyncOrderChanges()
            );
        }
        final Map<Exchange, List<MakerTriggerOrderDTO>> triggersToCancel = getTriggersToCancel(opportunities);
        for(Exchange triggerExchange : triggersToCancel.keySet()) {
            ordersToCancel.computeIfAbsent(triggerExchange, a -> new ArrayList<>()).addAll(triggersToCancel.get(triggerExchange));
        }

        final Map<Exchange, List<MakerOpportunityDTO>> triggersForOpportunitiesToCreate = getOpportunitiesForTriggerOrdersToCreate(opportunities, ordersToCancel);

        executeCreateCancelTriggers(ordersToCancel, triggersForOpportunitiesToCreate);

        LOGGER.info("Processing MAKER opportunities (iteration " + iterationId + ") FINISHED, took " + (System.currentTimeMillis() - processingStartTimestamp) + " ms");
    }

    private void executeCreateCancelTriggers(Map<Exchange, List<MakerTriggerOrderDTO>> ordersToCancel,
                                             Map<Exchange, List<MakerOpportunityDTO>> opportunitiesForTriggerCreate) {
        final Set<Exchange> triggerExchanges = new HashSet<>(ordersToCancel.keySet());
        if(!opportunitiesForTriggerCreate.isEmpty()) {
            triggerExchanges.addAll(opportunitiesForTriggerCreate.keySet());
        }

        triggerExchanges.retainAll(config.getAllowedMakerTriggerExchanges());

        for(Exchange triggerExchange : triggerExchanges) {
            final IExchangeService exchangeService = exchangeServices.get(triggerExchange);

            if(massExecutionEnabled && exchangeService instanceof IMassCancelCreateCapable) {
                final Map<String, BigDecimal> freeAmounts = exchangeService.getWallet().getFreeAmounts();

                final List<MakerTriggerOrderDTO> exchangeTriggerOrdersToCancel = ordersToCancel.getOrDefault(triggerExchange, new ArrayList<>());
                final List<MakerOpportunityDTO> opportunitiesForOrderCreate = opportunitiesForTriggerCreate.getOrDefault(triggerExchange, new ArrayList<>())
                        .stream().filter(makerOpportunityDTO -> {
                            if(MakerOpportunityType.BL.equals(makerOpportunityDTO.getType())) {
                                final BigDecimal free = freeAmounts.get(makerOpportunityDTO.getFromQuote());

                                final boolean hasFunds = free.compareTo(makerOpportunityDTO.getFromQuoteAmount()) >= 0;
                                if(hasFunds) {
                                    freeAmounts.put(makerOpportunityDTO.getFromQuote(), free.subtract(makerOpportunityDTO.getFromQuoteAmount()));
                                }
                                return hasFunds;
                            } else {
                                final BigDecimal free = freeAmounts.get(makerOpportunityDTO.getBase());

                                final boolean hasFunds =  free.compareTo(makerOpportunityDTO.getBaseAmount()) >= 0;
                                if(hasFunds) {
                                    freeAmounts.put(makerOpportunityDTO.getBase(), free.subtract(makerOpportunityDTO.getBaseAmount()));
                                }

                                return hasFunds;
                            }
                        }).collect(Collectors.toList());

                final int maxElementCount = Math.max(exchangeTriggerOrdersToCancel.size(), opportunitiesForOrderCreate.size());
                // TODO: read from config
                final int MAX_ELEMENTS_PER_REQUEST = 50;

                for(int i = 0; i < Math.ceil((double) maxElementCount / MAX_ELEMENTS_PER_REQUEST); i++) {
                    final int from = i * MAX_ELEMENTS_PER_REQUEST;
                    final int to = from + MAX_ELEMENTS_PER_REQUEST;

                    executeMassCancelCreateTriggers(
                            exchangeService,
                            exchangeTriggerOrdersToCancel.subList(
                                    Math.min(from, exchangeTriggerOrdersToCancel.size()),
                                    Math.min(to, exchangeTriggerOrdersToCancel.size())
                            ),
                            opportunitiesForOrderCreate.subList(
                                    Math.min(from, opportunitiesForOrderCreate.size()),
                                    Math.min(to, opportunitiesForOrderCreate.size())
                            )
                    );
                }
            } else {
                // TODO: implement the one-by-one cancel/create
                // cancel triggers
                final List<MakerTriggerOrderDTO> exchangeTriggerOrdersToCancel = ordersToCancel.getOrDefault(triggerExchange, new ArrayList<>());
                for(MakerTriggerOrderDTO triggerOrderDTO : exchangeTriggerOrdersToCancel) {
                    cancelSingleTrigger(triggerOrderDTO);
                }

                // create triggers
                final List<MakerOpportunityDTO> opportunitiesForOrderCreate = opportunitiesForTriggerCreate.getOrDefault(triggerExchange, new ArrayList<>());
                for (MakerOpportunityDTO opportunityDTO : opportunitiesForOrderCreate) {
                    final CreateOrderResult orderResult = createSingleTriggerOrder(opportunityDTO);
                    saveCreatedTriggerOrder(opportunityDTO, orderResult);
                }
                throw new IllegalStateException("Mass order cancel/create not implemented for exchange " + triggerExchange);
            }
        }
    }

    private void executeMassCancelCreateTriggers(
            IExchangeService exchangeService,
            @NonNull List<MakerTriggerOrderDTO> exchangeTriggerOrdersToCancel,
            @NonNull List<MakerOpportunityDTO> opportunitiesForOrderCreate
    ) {
        LOGGER.info("Executing mass cancel/create triggers");

        final MassCancelCreateOrderRequest request = new MassCancelCreateOrderRequest();

        if (!exchangeTriggerOrdersToCancel.isEmpty()) {
            for (MakerTriggerOrderDTO triggerOrderToCancel : exchangeTriggerOrdersToCancel) {
                request.addOrderToCancel(triggerOrderToCancel.getExchangeOrderId());
            }
        }

        if (!opportunitiesForOrderCreate.isEmpty()) {
            for (MakerOpportunityDTO triggerToCreate : opportunitiesForOrderCreate) {
                request.addOrderToCreate(
                        MakerOpportunityType.BL.equals(triggerToCreate.getType())
                                ? OrderType.BUY
                                : OrderType.SELL,
                        CurrencyUtils.getPair(triggerToCreate.getBase(), MakerOpportunityType.BL.equals(triggerToCreate.getType())
                                ? triggerToCreate.getFromQuote()
                                : triggerToCreate.getToQuote()),
                        triggerToCreate.getTriggerPrice(),
                        triggerToCreate.getBaseAmount()
                );
            }
        }

        try {
            final MassCancelCreateOrderResult result = ((IMassCancelCreateCapable) exchangeService).massCancelCreateOrders(request);

            if (result.getError() != null && !"".equals(result.getError())) {
                notify("Error during mass order cancel/create: " + result.getError(), NotificationService.Level.ERROR);
            }

            // process cancelled result
            for (int i = 0; i < result.getCancelOrderResultList().size(); i++) {
                final CancelOrderResult cancelOrderResult = result.getCancelOrderResultList().get(i);
                final MakerTriggerOrderDTO triggerOrderDTO = exchangeTriggerOrdersToCancel.get(i);

                if (!triggerOrderDTO.getExchangeOrderId().equals(cancelOrderResult.getOrderId())) {
                    notify("Mass cancel trigger request-result order id doesn't match: " +
                            triggerOrderDTO.getExchangeOrderId() + "-" + cancelOrderResult.getOrderId(), NotificationService.Level.ERROR);
                } else if(cancelOrderResult.getError() != null && !"".equals(cancelOrderResult.getError())) {
                    LOGGER.error("Error while canceling order " + cancelOrderResult.getOrderId() + ": " + cancelOrderResult.getError());
                } /*else if (!triggerOrderDTO.getAmount().equals(cancelOrderResult.getRemainingAmount())) {
                    LOGGER.info("Remaining amount doesn't match original amount, finishing arbitrage: " + triggerOrderDTO + ", " + cancelOrderResult);

                    final ExchangeOrder exchangeOrder = exchangeService.getOrderDetail(triggerOrderDTO.getExchangeOrderId()).orElse(null);

                    if (exchangeOrder == null) {
                        notify("Cannot get exchange order detail: " + triggerOrderDTO.getExchangeOrderId(), NotificationService.Level.ERROR);
                    } else {
                        // TODO: handle this
                        //finishArbitrage(triggerOrderDTO, exchangeOrder);
                    }
                } */
                else {
                    cleanupCanceledTriggerOrderReferences(triggerOrderDTO);
                }
            }

            // process created result
            for (int i = 0; i < result.getCreateOrderResultList().size(); i++) {
                final CreateOrderResult createOrderResult = result.getCreateOrderResultList().get(i);
                final CreateOrder createOrderRequest = request.getOrdersToCreateList().get(i);

                if (createOrderResult.getError() != null && !"".equals(createOrderResult.getError())) {
                    notify("Mass create: Error while creating order: " + createOrderResult);
                } else if (createOrderResult.getOrderId() == null || "".equals(createOrderResult.getOrderId())) {
                    notify("Mass create: Created order doesn't contain order id: " + createOrderResult);
                } else if (!createOrderRequest.getPair().equals(createOrderResult.getPair()) ||
                        !createOrderRequest.getPrice().equals(createOrderResult.getPrice()) ||
                        !createOrderRequest.getType().equals(createOrderResult.getType())) {
                    notify("Mass create: The request and response order doesn't match: " + createOrderRequest + " - " + createOrderResult);
                } else {
                    saveCreatedTriggerOrder(opportunitiesForOrderCreate.get(i), createOrderResult);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.toString());
            for(StackTraceElement element : e.getStackTrace()) {
                LOGGER.error(element.toString());
            }
            notify("ERROR: " + e.getMessage(), NotificationService.Level.ERROR);
        }
    }

    private Map<Exchange, List<MakerOpportunityDTO>> getOpportunitiesForTriggerOrdersToCreate(
            NavigableMap<Double, MakerOpportunityDTO> opportunities,
            Map<Exchange, List<MakerTriggerOrderDTO>> triggersBeingCancelled) {
        LOGGER.info("Active orders: " + activeOrders.getActiveOrders());
        LOGGER.info("Active triggers: " + activeTriggers);

        final Map<Exchange, Set<String>> triggerKeysBeingCancelled = new HashMap<>();
        for(Exchange exchange : triggersBeingCancelled.keySet()) {
            triggerKeysBeingCancelled.put(exchange,
                    triggersBeingCancelled.get(exchange)
                            .stream()
                            .map(makerTriggerOrderDTO -> getKeyForActiveTrigger(makerTriggerOrderDTO.getOpportunity()))
                            .collect(Collectors.toSet())
            );
        }

        final Map<Exchange, List<MakerOpportunityDTO>> opportunitiesForTriggerOrdersToCreate = new HashMap<>();

        if (opportunities.isEmpty()) {
            LOGGER.info("No maker opportunity found");
            return opportunitiesForTriggerOrdersToCreate;
        }

        final Map<Exchange, Set<String>> newTriggerKeys = new HashMap<>();
        for(Map.Entry<Double, MakerOpportunityDTO> entry : opportunities.descendingMap().entrySet())  {
            final MakerOpportunityDTO opportunityDTO = entry.getValue();
            final String opportunityDescr = getOpportunityDescr(opportunityDTO);

            LOGGER.debug("Processing opportunity: " + opportunityDTO);

            makerOpportunityRepository.save(modelMapper.map(opportunityDTO, MakerOpportunity.class));

            if(!config.isMakerTradingEnabled()) {
                LOGGER.error("Maker trading disabled, skipping opportunity: " + opportunityDescr);
                break;
            }

            if(ourTriggerExists(opportunityDTO)) {
                LOGGER.debug("Our trigger with the best trigger price already exists, skipping");
                continue;
            }

            if(config.getMakerCreateTriggerThreshold().multiply(opportunityDTO.getFromQuoteAmount()).compareTo(opportunityDTO.getToQuoteAmount()) > 0) {
                LOGGER.debug("Opportunity gain lower than create threshold (" + config.getMakerCreateTriggerThreshold() + "), skipping");
                continue;
            }

            if(opportunityDTO.getHasInsufficientBaseFunds() || opportunityDTO.getHasInsufficientQuoteFunds()) {
                LOGGER.debug("Insufficient funds, skipping");
                continue;
            }

            if(!hasEnoughRemainingFunds(opportunityDTO)) {
                LOGGER.info("Not enough funds remaining for opportunity, skipping: " + opportunityDescr);
                continue;
            }

            if (hasActiveTriggerForPairAndType(opportunityDTO, triggerKeysBeingCancelled)) {
                LOGGER.info("Trigger for opportunity already active: " + opportunityDescr);
                continue;
            }

            final Set<String> triggerKeysForExchange
                    = newTriggerKeys.computeIfAbsent(opportunityDTO.getTriggerExchange(), a -> new HashSet<>());
            final String triggerKey = getKeyForActiveTrigger(opportunityDTO);
            if (triggerKeysForExchange.contains(triggerKey)) {
                LOGGER.info("Trigger for opportunity already creating: " + opportunityDescr);
                continue;
            }

            triggerKeysForExchange.add(triggerKey);
            opportunitiesForTriggerOrdersToCreate
                    .computeIfAbsent(opportunityDTO.getTriggerExchange(), a -> new ArrayList<>())
                    .add(opportunityDTO);
        }
        
        //updateFundsDistribution(opportunitiesForTriggerOrdersToCreate);

        return opportunitiesForTriggerOrdersToCreate;
    }

    private void updateFundsDistribution(Map<Exchange, List<MakerOpportunityDTO>> opportunitiesForTriggerOrdersToCreate) {
    }

    private boolean ourTriggerExists(MakerOpportunityDTO opportunityDTO) {
        if(MakerOpportunityType.BL.equals(opportunityDTO.getType())) {
            return activeOrders.has(
                    opportunityDTO.getTriggerExchange(),
                    CurrencyUtils.getPair(opportunityDTO.getBase(), opportunityDTO.getFromQuote()),
                    OrderType.BUY,
                    opportunityDTO.getTriggerHighestBid());
        } else if (MakerOpportunityType.SH.equals(opportunityDTO.getType())) {
            return activeOrders.has(
                    opportunityDTO.getTriggerExchange(),
                    CurrencyUtils.getPair(opportunityDTO.getBase(), opportunityDTO.getToQuote()),
                    OrderType.SELL,
                    opportunityDTO.getTriggerLowestAsk());
        }

        return false;
    }

    private boolean hasEnoughRemainingFunds(MakerOpportunityDTO opportunityDTO) {
        final Wallet triggerWallet = exchangeServices.get(opportunityDTO.getTriggerExchange()).getWallet();
        final Wallet partnerWallet = exchangeServices.get(opportunityDTO.getPartnerExchange()).getWallet();
        if(MakerOpportunityType.BL.equals(opportunityDTO.getType())) {
            return triggerWallet.getFreeAmount(opportunityDTO.getFromQuote()).compareTo(opportunityDTO.getFromQuoteAmount()) >= 0 &&
                    partnerWallet.getTotalAmount(opportunityDTO.getBase()).compareTo(opportunityDTO.getBaseAmount()) >= 0;
        } else {
            return triggerWallet.getFreeAmount(opportunityDTO.getBase()).compareTo(opportunityDTO.getBaseAmount()) >= 0 &&
                    partnerWallet.getTotalAmount(opportunityDTO.getFromQuote()).compareTo(opportunityDTO.getFromQuoteAmount()) >= 0;
        }
    }

    private synchronized CreateOrderResult createSingleTriggerOrder(MakerOpportunityDTO opportunityDTO) {
        CreateOrderResult order = null;
        try {

            if (MakerOpportunityType.BL.equals(opportunityDTO.getType())) {
                order = createBuyLowTriggerOrder(opportunityDTO);
            } else if (MakerOpportunityType.SH.equals(opportunityDTO.getType())) {
                order = createSellHighTriggerOrder(opportunityDTO);
            } else {
                throw new IllegalArgumentException("Invalid opportunity type: " + opportunityDTO.getType());
            }
        } catch (TradeException e) {
            final String message = "Unable to create trigger order for opportunity: " + getOpportunityDescr(opportunityDTO) + ", cause: " + e.getMessage();
            LOGGER.error(message);
            notificationService.send(message, NotificationService.Level.DEBUG);
        }
        return order;
    }

    private synchronized void saveCreatedTriggerOrder(MakerOpportunityDTO opportunityDTO, CreateOrderResult order) {
        notify("New maker trigger: " + getOpportunityDescr(opportunityDTO) + " (exchange ID: " + order.getOrderId() + ")", NotificationService.Level.DEBUG);

        try {
            final MakerTriggerOrderDTO makerTriggerOrderDTO = new MakerTriggerOrderDTO();
            makerTriggerOrderDTO.setOpportunity(opportunityDTO);
            makerTriggerOrderDTO.setAmount(order.getAmount());
            makerTriggerOrderDTO.setCreateDatetime(new Date());
            makerTriggerOrderDTO.setExchangeOrderId(order.getOrderId());
            makerTriggerOrderDTO.setPair(order.getPair());
            makerTriggerOrderDTO.setPrice(order.getPrice());
            makerTriggerOrderDTO.setStatus(ExchangeOrder.Status.CREATED);

            saveActiveTrigger(makerTriggerOrderDTO);
        } catch (Exception e) {
            LOGGER.error("Error while creating trigger order: " + e.getMessage());
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * Go through active triggers and find the ones that art outdated or not profitable anymore
     * @param newOpportunities
     */
    public synchronized Map<Exchange, List<MakerTriggerOrderDTO>> getTriggersToCancel(NavigableMap<Double, MakerOpportunityDTO> newOpportunities) {
        final Map<Exchange, List<MakerTriggerOrderDTO>> triggersToCancel = new HashMap<>();

        for(Exchange exchange : activeTriggers.keySet()) {
            final List<MakerTriggerOrderDTO> triggersToCancelOnExchange = triggersToCancel.computeIfAbsent(exchange, a -> new ArrayList<>());

            for(MakerTriggerOrderDTO activeTrigger : activeTriggers.get(exchange).values()) {
                final MakerOpportunityDTO currentOpportunity = findCurrentOpportunityForActiveTrigger(newOpportunities, activeTrigger);

                final BigDecimal gain = currentOpportunity == null ?
                        BigDecimal.ZERO :
                        currentOpportunity.getToQuoteAmount().divide(currentOpportunity.getFromQuoteAmount(), 6, RoundingMode.HALF_UP);

                if (currentOpportunity == null || gain.compareTo(config.getMakerCancelTriggerThreshold()) < 0) {
                    notify("Not profitable anymore (gain " + gain + ") marking for canceling: " + getOpportunityDescr(activeTrigger.getOpportunity()),
                            NotificationService.Level.DEBUG);

                    triggersToCancelOnExchange.add(activeTrigger);
                    //cancelTrigger(activeTrigger);
                } else if (currentOpportunity.getTriggerPrice().compareTo(activeTrigger.getPrice()) != 0 &&
                        activeTrigger.getPrice().compareTo(
                                MakerOpportunityType.SH.equals(currentOpportunity.getType())
                                ? currentOpportunity.getTriggerLowestAsk()
                                        : currentOpportunity.getTriggerHighestBid()
                        ) != 0) {
                    notify("Trigger price changed (new: " + currentOpportunity.getTriggerPrice()
                                    + ", old: " + activeTrigger.getPrice() + "), canceling: " + getOpportunityDescr(activeTrigger.getOpportunity()),
                            NotificationService.Level.DEBUG);

                    triggersToCancelOnExchange.add(activeTrigger);
                    //cancelTrigger(activeTrigger);
                }
            }
        }

        return triggersToCancel;
    }

    public synchronized Map<Exchange, List<MakerTriggerOrderDTO>> handleAsyncOrderChanges() {
        LOGGER.info("Checking asynchronous order changes");

        final List<MakerTriggerOrder> openOrdersFromDb = makerTriggerOrderRepository.findByStatus(ExchangeOrder.Status.CREATED);

        // get active trigger order IDs from memory
        final Map<Exchange, Set<String>> activeTriggerOrderIds = new HashMap<>();
        for(Exchange exchange : activeTriggers.keySet()) {
            activeTriggerOrderIds.put(
                    exchange,
                    activeTriggers.get(exchange).values().stream()
                            .map(MakerTriggerOrderDTO::getExchangeOrderId)
                            .collect(Collectors.toSet())
            );
        }

        final Map<Exchange, Set<MakerTriggerOrderDTO>> exchangeOrdersToCheckAndCancel = new HashMap<>();

        // cancel trigger orders present in DB but absent in memory
        for(MakerTriggerOrder order : openOrdersFromDb) {
            if (!activeTriggerOrderIds.containsKey(order.getOpportunity().getTriggerExchange()) ||
                    !activeTriggerOrderIds.get(order.getOpportunity().getTriggerExchange()).contains(order.getExchangeOrderId())) {
                final Set<MakerTriggerOrderDTO> orders
                        = exchangeOrdersToCheckAndCancel.computeIfAbsent(order.getOpportunity().getTriggerExchange(), a -> new HashSet<>());
                orders.add(modelMapper.map(order, MakerTriggerOrderDTO.class));
            }
        }

        LOGGER.info("Exchange orders to check and cancel: " + exchangeOrdersToCheckAndCancel);

        return syncWithExchangeStatus(exchangeOrdersToCheckAndCancel);
    }

    public synchronized void cancelAllTriggerOrders() {
        LOGGER.info("Canceling all trigger orders");

        for(Map<String, MakerTriggerOrderDTO> triggerOrders : activeTriggers.values()) {
            for(MakerTriggerOrderDTO makerTriggerOrderDTO : triggerOrders.values()) {
                cancelSingleTrigger(makerTriggerOrderDTO);
            }
        }
    }

    private synchronized Map<Exchange, List<MakerTriggerOrderDTO>> syncWithExchangeStatus(Map<Exchange, Set<MakerTriggerOrderDTO>> exchangeOrdersToCheckAndCancel) {
        final Map<Exchange, List<MakerTriggerOrderDTO>> ordersToCancel = new HashMap<>();

        for(Exchange exchange : exchangeServices.keySet()) {
            Map<String, ExchangeOrder> exchangeOpenOrders = null;
            int tries = 0;
            int maxTries = 2;
            do {
                try {
                    exchangeOpenOrders = exchangeServices.get(exchange).getOpenOrders();
                } catch (ExchangeApiException e) {
                    LOGGER.error("Cannot get exchange open orders from " + exchange + ": " + e.getMessage());
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        throw new RuntimeException(ex);
                    }

                    if(tries == maxTries) {
                        exchangeOpenOrders = new HashMap<>();
                    }
                }
            } while(tries++ <= maxTries && exchangeOpenOrders == null);

            final List<MakerTriggerOrderDTO> ordersToCancelOnExchange = ordersToCancel.computeIfAbsent(exchange, a -> new ArrayList<>());

            LOGGER.info("Exchange " + exchange + " open orders: " + exchangeOpenOrders.values().stream().map(ExchangeOrder::getId).collect(Collectors.toList()));

            final Set<MakerTriggerOrderDTO> triggersToCheck = new HashSet<>();
            Set<String> exchangeOrderIdsToForceCancel = new HashSet<>();
            if(activeTriggers.containsKey(exchange)) {
                triggersToCheck.addAll(activeTriggers.get(exchange).values());
            }
            if(exchangeOrdersToCheckAndCancel.containsKey(exchange)) {
                triggersToCheck.addAll(exchangeOrdersToCheckAndCancel.get(exchange));
                exchangeOrderIdsToForceCancel =
                        exchangeOrdersToCheckAndCancel.get(exchange).stream().map(MakerTriggerOrderDTO::getExchangeOrderId).collect(Collectors.toSet());
            }

            // go through orders which we still register as open orders
            for(MakerTriggerOrderDTO triggerOrderDTO : triggersToCheck) {
                ExchangeOrder openOrder = exchangeOpenOrders.remove(triggerOrderDTO.getExchangeOrderId());
                if (openOrder == null || BigDecimal.ZERO.compareTo(openOrder.getExecutedAmount()) < 0) {
                    if(openOrder == null) {
                        LOGGER.info("Locally registered exchange order " + triggerOrderDTO.getExchangeOrderId() + " not found in exchange open order list, querying order detail");
                    } else {
                        LOGGER.info("Exchange order " + triggerOrderDTO.getExchangeOrderId() + " executed amount bigger than 0 (" + openOrder.getExecutedAmount() + "), querying order detail");
                    }
                    openOrder = exchangeServices.get(exchange).getOrderDetail(triggerOrderDTO.getExchangeOrderId()).orElse(null);
                }

                if (openOrder == null) {
                    final String message = "Cannot get details for order id " + triggerOrderDTO.getExchangeOrderId() + " on exchange " + exchange;
                    LOGGER.error(message);
                    notificationService.send(message, NotificationService.Level.DEBUG);
                    continue;
                }

                boolean isExecuted = BigDecimal.ZERO.compareTo(openOrder.getExecutedAmount()) < 0;

                if (isExecuted) {
                    notify("Order " + triggerOrderDTO.getExchangeOrderId() + " (status=" + openOrder.getStatus() + ") asynchronously executed ("  + openOrder.getExecutedAmount() + " " + openOrder.getBase() + ") on exchange " + exchange + ", finishing arbitrage on " + triggerOrderDTO.getOpportunity().getPartnerExchange(),
                            NotificationService.Level.INFO);

                    if(ExchangeOrder.Status.PARTIALLY_EXECUTED_CANCELLED.equals(openOrder.getStatus())) {
                        notify("Order " + triggerOrderDTO.getExchangeOrderId() + " already cancelled, not finishing on partner exchange", NotificationService.Level.ERROR);
                    } else {
                        final MakerTriggerOrderDTO makerTriggerToCancel = finishArbitrage(triggerOrderDTO, openOrder);
                        if (makerTriggerToCancel != null) {
                            ordersToCancelOnExchange.add(makerTriggerToCancel);
                        }
                    }
                } else if (ExchangeOrder.Status.CANCELLED.equals(openOrder.getStatus()) || ExchangeOrder.Status.PARTIALLY_EXECUTED_CANCELLED.equals(openOrder.getStatus())) {
                    cleanupCanceledTriggerOrderReferences(triggerOrderDTO);
                } else if (exchangeOrderIdsToForceCancel.contains(triggerOrderDTO.getExchangeOrderId())) {
                    ordersToCancelOnExchange.add(triggerOrderDTO);
                }
            }

            // process the exchange order which we could not map to locally registered ones
            for (ExchangeOrder order : exchangeOpenOrders.values()) {
                final Optional<MakerTriggerOrder> triggerOrder = makerTriggerOrderRepository.findByExchangeOrderId(order.getId());

                if (triggerOrder.isEmpty()) {
                    notify("Order " + order + " from exchange " + exchange + " could not be found in the local register",
                            NotificationService.Level.WARN);
                } else {
                    notify("Order " + order + " from exchange " + exchange + " found in local register but is out of sync with exchange: " + triggerOrder.get(),
                            NotificationService.Level.ERROR);
                }
            }

            ordersToCancel.put(exchange, ordersToCancelOnExchange);
        }

        return ordersToCancel;
    }

    private MakerTriggerOrderDTO finishArbitrage(MakerTriggerOrderDTO triggerOrderDTO, ExchangeOrder triggerOrder) {
        MakerTriggerOrderDTO orderToCancel = null;

        triggerOrderDTO.setExecutedAmount(triggerOrder.getExecutedAmount());
        triggerOrderDTO.setUpdateDatetime(new Date());
        triggerOrderDTO.setStatus(triggerOrder.getStatus());

        final MakerTriggerOrder makerTriggerOrder = modelMapper.map(triggerOrderDTO, MakerTriggerOrder.class);
        makerTriggerOrderRepository.save(makerTriggerOrder);

        final MakerOpportunityDTO opportunityDTO = triggerOrderDTO.getOpportunity();

        if(ExchangeOrder.Status.PARTIALLY_EXECUTED.equals(triggerOrder.getStatus())) {
            notify("Marking for cancelling partially executed trigger order " + triggerOrder.getId() + " on exchange " + opportunityDTO.getTriggerExchange());

            orderToCancel = triggerOrderDTO;
            // cancelTrigger(triggerOrderDTO);
        } else {
            cleanupCanceledTriggerOrderReferences(triggerOrderDTO);
        }

        try {
            final Exchange partnerExchange = opportunityDTO.getPartnerExchange();
            final MakerTrade makerTrade = new MakerTrade();
            makerTrade.setTriggerOrder(makerTriggerOrder);
            makerTrade.setBase(opportunityDTO.getBase());
            makerTrade.setDepositStatus(TransactionStatus.WAITING);
            makerTrade.setWithdrawStatus(TransactionStatus.WAITING);
            makerTrade.setFromQuote(opportunityDTO.getFromQuote());
            makerTrade.setToQuote(opportunityDTO.getToQuote());
            makerTrade.setDatetime(new Date());
            makerTrade.setBaseAmount(triggerOrder.getExecutedAmount());

            if (MakerOpportunityType.BL.equals(opportunityDTO.getType())) {
                final SellResult sellResult = exchangeServices.get(partnerExchange).sell(
                        CurrencyUtils.getPair(opportunityDTO.getBase(), opportunityDTO.getToQuote()),
                        triggerOrder.getExecutedAmount()
                );

                makerTrade.setInitialExchange(opportunityDTO.getTriggerExchange());
                makerTrade.setFinalExchange(partnerExchange);
                makerTrade.setSellFee(sellResult.getSellFee());
                makerTrade.setSellFeeCurrency(sellResult.getSellFeeCurrency());
                makerTrade.setBuyFee(triggerOrder.getExecutedFee());
                makerTrade.setBuyFeeCurrency(triggerOrder.getExecutedFeeCurrency());
                makerTrade.setBuyQuoteAmount(triggerOrder.getExecutedAmount().multiply(triggerOrder.getPrice()));
                makerTrade.setSellQuoteAmount(sellResult.getReceivedAmount());
                makerTrade.setInitialExchangeOrderId(triggerOrder.getId());
                makerTrade.setFinalExchangeOrderId(sellResult.getOrderId());
            } else if (MakerOpportunityType.SH.equals(opportunityDTO.getType())) {
                final BuyResult buyResult = exchangeServices.get(partnerExchange).buy(
                        CurrencyUtils.getPair(opportunityDTO.getBase(), opportunityDTO.getFromQuote()),
                        triggerOrder.getExecutedAmount(),
                        false
                );

                makerTrade.setInitialExchange(partnerExchange);
                makerTrade.setFinalExchange(opportunityDTO.getTriggerExchange());
                makerTrade.setBuyFee(buyResult.getBuyFee());
                makerTrade.setBuyFeeCurrency(buyResult.getBuyFeeCurrency());
                makerTrade.setSellFee(triggerOrder.getExecutedFee());
                makerTrade.setSellFeeCurrency(triggerOrder.getExecutedFeeCurrency());
                makerTrade.setBuyQuoteAmount(buyResult.getInitialAmount());
                makerTrade.setSellQuoteAmount(triggerOrder.getExecutedAmount().multiply(triggerOrder.getPrice()));
                makerTrade.setInitialExchangeOrderId(buyResult.getOrderId());
                makerTrade.setFinalExchangeOrderId(triggerOrder.getId());
            }

            makerTradeRepository.save(makerTrade);

            notify(getExecutedArbitrageDescription(makerTrade));
        } catch (Exception e) {
            notify("Unable to complete arbitrage: " + e.getMessage());
            e.printStackTrace();
            notify("Disabling maker trading");
            config.setMakerTradingEnabled(false);
            cancelAllTriggerOrders();
        }

        return orderToCancel;
    }

    private static String getExecutedArbitrageDescription(MakerTrade makerTrade) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Maker arbitrage\n")
                .append(CurrencyUtils.getPair(makerTrade.getBase(), makerTrade.getFromQuote()))
                .append(" -> ")
                .append(CurrencyUtils.getPair(makerTrade.getBase(), makerTrade.getToQuote()))
                .append("\n");
        sb.append(makerTrade.getInitialExchange())
                .append(" -> ")
                .append(makerTrade.getFinalExchange())
                .append("\n");
        // TODO: include the fees
        sb.append("Initial amount: ")
                .append(String.format("%.2f", makerTrade.getBuyQuoteAmount()))
                .append("\n");
        sb.append("Final amount: ")
                .append(String.format("%.2f", makerTrade.getSellQuoteAmount()))
                .append("\n");
        sb.append("ID: ").append(makerTrade.getId());

        return sb.toString();
    }

    private void cancelSingleTrigger(MakerTriggerOrderDTO triggerToCancel) {
        final String key = getKeyForActiveTrigger(triggerToCancel.getOpportunity());
        final Exchange exchange = triggerToCancel.getOpportunity().getTriggerExchange();

        LOGGER.info("Canceling trigger order " + triggerToCancel.getExchangeOrderId() + " (" + key + ") on " + exchange);

        final CancelOrderResult result = exchangeServices.get(exchange).cancel(triggerToCancel.getExchangeOrderId());
        if(!result.isSuccess()) {
            final Optional<ExchangeOrder> orderDetail = exchangeServices.get(exchange).getOrderDetail(triggerToCancel.getExchangeOrderId());
            if(orderDetail.isEmpty() || !ExchangeOrder.Status.CANCELLED.equals(orderDetail.get().getStatus())) {
                notify("Unable to cancel order " + triggerToCancel.getExchangeOrderId() + " (" + key +
                        ") on exchange " + exchange + ": " + result.getError(), NotificationService.Level.ERROR);
                return;
            } else {
                LOGGER.info("Order " + triggerToCancel.getExchangeOrderId() + " on " + exchange + " already cancelled");
            }
        }

        cleanupCanceledTriggerOrderReferences(triggerToCancel);

    }

    private synchronized void cleanupCanceledTriggerOrderReferences(MakerTriggerOrderDTO triggerToCancel) {
        final String key = getKeyForActiveTrigger(triggerToCancel.getOpportunity());
        final Exchange exchange = triggerToCancel.getOpportunity().getTriggerExchange();
        final MakerOpportunityDTO opportunity = triggerToCancel.getOpportunity();

        if(!ExchangeOrder.Status.PARTIALLY_EXECUTED.equals(triggerToCancel.getStatus())) {
            LOGGER.info("Saving CANCELLED status for " + exchange + " order " + triggerToCancel.getExchangeOrderId() + " (" + triggerToCancel.getId() + ")");
            makerTriggerOrderRepository.setStatus(triggerToCancel.getId(), ExchangeOrder.Status.CANCELLED);
        }

        if(activeTriggers.get(exchange) != null) {
            activeTriggers.get(exchange).remove(key);
        }

        try {
            activeOrders.remove(exchange,
                    triggerToCancel.getPair(),
                    opportunity.getType().equals(MakerOpportunityType.BL)
                            ? OrderType.BUY
                            : OrderType.SELL,
                    triggerToCancel.getPrice(),
                    triggerToCancel.getAmount());

            final Wallet partnerWallet = exchangeServices.get(opportunity.getPartnerExchange()).getWallet();
            final Wallet triggerWallet = exchangeServices.get(opportunity.getTriggerExchange()).getWallet();
            if(opportunity.getType().equals(MakerOpportunityType.BL)) {
                //partnerWallet.unlockAmount(opportunity.getBase(), opportunity.getBaseAmount());
                triggerWallet.addAmount(opportunity.getFromQuote(), opportunity.getFromQuoteAmount());
            } else {
                //partnerWallet.unlockAmount(opportunity.getFromQuote(), opportunity.getFromQuoteAmount());
                triggerWallet.addAmount(opportunity.getBase(), opportunity.getBaseAmount());
            }
        } catch (Exception e) {
            LOGGER.error("Unable to remove order from local ActiveOrders register: " + e);
        }
    }

    private MakerOpportunityDTO findCurrentOpportunityForActiveTrigger(NavigableMap<Double, MakerOpportunityDTO> opportunities, MakerTriggerOrderDTO activeTrigger) {
        for(MakerOpportunityDTO currentOpportunity : opportunities.values()) {
            if(activeTrigger.getOpportunity().getTriggerExchange().equals(currentOpportunity.getTriggerExchange()) &&
                    getKeyForActiveTrigger(currentOpportunity).equals(getKeyForActiveTrigger(activeTrigger.getOpportunity()))) {
                return currentOpportunity;
            }
        }

        return null;
    }

    private CreateOrderResult createSellHighTriggerOrder(MakerOpportunityDTO opportunityDTO) throws TradeException {
        final String pair = CurrencyUtils.getPair(opportunityDTO.getBase(), opportunityDTO.getToQuote());
        final BigDecimal price = opportunityDTO.getTriggerPrice();

        return exchangeServices.get(opportunityDTO.getTriggerExchange()).limitSell(
                pair,
                opportunityDTO.getBaseAmount(),
                price
        );
    }

    private CreateOrderResult createBuyLowTriggerOrder(MakerOpportunityDTO opportunityDTO) throws TradeException {
        final String pair = CurrencyUtils.getPair(opportunityDTO.getBase(), opportunityDTO.getFromQuote());
        final BigDecimal price = opportunityDTO.getTriggerPrice();

        return exchangeServices.get(opportunityDTO.getTriggerExchange()).limitBuy(
                pair,
                opportunityDTO.getBaseAmount(),
                price
        );
    }

    private void saveActiveTrigger(MakerTriggerOrderDTO makerTriggerOrderDTO) {
        final MakerOpportunityDTO opportunityDTO = makerTriggerOrderDTO.getOpportunity();
        final Exchange triggerExchange = opportunityDTO.getTriggerExchange();
        final Exchange partnerExchange = opportunityDTO.getPartnerExchange();

        final Map<String, MakerTriggerOrderDTO> triggersOnExchange = activeTriggers.computeIfAbsent(triggerExchange, k -> new ConcurrentHashMap<>());
        triggersOnExchange.put(getKeyForActiveTrigger(opportunityDTO), makerTriggerOrderDTO);

        MakerTriggerOrder savedOrder = makerTriggerOrderRepository.save(modelMapper.map(makerTriggerOrderDTO, MakerTriggerOrder.class));
        makerTriggerOrderDTO.setId(savedOrder.getId());

        // TODO: duplicate register of open orders, refactor to a single register
        OrderType orderType = opportunityDTO.getType().equals(MakerOpportunityType.BL)
                ? OrderType.BUY
                : OrderType.SELL;

        activeOrders.add(triggerExchange,
                orderType,
                makerTriggerOrderDTO.getPair(),
                makerTriggerOrderDTO.getPrice(),
                makerTriggerOrderDTO.getAmount()
        );

        final String tickerToLock = opportunityDTO.getType().equals(MakerOpportunityType.BL)
                ? opportunityDTO.getBase()
                : opportunityDTO.getFromQuote();

        final BigDecimal amountToLock = opportunityDTO.getType().equals(MakerOpportunityType.BL)
                ? opportunityDTO.getBaseAmount()
                : opportunityDTO.getFromQuoteAmount();

        final Wallet partnerWallet = exchangeServices.get(partnerExchange).getWallet();
        if(partnerWallet.getLockedAmount(tickerToLock).compareTo(amountToLock) < 0) {
            partnerWallet.setLockedAmount(tickerToLock, amountToLock);
        }
    }

    private boolean hasActiveTriggerForPairAndType(MakerOpportunityDTO opportunityDTO, Map<Exchange, Set<String>> triggerKeysBeingCancelled) {
        final Map<String, MakerTriggerOrderDTO> triggersOnExchange = activeTriggers.get(opportunityDTO.getTriggerExchange());
        final Set<String> triggersBeingCancelled = triggerKeysBeingCancelled.get(opportunityDTO.getTriggerExchange());
        final String key = getKeyForActiveTrigger(opportunityDTO);

        return triggersOnExchange != null && triggersOnExchange.containsKey(key) &&
                (triggersBeingCancelled == null || !triggersBeingCancelled.contains(key));
    }

    private static String getKeyForActiveTrigger(MakerOpportunityDTO opportunityDTO) {
        final String pair = CurrencyUtils.getPair(opportunityDTO.getBase(),
                MakerOpportunityType.BL.equals(opportunityDTO.getType())
                        ? opportunityDTO.getFromQuote()
                        : opportunityDTO.getToQuote());

        return pair + "-" + opportunityDTO.getType();
    }

    private static String getOpportunityDescr(MakerOpportunityDTO opportunityDTO) {
        final String arrow = opportunityDTO.getType() == MakerOpportunityType.BL ? "->" : "<-";
        final String triggerQuote = opportunityDTO.getType() == MakerOpportunityType.BL ? opportunityDTO.getFromQuote() : opportunityDTO.getToQuote();
        final BigDecimal amount = opportunityDTO.getType() == MakerOpportunityType.BL ? opportunityDTO.getFromQuoteAmount() : opportunityDTO.getToQuoteAmount();

        return opportunityDTO.getTriggerExchange() + arrow + opportunityDTO.getPartnerExchange()
                + "(" + opportunityDTO.getType() + "):" + CurrencyUtils.getPair(opportunityDTO.getBase(), triggerQuote) + "-" + amount + "@" + opportunityDTO.getTriggerPrice();
    }

    private void notify(String message) {
        notify(message, NotificationService.Level.INFO);
    }

    private void notify(String message, NotificationService.Level level) {
        if (NotificationService.Level.ERROR.equals(level)) {
            LOGGER.error(message);
        } else {
            LOGGER.info(message);
        }
        notificationService.send(message, level);
    }

    public String getStatusString() {
        final StringBuilder sb = new StringBuilder();

        for(Exchange exchange : activeTriggers.keySet()) {
            sb.append("Active maker triggers on ").append(exchange.toString()).append(":\n");

            for(MakerTriggerOrderDTO triggerOrderDTO : activeTriggers.get(exchange).values()) {
                sb.append(getOpportunityDescr(triggerOrderDTO.getOpportunity())).append("\n");
            }
        }

        return sb.toString();
    }

    public void stopMakerTrading() {
        handleAsyncOrderChanges();
        cancelAllTriggerOrders();

        for(IExchangeService exchangeService : exchangeServices.values()) {
            exchangeService.getWallet().resetLockedAmounts();
        }
    }

    public void setMassExecutionEnabled(boolean massExecutionEnabled) {
        this.massExecutionEnabled = massExecutionEnabled;
    }
}
