package com.tkelemen.arbitrader.service.arbitrage.funding;

import com.tkelemen.arbitrader.exception.FundingException;
import com.tkelemen.arbitrader.model.Exchange;

import java.math.BigDecimal;

public interface FundingService {
    void setExchangeFundingLimit(Exchange exchange, String currency, BigDecimal amount);

    BigDecimal getCurrentFunding(Exchange exchange, String currency);

    BigDecimal getFundingLimit(Exchange exchange, String currency);

    boolean canBeFunded(Exchange exchange, String currency, BigDecimal amount);

    void fund(Exchange exchange, String currency, BigDecimal amount) throws FundingException;

    void repay(Exchange exchange, String currency, BigDecimal amount) throws FundingException;
}
