package com.tkelemen.arbitrader.service.arbitrage.withdraw;

import com.tkelemen.arbitrader.exception.AddressNotFoundException;
import com.tkelemen.arbitrader.exception.WithdrawException;
import com.tkelemen.arbitrader.model.Address;
import com.tkelemen.arbitrader.model.WithdrawResult;
import com.tkelemen.arbitrader.service.trade.KrakenExchangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class KrakenWithdrawExecutorService implements WithdrawExecutor {
    private final KrakenExchangeService krakenExchangeService;

    @Autowired
    public KrakenWithdrawExecutorService(KrakenExchangeService krakenExchangeService) {
        this.krakenExchangeService = krakenExchangeService;
    }


    @Override
    public WithdrawResult initiateWithdraw(String asset, BigDecimal amount, Address destination) throws WithdrawException {
        if(destination.getAlias() == null || destination.getAlias().isEmpty()) {
            throw new WithdrawException("Withdrawal alias missing for asset " + asset + " and destination " + destination);
        }
        return krakenExchangeService.withdraw(asset, amount, destination, getAddressName(asset, destination));
    }

    @Override
    public WithdrawResult confirmWithdraw(String asset, BigDecimal amount, Address destination, String confirmUrl) {
        // not applicable for Kraken
        return null;
    }

    private String getAddressName(String asset, Address destination) {
        String prefix = destination.getExchange() != null ?
                destination.getExchange().name() : destination.getAddress().substring(destination.getAddress().length() - 4);
        return prefix + "_" + asset;
    }
}
