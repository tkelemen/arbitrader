package com.tkelemen.arbitrader.service.trade;

import com.tkelemen.arbitrader.client.kraken.domain.*;
import com.tkelemen.arbitrader.client.kraken.rest.AddOrderClient;
import com.tkelemen.arbitrader.client.kraken.rest.KrakenGetBalanceClient;
import com.tkelemen.arbitrader.client.kraken.rest.GetOrderInfoClient;
import com.tkelemen.arbitrader.client.kraken.rest.WithdrawFundsClient;
import com.tkelemen.arbitrader.db.entity.Arbitrage;
import com.tkelemen.arbitrader.exception.RestClientException;
import com.tkelemen.arbitrader.exception.TradeException;
import com.tkelemen.arbitrader.model.*;
import com.tkelemen.arbitrader.service.market.FeeService;
import com.tkelemen.arbitrader.util.CurrencyUtils;
import com.tkelemen.arbitrader.util.TradeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class KrakenExchangeService implements IExchangeService {
    public static final Map<String, String> ASSET_MAP = new HashMap<>();
    static {
        ASSET_MAP.put("ZEUR", "EUR");
        ASSET_MAP.put("XLTC", "LTC");
        ASSET_MAP.put("XETH", "ETH");
        ASSET_MAP.put("ZUSD", "USD");
        ASSET_MAP.put("XXBT", "BTC");
        ASSET_MAP.put("XXRP", "XRP");
        ASSET_MAP.put("XXLM", "XLM");
        ASSET_MAP.put("XETC", "ETC");
        ASSET_MAP.put("XMLN", "MLN");
        ASSET_MAP.put("XREP", "REP");
        ASSET_MAP.put("XXMR", "XMR");
        ASSET_MAP.put("XZEC", "ZEC");
        ASSET_MAP.put("ZCAD", "CAD");
        ASSET_MAP.put("ZGBP", "GBP");
        ASSET_MAP.put("ZJPY", "JPY");
        ASSET_MAP.put("ZAUD", "AUD");
        ASSET_MAP.put("XXDG", "DOGE");
    }
    private static final Logger LOGGER = LoggerFactory.getLogger(KrakenExchangeService.class);

    private final AddOrderClient addOrderClient;
    private final GetOrderInfoClient getOrderInfoClient;
    private final KrakenGetBalanceClient getBalanceClient;
    private final WithdrawFundsClient withdrawFundsClient;
    private final FeeService feeService;
    private final Wallet walletCache = new Wallet();
    private volatile boolean isTransactionInProgress = false;

    @Autowired
    public KrakenExchangeService(AddOrderClient addOrderClient,
                                 GetOrderInfoClient getOrderInfoClient,
                                 KrakenGetBalanceClient getBalanceClient,
                                 WithdrawFundsClient withdrawFundsClient,
                                 FeeService feeService) {
        this.addOrderClient = addOrderClient;
        this.getOrderInfoClient = getOrderInfoClient;
        this.getBalanceClient = getBalanceClient;
        this.withdrawFundsClient = withdrawFundsClient;
        this.feeService = feeService;
    }

    @Override
    public BuyResult buy(String pair, BigDecimal amount, boolean isAmountInQuoteCurrency) {
        final BuyResult result = new BuyResult();
        result.setPair(pair);

        LOGGER.info("Creating MARKET BUY order for pair " + pair + " [ amount = " + amount + ", isAmountInQuoteCurrency = " + isAmountInQuoteCurrency + " ]");

        String quote = CurrencyUtils.getQuote(pair);

        LOGGER.info("Free in wallet: " + walletCache.getFreeAmount(quote) + " " + quote);
        AddOrderResponse response = null;
        try {
            response = addOrderClient.buy(OrderType.MARKET, pair, amount, isAmountInQuoteCurrency);
        } catch (RestClientException e) {
            LOGGER.error("REST client error while creating order: " + e);
        }

        if (response == null || (response.getError() != null && !response.getError().isEmpty())) {
            final String errorMessage = "Buy order not filled: " + response;
            LOGGER.error(errorMessage);
            result.setBuyStatus(Arbitrage.STATUS_FAIL);
            result.setErrorMessage(errorMessage);
            return result;
        }

        if (response.getResult().getTxid().size() != 1) {
            final String errorMessage = "Incorrect transaction count for buy order: " + response;
            LOGGER.error(errorMessage);
            result.setBuyStatus(Arbitrage.STATUS_FAIL);
            result.setErrorMessage(errorMessage);
            return result;
        }

        final String txId = response.getResult().getTxid().get(0);

        int maxTries = 10;
        int tries = 0;

        GetOrderInfoResponse getOrderResponse = null;
        try {
            do {
                // TODO: solve this in a different way
                if(tries > 0) {
                    Thread.sleep(500L * tries);
                }
                LOGGER.info("Getting order details [ orderId = " + txId + " ] (#" + tries + ")");
                getOrderResponse = getOrderInfoClient.get(txId);
            } while ( isOrderPending(txId, getOrderResponse) && tries++ < maxTries);
        } catch (RestClientException | InterruptedException e) {
            e.printStackTrace();
        }

        LOGGER.info("Order details: " + getOrderResponse);

        if (getOrderResponse == null) {
            final String errorMessage = "Could not get order details: " + txId;
            LOGGER.error(errorMessage);
            result.setErrorMessage(errorMessage);
        } else {
            final GetOrderInfoResponse.Order orderDetail = getOrderResponse.getResult().get(txId);

            result.setInitialAmount(orderDetail.getCost());
            result.setBuyFee(orderDetail.getFee());
            result.setBuyFeeCurrency(quote);
            result.setBoughtAmount(orderDetail.getVol_exec());
            result.setBuyStatus(Arbitrage.STATUS_OK);
            result.setOrderId(txId);
        }

        final BigDecimal withdrawFee = feeService.getWithdrawFee(Exchange.KRAKEN, CurrencyUtils.getBase(pair));
        result.setWithdrawFee(withdrawFee);

        walletCache.subtractAmount(quote, TradeUtils.getInitialExpenses(result));
        walletCache.addAmount(CurrencyUtils.getBase(pair), TradeUtils.getAmountToWithdraw(result));

        LOGGER.info("Wallet after buy: " + walletCache);

        return result;
    }

    @Override
    public CreateOrderResult limitBuy(String pair, BigDecimal amount, BigDecimal price) throws TradeException {
        return null;
    }

    @Override
    public SellResult sell(String pair, BigDecimal amount) throws TradeException {
        LOGGER.info("Creating MARKET SELL order for pair " + pair + " [ amount = " + amount + " ]");

        String base = CurrencyUtils.getBase(pair);
        String quote = CurrencyUtils.getQuote(pair);

        LOGGER.info("Free in wallet: " + walletCache.getFreeAmount(base) + " " + base);

        AddOrderResponse response = null;
        try {
            response = addOrderClient.sell(OrderType.MARKET, pair, amount);
        } catch (RestClientException e) {
            LOGGER.error("REST client error while creating order: " + e);
        }

        SellResult sellResult = new SellResult();
        if (response == null || (response.getError() != null && !response.getError().isEmpty())) {
            LOGGER.error("Unable to sell " + pair + " [ amount = " + amount + " ]: " + response);
            sellResult.setStatus(Arbitrage.STATUS_FAIL);
            if(response != null) {
                sellResult.setErrorMessage(String.join(",", response.getError()));
            }
            return sellResult;
        }

        LOGGER.info("Sell order executed for pair " + pair + ": " + response);

        if (response.getResult().getTxid().size() != 1) {
            String errorMessage = "Incorrect transaction count for buy order: " + response;
            LOGGER.error(errorMessage);
        }

        String txId = response.getResult().getTxid().get(0);

        int maxTries = 10;
        int tries = 0;

        GetOrderInfoResponse getOrderResponse = null;
        try {
            do {
                // TODO: solve this in a different way
                if(tries > 0) {
                    Thread.sleep(500L * tries);
                }
                LOGGER.info("Getting order details [ orderId = " + txId + " ] (#" + tries + ")");
                getOrderResponse = getOrderInfoClient.get(txId);
            } while ( isOrderPending(txId, getOrderResponse) && tries++ < maxTries);
        } catch (RestClientException | InterruptedException e) {
            e.printStackTrace();
        }

        LOGGER.info("Order details: " + getOrderResponse);

        BigDecimal receivedAmount;
        BigDecimal sellFee;
        BigDecimal soldAmount;

        sellResult.setOrderId(txId);

        if (getOrderResponse == null || getOrderResponse.getResult().isEmpty()) {
            String errorMessage = "Could not get order details for TX " + txId;
            LOGGER.error(errorMessage);

            sellResult.setErrorMessage(errorMessage);
            sellResult.setStatus(Arbitrage.STATUS_FAIL);
            return sellResult;
        } else {
            GetOrderInfoResponse.Order orderDetail = getOrderResponse.getResult().get(txId);

            if(!GetOrderInfoResponse.Order.STATUS_CANCELLED.equals(orderDetail.getStatus())) {
                receivedAmount = orderDetail.getCost();
            } else {
                sellResult.setErrorMessage("Error while executing sell order " + txId + ": order cancelled");
                sellResult.setStatus(Arbitrage.STATUS_FAIL);
                return sellResult;
            }

            sellFee = orderDetail.getFee();
            soldAmount = orderDetail.getVol_exec();
        }

        sellResult.setSellFeeCurrency(quote);
        sellResult.setSellFee(sellFee);
        sellResult.setReceivedAmount(receivedAmount);
        sellResult.setSoldAmount(soldAmount);
        sellResult.setStatus(Arbitrage.STATUS_OK);

        walletCache.addAmount(quote, receivedAmount.subtract(sellFee));
        walletCache.subtractAmount(base, soldAmount);

        LOGGER.info("Wallet after sell: " + walletCache);

        return sellResult;
    }

    private boolean isOrderPending(String txId, GetOrderInfoResponse getOrderResponse) {
        return getOrderResponse == null ||
                getOrderResponse.getError() != null ||
                !getOrderResponse.getError().isEmpty() ||
                getOrderResponse.getResult().isEmpty() ||
                getOrderResponse.getResult().get(txId) == null ||
                GetOrderInfoResponse.Order.STATUS_OPEN.equals(getOrderResponse.getResult().get(txId).getStatus());
    }

    @Override
    public CreateOrderResult limitSell(String pair, BigDecimal amount, BigDecimal price) throws TradeException {
        return null;
    }

    @Override
    public WithdrawResult withdraw(String ticker, BigDecimal amount, Address address, String addressName)  {
        int maxTries = 5;
        int tries = 0;
        boolean isSuccess = false;
        WithdrawResponse withdrawResponse = null;

        do {
            try {
                withdrawResponse = withdrawFundsClient.withdraw(ticker, address.getAlias(), amount);
                LOGGER.info("Withdrawal detail: " + withdrawResponse);

                isSuccess = withdrawResponse.getError().isEmpty() && withdrawResponse.getResult().getRefid() != null && !withdrawResponse.getResult().getRefid().isEmpty();
                LOGGER.info("Withdrawal not successful, retrying");
            } catch (RestClientException e) {
                LOGGER.error("Withdrawal request failed: " + e);
            }
        } while (!isSuccess && tries++ < maxTries);

        WithdrawResult withdrawResult = new WithdrawResult(isSuccess ? WithdrawStage.REQUESTED : WithdrawStage.ERROR);
        withdrawResult.setSuccess(isSuccess);

        if(withdrawResponse != null) {
            withdrawResult.setMessage(String.join(",", withdrawResponse.getError()));
            withdrawResult.setWithdrawId(withdrawResponse.getResult() != null ? withdrawResponse.getResult().getRefid() : null);
        }

        return withdrawResult;
    }

    @Override
    public CancelOrderResult cancel(String orderId) {
        return null;
    }

    @Override
    public boolean hasSufficientFunds(String ticker, BigDecimal amount) {
        return amount.compareTo(walletCache.getFreeAmount(ticker)) <= 0;
    }

    @Override
    public void setAvailableFunds(String ticker, BigDecimal amount) {
        walletCache.setTotalAmount(ticker, amount);
    }

    @Override
    public BigDecimal getAvailableFunds(String ticker) {
        return null;
    }

    @Override
    public Wallet getWallet() {
        return walletCache;
    }

    @Override
    public synchronized void updateWallet() {
        try {
            final GetBalanceResponse response = getBalanceClient.get();

            if(response == null || response.getResult() == null) {
                LOGGER.error("Error updating Kraken wallet: empty response");
                return;
            }

            for (Map.Entry<String, BigDecimal> entry : response.getResult().entrySet()) {
                final String asset = ASSET_MAP.getOrDefault(entry.getKey(), entry.getKey());

                walletCache.setTotalAmount(asset, entry.getValue());
            }
        } catch (RestClientException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getAssetPrecision(String ticker) {
        return 0; // TODO: implement
    }

    @Override
    public int getPairPricePrecision(String pair) {
        // TODO: implement
        return 0;
    }

    @Override
    public Map<String, BigDecimal> getTradeVolumesByAssetPair() {
        return new HashMap<>();  // TODO: implement
    }

    @Override
    public Map<String, BigDecimal> getLastPricesByAssetPair()  {
        return new HashMap<>();  // TODO: implement
    }

    @Override
    public BigDecimal getPredictedTransferredBaseAmountAfterBuy(Opportunity opportunity) {
        BigDecimal buyFee = feeService.getTradingFeeTaker(Exchange.KRAKEN).multiply(opportunity.getTransferAmount());

        return opportunity.getTransferAmount().subtract(buyFee).subtract(opportunity.getWithdrawFee()).subtract(opportunity.getDepositFee());
    }

    @Override
    public Map<String, ExchangeOrder> getOpenOrders() {
        return new HashMap<>();
    }

    @Override
    public Optional<ExchangeOrder> getOrderDetail(String id) {
        return Optional.empty();
    }

}
