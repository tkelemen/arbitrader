package com.tkelemen.arbitrader.service.trade;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.OrderStatus;
import com.binance.api.client.domain.account.*;
import com.binance.api.client.domain.account.WithdrawResult;
import com.binance.api.client.domain.general.FilterType;
import com.binance.api.client.domain.general.SymbolFilter;
import com.binance.api.client.domain.general.SymbolInfo;
import com.binance.api.client.exception.BinanceApiException;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.db.entity.Arbitrage;
import com.tkelemen.arbitrader.exception.TradeException;
import com.tkelemen.arbitrader.model.*;
import com.tkelemen.arbitrader.service.market.BinanceOrderBookService;
import com.tkelemen.arbitrader.service.market.FeeService;
import com.tkelemen.arbitrader.service.market.OrderBookHelper;
import com.tkelemen.arbitrader.util.CurrencyUtils;
import com.tkelemen.arbitrader.util.TradeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service()
public class BinanceExchangeService implements IExchangeService {
    private static final Logger LOGGER = LoggerFactory.getLogger(BinanceExchangeService.class);

    private BinanceApiRestClient restClient = null;
    private final Wallet walletCache = new Wallet();
    private final Map<String, SymbolInfo> allSymbolsMap = new HashMap<>();;
    private final FeeService feeService;
    private final BinanceOrderBookService orderBookService;

    @Autowired
    public BinanceExchangeService(ApplicationConfig config, FeeService feeService, BinanceOrderBookService orderBookService) {
        this.feeService = feeService;
        this.orderBookService = orderBookService;

        try {
            restClient = BinanceApiClientFactory.newInstance(config.getBinanceApiKey(), config.getBinanceSecretKey()).newRestClient();
            updateExchangeInfo();
        } catch (BinanceApiException e) {
            LOGGER.error("Unable to instantiate Binance API client: " + e);
        }
    }

    private void updateExchangeInfo() {
        for (SymbolInfo symbolInfo : restClient.getExchangeInfo().getSymbols()) {
            allSymbolsMap.put(symbolInfo.getSymbol(), symbolInfo);
        }
    }

    @Override
    public BuyResult buy(String pair, BigDecimal amount, boolean isAmountInQuoteCurrency) {
        final BuyResult result = new BuyResult();
        result.setPair(pair);

        NewOrderResponse response;

        try {
            response = marketBuy(pair, amount, isAmountInQuoteCurrency);

            if (response.getStatus() != OrderStatus.FILLED) {
                throw new TradeException("Buy order not filled: " + response);
            }
        } catch (TradeException e) {
            LOGGER.error("Buy error: " + e);
            result.setBuyStatus(Arbitrage.STATUS_FAIL);
            result.setErrorMessage(e.getMessage());
            return result;
        }

        result.setInitialAmount(calculatePrice(response.getFills()));
        result.setBoughtAmount(new BigDecimal(response.getExecutedQty()));
        result.setBuyFee(calculateFee(response.getFills()));
        result.setBuyFeeCurrency(fromBinanceAsset(response.getFills().get(0).getCommissionAsset()));
        result.setOrderId(response.getOrderId().toString());

        final BigDecimal withdrawFee = feeService.getWithdrawFee(Exchange.BINANCE, CurrencyUtils.getBase(pair));
        result.setWithdrawFee(withdrawFee);
        result.setBuyStatus(Arbitrage.STATUS_OK);

        walletCache.subtractAmount(CurrencyUtils.getQuote(pair), TradeUtils.getInitialExpenses(result));
        walletCache.addAmount(CurrencyUtils.getBase(pair), TradeUtils.getAmountToWithdraw(result));

        return result;
    }

    @Override
    public CreateOrderResult limitBuy(String pair, BigDecimal amount, BigDecimal price) throws TradeException {
        return null;
    }

    private NewOrderResponse marketBuy(String pair, BigDecimal amount, boolean isAmountInQuoteCurrency) throws TradeException {
        final String symbol = toExchangeSymbol(pair);
        final String quantityStr = CurrencyUtils.formatCrypto(amount);

        LOGGER.info("Creating MARKET BUY order for pair " + symbol + " [ amount = " + quantityStr + ", isAmountInQuoteCurrency = " + isAmountInQuoteCurrency + " ]");
        LOGGER.info("Free in wallet " + walletCache.getFreeAmount(CurrencyUtils.getQuote(pair)) + " " + CurrencyUtils.getQuote(pair));

        boolean isQuoteOrderQtyMarketAllowed = allSymbolsMap.get(symbol).isQuoteOrderQtyMarketAllowed();

        NewOrder newOrder;
        if(isAmountInQuoteCurrency && isQuoteOrderQtyMarketAllowed) {
            newOrder = NewOrder.marketBuy(symbol, null);
            newOrder.quoteOrderQty(quantityStr);
        } else {
            final int scale = getSymbolTradingScale(symbol);
            final BigDecimal qtyToBuy = isAmountInQuoteCurrency
                    ? OrderBookHelper.calculateBaseQuantity(orderBookService.getOrderBookSnapshots().get(pair), amount)
                    : amount;
            newOrder = NewOrder.marketBuy(symbol, CurrencyUtils.formatCrypto(qtyToBuy, scale));
        }
        newOrder.newOrderRespType(NewOrderResponseType.FULL);

        try {
            final NewOrderResponse response = restClient.newOrder(newOrder);

            BigDecimal totalPrice = new BigDecimal(0);
            if (response.getStatus() == OrderStatus.FILLED) {
                totalPrice = calculatePrice(response.getFills());
            }

            LOGGER.info("MARKET BUY order executed [ status = " + response.getStatus() +
                    ", total price " + totalPrice +
                    ", order id = " + response.getClientOrderId() + " ] " + response);

            return response;
        } catch (BinanceApiException e) {
            throw new TradeException("Unable to execute MARKET BUY for " + pair + ": " + e.toString());
        }
    }

    private NewOrderResponse marketSell(String pair, BigDecimal quantity) throws TradeException {
        String symbol = toExchangeSymbol(pair);
        int scale = getSymbolTradingScale(symbol);
        quantity = quantity.setScale(scale, RoundingMode.DOWN);
        String quantityStr = CurrencyUtils.formatCrypto(quantity, scale);

        LOGGER.info("Creating MARKET SELL order for pair " + symbol + " [ quantity = " + quantityStr + " ]");
        LOGGER.info("Free in wallet " + walletCache.getFreeAmount(CurrencyUtils.getBase(pair)) + " " + CurrencyUtils.getBase(pair));

        try {
            NewOrderResponse response = restClient.newOrder(
                    NewOrder.marketSell(symbol, quantityStr).newOrderRespType(NewOrderResponseType.FULL)
            );

            BigDecimal totalPrice = new BigDecimal(0);
            if (response.getStatus() == OrderStatus.FILLED) {
                totalPrice = calculatePrice(response.getFills());
            }

            LOGGER.info("MARKET SELL order executed [ status = " + response.getStatus() +
                    ", total price " + totalPrice +
                    ", order id = " + response.getClientOrderId() + " ] " + response);

            return response;
        } catch (BinanceApiException e) {
            throw new TradeException("Unable to execute MARKET SELL for " + pair + ": " + e);
        }
    }

    public com.tkelemen.arbitrader.model.WithdrawResult withdraw(String ticker, BigDecimal amount, Address address, String addressName) {
        // TODO: get from config
        String network = "";
        if ("BNB".equals(ticker) || "BEL".equals(ticker)) {
            network = "BSC";
        }

        WithdrawResult binanceApiResult;
        int maxTries = 5;
        int tries = 0;
        boolean isSuccess;

        LOGGER.info("Requesting withdrawal: " + amount + " " + ticker + " to " + address);

        do {
            binanceApiResult = restClient.withdraw(
                    toBinanceAsset(ticker),
                    address.getAddress(),
                    amount.toPlainString(),
                    addressName,
                    address.getTag() != null ? address.getTag().toString() : null,
                    network
            );

            LOGGER.info("Withdraw detail: " + binanceApiResult);

            isSuccess = binanceApiResult.getId() != null && !binanceApiResult.getId().isEmpty();

            if(!isSuccess) {
                LOGGER.info("Withdraw not successful, retrying" + binanceApiResult.getMsg());
            }
        } while (!isSuccess && tries++ < maxTries);

        com.tkelemen.arbitrader.model.WithdrawResult withdrawResult
                = new com.tkelemen.arbitrader.model.WithdrawResult(WithdrawStage.REQUESTED);
        withdrawResult.setMessage(binanceApiResult.getMsg());
        withdrawResult.setSuccess(isSuccess);
        withdrawResult.setWithdrawId(binanceApiResult.getId());

        if(isSuccess) {
            walletCache.subtractAmount(ticker, amount);
        }

        return withdrawResult;
    }

    @Override
    public SellResult sell(String pair, BigDecimal amount) throws TradeException {
        final SellResult sellResult = new SellResult();

        try {
            final NewOrderResponse response = marketSell(pair, amount);

            sellResult.setOrderId(response.getOrderId().toString());
            sellResult.setAdditionalOrderId(response.getClientOrderId());
            sellResult.setSoldAmount(new BigDecimal(response.getExecutedQty()));
            sellResult.setReceivedAmount(calculatePrice(response.getFills()));
            sellResult.setSellFee(calculateFee(response.getFills()));
            sellResult.setSellFeeCurrency(fromBinanceAsset(response.getFills().get(0).getCommissionAsset()));
            sellResult.setStatus(Arbitrage.STATUS_OK);

            // TODO: fees are not included
            walletCache.addAmount(CurrencyUtils.getQuote(pair), sellResult.getReceivedAmount());
            walletCache.subtractAmount(CurrencyUtils.getBase(pair), sellResult.getSoldAmount());

            LOGGER.debug("Wallet: " + walletCache);
        } catch (TradeException e) {
            sellResult.setStatus(Arbitrage.STATUS_FAIL);
            sellResult.setErrorMessage(e.getMessage());
        }

        return sellResult;
    }

    @Override
    public CreateOrderResult limitSell(String pair, BigDecimal amount, BigDecimal price) throws TradeException {
        return null;
    }

    @Override
    public CancelOrderResult cancel(String orderId) {
        return null;
    }

    @Override
    public boolean hasSufficientFunds(String ticker, BigDecimal amount) {
        return amount.compareTo(walletCache.getFreeAmount(ticker)) <= 0;
    }

    @Override
    public void setAvailableFunds(String ticker, BigDecimal amount) {
    }

    @Override
    public BigDecimal getAvailableFunds(String ticker) {
        return null;
    }

    @Override
    public Wallet getWallet() {
        return walletCache;
    }

    @Override
    public void updateWallet() {
        try {
            Account account = restClient.getAccount();

            for (AssetBalance balance : account.getBalances()) {
                walletCache.setTotalAmount(fromBinanceAsset(balance.getAsset()), new BigDecimal(balance.getFree()));
            }

        } catch (BinanceApiException e) {
            LOGGER.error("Failed to get Binance account balance: " + e.toString());
        }

        updateExchangeInfo();
    }

    @Override
    public BigDecimal getPredictedTransferredBaseAmountAfterBuy(Opportunity opportunity) {
        BigDecimal buyFee = feeService.getTradingFeeTaker(Exchange.BINANCE).multiply(opportunity.getTransferAmount());

        return opportunity.getTransferAmount().subtract(buyFee).subtract(opportunity.getWithdrawFee()).subtract(opportunity.getDepositFee());
    }

    @Override
    public int getAssetPrecision(String ticker) {
        return 0;
    }

    @Override
    public int getPairPricePrecision(String pair) {
        // TODO: implement
        return 0;
    }

    @Override
    public Map<String, BigDecimal> getTradeVolumesByAssetPair() {
        return new HashMap<>();  // TODO: implement
    }

    @Override
    public Map<String, BigDecimal> getLastPricesByAssetPair()  {
        return new HashMap<>();  // TODO: implement
    }

    @Override
    public Map<String, ExchangeOrder> getOpenOrders() {
        return new HashMap<>();
    }

    @Override
    public Optional<ExchangeOrder> getOrderDetail(String id) {
        return Optional.empty();
    }

    private BigDecimal calculatePrice(List<Trade> tradeList) {
        BigDecimal totalPrice = BigDecimal.ZERO;
        for (Trade trade : tradeList) {
            totalPrice = totalPrice.add(new BigDecimal(trade.getPrice()).multiply(new BigDecimal(trade.getQty())));
        }

        return totalPrice;
    }

    private BigDecimal calculateFee(List<Trade> tradeList) {
        BigDecimal totalFee = BigDecimal.ZERO;
        for (Trade trade : tradeList) {
            totalFee = totalFee.add(new BigDecimal(trade.getCommission()));
        }

        return totalFee;
    }

    private int getSymbolTradingScale(String symbol) {
        SymbolInfo symbolInfo = allSymbolsMap.get(symbol);

        for (SymbolFilter filter : symbolInfo.getFilters()) {
            if (filter.getFilterType() == FilterType.LOT_SIZE) {
                int onePos = filter.getStepSize().indexOf("1");

                return onePos == 0 ? 0 : onePos - 1;
            }
        }

        return 0;
    }

    private static String toExchangeSymbol(String generalPair) {
        return toBinanceAsset(CurrencyUtils.getBase(generalPair)) + CurrencyUtils.getQuote(generalPair);
    }

    public static String toBinanceAsset(String asset) {
        if(asset.equals("LUNA")) {
            return "LUNC";
        }
        if(asset.equals("LUNA2")) {
            return "LUNA";
        }

        return asset;
    }

    public static String fromBinanceAsset(String asset) {
        if(asset.equals("LUNC")) {
            return "LUNA";
        }
        if(asset.equals("LUNA")) {
            return "LUNA2";
        }

        return asset;
    }
}
