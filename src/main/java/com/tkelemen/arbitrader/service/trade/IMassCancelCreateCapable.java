package com.tkelemen.arbitrader.service.trade;

import com.tkelemen.arbitrader.exception.TradeException;
import com.tkelemen.arbitrader.model.MassCancelCreateOrderRequest;
import com.tkelemen.arbitrader.model.MassCancelCreateOrderResult;

public interface IMassCancelCreateCapable {
    MassCancelCreateOrderResult massCancelCreateOrders(MassCancelCreateOrderRequest request) throws TradeException;
}
