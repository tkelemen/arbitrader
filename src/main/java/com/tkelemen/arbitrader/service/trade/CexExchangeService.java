package com.tkelemen.arbitrader.service.trade;

import com.tkelemen.arbitrader.client.cex.rest.*;
import com.tkelemen.arbitrader.client.cex.rest.message.*;
import com.tkelemen.arbitrader.db.entity.Arbitrage;
import com.tkelemen.arbitrader.exception.AddressNotFoundException;
import com.tkelemen.arbitrader.exception.ExchangeApiException;
import com.tkelemen.arbitrader.exception.TradeException;
import com.tkelemen.arbitrader.model.*;
import com.tkelemen.arbitrader.model.enums.OrderType;
import com.tkelemen.arbitrader.service.market.*;
import com.tkelemen.arbitrader.util.CurrencyUtils;
import com.tkelemen.arbitrader.util.TradeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CexExchangeService implements IExchangeService, IMassCancelCreateCapable {
    private static final Logger LOGGER = LoggerFactory.getLogger(CexExchangeService.class);

    private final PlaceOrderClient placeOrderClient;

    private final GetOrderClient getOrderClient;

    private final GetBalanceClient getBalanceClient;

    private final WalletService walletService;

    private final GetTickersClient getTickersClient;

    private final FeeService feeService;

    private final CurrencyLimitsClient currencyLimitsClient;

    private final CancelOrderClient cancelOrderClient;

    private final OpenOrdersClient openOrdersClient;

    private final MassCancelPlaceOrdersClient massCancelPlaceOrdersClient;

    private final OrderBookService orderBookService;

    private final Wallet walletCache = new Wallet();

    private final Map<String, PairDetail> currencyLimitDetails = new HashMap<>();

    @Autowired
    public CexExchangeService(PlaceOrderClient placeOrderClient,
                              GetOrderClient getOrderClient,
                              GetBalanceClient getBalanceClient,
                              WalletService walletService,
                              GetTickersClient getTickersClient,
                              FeeService feeService,
                              CurrencyLimitsClient currencyLimitsClient,
                              CancelOrderClient cancelOrderClient,
                              OpenOrdersClient openOrdersClient,
                              MassCancelPlaceOrdersClient massCancelPlaceOrdersClient,
                              CexOrderBookService orderBookService) {
        this.placeOrderClient = placeOrderClient;
        this.getOrderClient = getOrderClient;
        this.getBalanceClient = getBalanceClient;
        this.walletService = walletService;
        this.getTickersClient = getTickersClient;
        this.feeService = feeService;
        this.currencyLimitsClient = currencyLimitsClient;
        this.cancelOrderClient = cancelOrderClient;
        this.openOrdersClient = openOrdersClient;
        this.massCancelPlaceOrdersClient = massCancelPlaceOrdersClient;
        this.orderBookService = orderBookService;

        // TODO: refresh periodically
        refreshCurrencyLimits();
    }

    public void refreshCurrencyLimits() {
        try {
            CurrencyLimitsResponse response = currencyLimitsClient.get();

            if ("ok".equals(response.getOk())) {
                for (PairDetail detail : response.getData().getPairs()) {
                    currencyLimitDetails.put(CurrencyUtils.getPair(fromCexAsset(detail.getSymbol1()), detail.getSymbol2()), detail);
                }
            } else {
                LOGGER.error("CEX currency limits operation returned error: " + response.getE());
            }
        } catch (Exception e) {
            LOGGER.error("Error while getting CEX currency limits: " + e.getMessage(), e);
        }
    }

    @Override
    public BuyResult buy(String pair, BigDecimal amount, boolean isAmountInQuoteCurrency) {
        final BuyResult result = new BuyResult();
        result.setPair(pair);

        LOGGER.info("Creating MARKET BUY order for pair " + pair + " [ amount = " + amount + ", isAmountInQuoteCurrency = " + isAmountInQuoteCurrency + " ]");

        final BigDecimal amountToSpend = isAmountInQuoteCurrency ? amount : OrderBookHelper.calculateBuyQuoteQuantity(orderBookService.getOrderBookSnapshots().get(pair), amount);
        PlaceMarketOrderResponse response = null;

        int maxTries = 3;
        int tries = 0;
        do {
            if(tries > 0) {
                LOGGER.error("Buy order returned error '" + response.getError() + "', retrying");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            response = placeOrderClient.marketBuy(toExchangeSymbol(pair), amountToSpend);
        } while (response.getError() != null && tries++ < maxTries);

        final String base = CurrencyUtils.getBase(pair);
        final String quote = CurrencyUtils.getQuote(pair);

        if (response.getError() != null) {
            LOGGER.error("Buy order not filled: " + response);
            result.setBuyStatus(Arbitrage.STATUS_FAIL);
            result.setErrorMessage(response.toString());
            return result;
        }

        LOGGER.info("Getting order details [ orderId = " + response.getId() + " ]");

        // TODO: refactor to try-loop service
        tries = 0;

        GetOrderResponse getOrderResponse;

        do {
            getOrderResponse = getOrderClient.get(response.getId());
        } while (getOrderResponse.getCreditDebitSaldoAmount(toCexAsset(base)) == null && tries++ < maxTries);

        LOGGER.info("Order details: " + getOrderResponse);

        result.setBuyStatus(Arbitrage.STATUS_OK);
        result.setOrderId(response.getId());

        BigDecimal initialAmount = getOrderResponse.getTotalTakerAmount(quote);
        BigDecimal buyFee = getOrderResponse.getTakerFeeAmount(quote);
        BigDecimal boughtAmount = getOrderResponse.getCreditDebitSaldoAmount(toCexAsset(base));

        // in case we failed to get the order details, we try to figure out the amounts from PlaceOrder response
        if (boughtAmount == null) {
            result.setErrorMessage("Could not get order details");
            boughtAmount = getAmountFromSymbolAmount(response.getSymbol1Amount(), base);
            initialAmount = getAmountFromSymbolAmount(response.getSymbol2Amount(), quote);
            buyFee = initialAmount.multiply(feeService.getTradingFeeTaker(Exchange.CEX));
            initialAmount = initialAmount.subtract(buyFee);
        }

        result.setInitialAmount(initialAmount);
        result.setBuyFee(buyFee);
        result.setBuyFeeCurrency(quote);
        result.setBoughtAmount(boughtAmount);

        BigDecimal withdrawFee = feeService.getWithdrawFee(Exchange.CEX, base);
        result.setWithdrawFee(withdrawFee);

        walletCache.subtractAmount(quote, TradeUtils.getInitialExpenses(result));
        walletCache.addAmount(base, TradeUtils.getAmountToWithdraw(result));

        // TODO: disabled till CEX auto withdrawals not working correctly
        //walletCache.addLockedAmount(baseTicker, qtyToWithdraw.subtract(withdrawFee));

        return result;
    }

    @Override
    public CreateOrderResult limitBuy(String pair, BigDecimal amount, BigDecimal price) throws TradeException {
        final String base = CurrencyUtils.getBase(pair);
        amount = amount.setScale(getAssetPrecision(base), RoundingMode.HALF_UP);

        LOGGER.info("Creating LIMIT BUY order for pair " + pair + " [ quantity = " + amount + ", price = " + price + " ]");

        final PlaceOrderResponse response = placeOrderClient.limitBuy(toExchangeSymbol(pair), amount, price);

        LOGGER.debug("Buy order details: " + response);

        if (response.getError() != null) {
            throw new TradeException("Unable to create buy order for pair " + pair + " [ amount = " + amount + ", price = " + price + " ]: " + response.toString());
        }

        walletCache.subtractAmount(CurrencyUtils.getQuote(pair), amount.multiply(price));

        return new CreateOrderResult(pair, response.getId(), price, amount);
    }

    @Override
    public SellResult sell(String pair, BigDecimal amount) throws TradeException {
        String quote = CurrencyUtils.getQuote(pair);
        String base = CurrencyUtils.getBase(pair);

        amount = amount.setScale(getAssetPrecision(base), RoundingMode.HALF_UP);

        LOGGER.info("Creating MARKET SELL order for pair " + pair + " [ quantity = " + amount + " ]");

        PlaceMarketOrderResponse response = null;

        int maxTries = 3;
        int tries = 0;
        do {
            if(tries > 0) {
                LOGGER.error("Sell order returned error '" + response.getError() + "', retrying");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            response = placeOrderClient.marketSell(toExchangeSymbol(pair), amount);
        } while (response.getError() != null && tries++ < maxTries);

        SellResult sellResult = new SellResult();
        if (response.getError() != null) {
            LOGGER.error("Sell order not filled: " + response);
            sellResult.setStatus(Arbitrage.STATUS_FAIL);
            return sellResult;
        }

        LOGGER.info("Sell order executed for pair " + pair + ": " + response);

        int maxGetOrderTries = 3;
        int getOrderTries = 0;
        GetOrderResponse getOrderResponse = getOrderClient.get(response.getId());

        while (getOrderResponse.getCreditDebitSaldoAmount(toCexAsset(base)) == null && getOrderTries++ < maxGetOrderTries) {
            getOrderResponse = getOrderClient.get(response.getId());
        }

        LOGGER.info("Sell order detail: " + getOrderResponse);

        BigDecimal receivedAmount = getOrderResponse.getTotalTakerAmount(quote);
        BigDecimal soldAmount = getOrderResponse.getCreditDebitSaldoAmount(toCexAsset(base));
        BigDecimal feeAmount = getOrderResponse.getTakerFeeAmount(quote);

        sellResult.setOrderId(response.getId());

        if (receivedAmount == null) {
            receivedAmount = getAmountFromSymbolAmount(response.getSymbol2Amount(), quote);
            feeAmount = receivedAmount.multiply(feeService.getTradingFeeTaker(Exchange.CEX));
            soldAmount = getAmountFromSymbolAmount(response.getSymbol1Amount(), base);
        }

        sellResult.setSellFeeCurrency(quote);
        sellResult.setSellFee(feeAmount);
        sellResult.setReceivedAmount(receivedAmount);
        sellResult.setSoldAmount(soldAmount);
        sellResult.setStatus(Arbitrage.STATUS_OK);

        walletCache.addAmount(quote, receivedAmount.subtract(feeAmount));
        walletCache.subtractAmount(base, soldAmount);

        LOGGER.debug("Wallet: " + walletCache);

        return sellResult;
    }

    @Override
    public CreateOrderResult limitSell(String pair, BigDecimal amount, BigDecimal price) throws TradeException {
        final String base = CurrencyUtils.getBase(pair);

        amount = amount.setScale(getAssetPrecision(base), RoundingMode.HALF_UP);

        LOGGER.info("Creating LIMIT SELL order for pair " + pair + " [ quantity = " + amount + ", price = " + price + " ]");

        final PlaceOrderResponse response = placeOrderClient.limitSell(toExchangeSymbol(pair), amount, price);

        LOGGER.info("Sell order details: " + response);

        if (response.getError() != null) {
            throw new TradeException("Unable to sell " + pair + " [ amount = " + amount + " ]: " + response);
        }

        walletCache.subtractAmount(base, amount);

        return new CreateOrderResult(pair, response.getId(), price, amount);
    }

    @Override
    public WithdrawResult withdraw(String ticker, BigDecimal amount, Address address, String addressName) throws Exception {
        return null;
    }

    @Override
    public CancelOrderResult cancel(String orderId) {
        CancelOrderResponse response = null;

        int tries = 0;
        int maxTries = 3;
        do {
            if(tries > 0 && response.getError() != null) {
                try {
                    Thread.sleep(500L * tries);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            response = cancelOrderClient.cancel(orderId);
        } while ((!response.isSuccess() || (response.getError() != null && response.getError().contains("Invalid signature"))) && tries++ < maxTries);

        return new CancelOrderResult(orderId, response.isSuccess(), response.getError());
    }

    @Override
    public boolean hasSufficientFunds(String ticker, BigDecimal amount) {
        return amount.compareTo(walletCache.getFreeAmount(ticker)) <= 0;
    }

    @Override
    public void setAvailableFunds(String ticker, BigDecimal amount) {
        walletCache.setTotalAmount(ticker, amount);
    }

    @Override
    public BigDecimal getAvailableFunds(String ticker) {
        return null;
    }

    @Override
    public Wallet getWallet() {
        return walletCache;
    }

    @Override
    public synchronized void updateWallet() {
        try {
            final GetBalanceResponse response = getBalanceClient.get();
            for (Map.Entry<String, Balance> entry : response.getBalances().entrySet()) {
                walletCache.setTotalAmount(fromCexAsset(entry.getKey()), entry.getValue().getAvailable());
            }
        } catch (Exception e) {
            LOGGER.error("Error getting CEX balances: " + e.getMessage(), e);
        }
    }

    @Override
    public BigDecimal getPredictedTransferredBaseAmountAfterBuy(Opportunity opportunity) {
        BigDecimal buyFee = feeService.getTradingFeeTaker(Exchange.CEX).multiply(opportunity.getTransferAmount());

        return opportunity.getTransferAmount().subtract(buyFee).subtract(opportunity.getWithdrawFee()).subtract(opportunity.getDepositFee());
    }

    @Override
    public int getAssetPrecision(String ticker) {
        int precision = 0;

        // TODO: move entirely into config
        switch (ticker) {
            case "SHIB":
                precision = 0;
                break;
            case "USD":
            case "GBP":
            case "EUR":
                precision = 2;
                break;
            case "BAL":
            case "BTT":
            case "TRX":
            case "USDT":
            case "ADA":
            case "ATOM":
            case "XTZ":
            case "XRP":
            case "SNX":
            case "KAVA":
            case "DOT":
            case "DOGE":
            case "NEO":
            case "KSM":
            case "BEL":
            case "BNB":
            case "CAKE":
            case "ICP":
            case "ONT":
            case "ONG":
            case "SOL":
            case "XVS":
            case "1INCH":
            case "AKRO":
            case "ANT":
            case "ARPA":
            case "AXS":
            case "BAND":
            case "BAT":
            case "COTI":
            case "CREAM":
            case "DAI":
            case "DNT":
            case "DYDX":
            case "FARM":
            case "FUN":
            case "GLM":
            case "GRT":
            case "INJ":
            case "JASMY":
            case "KP3R":
            case "LRC":
            case "MKR":
            case "OCEAN":
            case "OMG":
            case "PAXG":
            case "REN":
            case "RSR":
            case "SGB":
            case "SRM":
            case "STORJ":
            case "SUSHI":
            case "TRB":
            case "UMA":
            case "UTK":
            case "WABI":
            case "WNXM":
            case "YFI":
            case "YFII":
            case "ZRX":
                precision = 6;
                break;
            case "XLM":
                precision = 7;
                break;
            case "BTC":
            case "ETH":
            case "ZIL":
            case "BCH":
            case "LTC":
            case "GAS":
                precision = 8;
                break;
            default:
                try {
                    Integer configPrecision = walletService.getAddress(Exchange.CEX, ticker).getPrecision();
                    if (configPrecision != null) {
                        precision = configPrecision;
                    }
                } catch (AddressNotFoundException e) {
                    // do nothing
                }

        }

        return precision;
    }

    @Override
    public int getPairPricePrecision(String pair) {
        PairDetail detail = currencyLimitDetails.get(pair);

        if (detail != null) {
            return detail.getPricePrecision();
        }

        return 0;
    }

    @Override
    public Map<String, BigDecimal> getTradeVolumesByAssetPair() {
        Map<String, BigDecimal> volumes = new HashMap<>();

        GetTickersResponse tickersResponse = getTickersClient.get(new ArrayList<>(Arrays.asList("USD", "GBP", "EUR", "USDT")));
        tickersResponse.getData().forEach(tickerDetail -> {
            String pair = tickerDetail.getPair().replace(':', '/');

            BigDecimal volume;
            if(tickerDetail.getVolume() == null || tickerDetail.getLast() == null) {
                volume = BigDecimal.ZERO;
            } else {
                volume = tickerDetail.getVolume();
            }

            volumes.put(CurrencyUtils.getPair(fromCexAsset(CurrencyUtils.getBase(pair)), CurrencyUtils.getQuote(pair)), volume.multiply(tickerDetail.getLast()));
        });

        return volumes;
    }

    @Override
    public Map<String, BigDecimal> getLastPricesByAssetPair() {
        Map<String, BigDecimal> volumes = new HashMap<>();

        GetTickersResponse tickersResponse = getTickersClient.get(new ArrayList<>(Arrays.asList("USD", "GBP", "EUR", "USDT")));
        tickersResponse.getData().forEach(tickerDetail -> {
            String pair = tickerDetail.getPair().replace(':', '/');

            volumes.put(CurrencyUtils.getPair(fromCexAsset(CurrencyUtils.getBase(pair)), CurrencyUtils.getQuote(pair)), tickerDetail.getLast());
        });

        return volumes;
    }

    @Override
    public Map<String, ExchangeOrder> getOpenOrders() throws ExchangeApiException {
        LOGGER.info("Getting CEX open orders list");
        final OpenOrdersResponse openOrders = openOrdersClient.get();

        if(openOrders.getError() != null && !"".equals(openOrders.getError())) {
            LOGGER.error("CEX open orders request returned an error: " + openOrders.getError());
            throw new ExchangeApiException("Failed to get the list of CEX open orders: " + openOrders.getError());
        }

        if(openOrders.getOpenOrders().isEmpty()) {
            return new HashMap<>();
        }

        return openOrders.getOpenOrders().stream().map(order -> {
            ExchangeOrder openOrder = new ExchangeOrder();
            openOrder.setId(order.getId());
            openOrder.setAmount(order.getAmount());
            openOrder.setExecutedAmount(order.getAmount().subtract(order.getPending()));
            openOrder.setPrice(order.getPrice());
            openOrder.setBase(order.getSymbol1());
            openOrder.setQuote(order.getSymbol2());
            if("buy".equalsIgnoreCase(order.getType())) {
                openOrder.setType(OrderType.BUY);
            } else if("sell".equalsIgnoreCase(order.getType())) {
                openOrder.setType(OrderType.SELL);
            }

            return openOrder;
        }).collect(Collectors.toMap(ExchangeOrder::getId, openOrder -> openOrder));
    }

    @Override
    public Optional<ExchangeOrder> getOrderDetail(String id) {
        GetOrderResponse order;

        // TODO: move into config
        int maxGetOrderTries = 3;
        int getOrderTries = 0;

        do {
            order = getOrderClient.get(id);
        } while ((order == null || order.getError() != null || order.getCreditDebitSaldoAmount(toCexAsset(order.getSymbol1())) == null) && getOrderTries++ < maxGetOrderTries);

        if (order == null || order.getError() != null) {
            LOGGER.error("Error getting order detail for ID " + id + ": " + order);
            return Optional.empty();
        }

        ExchangeOrder exchangeOrder = new ExchangeOrder();
        exchangeOrder.setId(order.getId());
        exchangeOrder.setAmount(order.getAmount());
        exchangeOrder.setExecutedAmount(order.getAmount().subtract(order.getRemains()));
        exchangeOrder.setPrice(order.getPrice());
        exchangeOrder.setBase(order.getSymbol1());
        exchangeOrder.setQuote(order.getSymbol2());
        exchangeOrder.setExecutedFee(order.getTradingFeeMaker());
        exchangeOrder.setExecutedFeeCurrency(order.getSymbol2());

        if("buy".equalsIgnoreCase(order.getType())) {
            exchangeOrder.setType(OrderType.BUY);
        } else if("sell".equalsIgnoreCase(order.getType())) {
            exchangeOrder.setType(OrderType.SELL);
        }

        if("a".equalsIgnoreCase(order.getStatus())) {
            exchangeOrder.setStatus(BigDecimal.ZERO.compareTo(exchangeOrder.getExecutedAmount()) < 0
                    ? ExchangeOrder.Status.PARTIALLY_EXECUTED
                    : ExchangeOrder.Status.CREATED);
        } else if("d".equalsIgnoreCase(order.getStatus())) {
            exchangeOrder.setStatus(ExchangeOrder.Status.DONE);
        } else if("c".equalsIgnoreCase(order.getStatus())) {
            exchangeOrder.setStatus(ExchangeOrder.Status.CANCELLED);
        } else if("cd".equalsIgnoreCase(order.getStatus())) {
            exchangeOrder.setStatus(ExchangeOrder.Status.PARTIALLY_EXECUTED_CANCELLED);
        }

        return Optional.of(exchangeOrder);
    }

    @Override
    public MassCancelCreateOrderResult massCancelCreateOrders(MassCancelCreateOrderRequest massCancelCreateOrderRequest) throws TradeException {
        LOGGER.info("Creating mass cancel/create orders request: " + massCancelCreateOrderRequest);

        final MassCancelPlaceOrdersRequest request = new MassCancelPlaceOrdersRequest();
        massCancelCreateOrderRequest.getOrdersToCancelIdList().forEach(request::addCancelOrder);
        massCancelCreateOrderRequest.getOrdersToCreateList().forEach(createOrder -> {
            final BigDecimal amount = createOrder.getAmount().setScale(getAssetPrecision(CurrencyUtils.getBase(createOrder.getPair())), RoundingMode.HALF_UP);
            request.addPlaceOrder(
                    createOrder.getPair(),
                    OrderType.BUY.equals(createOrder.getType())
                            ? MassCancelPlaceOrdersRequest.PlaceOrderType.BUY
                            : MassCancelPlaceOrdersRequest.PlaceOrderType.SELL,
                    createOrder.getPrice(),
                    amount
            );
        });

        final MassCancelCreateOrderResult result = new MassCancelCreateOrderResult();
        final MassCancelPlaceOrdersResponse response = massCancelPlaceOrdersClient.cancelPlace(request);
        if(response.getError() != null && !"".equals(response.getError())) {
            LOGGER.error("Error while mass cancel/create orders request: " + response.getError());
            result.setError(response.getError());
        }

        if (response.getData() == null) {
            return result;
        }

        // process cancelled orders
        final List<MassCancelPlaceOrdersResponse.CancelledOrder> cancelledOrders = response.getData().getCancelOrders();
        if(cancelledOrders.size() != massCancelCreateOrderRequest.getOrdersToCancelIdList().size()) {
            final String message = "The number of cancel order requests doesn't match the number in response";
            LOGGER.error(message);
            result.setError(result.getError() + "; " + message);
        }

        for(int i = 0; i < cancelledOrders.size(); i++) {
            final MassCancelPlaceOrdersResponse.CancelledOrder cancelledOrder = cancelledOrders.get(i);
            final String requestCancelOrderId = massCancelCreateOrderRequest.getOrdersToCancelIdList().get(i);
            final CancelOrderResult cancelOrderResult = new CancelOrderResult();

            if(cancelledOrder.getError() != null && !"".equals(cancelledOrder.getError())) {
                cancelOrderResult.setError(cancelledOrder.getError());
                cancelOrderResult.setOrderId(requestCancelOrderId);
            } else {
                if(!requestCancelOrderId.equals(cancelledOrder.getOrderId())) {
                    final String message ="Cancelled order id doesn't match the requested order id to cancel at position " +
                            i + " (request: " + requestCancelOrderId + ", response: " + cancelledOrder.getOrderId() + ")";
                    LOGGER.error(message);
                    result.setError(result.getError() + "; " + message);
                }
                cancelOrderResult.setOrderId(cancelledOrder.getOrderId());
                cancelOrderResult.setRemainingAmount(cancelledOrder.getRemaining());
                cancelOrderResult.setError(cancelledOrder.getError());
            }

            result.addCancelOrderResult(cancelOrderResult);
        }

        // process created orders
        final List<MassCancelPlaceOrdersResponse.PlacedOrder> placedOrders = response.getData().getPlaceOrders();
        if(placedOrders.size() != massCancelCreateOrderRequest.getOrdersToCreateList().size()) {
            final String message = "The number of place order requests doesn't match the number in response";
            LOGGER.error(message);
            result.setError(result.getError() + "; " + message);
        }

        for(int i = 0; i < placedOrders.size(); i++) {
            final CreateOrder createOrderRequest = massCancelCreateOrderRequest.getOrdersToCreateList().get(i);
            final MassCancelPlaceOrdersResponse.PlacedOrder placedOrder = placedOrders.get(i);
            final CreateOrderResult createOrderResult = new CreateOrderResult(
                    createOrderRequest.getPair(),
                    placedOrder.getId(),
                    createOrderRequest.getPrice(),
                    placedOrder.getAmount()
            );
            createOrderResult.setType(createOrderRequest.getType());
            createOrderResult.setError(placedOrder.getError());

            if(placedOrder.getError() == null || "".equals(placedOrder.getError())) {
                if(OrderType.BUY.equals(createOrderRequest.getType())) {
                    walletCache.subtractAmount(CurrencyUtils.getQuote(createOrderRequest.getPair()), createOrderRequest.getAmount().multiply(createOrderRequest.getPrice()));
                } else {
                    walletCache.subtractAmount(CurrencyUtils.getBase(createOrderRequest.getPair()), createOrderRequest.getAmount());
                }
            }

            result.addCreateOrderResult(createOrderResult);
        }

        return result;
    }

    private BigDecimal getAmountFromSymbolAmount(BigDecimal symbolAmount, String ticker) {
        int precision = getAssetPrecision(ticker);

        if (precision > 0) {
            BigDecimal divisor = new BigDecimal(10).pow(precision);

            return symbolAmount.divide(divisor, divisor.toPlainString().length() - 1, RoundingMode.HALF_EVEN);
        } else {
            throw new NumberFormatException("Could not determine divisor for " + ticker + " symbolAmount");
        }
    }

    private static String toExchangeSymbol(String generalPair) {
        return toCexAsset(CurrencyUtils.getBase(generalPair)) + "/" + CurrencyUtils.getQuote(generalPair);
    }

    public static String toCexAsset(String asset) {
        if (asset == null) {
            return null;
        }
        if (asset.equals("LUNA")) {
            return "LUNC";
        }
        /*if(asset.equals("LUNA2")) {
            return "LUNA";
        }*/

        return asset;
    }

    public static String fromCexAsset(String asset) {
        if (asset.equals("LUNC")) {
            return "LUNA";
        }
        /*if(asset.equals("LUNA")) {
            return "LUNA2";
        }*/

        return asset;
    }
}
