package com.tkelemen.arbitrader.service.trade;

import com.tkelemen.arbitrader.exception.ExchangeApiException;
import com.tkelemen.arbitrader.exception.TradeException;
import com.tkelemen.arbitrader.model.*;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;

public interface IExchangeService {
    BuyResult buy(String pair, BigDecimal amount, boolean isAmountInQuoteCurrency);

    CreateOrderResult limitBuy(String pair, BigDecimal amount, BigDecimal price) throws TradeException;

    SellResult sell(String pair, BigDecimal amount) throws TradeException;

    CreateOrderResult limitSell(String pair, BigDecimal amount, BigDecimal price) throws TradeException;

    WithdrawResult withdraw(String ticker, BigDecimal amount, Address address, String addressName) throws Exception;

    CancelOrderResult cancel(String orderId);

    boolean hasSufficientFunds(String ticker, BigDecimal amount);

    void setAvailableFunds(String ticker, BigDecimal amount);

    BigDecimal getAvailableFunds(String ticker);

    Wallet getWallet();

    void updateWallet();

    BigDecimal getPredictedTransferredBaseAmountAfterBuy(Opportunity opportunity);

    int getAssetPrecision(String ticker);

    int getPairPricePrecision(String pair);

    Map<String, BigDecimal> getTradeVolumesByAssetPair();

    Map<String, BigDecimal> getLastPricesByAssetPair();

    Map<String, ExchangeOrder> getOpenOrders() throws ExchangeApiException;

    Optional<ExchangeOrder> getOrderDetail(String id);


}
