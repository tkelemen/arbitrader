package com.tkelemen.arbitrader.service.trade;

import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.model.Exchange;
import com.tkelemen.arbitrader.service.arbitrage.withdraw.WithdrawExecutor;
import com.tkelemen.arbitrader.service.market.OrderBookService;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class ExchangeManagerService {
    private final Map<Exchange, IExchangeService> exchangeServiceMap = new HashMap<>();
    private final Map<Exchange, WithdrawExecutor> withdrawExecutorMap = new HashMap<>();
    private final Map<Exchange, OrderBookService> orderBookServiceMap = new HashMap<>();

    @Autowired
    public ExchangeManagerService(BeanFactory beanFactory,
                                  ApplicationConfig config) {
        for(Exchange exchange : config.getAllowedExchanges()) {
            String exchangeBeanPrefix = exchange.name().toLowerCase();
            exchangeServiceMap.put(exchange, beanFactory.getBean(exchangeBeanPrefix + "ExchangeService", IExchangeService.class));
            withdrawExecutorMap.put(exchange, beanFactory.getBean(exchangeBeanPrefix + "WithdrawExecutorService", WithdrawExecutor.class));
            orderBookServiceMap.put(exchange, beanFactory.getBean(exchangeBeanPrefix + "OrderBookService", OrderBookService.class));
        }
    }

    public Map<Exchange, IExchangeService> getExchangeServices() {
        return exchangeServiceMap;
    }

    public IExchangeService getExchangeService(Exchange exchange) {
        return exchangeServiceMap.get(exchange);
    }

    public Map<Exchange, OrderBookService> getOrderBookServices() {
        return orderBookServiceMap;
    }

    public OrderBookService getOrderBookService(Exchange exchange) {
        return orderBookServiceMap.get(exchange);
    }

    public WithdrawExecutor getWithdrawExecutor(Exchange exchange) {
        return withdrawExecutorMap.get(exchange);
    }
}
