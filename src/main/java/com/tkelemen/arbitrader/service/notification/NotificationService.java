package com.tkelemen.arbitrader.service.notification;

import com.pengrad.telegrambot.ExceptionHandler;
import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.TelegramException;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.request.SendMessage;
import com.tkelemen.arbitrader.ArbitraderApplication;
import com.tkelemen.arbitrader.client.cex.rest.PrivateCexRestQuery;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.db.repository.ArbitrageRepository;
import com.tkelemen.arbitrader.model.Exchange;
import com.tkelemen.arbitrader.service.arbitrage.ArbitrageManagerService;
import com.tkelemen.arbitrader.service.trade.ExchangeManagerService;
import com.tkelemen.arbitrader.service.trade.IExchangeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class NotificationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationService.class);

    private final TelegramBot telegramBot;
    private final long startTimestamp;
    private final ApplicationConfig applicationConfig;
    private ArbitrageManagerService arbitrageManagerService;
    private ExchangeManagerService exchangeManagerService;
    private final ArbitrageRepository arbitrageRepository;
    private Level level = Level.INFO;

    @Autowired
    public NotificationService(ApplicationConfig config, ExchangeManagerService exchangeManagerService, ArbitrageRepository arbitrageRepository) {
        this.exchangeManagerService = exchangeManagerService;
        this.arbitrageRepository = arbitrageRepository;
        telegramBot = new TelegramBot(config.getTelegramBotToken());
        startTimestamp = System.currentTimeMillis() / 1000L;
        applicationConfig = config;
    }

    public void send(String message) {
        send(message, Level.INFO);
    }

    public void send(String message, Level level) {
        if (!applicationConfig.isNotificationEnabled()) {
            LOGGER.debug("Notification disabled, ignoring message: " + message);
            return;
        }

        if (level.ordinal() < this.level.ordinal()) {
            LOGGER.debug("Notification level not reached, ignoring message: " + message);
            return;
        }

        telegramBot.execute(new SendMessage(applicationConfig.getTelegramChatId(), message));
    }

    public void sendMultipart(String longMessage) {
        int parts = (int) Math.ceil(longMessage.length() / 4000f);
        for (int i = 0; i < parts; i++) {
            int startIndex = i * 4000;
            int endIndex = startIndex + 4000;

            if (endIndex > longMessage.length()) {
                endIndex = longMessage.length();
            }

            send(longMessage.substring(startIndex, endIndex));
        }
    }

    public void subscribeForCommands(ArbitrageManagerService arbitrageManagerService) {
        if (!applicationConfig.isNotificationEnabled()) {
            return;
        }

        this.arbitrageManagerService = arbitrageManagerService;

        telegramBot.setUpdatesListener(new CommandUpdateListener(), new ExceptionHandler() {
            @Override
            public void onException(TelegramException e) {
                send("Error while executing command: " + e.toString());
            }
        });
    }

    private class CommandUpdateListener implements UpdatesListener {
        @Override
        public int process(List<Update> list) {
            for (Update update : list) {
                if(update.message() == null) {
                    continue;
                }

                String text = update.message().text();

                if (startTimestamp > update.message().date()) {
                    LOGGER.info("Ignoring old message: " + text);
                    continue;
                }

                if (text.matches("^/shutdown")) {
                    send("Shutting down application");
                    ArbitraderApplication.shutdown();
                } else if (text.matches("^/status") || text.matches("^/s")) {
                    send("Status:");

                    send(arbitrageManagerService.getStatusString());
                } else if (text.matches("^/threshold (.+)") || text.matches("^/t (.+)")) {
                    String threshold = text.substring(text.indexOf(" ") + 1);
                    send("Setting common threshold to " + threshold);
                    arbitrageManagerService.setThreshold(new BigDecimal(threshold));
                } else if (text.matches("^/fundingthreshold (.+)") || text.matches("^/ft (.+)")) {
                    String threshold = text.substring(text.indexOf(" ") + 1);
                    send("Setting with funding threshold to " + threshold);
                    arbitrageManagerService.setWithFundingThreshold(new BigDecimal(threshold));
                } else if (text.matches("^/generalfundingthreshold (.+)") || text.matches("^/gft (.+)")) {
                    String threshold = text.substring(text.indexOf(" ") + 1);
                    send("Setting general funding threshold to " + threshold);
                    arbitrageManagerService.setGeneralFundingThreshold(new BigDecimal(threshold));
                } else if (text.matches("^/m (.+)")) {
                    String ticker = text.substring(text.indexOf(" ") + 1).toUpperCase();
                    String messageBefore = "Setting manual withdrawal for " + ticker;
                    send(messageBefore);
                    LOGGER.info(messageBefore);
                    try {
                        int num = arbitrageRepository.setManualWithdrawStatus(ticker);
                        String messageAfter = "Manual withdrawal set for " + num + " " + ticker + " transactions";
                        LOGGER.info(messageAfter);
                        send(messageAfter);
                    } catch (Throwable e) {
                        send("Error while setting withdraw status: " + e.toString());
                    }
                } else if (text.matches("^/restart")) {
                    send("Restarting processing");
                    arbitrageManagerService.subscribeForMarketData();
                    arbitrageManagerService.initTrading();
                } else if (text.matches("^/stop")) {
                    send("Stopping trading");
                    arbitrageManagerService.setTradingEnabled(false);
                } else if (text.matches("^/start")) {
                    send("Starting trading");
                    arbitrageManagerService.setTradingEnabled(true);
                } else if (text.matches("^/predictions") || text.matches("^/p")) {
                    send("Last predictions:");
                    sendMultipart(arbitrageManagerService.getLastPredictionsString());
                } else if (text.matches("^/f")) {
                    send("Last funding predictions:");

                    sendMultipart(arbitrageManagerService.getLastFundingPredictionsString());
                } else if (text.matches("^/a")) {
                    send("Active subscriptions:");

                    sendMultipart(arbitrageManagerService.getActiveSubscriptionsString());
                } else if (text.matches("^/l (ERROR|WARN|INFO|DEBUG)")) {
                    String levelStr = text.substring(text.indexOf(" ") + 1);

                    try {
                        level = Level.valueOf(levelStr);
                    } catch (IllegalArgumentException e) {
                        send("Unknown level: " + levelStr);
                    }

                    send("Notification level set to " + level);
                } else if (text.matches("^/sell (.*)")) {
                    String[] params = text.substring(text.indexOf(" ") + 1).split(" ");
                    if(params.length != 3) {
                        send("Invalid arguments: " + String.join(" ", params));
                    }
                } else if (text.matches("^/mt")) {
                    send("Maker triggers:");

                    sendMultipart(arbitrageManagerService.getActiveMakerTriggerOrdersString());
                } else if (text.matches("^/mstop")) {
                    send("Stopping maker trading");
                    applicationConfig.setMakerTradingEnabled(false);
                    arbitrageManagerService.stopMakerTrading();
                } else if (text.matches("^/mstart")) {
                    send("Starting maker trading");
                    applicationConfig.setMakerTradingEnabled(true);
                } else if (text.matches("^/w")) {
                    final StringBuilder sb = new StringBuilder();
                    for(Map.Entry<Exchange, IExchangeService> exchangeService : exchangeManagerService.getExchangeServices().entrySet()) {
                        sb.append(exchangeService.getKey()).append(" wallet:\n");
                        sb.append(exchangeService.getValue().getWallet().asFormattedString());
                    }
                    sendMultipart(sb.toString());
                } else if (text.matches("^/mmen")) {
                    send("Mass maker execution enabled");
                    arbitrageManagerService.setMassMakerExecutionEnabled(true);
                } else if (text.matches("^/mmdis")) {
                    send("Mass maker execution disabled");
                    arbitrageManagerService.setMassMakerExecutionEnabled(false);
                } else if (text.matches("^/rs")) {
                    try {
                        final long now = System.currentTimeMillis();
                        final Map<String, Long> in1minuteRequestCountMap = new HashMap<>();
                        final Map<String, Long> in5minutesRequestCountMap = new HashMap<>();
                        final Map<String, Long> in10minutesRequestCountMap = new HashMap<>();

                        LOGGER.info("Getting CEX request log");
                        final Map<Long, String> requestLog = PrivateCexRestQuery.getRequestLog();
                        LOGGER.info("CEX Request log size: " + requestLog.size());

                        for (Map.Entry<Long, String> entry : requestLog.entrySet()) {
                            if (entry.getKey() > (now - 60000)) {
                                if (!in1minuteRequestCountMap.containsKey(entry.getValue())) {
                                    in1minuteRequestCountMap.put(entry.getValue(), 1L);
                                } else {
                                    in1minuteRequestCountMap.put(entry.getValue(), in1minuteRequestCountMap.get(entry.getValue()) + 1);
                                }
                            }
                            if (entry.getKey() > (now - 300000)) {
                                if (!in5minutesRequestCountMap.containsKey(entry.getValue())) {
                                    in5minutesRequestCountMap.put(entry.getValue(), 1L);
                                } else {
                                    in5minutesRequestCountMap.put(entry.getValue(), in5minutesRequestCountMap.get(entry.getValue()) + 1);
                                }
                            }
                            if (entry.getKey() > (now - 600000)) {
                                if (!in10minutesRequestCountMap.containsKey(entry.getValue())) {
                                    in10minutesRequestCountMap.put(entry.getValue(), 1L);
                                } else {
                                    in10minutesRequestCountMap.put(entry.getValue(), in10minutesRequestCountMap.get(entry.getValue()) + 1);
                                }
                            }
                        }
                        final StringBuilder sb = new StringBuilder();
                        sb.append("CEX requests:\n");
                        long req1minSum = in1minuteRequestCountMap.values().stream().mapToLong(Long::longValue).sum();
                        sb.append("1 minute (total ").append(req1minSum).append("):\n");
                        for (Map.Entry<String, Long> entry : in1minuteRequestCountMap.entrySet()) {
                            sb.append("- ").append(entry.getKey()).append(": ").append(entry.getValue()).append("\n");
                        }

                        long req5minSum = in5minutesRequestCountMap.values().stream().mapToLong(Long::longValue).sum();
                        sb.append("5 minutes (total ").append(req5minSum).append("):\n");
                        for (Map.Entry<String, Long> entry : in5minutesRequestCountMap.entrySet()) {
                            sb.append("- ").append(entry.getKey()).append(": ").append(entry.getValue()).append("\n");
                        }

                        long req10minSum = in10minutesRequestCountMap.values().stream().mapToLong(Long::longValue).sum();
                        sb.append("10 minutes (total ").append(req10minSum).append("):\n");
                        for (Map.Entry<String, Long> entry : in10minutesRequestCountMap.entrySet()) {
                            sb.append("- ").append(entry.getKey()).append(": ").append(entry.getValue()).append("\n");
                        }
                        LOGGER.info(sb.toString());
                        sendMultipart(sb.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                        LOGGER.error("Error getting request log: " + e);
                    }
                } else {
                    send("Unknown command: " + text);
                }
            }

            return UpdatesListener.CONFIRMED_UPDATES_ALL;
        }
    }

    public enum Level {
        DEBUG, INFO, WARN, ERROR
    }
}
