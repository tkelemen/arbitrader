package com.tkelemen.arbitrader.service.market;

import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.model.Exchange;
import com.tkelemen.arbitrader.service.trade.ExchangeManagerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MarketDataService {
    private static final Logger logger = LoggerFactory.getLogger(MarketDataService.class);

    private Map<Exchange, OrderBookService> orderBookServices;

    private ApplicationConfig config;

    @Autowired
    public MarketDataService(ApplicationConfig config, ExchangeManagerService exchangeManagerService) {
        this.config = config;
        this.orderBookServices = exchangeManagerService.getOrderBookServices();
    }

    /**
     * This method should be called periodically, because order books can become out of sync
     */
    public void syncOrderBooks() {
        logger.info("Synchronizing order books:");

        orderBookServices.forEach((exchange, orderBookService) -> {
            orderBookService.resynchronize();
            logger.info(exchange + " connected: " + orderBookService.isConnected());
            logger.info(exchange + " subscriptions: " + orderBookService.getActiveSubscriptions().toString());
            logger.info(exchange + " order books present: " + orderBookService.getOrderBookSnapshots().keySet());
        });
    }

    public void subscribeForMarketData() {
        if (config.isPredictionsEnabled()) {
            subscribeForOrderBookUpdates();
        }
    }

    public Map<Exchange, OrderBookService> getOrderBookServices() {
        return orderBookServices;
    }

    public String getStatusString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Active subscriptions (order books):");

        for (Map.Entry<Exchange, OrderBookService> entry : orderBookServices.entrySet()) {
            sb.append(entry.getKey()).append(": ");
            sb.append(entry.getValue().isConnected() ? "connected" : "disconnected").append(" ");
            sb.append(entry.getValue().getActiveSubscriptions().size()).append(" (");
            sb.append(entry.getValue().getOrderBookSnapshots().size()).append(")");
            sb.append("\n");
        }

        return sb.toString();
    }

    public List<Exchange> getDisconnectedOrderBookProviders() {
        List<Exchange> disconnectedList = new ArrayList<>();

        for (Map.Entry<Exchange, OrderBookService> entry : orderBookServices.entrySet()) {
            if (!entry.getValue().isConnected()) {
                disconnectedList.add(entry.getKey());
            }
        }

        return disconnectedList;
    }

    private void subscribeForOrderBookUpdates() {
        orderBookServices.forEach((exchange, orderBookService) -> {
            Set<String> pairs = new HashSet<>();
            switch (exchange) {
                case CEX:
                    pairs = new HashSet<>(config.getCexAllowedPairs());
                    break;
                case BINANCE:
                    pairs = new HashSet<>(config.getBinanceAllowedPairs());
                    break;
                case KRAKEN:
                    pairs = new HashSet<>(config.getKrakenAllowedPairs());
                    break;
            }
            logger.info("Subscribing for pairs on " + exchange + ": " + pairs);

            orderBookService.subscribeForOrderBookUpdates(pairs);
        });
    }
}
