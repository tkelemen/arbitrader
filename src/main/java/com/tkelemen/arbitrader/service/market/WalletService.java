package com.tkelemen.arbitrader.service.market;

import com.tkelemen.arbitrader.config.WalletConfig;
import com.tkelemen.arbitrader.exception.AddressNotFoundException;
import com.tkelemen.arbitrader.model.Address;
import com.tkelemen.arbitrader.model.Exchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class WalletService {
    @Autowired
    private WalletConfig walletConfig;

    public Address getAddress(Exchange exchange, String ticker) throws AddressNotFoundException {
        Map<String, Address> wallet = walletConfig.getWallet(exchange);

        if (wallet == null) {
            throw new AddressNotFoundException("Address configuration not found for exchange " + exchange + " and ticker " + ticker);
        }

        Address address = wallet.get(ticker);

        if (address == null) {
            throw new AddressNotFoundException(ticker + " address not found for exchange " + exchange);
        }

        address.setExchange(exchange);

        return address;
    }
}
