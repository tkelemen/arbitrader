package com.tkelemen.arbitrader.service.market;

import com.binance.api.client.BinanceApiAsyncRestClient;
import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiWebSocketClient;
import com.binance.api.client.domain.event.DepthEvent;
import com.binance.api.client.domain.market.OrderBookEntry;
import com.binance.api.client.exception.BinanceApiException;
import com.tkelemen.arbitrader.model.DefaultOrderBook;
import com.tkelemen.arbitrader.model.Exchange;
import com.tkelemen.arbitrader.model.OrderBook;
import com.tkelemen.arbitrader.util.CurrencyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.Closeable;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;

@Service
public class BinanceOrderBookService implements OrderBookService {
    private static final long ORDER_BOOK_MAX_AGE_MS = 15000;
    private static final long ORDER_BOOK_MAX_LEVELS = 50;
    private static final long ORDER_ENTRY_MAX_AGE_MS = 60000;
    private static final long MAX_SUBSCRIPTION_AGE = 60000;

    private static final Logger LOGGER = LoggerFactory.getLogger(BinanceOrderBookService.class);

    private Set<String> desiredSubscriptions = new HashSet<>();
    private final Map<String, OrderBook> orderBookMap = new ConcurrentHashMap<>();
    private final Set<String> activeSubscriptions = new ConcurrentSkipListSet<>();
    private final Map<String, Long> lastUpdateIds = new ConcurrentHashMap<>();
    private final Map<String, String> exchangeToLocalPairMap = new ConcurrentHashMap<>();
    private final Map<String, List<DepthEvent>> eventBuffer = new ConcurrentHashMap<>();

    private final BinanceApiWebSocketClient wsClient;
    private final BinanceApiAsyncRestClient restClient;
    private final Map<String, Closeable> webSockets = new ConcurrentHashMap<>();
    private final Map<String, Long> subscriptionTimes = new ConcurrentHashMap<>();


    public BinanceOrderBookService() {
        BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance();
        wsClient = factory.newWebSocketClient();
        restClient = factory.newAsyncRestClient();
    }

    @Override
    public void subscribeForOrderBookUpdates(Set<String> symbols) {
        this.activeSubscriptions.clear();
        this.orderBookMap.clear();
        this.subscriptionTimes.clear();

        this.desiredSubscriptions = symbols;

        for (String symbol : symbols) {
            subscribe(symbol);
        }
    }

    @Override
    public void resubscribe(String symbol) {

    }

    @Override
    public void resubscribe(Set<String> symbols) {
        for(String symbol : symbols) {
            subscribe(symbol);
        }
    }

    @Override
    public Map<String, OrderBook> getOrderBookSnapshots() {
        return orderBookMap;
    }

    @Override
    public void resynchronize() {
        long now = System.currentTimeMillis();
        LOGGER.info("Synchronizing Binance order book...");

        Set<String> needsResubscribe = new HashSet<>();
        for (String pair : activeSubscriptions) {
            LOGGER.debug("Checking Binance " + pair + " order book ages...");

            long lastUpdateTimestamp = orderBookMap.get(pair).getLastUpdatedTimestamp();
            if (now - ORDER_BOOK_MAX_AGE_MS > lastUpdateTimestamp) {
                LOGGER.debug("Order book of " + pair + " out of sync, resubscribing [ last update " +
                        (now - lastUpdateTimestamp) + " ms ago ]");
                needsResubscribe.add(pair);
                continue;
            }

            if(subscriptionTimes.get(pair) != null && subscriptionTimes.get(pair) < now - MAX_SUBSCRIPTION_AGE) {
                LOGGER.info("Order book subscription for " + pair + " too old, [ " + subscriptionTimes.get(pair) + " ], resubscribing");
                needsResubscribe.add(pair);
                continue;
            }

            LOGGER.debug("Checking Binance " + pair + " order level ages...");

            int pos = 0;
            for (Map.Entry<String, Long> entry : orderBookMap.get(pair).getAskLevelTimes().entrySet()) {
                if (pos++ > 10) {
                    break;
                }

                long levelTimestamp = entry.getValue();

                if (levelTimestamp < now - ORDER_ENTRY_MAX_AGE_MS) {
                    LOGGER.debug(pair + " ask order book entry " + entry.getKey() + " (#" + pos + ") too old, resubscribing [ age " +
                            (now - levelTimestamp) + " ms ]");
                    needsResubscribe.add(pair);
                    break;
                }
            }

            pos = 0;
            for (Map.Entry<String, Long> entry : orderBookMap.get(pair).getBidLevelTimes().descendingMap().entrySet()) {
                if (pos++ > 10) {
                    break;
                }

                long levelTimestamp = entry.getValue();

                if (levelTimestamp < now - ORDER_ENTRY_MAX_AGE_MS) {
                    LOGGER.debug(pair + " bid order book entry " + entry.getKey() + " (#" + pos + ") too old, resubscribing [ age " +
                            (now - levelTimestamp) + " ms ]");
                    needsResubscribe.add(pair);
                    break;
                }
            }


            LOGGER.debug("Checking Binance " + pair + " order book sizes...");
            if (orderBookMap.get(pair).getAskLevels().size() > ORDER_BOOK_MAX_LEVELS ||
                    orderBookMap.get(pair).getAskLevels().size() > ORDER_BOOK_MAX_LEVELS) {
                LOGGER.debug("Binance " + pair + " order book has too many records, resubscribing");
                needsResubscribe.add(pair);
            }
        }

        resubscribe(needsResubscribe);
    }

    @Override
    public boolean isConnected() {
        return !webSockets.isEmpty();
    }

    @Override
    public Set<String> getDesiredSubscriptions() {
        return desiredSubscriptions;
    }

    private synchronized void subscribe(String symbol) {
        String exchangePair = transformToExchangePair(symbol);
        exchangeToLocalPairMap.put(exchangePair, symbol);

        eventBuffer.remove(exchangePair);
        lastUpdateIds.remove(exchangePair);
        activeSubscriptions.remove(symbol);
        orderBookMap.remove(symbol);

        try {
            Closeable webSocket = webSockets.get(symbol);
            if (webSocket != null) {
                webSocket.close();
            }
        } catch (IOException e) {
            LOGGER.error("Error closing websocket: " + e.toString());
        }

        Closeable webSocket = wsClient.onDepthEvent(exchangePair.toLowerCase(), response -> {
            LOGGER.debug("Received: " + response.toString());
            LOGGER.debug("Received update for " + response.getSymbol() + " [ firstUpdateId = " + response.getFirstUpdateId() + ", finalUpdateId = " + response.getFinalUpdateId() + " ]");
            if (isFirstUpdate(exchangePair)) {
                List<DepthEvent> pairResponseBuffer = getPairEventBuffer(exchangePair);

                if (pairResponseBuffer.isEmpty()) {
                    initializeOrderBook(exchangePair);
                }

                pairResponseBuffer.add(response);
            } else {
                processUpdateEvent(response);
            }
        });

        subscriptionTimes.put(symbol, System.currentTimeMillis());

        webSockets.put(symbol, webSocket);

    }

    private boolean isFirstUpdate(String exchangePair) {
        LOGGER.debug("First update for " + exchangePair);
        return lastUpdateIds.get(exchangePair) == null;
    }

    private List<DepthEvent> getPairEventBuffer(String exchangePair) {
        return eventBuffer.computeIfAbsent(exchangePair, k -> Collections.synchronizedList(new ArrayList<>()));
    }

    private void processUpdateEvent(DepthEvent event) {
        LOGGER.debug("Processing update " + event.getSymbol() + " [ firstUpdateId = " + event.getFirstUpdateId() + ", finalUpdateId = " + event.getFinalUpdateId() + " ]");

        long lastUpdateId = lastUpdateIds.get(event.getSymbol());

        if ((event.getFirstUpdateId() <= lastUpdateId && event.getFinalUpdateId() >= lastUpdateId) ||
                (event.getFirstUpdateId() == lastUpdateId + 1)) {
            lastUpdateIds.put(event.getSymbol(), event.getFinalUpdateId());
            updateOrderBook(transformFromExchangePair(event.getSymbol()), event.getAsks(), event.getBids());
        } else if (event.getFinalUpdateId() < lastUpdateId) {
            LOGGER.debug("Old update for " + event.getSymbol() + ", ignoring [ lastUpdateId = " + lastUpdateId + ", firstUpdateId = " + event.getFirstUpdateId() + ", finalUpdateId = " + event.getFinalUpdateId() + " ]");
        } else {
            LOGGER.debug("Order book of " + event.getSymbol() + " out of sync, resubscribing [ lastUpdateId = " + lastUpdateId + ", firstUpdateId = " + event.getFirstUpdateId() + ", finalUpdateId = " + event.getFinalUpdateId() + " ]");
            subscribe(transformFromExchangePair(event.getSymbol()));
        }

    }

    private void initializeOrderBook(String exchangePair) {
        try {
            restClient.getOrderBook(exchangePair.toUpperCase(), 10, orderBook -> {
                String pair = transformFromExchangePair(exchangePair);

                this.lastUpdateIds.put(exchangePair, orderBook.getLastUpdateId());
                this.activeSubscriptions.add(pair);

                LOGGER.info("Initializing order book " + exchangePair + " [ lastUpdateId = " + orderBook.getLastUpdateId() + " ]");
                LOGGER.debug(orderBook.toString());

                updateOrderBook(pair, orderBook.getAsks(), orderBook.getBids());

                processBufferedEvents(exchangePair);
            });
        } catch (BinanceApiException e) {
            LOGGER.error("Failed to initialize " + exchangePair + " orderbook: " + exchangePair);
        }
    }

    private void processBufferedEvents(String exchangePair) {
        synchronized (this) {
            if (eventBuffer.get(exchangePair) != null) {
                for (DepthEvent event : eventBuffer.get(exchangePair)) {
                    processUpdateEvent(event);
                }

                eventBuffer.remove(exchangePair);
            }
        }
    }

    private void updateOrderBook(String pair, List<OrderBookEntry> asks, List<OrderBookEntry> bids) {
        OrderBook orderBook = orderBookMap.get(pair);

        if (orderBook == null) {
            orderBook = new DefaultOrderBook(pair);
            orderBook.setExchange(Exchange.BINANCE);
            orderBookMap.put(pair, orderBook);
        }

        orderBook.recordUpdate(new OrderBookUpdate(asks, bids));

        for (OrderBookEntry entry : asks) {
            BigDecimal qty = new BigDecimal(entry.getQty());

            if (qty.compareTo(BigDecimal.ZERO) == 0) {
                LOGGER.debug("Removing " + pair + " ask price level " + entry.getPrice());
                LOGGER.debug(orderBook.toString());
                BigDecimal removedQty = orderBook.removeAskLevel(entry.getPrice());

                if (removedQty == null) {
                    LOGGER.debug("Could not find ask price to remove: " + entry.getPrice());
                    LOGGER.debug(orderBook.toString());
                } else {
                    LOGGER.debug("Removed ask qty for price " + entry.getPrice() + ": " + removedQty);
                }
            } else {
                orderBook.setAskLevel(entry.getPrice(), qty);
            }
        }

        for (OrderBookEntry entry : bids) {
            BigDecimal qty = new BigDecimal(entry.getQty());

            if (qty.compareTo(BigDecimal.ZERO) == 0) {
                LOGGER.debug("Removing " + pair + " bid price level " + entry.getPrice());
                LOGGER.debug(orderBook.toString());
                BigDecimal removedQty = orderBook.removeBidLevel(entry.getPrice());

                if (removedQty == null) {
                    LOGGER.debug("Could not find bid price to remove: " + entry.getPrice());
                    LOGGER.debug(orderBook.toString());
                } else {
                    LOGGER.debug("Removed bid qty for price " + entry.getPrice() + ": " + removedQty);
                }
            } else {
                orderBook.setBidLevel(entry.getPrice(), qty);
            }
        }
    }

    @Override
    public Set<String> getActiveSubscriptions() {
        return this.activeSubscriptions;
    }

    @Override
    public Set<String> getActiveConnections() {
        return webSockets.keySet();
    }

    private String transformToExchangePair(String pair) {
        if (pair == null) {
            return pair;
        }
        return toBinanceAsset(CurrencyUtils.getBase(pair)) + CurrencyUtils.getQuote(pair);
    }

    private String transformFromExchangePair(String pair) {
        return exchangeToLocalPairMap.get(pair);
    }

    public static String toBinanceAsset(String asset) {
        if (asset.equals("LUNA")) {
            return "LUNC";
        }
        if (asset.equals("LUNA2")) {
            return "LUNA";
        }

        return asset;
    }

    private static class OrderBookUpdate {
        private final List<OrderBookEntry> asks;
        private final List<OrderBookEntry> bids;

        public OrderBookUpdate(List<OrderBookEntry> asks, List<OrderBookEntry> bids) {
            this.asks = asks;
            this.bids = bids;
        }

        @Override
        public String toString() {
            return "OrderBookUpdate{" +
                    "asks=" + asks +
                    ", bids=" + bids +
                    '}';
        }
    }
}
