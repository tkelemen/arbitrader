package com.tkelemen.arbitrader.service.market;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.tkelemen.arbitrader.client.cex.ws.CexWsClient;
import com.tkelemen.arbitrader.client.cex.ws.message.*;
import com.tkelemen.arbitrader.client.ws.WsEventCallback;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.exception.WebsocketClientException;
import com.tkelemen.arbitrader.model.DefaultOrderBook;
import com.tkelemen.arbitrader.model.Exchange;
import com.tkelemen.arbitrader.model.OrderBook;
import com.tkelemen.arbitrader.util.CurrencyUtils;
import org.java_websocket.handshake.Handshakedata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;

@Service
public class CexOrderBookService implements OrderBookService {
    private static final long ORDER_BOOK_MAX_AGE_MS = 15000;
    private static final long ORDER_ENTRY_MAX_AGE_MS = 120000;
    private static final long MAX_SUBSCRIPTION_AGE = 60000;

    private static final Logger LOGGER = LoggerFactory.getLogger(CexOrderBookService.class);

    private final ObjectReader objectReader = (new ObjectMapper()).readerFor(CexEvent.class);
    private final ObjectWriter objectWriter = (new ObjectMapper()).writer();

    private CexWsClient client;

    private final ApplicationConfig config;

    private final Set<String> desiredSubscriptions = new ConcurrentSkipListSet<>();
    private final Set<String> activeSubscriptions = new ConcurrentSkipListSet<>();
    private final Map<String, Long> lastUpdateIds = new ConcurrentHashMap<>();
    private final Map<String, OrderBookData> lastUpdates = new ConcurrentHashMap<>();
    private final Map<String, OrderBook> orderBookMap = new ConcurrentHashMap<>();
    private final Map<String, Long> subscriptionTimes = new ConcurrentHashMap<>();
    private final Map<String, String> exchangeToLocalPairMap = new ConcurrentHashMap<>();

    private long lastConnectTime;

    @Value("${scheduler.reconnectRateMs}")
    private long reconnectRate;

    @Autowired
    public CexOrderBookService(ApplicationConfig config) {
        this.config = config;
    }

    @Override
    public void subscribeForOrderBookUpdates(Set<String> symbols) {
        this.desiredSubscriptions.clear();
        this.desiredSubscriptions.addAll(symbols);

        cleanup();

        try {
            client = new CexWsClient(config);
            lastConnectTime = System.currentTimeMillis();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        client.connect(new EventProcessorCallback(symbols));
    }

    @Override
    public Map<String, OrderBook> getOrderBookSnapshots() {
        return orderBookMap;
    }

    @Override
    public void resynchronize() {
        LOGGER.info("Synchronizing CEX order book...");

        if (client == null || !client.isConnected()) {
           LOGGER.error("Client disconnected, cannot resynchronize");
           return;
        }

        long now = System.currentTimeMillis();

        // TODO: refactor
        long reconnectIn = lastConnectTime + reconnectRate - now;
        if(reconnectIn < MAX_SUBSCRIPTION_AGE/2) {
            LOGGER.info("Reconnecting in  " + reconnectIn + "ms, skipping sync");
            return;
        }

        Set<String> needsResubscribe = new HashSet<>();
        for (String pair : desiredSubscriptions) {
            OrderBook orderBook = orderBookMap.get(pair);

            if (orderBook == null) {
                LOGGER.error("Could not synchronize CEX order book for " + pair + ": not found");
                continue;
            }

            if (subscriptionTimes.get(pair) != null && subscriptionTimes.get(pair) < now - MAX_SUBSCRIPTION_AGE) {
                LOGGER.info("Order book subscription for " + pair + " too old, [ " + subscriptionTimes.get(pair) + " ]");
                needsResubscribe.add(pair);
                continue;
            }

            if (now - ORDER_BOOK_MAX_AGE_MS > orderBook.getLastUpdatedTimestamp()) {
                LOGGER.info("Order book " + pair + " out of sync");
                needsResubscribe.add(pair);
                continue;
            }

            int pos = 0;
            for (Map.Entry<String, Long> entry : orderBook.getAskLevelTimes().entrySet()) {
                if(pos++ > 10) {
                    break; // checking only first 10 records
                }
                if (entry.getValue() < now - ORDER_ENTRY_MAX_AGE_MS) {
                    LOGGER.info(pair + " ask order book entry " + entry.getKey() + " too old");
                    needsResubscribe.add(pair);
                    break;
                }
            }

            pos = 0;
            for (Map.Entry<String, Long> entry : orderBook.getBidLevelTimes().entrySet()) {
                if(pos++ > 10) {
                    break; // checking only first 10 records
                }
                if (entry.getValue() < now - ORDER_ENTRY_MAX_AGE_MS) {
                    LOGGER.info(pair + " bid order book entry " + entry.getKey() + " too old");
                    needsResubscribe.add(pair);
                    break;
                }
            }
        }

        resubscribe(needsResubscribe);
    }

    private void updateOrderBook(String pair, OrderBookData data) {
        OrderBook orderBook = orderBookMap.get(pair);

        if (orderBook == null) {
            orderBook = new DefaultOrderBook(pair);
            orderBook.setExchange(Exchange.CEX);
            orderBookMap.put(pair, orderBook);
        }

        orderBook.recordUpdate(data);

        for (BigDecimal[] entry : data.getAsks()) {
            String price = entry[0].toPlainString();
            BigDecimal qty = entry[1];

            if (qty.compareTo(BigDecimal.ZERO) == 0) {
                LOGGER.debug("Removing " + pair + " ask price level " + price);
                BigDecimal removedQty = orderBook.removeAskLevel(price);

                if (removedQty == null) {
                    LOGGER.debug("Could not find " + pair + " ask price to remove: " + price);
                    LOGGER.debug(orderBook.toString());
                } else {
                    LOGGER.debug("Removed ask qty for price " + price + ": " + removedQty);
                }
            } else {
                orderBook.setAskLevel(price, qty);
            }
        }

        for (BigDecimal[] entry : data.getBids()) {
            String price = entry[0].toPlainString();
            BigDecimal qty = entry[1];

            if (qty.compareTo(BigDecimal.ZERO) == 0) {
                LOGGER.debug("Removing " + pair + " bid price level " + price);
                BigDecimal removedQty = orderBook.removeBidLevel(price);

                if (removedQty == null) {
                    LOGGER.debug("Could not find " + pair + " bid price to remove: " + price);
                    LOGGER.debug(orderBook.toString());
                } else {
                    LOGGER.debug("Removed bid qty for price " + price + ": " + removedQty);
                }
            } else {
                orderBook.setBidLevel(price, qty);
            }
        }
    }

    @Override
    public Set<String> getActiveSubscriptions() {
        return this.activeSubscriptions;
    }

    @Override
    public Set<String> getActiveConnections() {
        return null;
    }

    @Override
    public void resubscribe(String pair) {
        LOGGER.info("Resubscribing for order book " + pair);
        orderBookMap.remove(pair);

        try {
            subscribe(pair);
        } catch (WebsocketClientException e) {
            LOGGER.error("Resubscribe failed: " + e);
            e.printStackTrace();
        }
        lastUpdateIds.remove(pair);
    }

    public void subscribe(String pair) throws WebsocketClientException {
        subscriptionTimes.put(pair, System.currentTimeMillis());

        String exchangePair = transformToExchangePair(pair);
        exchangeToLocalPairMap.put(exchangePair, pair);

        client.subscribe(exchangePair);
    }

    @Override
    public void resubscribe(Set<String> symbols) {
        Timer timer = new Timer();

        int seq = 0;
        for (String symbol : symbols) {
            timer.schedule(new TimerTask() {
                               @Override
                               public void run() {
                                   resubscribe(symbol);
                               }
                           },
                    250L * seq++);
        }
    }

    private static String toGeneralPair(String cexPair) {
        return cexPair.replace(':', '/');
    }

    class EventProcessorCallback implements WsEventCallback {
        private static final int MAX_AUTH_TRIES = 3;

        private final Set<String> symbols;
        private int authTries = 0;

        EventProcessorCallback(Set<String> symbols) {
            this.symbols = symbols;
        }

        @Override
        public void onOpen(Handshakedata handshakedata) {
            client.sendAuthenticationRequest();
        }

        @Override
        public void onClose(int code, String reason, boolean remote) {
            if (remote) {
                LOGGER.info("Disconnected, reconnecting, resubscribing");

                subscribeForOrderBookUpdates(symbols);
            }
        }

        @Override
        public void onMessage(String message) {
            try {
                CexEvent event = objectReader.readValue(message);

                if (event instanceof PingEvent) {
                    client.send(objectWriter.writeValueAsString(new PongResponse()));
                } else if (event instanceof AuthEvent) {
                    LOGGER.info("AuthEvent received: " + event);
                    if (AuthEvent.OK.equals(((AuthEvent) event).getOk())) {
                        for (String symbol : desiredSubscriptions) {
                            subscribe(symbol);
                        }
                    } else {
                        LOGGER.error("Not logged in: " + event);
                        if(authTries++ < MAX_AUTH_TRIES) {
                            Timer timer = new Timer();
                            timer.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    LOGGER.info("Retrying login... ");
                                    client.sendAuthenticationRequest();
                                }
                            }, 1000);
                        } else {
                            cleanup();
                        }
                    }
                } else if (event instanceof OrderBookUpdateEvent) {
                    final OrderBookData data = ((OrderBookUpdateEvent) event).getData();
                    if (data.getError() != null) {
                        LOGGER.error("Order book subscription error: " + data.getError());
                    } else {
                        String pair = transformFromExchangePair(toGeneralPair(data.getPair()));

                        if (event instanceof OrderBookSubscribeEvent) {
                            subscriptionTimes.put(pair, System.currentTimeMillis());
                            activeSubscriptions.add(pair);

                            if (orderBookMap.containsKey(pair)) {
                                orderBookMap.get(pair).reset();
                            }
                        }

                        Long lastUpdateId = lastUpdateIds.get(pair);

                        if (lastUpdateId != null && data.getId() != lastUpdateId && data.getId() != (lastUpdateId + 1)) {
                            LOGGER.error("Received update for " + pair + " is out of sync (" + (data.getId() + " <-> " + (lastUpdateId + 1)) + "), resubscribing");
                            LOGGER.error("Last update: " + lastUpdates.get(pair));
                            LOGGER.error("Current update: " + data);

                            resubscribe(pair);
                        } else {
                            updateOrderBook(pair, data);
                            lastUpdateIds.put(pair, data.getId());
                            lastUpdates.put(pair, data);
                        }
                    }
                } else if (event instanceof TransactionEvent) {
                    LOGGER.info("Transaction info: " + ((TransactionEvent) event).getData());
                } else {
                    LOGGER.error("Unknown message: " + message);
                }
            } catch (JsonProcessingException | WebsocketClientException e) {
                LOGGER.error(e.toString());
            }
        }
    }

    @Override
    public boolean isConnected() {
        return client != null && client.isConnected();
    }

    @Override
    public Set<String> getDesiredSubscriptions() {
        return desiredSubscriptions;
    }

    private void cleanup() {
        if (client != null) {
            client.close();
        }

        orderBookMap.clear();
        activeSubscriptions.clear();
        subscriptionTimes.clear();
        lastUpdateIds.clear();
        lastUpdates.clear();
    }

    private String transformToExchangePair(String pair) {
        if (pair == null) {
            return pair;
        }
        return toCexAsset(CurrencyUtils.getBase(pair)) + "/" + CurrencyUtils.getQuote(pair);
    }

    private String transformFromExchangePair(String pair) {
        return exchangeToLocalPairMap.get(pair);
    }

    public static String toCexAsset(String asset) {
        if (asset.equals("LUNA")) {
            return "LUNC";
        }
        //if (asset.equals("LUNA2")) {
        //    return "LUNA";
        //}

        return asset;
    }
}
