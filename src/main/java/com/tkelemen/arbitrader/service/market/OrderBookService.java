package com.tkelemen.arbitrader.service.market;

import com.tkelemen.arbitrader.model.OrderBook;

import java.util.Map;
import java.util.Set;

public interface OrderBookService {
    void subscribeForOrderBookUpdates(Set<String> symbols);

    void resubscribe(String symbol);

    void resubscribe(Set<String> symbols);

    Map<String, OrderBook> getOrderBookSnapshots();

    void resynchronize();

    boolean isConnected();

    Set<String> getDesiredSubscriptions();

    Set<String> getActiveSubscriptions();

    Set<String> getActiveConnections();
}
