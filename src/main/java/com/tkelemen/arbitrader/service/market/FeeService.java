package com.tkelemen.arbitrader.service.market;

import com.tkelemen.arbitrader.config.FeeConfig;
import com.tkelemen.arbitrader.model.Exchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;

@Service
public class FeeService {
    @Autowired
    private FeeConfig feeConfig;

    public BigDecimal getTradingFeeTaker(Exchange exchange) {
        return feeConfig.getTradingTaker().get(exchange);
    }

    public BigDecimal getTradingFeeMaker(Exchange exchange) {
        return feeConfig.getTradingMaker().get(exchange);
    }

    public BigDecimal getWithdrawFee(Exchange exchange, String currency) {
        Map<String, BigDecimal> exchangeFees = feeConfig.getWithdraw().get(exchange);

        if (exchangeFees != null && exchangeFees.get(currency) != null) {
            return exchangeFees.get(currency);
        } else {
            return BigDecimal.ZERO;
        }
    }

    public BigDecimal getDepositFee(Exchange exchange, String currency) {
        Map<String, BigDecimal> exchangeFees = feeConfig.getDeposit().get(exchange);

        if (exchangeFees != null && exchangeFees.get(currency) != null) {
            return exchangeFees.get(currency);
        } else {
            return BigDecimal.ZERO;
        }
    }
}
