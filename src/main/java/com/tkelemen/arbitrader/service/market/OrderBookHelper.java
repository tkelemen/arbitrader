package com.tkelemen.arbitrader.service.market;

import com.tkelemen.arbitrader.model.OrderBook;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class OrderBookHelper {
    public static synchronized BigDecimal calculateBaseQuantity(final OrderBook orderbook, BigDecimal quoteQuantity) {
        BigDecimal baseQty = BigDecimal.ZERO;

        final Set<Map.Entry<String, BigDecimal>> entries = orderbook.getAskLevels().entrySet();

        final Iterator<Map.Entry<String, BigDecimal>> entryIterator = entries.iterator();
        while (quoteQuantity.compareTo(BigDecimal.ZERO) > 0 && entryIterator.hasNext()) {
            final Map.Entry<String, BigDecimal> entry = entryIterator.next();

            final BigDecimal entryPrice = new BigDecimal(entry.getKey());
            final BigDecimal entryValue = entry.getValue();
            final BigDecimal entryTotal = entryValue.multiply(entryPrice);

            if (entryTotal.compareTo(quoteQuantity) > 0) {
                baseQty = baseQty.add(quoteQuantity.divide(entryPrice, 8, RoundingMode.HALF_UP));
                quoteQuantity = BigDecimal.ZERO;
            } else {
                baseQty = baseQty.add(entryValue);
                quoteQuantity = quoteQuantity.subtract(entryTotal);
            }
        }

        return baseQty.setScale(8, RoundingMode.HALF_UP);
    }

    public static synchronized BigDecimal calculateQuoteQuantity(final OrderBook orderbook, BigDecimal baseQuantity) {
        BigDecimal quoteQuantity = BigDecimal.ZERO;

        final Set<Map.Entry<String, BigDecimal>> entries = orderbook.getBidLevels().descendingMap().entrySet();

        final Iterator<Map.Entry<String, BigDecimal>> entryIterator = entries.iterator();
        while (baseQuantity.compareTo(BigDecimal.ZERO) > 0 && entryIterator.hasNext()) {
            final Map.Entry<String, BigDecimal> entry = entryIterator.next();

            final BigDecimal entryPrice = new BigDecimal(entry.getKey());
            final BigDecimal entryValue = entry.getValue();
            final BigDecimal entryTotal = entryValue.multiply(entryPrice);

            if (entryValue.compareTo(baseQuantity) > 0) {
                quoteQuantity = quoteQuantity.add(baseQuantity.multiply(entryPrice));
                baseQuantity = BigDecimal.ZERO;
            } else {
                quoteQuantity = quoteQuantity.add(entryTotal);
                baseQuantity = baseQuantity.subtract(entryValue);
            }
        }

        return quoteQuantity.setScale(8, RoundingMode.HALF_UP);
    }

    public static synchronized BigDecimal calculateBuyQuoteQuantity(final OrderBook orderbook, BigDecimal baseQuantityToBuy) {
        BigDecimal quoteQuantity = BigDecimal.ZERO;

        final Set<Map.Entry<String, BigDecimal>> entries = orderbook.getAskLevels().entrySet();

        final Iterator<Map.Entry<String, BigDecimal>> entryIterator = entries.iterator();
        while (baseQuantityToBuy.compareTo(BigDecimal.ZERO) > 0 && entryIterator.hasNext()) {
            final Map.Entry<String, BigDecimal> entry = entryIterator.next();

            final BigDecimal entryPrice = new BigDecimal(entry.getKey());
            final BigDecimal entryValue = entry.getValue();
            final BigDecimal entryTotal = entryValue.multiply(entryPrice);

            if (entryValue.compareTo(baseQuantityToBuy) > 0) {
                quoteQuantity = quoteQuantity.add(baseQuantityToBuy.multiply(entryPrice));
                baseQuantityToBuy = BigDecimal.ZERO;
            } else {
                quoteQuantity = quoteQuantity.add(entryTotal);
                baseQuantityToBuy = baseQuantityToBuy.subtract(entryValue);
            }
        }

        return quoteQuantity.setScale(8, RoundingMode.HALF_UP);
    }
}
