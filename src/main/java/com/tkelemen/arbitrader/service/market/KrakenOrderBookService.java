package com.tkelemen.arbitrader.service.market;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.tkelemen.arbitrader.client.kraken.ws.KrakenWsClient;
import com.tkelemen.arbitrader.client.ws.WsEventCallback;
import com.tkelemen.arbitrader.client.kraken.ws.message.*;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.exception.WebsocketClientException;
import com.tkelemen.arbitrader.model.Exchange;
import com.tkelemen.arbitrader.model.OrderBook;
import com.tkelemen.arbitrader.model.KrakenOrderBook;
import org.java_websocket.handshake.Handshakedata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.URISyntaxException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.zip.CRC32;

@Service
public class KrakenOrderBookService implements OrderBookService {
    private static final long ORDER_BOOK_MAX_AGE_MS = 10000;
    private static final long ORDER_ENTRY_MAX_AGE_MS = 60000;
    private static final long MAX_SUBSCRIPTION_AGE = 60000;

    private static final Logger LOGGER = LoggerFactory.getLogger(KrakenOrderBookService.class);

    private KrakenWsClient client;

    private final ApplicationConfig config;

    private final Set<String> desiredSubscriptions = new ConcurrentSkipListSet<>();
    private final Map<String, OrderBook> orderBookMap = new ConcurrentHashMap<>();
    private final Set<String> activeSubscriptions = new ConcurrentSkipListSet<>();
    private final Map<String, Long> subscriptionStats = new ConcurrentHashMap<>();
    private final Map<String, Long> subscriptionTimes = new ConcurrentHashMap<>();

    private final ObjectReader objectReader = (new ObjectMapper()).readerFor(KrakenEvent.class);

    private long lastConnectTime;

    @Value("${scheduler.reconnectRateMs}")
    private long reconnectRate;

    private Timer subscriptionTimer;

    @Autowired
    public KrakenOrderBookService(ApplicationConfig config) {
        this.config = config;
    }

    @Override
    public void subscribeForOrderBookUpdates(Set<String> symbols) {
        cleanup();

        this.desiredSubscriptions.clear();
        this.desiredSubscriptions.addAll(symbols);

        try {
            client = new KrakenWsClient(config);

            client.connect(new OrderBookUpdateProcessorCallback(symbols));
            lastConnectTime = System.currentTimeMillis();
        } catch (URISyntaxException e) {
            LOGGER.error("Failed to create Kraken WS client: " + e.toString());
        }
    }

    private void subscribeForSymbols(Set<String> symbols) {
        subscriptionTimer = new Timer();

        int seq = 0;
        for (String symbol : symbols) {
            subscriptionTimer.schedule(new TimerTask() {
                               @Override
                               public void run() {
                                   try {
                                       client.subscribe(symbol);
                                   } catch (WebsocketClientException e) {
                                       LOGGER.error(e.toString());
                                   }
                               }
                           },
                    350L * seq++);
        }
    }

    @Override
    public Map<String, OrderBook> getOrderBookSnapshots() {
        return orderBookMap;
    }

    @Override
    public void resynchronize() {
        final long now = System.currentTimeMillis();
        LOGGER.info("Synchronizing Kraken order books...");

        // TODO: refactor
        long reconnectIn = lastConnectTime + reconnectRate - now;
        if(reconnectIn < MAX_SUBSCRIPTION_AGE/2) {
            LOGGER.info("Reconnecting in  " +  reconnectIn + "ms, skipping sync");
            return;
        }

        if(!isConnected()) {
            LOGGER.warn("Kraken exchange is not connected, skipping resynchronization");
            return;
        }

        final Set<String> needsResubscribe = new HashSet<>();
        for (String pair : desiredSubscriptions) {
            OrderBook orderBook = orderBookMap.get(pair);

            if (orderBook == null) {
                LOGGER.error("Order book for pair " + pair + " not found");
                continue;
            }

            if(subscriptionTimes.get(pair) != null && subscriptionTimes.get(pair) < now - MAX_SUBSCRIPTION_AGE) {
                LOGGER.info("Order book subscription for " + pair + " too old, [ " + subscriptionTimes.get(pair) + " ], resubscribing");
                needsResubscribe.add(pair);
                continue;
            }

            if (now - ORDER_BOOK_MAX_AGE_MS > orderBook.getLastUpdatedTimestamp()) {
                LOGGER.info("Order book of " + pair + " out of sync [ " + orderBook.getLastUpdatedTimestamp() + " ], resubscribing");
                needsResubscribe.add(pair);
                continue;
            }

            int pos = 0;
            for (Map.Entry<String, Long> entry : orderBook.getAskLevelTimes().entrySet()) {
                if(pos++ > 10) {
                    break; // checking only first 10 records
                }
                if (entry.getValue() < now - ORDER_ENTRY_MAX_AGE_MS) {
                    LOGGER.info(pair + " ask order book entry " + entry.getKey() + " too old");
                    needsResubscribe.add(pair);
                    break;
                }
            }

            pos = 0;
            for (Map.Entry<String, Long> entry : orderBook.getBidLevelTimes().descendingMap().entrySet()) {
                if(pos++ > 10) {
                    break; // checking only first 10 records
                }
                if (entry.getValue() < now - ORDER_ENTRY_MAX_AGE_MS) {
                    LOGGER.info(pair + " bid order book entry " + entry.getKey() + " too old");
                    needsResubscribe.add(pair);
                    break;
                }
            }
        }

        resubscribe(needsResubscribe);
    }

    public synchronized void resubscribe(String pair) {
        if (orderBookMap.containsKey(pair)) {
            orderBookMap.get(pair).reset();
        }
        if (activeSubscriptions.contains(pair)) {
            activeSubscriptions.remove(pair);

            try {
                LOGGER.info("Unsubscribing/resubscribing " + pair);
                client.unsubscribe(pair);
            } catch (WebsocketClientException e) {
                LOGGER.error("Resubscribe failed: " + e);
                e.printStackTrace();
            }

            Long pairResubscriptions = subscriptionStats.get(pair);
            if (pairResubscriptions == null) {
                pairResubscriptions = 0L;
            }
            subscriptionStats.put(pair, ++pairResubscriptions);
        }
    }

    @Override
    public void resubscribe(Set<String> symbols) {
        Timer timer = new Timer();

        int seq = 0;
        for (String symbol : symbols) {
            timer.schedule(new TimerTask() {
                               @Override
                               public void run() {
                                   resubscribe(symbol);
                               }
                           },
                    500L * seq++);
        }
    }

    @Override
    public boolean isConnected() {
        return client != null && client.isConnected();
    }

    @Override
    public Set<String> getActiveSubscriptions() {
        return this.activeSubscriptions;
    }

    @Override
    public Set<String> getActiveConnections() {
        return null;
    }

    private OrderBook updateOrderBook(String pair, OrderBookData data) {
        OrderBook orderBook = orderBookMap.get(pair);

        if (orderBook == null) {
            orderBook = new KrakenOrderBook(pair);
            orderBook.setExchange(Exchange.KRAKEN);
            orderBookMap.put(pair, orderBook);
        }

        orderBook.recordUpdate(data);

        for (OrderBookEntry entry : data.getAskEntries()) {
            if (Float.parseFloat(entry.getQuantity()) == 0) {
                orderBook.removeAskLevel(entry.getPrice());
            } else {
                ((KrakenOrderBook) orderBook).setAskLevel(entry.getPrice(), entry.getQuantity());
            }
        }

        for (OrderBookEntry entry : data.getBidEntries()) {
            if (Float.parseFloat(entry.getQuantity()) == 0) {
                orderBook.removeBidLevel(entry.getPrice());
            } else {
                ((KrakenOrderBook) orderBook).setBidLevel(entry.getPrice(), entry.getQuantity());
            }
        }

        return orderBook;
    }

    private static long calculateChecksum(OrderBook orderBook) {
        NavigableSet<String> askPrices = ((ConcurrentSkipListMap) orderBook.getAskLevels()).navigableKeySet();
        NavigableSet<String> bidPrices = ((ConcurrentSkipListMap) orderBook.getBidLevels()).descendingKeySet();

        StringBuilder bidInput = new StringBuilder();
        StringBuilder askInput = new StringBuilder();

        if (askPrices.size() < 10 || bidPrices.size() < 10) {
            return 0;
        }

        Iterator<String> askIterator = askPrices.iterator();
        Iterator<String> bidIterator = bidPrices.iterator();

        for (int i = 0; i < 10; i++) {
            String askPrice = askIterator.next();
            String bidPrice = bidIterator.next();
            String askQty = orderBook.getAskLevelQuantity(askPrice).toPlainString();
            String bidQty = orderBook.getBidLevelQuantity(bidPrice).toPlainString();

            askInput.append(formatNumberForChecksum(askPrice));
            askInput.append(formatNumberForChecksum(askQty));
            bidInput.append(formatNumberForChecksum(bidPrice));
            bidInput.append(formatNumberForChecksum(bidQty));
        }

        String crcInput = askInput.append(bidInput).toString();

        LOGGER.debug("CRC32 input: " + crcInput);

        CRC32 crc32 = new CRC32();
        crc32.update(crcInput.getBytes());

        long checksum = crc32.getValue();
        LOGGER.debug("CRC32 value: " + checksum);

        return checksum;
    }

    private static String transformPair(String pair) {
        if (pair == null) {
            return pair;
        }
        return pair.replaceFirst("XBT", "BTC").replaceFirst("XDG", "DOGE");
    }

    private static String formatNumberForChecksum(String input) {
        return input.replaceAll("\\.", "").replaceFirst("^0*", "");
    }

    class OrderBookUpdateProcessorCallback implements WsEventCallback {
        private final Set<String> desiredSymbols;

        public OrderBookUpdateProcessorCallback(Set<String> desiredSymbols) {
            this.desiredSymbols = desiredSymbols;
        }

        @Override
        public void onOpen(Handshakedata handshakedata) {
            subscribeForSymbols(this.desiredSymbols);
        }

        @Override
        public void onClose(int code, String reason, boolean remote) {
            if (code != 1000) {
                LOGGER.info("Kraken WS disconnected, reconnecting, resubscribing [ code = " + code + ", reason = " + reason + " ]");

                subscribeForOrderBookUpdates(this.desiredSymbols);
            }
        }

        @Override
        public void onMessage(String message) {
            long now = System.currentTimeMillis();

            try {
                KrakenEvent event = objectReader.readValue(message);

                if (event instanceof OrderBookSnapshot) {
                    String pair = transformPair(((OrderBookSnapshot) event).getSymbol());

                    OrderBookData data = ((OrderBookSnapshot) event).getOrderBookData();

                    LOGGER.debug("Order book update received for " + pair);

                    if(!activeSubscriptions.contains(pair)) {
                        LOGGER.debug("Order book update pair " + pair + " not in active subscriptions list, ignoring");
                        return;
                    }

                    OrderBook updatedOrderBook = updateOrderBook(pair, data);

                    boolean verifyChecksum = now % config.getKrakenOrderBookChecksumCalculationFrequency() == 0;

                    if (verifyChecksum && activeSubscriptions.contains(pair)) {
                        long checksum = calculateChecksum(updatedOrderBook);

                        if (data.getChecksum() == 0) {
                            LOGGER.debug("Checksum not received, not verifying");
                        } else if (checksum != data.getChecksum()) {
                            LOGGER.debug("Wrong checksum for pair " + pair + ": " + checksum + " [received " + data.getChecksum() + "]");

                            resubscribe(pair);
                        } else {
                            LOGGER.debug("Checksum OK for pair " + pair);
                        }
                    }
                } else if (event instanceof SubscriptionStatus) {
                    String pair = transformPair(((SubscriptionStatus) event).getPair());

                    if ("unsubscribed".equals(((SubscriptionStatus) event).getStatus())) {
                        OrderBook orderbook = orderBookMap.get(pair);
                        if (orderbook != null) {
                            orderbook.reset();
                        }

                        LOGGER.debug("Unsubcribed from order book " + pair + ", subscribing again");
                        client.subscribe(((SubscriptionStatus) event).getPair());
                    } else if ("subscribed".equals(((SubscriptionStatus) event).getStatus())) {
                        OrderBook orderbook = orderBookMap.get(pair);
                        if (orderbook != null) {
                            orderbook.reset();
                        }

                        LOGGER.debug("Subscribed for order book " + pair);
                        subscriptionTimes.put(pair, now);
                        activeSubscriptions.add(pair);
                    }
                } else if (event instanceof Heartbeat) {
                    LOGGER.debug("Heartbeat received");
                } else if (event instanceof ErrorEvent) {
                    LOGGER.error("WS error event received: " + event);

                    if("Exceeded msg rate".equals(((ErrorEvent) event).getErrorMessage())) {
                        LOGGER.error("Exceeded message rate, stopping updates");
                        cleanup();
                    }
                } else {
                    LOGGER.error("Unknown message received: " + message);
                }
            } catch (JsonProcessingException | WebsocketClientException e) {
                LOGGER.error(e.toString());
            }
        }
    }

    private void cleanup() {
        if (client != null) {
            client.close();
        }

        orderBookMap.clear();
        activeSubscriptions.clear();
        subscriptionTimes.clear();

        if(subscriptionTimer != null) {
            subscriptionTimer.cancel();
            subscriptionTimer.purge();
        }
    }

    @Override
    public Set<String> getDesiredSubscriptions() {
        return desiredSubscriptions;
    }
}
