package com.tkelemen.arbitrader.service.log;

import com.tkelemen.arbitrader.db.entity.Arbitrage;
import com.tkelemen.arbitrader.db.entity.Prediction;
import com.tkelemen.arbitrader.db.repository.ArbitrageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Service
public class DbLoggerService {
    private List<Object> entitiesToSave = new CopyOnWriteArrayList<>();

    @Autowired
    private ArbitrageRepository arbitrageRepository;

    public void saveEntities() {
        for (Object entity : entitiesToSave) {
            if (entity instanceof Arbitrage) {
                arbitrageRepository.save((Arbitrage) entity);
            }
        }
    }

    public void logTrade(
            Prediction prediction,
            BigDecimal realInitialAmount,
            BigDecimal realFinalAmount,
            BigDecimal realBuyFee,
            BigDecimal realSellFee,
            BigDecimal realTransferFee
    ) {
        Arbitrage arbitrage = new Arbitrage();
        arbitrage.setPrediction(prediction);
        arbitrage.setInitialAmount(realInitialAmount);
        arbitrage.setReceivedAmount(realFinalAmount);
        arbitrage.setBuyFee(realBuyFee);
        arbitrage.setSellFee(realSellFee);
        arbitrage.setWithdrawFee(realTransferFee);

        entitiesToSave.add(arbitrage);
    }
}
