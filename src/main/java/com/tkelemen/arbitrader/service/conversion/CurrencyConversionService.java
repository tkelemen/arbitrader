package com.tkelemen.arbitrader.service.conversion;

import com.tkelemen.arbitrader.model.Conversion;
import com.tkelemen.arbitrader.model.Exchange;
import com.tkelemen.arbitrader.model.OrderBook;
import com.tkelemen.arbitrader.service.market.OrderBookHelper;
//import com.tkelemen.arbitrader.service.trade.ExchangeManagerService;
import com.tkelemen.arbitrader.service.trade.ExchangeManagerService;
import com.tkelemen.arbitrader.util.CurrencyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
public class CurrencyConversionService {
    private ExchangeManagerService exchangeManagerService;

    @Autowired
    public CurrencyConversionService(ExchangeManagerService exchangeManagerService) {
        this.exchangeManagerService = exchangeManagerService;
    }

    public List<Conversion> getConversionPossibilities(Exchange fromExchange, Exchange toExchange, String fromAsset, String toAsset, BigDecimal amount) {
        Map<String, OrderBook> fromOrderBookMap = exchangeManagerService.getOrderBookService(fromExchange).getOrderBookSnapshots();
        Map<String, OrderBook> toOrderBookMap = exchangeManagerService.getOrderBookService(toExchange).getOrderBookSnapshots();

        List<Conversion> possibleConversionList = new ArrayList<>();

        Set<String> availablePairs = fromOrderBookMap.keySet();
        for (String fromPair : availablePairs) {
            String fromQuote = CurrencyUtils.getQuote(fromPair);
            String fromBase = CurrencyUtils.getBase(fromPair);

            if (!fromQuote.equals(fromAsset)) {
                continue;
            }

            for (String toPair : availablePairs) {
                if (toPair.equals(fromPair)) {
                    continue;
                }

                String toQuote = CurrencyUtils.getQuote(toPair);
                String toBase = CurrencyUtils.getBase(toPair);

                if (!toBase.equals(fromBase)) {
                    continue;
                }
                if (!toQuote.equals(toAsset)) {
                    continue;
                }

                if (!toOrderBookMap.containsKey(toPair)) {
                    continue;
                }

                BigDecimal transferQty = OrderBookHelper.calculateBaseQuantity(fromOrderBookMap.get(fromPair), amount);
                BigDecimal finalQty = OrderBookHelper.calculateQuoteQuantity(toOrderBookMap.get(toPair), transferQty);

                Conversion conversion = new Conversion();
                conversion.setFromAmount(amount);
                conversion.setFromAsset(fromAsset);
                conversion.setToAsset(toAsset);
                conversion.setConversionAsset(fromBase);
                conversion.setFromExchange(fromExchange);
                conversion.setToExchange(toExchange);
                conversion.setToAmount(finalQty);

                possibleConversionList.add(conversion);
            }
        }

        possibleConversionList.sort(Comparator.comparing(Conversion::getToAmount).reversed());

        return possibleConversionList;
    }
}
