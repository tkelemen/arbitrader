package com.tkelemen.arbitrader.client.ws;

import org.java_websocket.handshake.Handshakedata;

@FunctionalInterface
public interface WsEventCallback {
    void onMessage(String message);

    default void onError(Throwable cause) {
    }

    default void onOpen(Handshakedata handshakedata) {
    }

    default void onClose(int code, String reason, boolean remote) {
    }
}
