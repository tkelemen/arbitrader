package com.tkelemen.arbitrader.client.cex.rest.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.tkelemen.arbitrader.client.cex.rest.message.GetOrderResponse;

import java.io.IOException;
import java.math.BigDecimal;

public class GetOrderDeserializer extends JsonDeserializer<GetOrderResponse> {
    @Override
    public GetOrderResponse deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        ObjectCodec oc = jsonParser.getCodec();

        JsonNode node = oc.readTree(jsonParser);
        GetOrderResponse response = new GetOrderResponse();

        if (node.has("error")) {
            response.setError(node.get("error").asText());
        } else {
            String symbol1 = node.get("symbol1").asText();
            response.setSymbol1(symbol1);
            String symbol2 = node.get("symbol2").asText();
            response.setSymbol2(symbol2);

            if (node.has("id")) {
                response.setId(node.get("id").asText());
            }
            if (node.has("orderId")) {
                response.setOrderId(node.get("orderId").asText());
            }
            if (node.has("type")) {
                response.setType(node.get("type").asText());
            }
            if (node.has("time")) {
                response.setTime(node.get("time").asText());
            }
            if (node.has("lastTx")) {
                response.setLastTx(node.get("lastTx").asText());
            }
            if (node.has("lastTxTime")) {
                response.setLastTxTime(node.get("lastTxTime").asText());
            }
            if (node.has("pos")) {
                response.setPos(node.get("pos").textValue());
            }
            if (node.has("user")) {
                response.setUser(node.get("user").asText());
            }
            if (node.has("status")) {
                response.setStatus(node.get("status").asText());
            }
            if (node.has("kind")) {
                response.setKind(node.get("kind").asText());
            }
            if (node.has("amount")) {
                response.setAmount(new BigDecimal(node.get("amount").asText()));
            }
            if (node.has("amount2")) {
                response.setAmount2(new BigDecimal(node.get("amount2").asText()));
            }
            if (node.has("remains")) {
                response.setRemains(new BigDecimal(node.get("remains").asText()));
            }
            if (node.has("price")) {
                response.setPrice(new BigDecimal(node.get("price").asText()));
            }
            if (node.has("tradingFeeUserVolumeAmount")) {
                response.setTradingFeeUserVolumeAmount(node.get("tradingFeeUserVolumeAmount").asText());
            }
            if (node.has("tradingFeeStrategy")) {
                response.setTradingFeeStrategy(node.get("tradingFeeStrategy").asText());
            }

            JsonNode tradingFeeTaker = node.get("tradingFeeTaker");
            if (tradingFeeTaker != null) {
                response.setTradingFeeTaker(new BigDecimal(tradingFeeTaker.asText()));
            }
            JsonNode tradingFeeMaker = node.get("tradingFeeMaker");
            if (tradingFeeMaker != null) {
                response.setTradingFeeMaker(new BigDecimal(tradingFeeMaker.asText()));
            }
            JsonNode takerFeeAmountSymbol1 = node.get("tfa:" + symbol1);
            if (takerFeeAmountSymbol1 != null) {
                response.setTakerFeeAmount(symbol1, new BigDecimal(takerFeeAmountSymbol1.asText()));
            }
            JsonNode totalFeeAmountSymbol2 = node.get("tfa:" + symbol2);
            if (totalFeeAmountSymbol2 != null) {
                response.setTakerFeeAmount(symbol2, new BigDecimal(totalFeeAmountSymbol2.asText()));
            }
            JsonNode totalTakerAmountSymbol1 = node.get("tta:" + symbol1);
            if (totalTakerAmountSymbol1 != null) {
                response.setTotalTakerAmount(symbol1, new BigDecimal(totalTakerAmountSymbol1.asText()));
            }
            JsonNode totalTakerAmountSymbol2 = node.get("tta:" + symbol2);
            if (totalTakerAmountSymbol2 != null) {
                response.setTotalTakerAmount(symbol2, new BigDecimal(totalTakerAmountSymbol2.asText()));
            }
            JsonNode creditDebitSaldoAmountSymbol1 = node.get("a:" + symbol1 + ":cds");
            if (creditDebitSaldoAmountSymbol1 != null) {
                response.setCreditDebitSaldoAmount(symbol1, new BigDecimal(creditDebitSaldoAmountSymbol1.asText()));
            }
            JsonNode creditDebitSaldoAmountSymbol2 = node.get("a:" + symbol2 + ":cds");
            if (creditDebitSaldoAmountSymbol2 != null) {
                response.setCreditDebitSaldoAmount(symbol2, new BigDecimal(creditDebitSaldoAmountSymbol2.asText()));
            }
        }

        return response;
    }
}