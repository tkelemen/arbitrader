package com.tkelemen.arbitrader.client.cex.rest;

import com.tkelemen.arbitrader.client.cex.rest.message.PlaceMarketOrderRequest;
import com.tkelemen.arbitrader.client.cex.rest.message.PlaceMarketOrderResponse;
import com.tkelemen.arbitrader.client.cex.rest.message.PlaceOrderRequest;
import com.tkelemen.arbitrader.client.cex.rest.message.PlaceOrderResponse;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class PlaceOrderClient extends PrivateCexRestQuery {
    private static final Logger LOGGER = LoggerFactory.getLogger(PlaceOrderClient.class);

    private static final String TYPE_BUY = "buy";
    private static final String TYPE_SELL = "sell";
    private static final String ORDER_TYPE_MARKET = "market";
    private static final String ORDER_TYPE_LIMIT = "limit";

    @Autowired
    public PlaceOrderClient(ApplicationConfig config) {
        super(config);
    }

    private static final String BASE_PATH = "place_order/";

    public PlaceMarketOrderResponse marketBuy(String pair, BigDecimal amount) {
        return placeMarket(pair, TYPE_BUY, ORDER_TYPE_MARKET, amount);
    }

    public PlaceMarketOrderResponse marketSell(String pair, BigDecimal amount) {
        return placeMarket(pair, TYPE_SELL, ORDER_TYPE_MARKET, amount);
    }

    public PlaceOrderResponse limitBuy(String pair, BigDecimal amount, BigDecimal price) {
        return placeLimit(pair, TYPE_BUY, ORDER_TYPE_LIMIT, amount, price);
    }

    public PlaceOrderResponse limitSell(String pair, BigDecimal amount, BigDecimal price) {
        return placeLimit(pair, TYPE_SELL, ORDER_TYPE_LIMIT, amount, price);
    }

    private PlaceMarketOrderResponse placeMarket(String pair, String type, String orderType, BigDecimal amount) {
        String path = BASE_PATH + pair;

        PlaceMarketOrderRequest request = new PlaceMarketOrderRequest();
        request.setAmount(amount);
        request.setType(type);
        request.setOrder_Type(orderType);

        LOGGER.debug("Placing CEX market order: " + request);

        return privateQuery(path, PlaceMarketOrderResponse.class, request);
    }

    private PlaceOrderResponse placeLimit(String pair, String type, String orderType, BigDecimal amount, BigDecimal price) {
        String path = BASE_PATH + pair;

        PlaceOrderRequest request = new PlaceOrderRequest();
        request.setAmount(amount);
        request.setType(type);
        request.setOrder_Type(orderType);
        request.setPrice(price);

        LOGGER.debug("Placing CEX limit order: " + request);

        return privateQuery(path, PlaceOrderResponse.class, request);
    }
}
