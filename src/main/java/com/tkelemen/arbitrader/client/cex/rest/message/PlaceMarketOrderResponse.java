package com.tkelemen.arbitrader.client.cex.rest.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PlaceMarketOrderResponse {
    private BigDecimal symbol2Amount;
    private BigDecimal symbol1Amount;
    private long time;
    private String message;
    private String type;
    private String id;
    private String error;
    private String safe;

    public BigDecimal getSymbol2Amount() {
        return symbol2Amount;
    }

    public void setSymbol2Amount(BigDecimal symbol2Amount) {
        this.symbol2Amount = symbol2Amount;
    }

    public BigDecimal getSymbol1Amount() {
        return symbol1Amount;
    }

    public void setSymbol1Amount(BigDecimal symbol1Amount) {
        this.symbol1Amount = symbol1Amount;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getSafe() {
        return safe;
    }

    public void setSafe(String safe) {
        this.safe = safe;
    }

    @Override
    public String toString() {
        return "PlaceMarketOrderResponse{" +
                "symbol2Amount=" + symbol2Amount +
                ", symbol1Amount=" + symbol1Amount +
                ", time=" + time +
                ", message='" + message + '\'' +
                ", type='" + type + '\'' +
                ", id='" + id + '\'' +
                ", error='" + error + '\'' +
                ", safe='" + safe + '\'' +
                "} " + super.toString();
    }

}
