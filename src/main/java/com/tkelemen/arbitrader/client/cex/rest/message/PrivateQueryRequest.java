package com.tkelemen.arbitrader.client.cex.rest.message;

public class PrivateQueryRequest {
    private String key;
    private String signature;
    private long nonce;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public long getNonce() {
        return nonce;
    }

    public void setNonce(long nonce) {
        this.nonce = nonce;
    }

    @Override
    public String toString() {
        return "PrivateQueryRequest{" +
                "key='" + key + '\'' +
                ", signature='" + signature + '\'' +
                ", nonce=" + nonce +
                '}';
    }
}
