package com.tkelemen.arbitrader.client.cex.ws.message;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize
public class PingEvent extends CexEvent {
    private long time;

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "PingEvent{" +
                "time=" + time +
                '}';
    }
}
