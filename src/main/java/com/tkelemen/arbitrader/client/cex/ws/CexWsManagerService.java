package com.tkelemen.arbitrader.client.cex.ws;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.tkelemen.arbitrader.client.cex.ws.message.*;
import com.tkelemen.arbitrader.client.ws.WsEventCallback;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.exception.WebsocketClientException;
import org.java_websocket.handshake.Handshakedata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

@Service
public class CexWsManagerService {
    /*
    private static final Logger LOGGER = LoggerFactory.getLogger(CexWsManagerService.class);

    private CexWsClient client;

    private final ObjectWriter objectWriter = (new ObjectMapper()).writer();
    private final ObjectReader objectReader = (new ObjectMapper()).readerFor(CexEvent.class);
    private final ApplicationConfig config;
    private long lastConnectTime;
    private Set<String> desiredSubscriptions = new HashSet<>();

    @Autowired
    public CexWsManagerService(ApplicationConfig config) {
        this.config = config;
    }

    public void connect() {
        try {
            client = new CexWsClient(config);
            lastConnectTime = System.currentTimeMillis();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        client.connect(new EventProcessorCallback());
    }

    public void addOrderBookSubscription(String symbol) {
        this.desiredSubscriptions.add(symbol);
    }

    public void setOrderBookSubscriptions(Set<String> symbols) {
        this.desiredSubscriptions = symbols;
    }

    class EventProcessorCallback implements WsEventCallback {
        private static final int MAX_AUTH_TRIES = 3;

        private int authTries = 0;

        EventProcessorCallback() {
        }

        @Override
        public void onOpen(Handshakedata handshakedata) {
            client.sendAuthenticationRequest();
        }

        @Override
        public void onClose(int code, String reason, boolean remote) {
            if (remote) {
                LOGGER.info("Disconnected, reconnecting, resubscribing");

                subscribeForOrderBookUpdates(symbols);
            }
        }

        @Override
        public void onMessage(String message) {
            try {
                CexEvent event = objectReader.readValue(message);

                if (event instanceof PingEvent) {
                    client.send(objectWriter.writeValueAsString(new PongResponse()));
                } else if (event instanceof AuthEvent) {
                    LOGGER.info("AuthEvent received: " + event);
                    if (AuthEvent.OK.equals(((AuthEvent) event).getOk())) {
                        for (String symbol : desiredSubscriptions) {
                            subscribe(symbol);
                        }
                    } else {
                        LOGGER.error("Not logged in: " + event);
                        if(authTries++ < MAX_AUTH_TRIES) {
                            Timer timer = new Timer();
                            timer.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    LOGGER.info("Retrying login... ");
                                    client.sendAuthenticationRequest();
                                }
                            }, 1000);
                        } else {
                            cleanup();
                        }
                    }
                } else if (event instanceof OrderBookUpdateEvent) {
                    final OrderBookData data = ((OrderBookUpdateEvent) event).getData();
                    if (data.getError() != null) {
                        LOGGER.error("Order book subscription error: " + data.getError());
                    } else {
                        String pair = transformFromExchangePair(toGeneralPair(data.getPair()));

                        if (event instanceof OrderBookSubscribeEvent) {
                            subscriptionTimes.put(pair, System.currentTimeMillis());
                            activeSubscriptions.add(pair);

                            if (orderBookMap.containsKey(pair)) {
                                orderBookMap.get(pair).reset();
                            }
                        }

                        Long lastUpdateId = lastUpdateIds.get(pair);

                        if (lastUpdateId != null && data.getId() != lastUpdateId && data.getId() != (lastUpdateId + 1)) {
                            LOGGER.error("Received update for " + pair + " is out of sync (" + (data.getId() + " <-> " + (lastUpdateId + 1)) + "), resubscribing");
                            LOGGER.error("Last update: " + lastUpdates.get(pair));
                            LOGGER.error("Current update: " + data);

                            resubscribe(pair);
                        } else {
                            updateOrderBook(pair, data);
                            lastUpdateIds.put(pair, data.getId());
                            lastUpdates.put(pair, data);
                        }
                    }
                } else if (event instanceof TransactionEvent) {
                    LOGGER.info("Transaction info: " + ((TransactionEvent) event).getData());
                } else {
                    LOGGER.error("Unknown message: " + message);
                }
            } catch (JsonProcessingException | WebsocketClientException e) {
                LOGGER.error(e.toString());
            }
        }
    }
*/
}
