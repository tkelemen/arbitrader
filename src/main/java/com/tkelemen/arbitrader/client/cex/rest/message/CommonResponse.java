package com.tkelemen.arbitrader.client.cex.rest.message;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize()
public class CommonResponse {
    private String error;

    public CommonResponse() {
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "CommonResponse{" +
                "error='" + error + '\'' +
                '}';
    }
}
