package com.tkelemen.arbitrader.client.cex.rest.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.tkelemen.arbitrader.client.cex.rest.deserializer.GetBalanceDeserializer;

import java.util.HashMap;
import java.util.Map;

@JsonDeserialize(using = GetBalanceDeserializer.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetBalanceResponse extends CommonResponse {
    public String timestamp;
    public String username;

    public Map<String, Balance> balances = new HashMap<>();

    public GetBalanceResponse() {
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Map<String, Balance> getBalances() {
        return balances;
    }

    public void setBalances(Map<String, Balance> balances) {
        this.balances = balances;
    }

    public void setBalance(String ticker, Balance balance) {
        this.balances.put(ticker, balance);
    }

    public Balance getBalance(String ticker) {
        return balances.get(ticker);
    }

    @Override
    public String toString() {
        return "GetBalanceResponse{" +
                "timestamp='" + timestamp + '\'' +
                ", username='" + username + '\'' +
                ", balances=" + balances +
                '}';
    }
}
