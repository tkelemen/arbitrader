package com.tkelemen.arbitrader.client.cex.rest.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MassCancelPlaceOrdersResponse extends CommonResponse {
    private String e;
    private String ok;
    private MassCancelPlaceOrdersResponseData data;

    public String getE() {
        return e;
    }

    public void setE(String e) {
        this.e = e;
    }

    public String getOk() {
        return ok;
    }

    public void setOk(String ok) {
        this.ok = ok;
    }

    public MassCancelPlaceOrdersResponseData getData() {
        return data;
    }

    public void setData(MassCancelPlaceOrdersResponseData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "MassCancelPlaceOrdersResponse{" +
                "e='" + e + '\'' +
                ", ok='" + ok + '\'' +
                ", data=" + data +
                "} " + super.toString();
    }

    public static class MassCancelPlaceOrdersResponseData {
        @JsonProperty("cancel-orders")
        private List<CancelledOrder> cancelOrders;

        @JsonProperty("place-orders")
        private List<PlacedOrder> placeOrders;

        @JsonProperty("placed-cancelled")
        private List<String> placedCancelled;

        public List<CancelledOrder> getCancelOrders() {
            return cancelOrders;
        }

        public void setCancelOrders(List<CancelledOrder> cancelOrders) {
            this.cancelOrders = cancelOrders;
        }

        public List<PlacedOrder> getPlaceOrders() {
            return placeOrders;
        }

        public void setPlaceOrders(List<PlacedOrder> placeOrders) {
            this.placeOrders = placeOrders;
        }

        public List<String> getPlacedCancelled() {
            return placedCancelled;
        }

        public void setPlacedCancelled(List<String> placedCancelled) {
            this.placedCancelled = placedCancelled;
        }

        @Override
        public String toString() {
            return "MassCancelPlaceOrdersResponseData{" +
                    ", cancelOrders=" + cancelOrders +
                    ", placeOrders=" + placeOrders +
                    ", placedCancelled=" + placedCancelled +
                    "} " + super.toString();
        }
    }

    public static class CancelledOrder {
        @JsonProperty("order_id")
        private String orderId;
        @JsonProperty("fremains")
        private BigDecimal remaining;
        private String error;

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public BigDecimal getRemaining() {
            return remaining;
        }

        public void setRemaining(BigDecimal remaining) {
            this.remaining = remaining;
        }

        public String getError() {
            return error;
        }

        public void setError(String error) {
            this.error = error;
        }

        @Override
        public String toString() {
            return "CancelledOrder{" +
                    "orderId=" + orderId +
                    ", remaining=" + remaining +
                    ", error='" + error + '\'' +
                    '}';
        }
    }

    public static class PlacedOrder {
        private String id;
        private boolean complete;
        private String type;
        private BigDecimal amount;
        private BigDecimal pending;
        private long time;
        private BigDecimal price;
        private String error;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public void setAmount(BigDecimal amount) {
            this.amount = amount;
        }

        public BigDecimal getPrice() {
            return price;
        }

        public void setPrice(BigDecimal price) {
            this.price = price;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public boolean isComplete() {
            return complete;
        }

        public void setComplete(boolean complete) {
            this.complete = complete;
        }

        public BigDecimal getPending() {
            return pending;
        }

        public void setPending(BigDecimal pending) {
            this.pending = pending;
        }

        public long getTime() {
            return time;
        }

        public void setTime(long time) {
            this.time = time;
        }

        public String getError() {
            return error;
        }

        public void setError(String error) {
            this.error = error;
        }
        @Override
        public String toString() {
            return "PlacedOrder{" +
                    "id='" + id + '\'' +
                    ", complete=" + complete +
                    ", type='" + type + '\'' +
                    ", amount=" + amount +
                    ", pending=" + pending +
                    ", time=" + time +
                    ", price=" + price +
                    ", error='" + error + '\'' +
                    '}';
        }
    }


}
