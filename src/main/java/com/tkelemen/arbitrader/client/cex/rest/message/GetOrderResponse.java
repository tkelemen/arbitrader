package com.tkelemen.arbitrader.client.cex.rest.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.tkelemen.arbitrader.client.cex.rest.deserializer.GetOrderDeserializer;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@JsonDeserialize(using = GetOrderDeserializer.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetOrderResponse extends CommonResponse {
    private String id;
    private String time;
    private String type;
    private BigDecimal price;
    private String user;
    private BigDecimal amount;
    private BigDecimal amount2;
    private String pending;
    private String symbol1;
    private String symbol2;
    private BigDecimal symbol1Amount;
    private BigDecimal symbol2Amount;
    private String lastTx;
    private String lastTxTime;
    private String kind;
    private String tradingFeeUserVolumeAmount;
    private Map<String, BigDecimal> totalAmount = new HashMap<>();//a:{symbol2};
    private Map<String, BigDecimal> totalTakerAmount = new HashMap<>(); //ta:{symbol2};
    private Map<String, BigDecimal> feeAmount = new HashMap<>(); //fa:{symbol2};
    private Map<String, BigDecimal> takerFeeAmount = new HashMap<>(); //tfa:{symbol2};
    private Map<String, BigDecimal> creditDebitSaldoAmount = new HashMap<>();//a:{symbol1}:cds;
    private BigDecimal tradingFeeMaker;
    private BigDecimal tradingFeeTaker;
    private String tradingFeeStrategy;
    private BigDecimal remains;
    private String orderId;
    private String pos;
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmount2() {
        return amount2;
    }

    public void setAmount2(BigDecimal amount2) {
        this.amount2 = amount2;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    public String getSymbol1() {
        return symbol1;
    }

    public void setSymbol1(String symbol1) {
        this.symbol1 = symbol1;
    }

    public String getSymbol2() {
        return symbol2;
    }

    public void setSymbol2(String symbol2) {
        this.symbol2 = symbol2;
    }

    public BigDecimal getSymbol1Amount() {
        return symbol1Amount;
    }

    public void setSymbol1Amount(BigDecimal symbol1Amount) {
        this.symbol1Amount = symbol1Amount;
    }

    public BigDecimal getSymbol2Amount() {
        return symbol2Amount;
    }

    public void setSymbol2Amount(BigDecimal symbol2Amount) {
        this.symbol2Amount = symbol2Amount;
    }

    public String getLastTx() {
        return lastTx;
    }

    public void setLastTx(String lastTx) {
        this.lastTx = lastTx;
    }

    public String getLastTxTime() {
        return lastTxTime;
    }

    public void setLastTxTime(String lastTxTime) {
        this.lastTxTime = lastTxTime;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getTradingFeeUserVolumeAmount() {
        return tradingFeeUserVolumeAmount;
    }

    public void setTradingFeeUserVolumeAmount(String tradingFeeUserVolumeAmount) {
        this.tradingFeeUserVolumeAmount = tradingFeeUserVolumeAmount;
    }

    public Map<String, BigDecimal> getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Map<String, BigDecimal> totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Map<String, BigDecimal> getTotalTakerAmount() {
        return totalTakerAmount;
    }

    public void setTotalTakerAmount(Map<String, BigDecimal> totalTakerAmount) {
        this.totalTakerAmount = totalTakerAmount;
    }

    public Map<String, BigDecimal> getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(Map<String, BigDecimal> feeAmount) {
        this.feeAmount = feeAmount;
    }

    public Map<String, BigDecimal> getTakerFeeAmount() {
        return takerFeeAmount;
    }

    public void setTakerFeeAmount(Map<String, BigDecimal> takerFeeAmount) {
        this.takerFeeAmount = takerFeeAmount;
    }

    public Map<String, BigDecimal> getCreditDebitSaldoAmount() {
        return creditDebitSaldoAmount;
    }

    public void setCreditDebitSaldoAmount(Map<String, BigDecimal> creditDebitSaldoAmount) {
        this.creditDebitSaldoAmount = creditDebitSaldoAmount;
    }

    public BigDecimal getTradingFeeMaker() {
        return tradingFeeMaker;
    }

    public void setTradingFeeMaker(BigDecimal tradingFeeMaker) {
        this.tradingFeeMaker = tradingFeeMaker;
    }

    public BigDecimal getTradingFeeTaker() {
        return tradingFeeTaker;
    }

    public void setTradingFeeTaker(BigDecimal tradingFeeTaker) {
        this.tradingFeeTaker = tradingFeeTaker;
    }

    public String getTradingFeeStrategy() {
        return tradingFeeStrategy;
    }

    public void setTradingFeeStrategy(String tradingFeeStrategy) {
        this.tradingFeeStrategy = tradingFeeStrategy;
    }

    public BigDecimal getRemains() {
        return remains;
    }

    public void setRemains(BigDecimal remains) {
        this.remains = remains;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTotalAmount(String symbol, BigDecimal amount) {
        this.totalAmount.put(symbol, amount);
    }

    public void setTotalTakerAmount(String symbol, BigDecimal amount) {
        this.totalTakerAmount.put(symbol, amount);
    }

    public void setTakerFeeAmount(String symbol, BigDecimal amount) {
        this.takerFeeAmount.put(symbol, amount);
    }

    public void setFeeAmount(String symbol, BigDecimal amount) {
        this.feeAmount.put(symbol, amount);
    }

    public void setCreditDebitSaldoAmount(String symbol, BigDecimal amount) {
        this.creditDebitSaldoAmount.put(symbol, amount);
    }

    public BigDecimal getTotalAmount(String symbol) {
        return this.totalAmount.get(symbol);
    }

    public BigDecimal getTotalTakerAmount(String symbol) {
        return this.totalTakerAmount.get(symbol);
    }

    public BigDecimal getTakerFeeAmount(String symbol) {
        return this.takerFeeAmount.get(symbol);
    }

    public BigDecimal getFeeAmount(String symbol) {
        return this.feeAmount.get(symbol);
    }

    public BigDecimal getCreditDebitSaldoAmount(String symbol) {
        return this.creditDebitSaldoAmount.get(symbol);
    }

    @Override
    public String toString() {
        return "GetOrderResponse{" +
                "id='" + id + '\'' +
                ", time='" + time + '\'' +
                ", type='" + type + '\'' +
                ", price=" + price +
                ", user='" + user + '\'' +
                ", amount=" + amount +
                ", amount2=" + amount2 +
                ", pending='" + pending + '\'' +
                ", symbol1='" + symbol1 + '\'' +
                ", symbol2='" + symbol2 + '\'' +
                ", symbol1Amount=" + symbol1Amount +
                ", symbol2Amount=" + symbol2Amount +
                ", lastTx='" + lastTx + '\'' +
                ", lastTxTime='" + lastTxTime + '\'' +
                ", kind='" + kind + '\'' +
                ", tradingFeeUserVolumeAmount='" + tradingFeeUserVolumeAmount + '\'' +
                ", totalAmount=" + totalAmount +
                ", totalTakerAmount=" + totalTakerAmount +
                ", feeAmount=" + feeAmount +
                ", takerFeeAmount=" + takerFeeAmount +
                ", creditDebitSaldoAmount=" + creditDebitSaldoAmount +
                ", tradingFeeMaker=" + tradingFeeMaker +
                ", tradingFeeTaker=" + tradingFeeTaker +
                ", tradingFeeStrategy='" + tradingFeeStrategy + '\'' +
                ", remains=" + remains +
                ", orderId='" + orderId + '\'' +
                ", pos='" + pos + '\'' +
                ", status='" + status + '\'' +
                "} " + super.toString();
    }
}
