package com.tkelemen.arbitrader.client.cex.rest.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TickerDetail {
    private long timestamp;
    private String pair;
    private BigDecimal low;
    private BigDecimal high;
    private BigDecimal last;
    private BigDecimal volume;
    private BigDecimal volume30d;
    private BigDecimal priceChange;

    // TODO: CEX puts a "-" sign into this field for new coins, and this causes InvalidFormatException while parsing BigDecimal
    // TODO: it should be handled via custom deserializer, we are ignoring the field for now
    //private BigDecimal priceChangePercentage;
    private BigDecimal bid;
    private BigDecimal ask;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public BigDecimal getLow() {
        return low;
    }

    public void setLow(BigDecimal low) {
        this.low = low;
    }

    public BigDecimal getHigh() {
        return high;
    }

    public void setHigh(BigDecimal high) {
        this.high = high;
    }

    public BigDecimal getLast() {
        return last;
    }

    public void setLast(BigDecimal last) {
        this.last = last;
    }

    public BigDecimal getVolume() {
        return volume;
    }

    public void setVolume(BigDecimal volume) {
        this.volume = volume;
    }

    public BigDecimal getVolume30d() {
        return volume30d;
    }

    public void setVolume30d(BigDecimal volume30d) {
        this.volume30d = volume30d;
    }

    public BigDecimal getPriceChange() {
        return priceChange;
    }

    public void setPriceChange(BigDecimal priceChange) {
        this.priceChange = priceChange;
    }
/*
    public BigDecimal getPriceChangePercentage() {
        return priceChangePercentage;
    }
    public void setPriceChangePercentage(BigDecimal priceChangePercentage) {
        this.priceChangePercentage = priceChangePercentage;
    }

*/
    public BigDecimal getBid() {
        return bid;
    }

    public void setBid(BigDecimal bid) {
        this.bid = bid;
    }

    public BigDecimal getAsk() {
        return ask;
    }

    public void setAsk(BigDecimal ask) {
        this.ask = ask;
    }

    @Override
    public String toString() {
        return "TickerDetail{" +
                "timestamp=" + timestamp +
                ", pair='" + pair + '\'' +
                ", low=" + low +
                ", high=" + high +
                ", last=" + last +
                ", volume=" + volume +
                ", volume30d=" + volume30d +
                ", priceChange=" + priceChange +
                //", priceChangePercentage=" + priceChangePercentage +
                ", bid=" + bid +
                ", ask=" + ask +
                '}';
    }
}
