package com.tkelemen.arbitrader.client.cex.ws.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tkelemen.arbitrader.client.cex.ws.message.*;

import java.io.IOException;

public class CexEventDeserializer extends JsonDeserializer<CexEvent> {

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public CexEvent deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        final ObjectCodec oc = jsonParser.getCodec();
        final JsonNode node = oc.readTree(jsonParser);

        CexEvent result = new CexEvent();

        if (node.has("e")) {
            final String eventType = node.get("e").asText();

            switch (eventType) {
                case CexEvent.EVENT_PING:
                    result = mapper.readerFor(PingEvent.class).readValue(node);
                    break;
                case CexEvent.EVENT_CONNECTED:
                    result.setEventType(CexEvent.EVENT_CONNECTED);
                    break;
                case CexEvent.EVENT_AUTH:
                    result = mapper.readerFor(AuthEvent.class).readValue(node);
                    break;
                case CexEvent.EVENT_ORDER_BOOK_SUBSCRIBE:
                    result = mapper.readerFor(OrderBookSubscribeEvent.class).readValue(node);
                    break;
                case CexEvent.EVENT_ORDER_BOOK_UPDATE:
                    result = mapper.readerFor(OrderBookUpdateEvent.class).readValue(node);
                    break;
                case CexEvent.EVENT_TRANSACTION:
                    result = mapper.readerFor(TransactionEvent.class).readValue(node);
                    break;
            }
        }

        return result;
    }
}
