package com.tkelemen.arbitrader.client.cex.rest.message;

import javax.xml.bind.annotation.XmlElement;
import java.math.BigDecimal;

public class PlaceOrderRequest extends PrivateQueryRequest {
    private String type;
    private BigDecimal amount;
    private BigDecimal price;

    @XmlElement(name = "order_type")
    private String order_type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getOrder_Type() {
        return order_type;
    }

    public void setOrder_Type(String orderType) {
        this.order_type = orderType;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "PlaceOrderRequest{" +
                "type='" + type + '\'' +
                ", amount=" + amount +
                ", price=" + price +
                ", order_type='" + order_type + '\'' +
                "} " + super.toString();
    }

}
