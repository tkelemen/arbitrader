package com.tkelemen.arbitrader.client.cex.rest;

import com.tkelemen.arbitrader.client.cex.rest.message.CancelOrderRequest;
import com.tkelemen.arbitrader.client.cex.rest.message.CancelOrderResponse;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CancelOrderClient extends PrivateCexRestQuery {
    private static final String BASE_PATH = "cancel_order/";

    @Autowired
    public CancelOrderClient(ApplicationConfig config) {
        super(config);
    }

    public CancelOrderResponse cancel(String orderId) {
        final CancelOrderRequest request = new CancelOrderRequest();
        request.setId(orderId);

        return privateQuery(BASE_PATH, CancelOrderResponse.class, request);
    }
}
