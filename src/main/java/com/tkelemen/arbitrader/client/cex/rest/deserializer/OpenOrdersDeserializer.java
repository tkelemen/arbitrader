package com.tkelemen.arbitrader.client.cex.rest.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.*;
import com.tkelemen.arbitrader.client.cex.rest.message.CancelOrderResponse;
import com.tkelemen.arbitrader.client.cex.rest.message.OpenOrdersResponse;
import com.tkelemen.arbitrader.client.cex.rest.message.Order;
import com.tkelemen.arbitrader.client.kraken.ws.message.OrderBookData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class OpenOrdersDeserializer extends JsonDeserializer<OpenOrdersResponse> {
    @Override
    public OpenOrdersResponse deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        final JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        final OpenOrdersResponse response = new OpenOrdersResponse();

        if (node.has("error")) {
            response.setError(node.get("error").asText());
        } else if(node.isArray()) {
            final List<Order> orderList = new ArrayList<>();
            final ObjectReader orderReader = (new ObjectMapper()).readerFor(Order.class);
            for (JsonNode orderNode : node) {
                orderList.add(orderReader.readValue(orderNode));
            }
            response.setOpenOrders(orderList);
        }

        return response;
    }
}