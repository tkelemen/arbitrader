package com.tkelemen.arbitrader.client.cex.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;

public abstract class CexRestQuery {
    protected ApplicationConfig config;

    protected WebClient client;

    CexRestQuery(ApplicationConfig config) {
        this.config = config;

        /*
            uncomment for debugging - logging of the request body

            HttpClient httpClient = HttpClient
                .create()
                .wiretap(true);
                */

        ExchangeStrategies strategies = ExchangeStrategies
                .builder()
                .codecs(clientDefaultCodecsConfigurer -> {
                    clientDefaultCodecsConfigurer.defaultCodecs().jackson2JsonEncoder(new Jackson2JsonEncoder(new ObjectMapper(), MediaType.APPLICATION_JSON));
                    clientDefaultCodecsConfigurer.defaultCodecs().jackson2JsonDecoder(new Jackson2JsonDecoder(new ObjectMapper(), MediaType.APPLICATION_JSON, new MediaType("text", "json")));
                }).build();
        client = WebClient.builder()
                .baseUrl(config.getCexApiRestBaseUrl())
                //.clientConnector(new ReactorClientHttpConnector(httpClient))
                .exchangeStrategies(strategies)
                .build();
    }


    <T> T get(String path, Class<T> responseType) {
        return client.get().uri(path)
                .retrieve()
                .bodyToMono(responseType).block();
    }
}
