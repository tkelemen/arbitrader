package com.tkelemen.arbitrader.client.cex.ws;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.tkelemen.arbitrader.client.cex.ws.message.AuthData;
import com.tkelemen.arbitrader.client.cex.ws.message.AuthRequest;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import org.springframework.messaging.simp.stomp.*;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class CexStompSessionHandler extends StompSessionHandlerAdapter {
    private final ObjectWriter objectWriter = new ObjectMapper().writer();

    private ApplicationConfig config;

    public CexStompSessionHandler(ApplicationConfig config) {
        this.config = config;
    }


    @Override
    public void afterConnected(StompSession stompSession, StompHeaders stompHeaders) {
        System.out.println("CexStompSessionHandler.afterConnected");
        printSession(stompSession);
        printHeaders(stompHeaders);
        sendAuthRequest(stompSession, config.getCexApiKey(), config.getCexApiSecret());
    }

    @Override
    public void handleException(StompSession stompSession, StompCommand stompCommand, StompHeaders stompHeaders, byte[] bytes, Throwable throwable) {
        System.out.println("CexStompSessionHandler.handleException");
        System.err.println("Exception: " + throwable);
        printSession(stompSession);
        printHeaders(stompHeaders);
    }

    @Override
    public void handleTransportError(StompSession stompSession, Throwable throwable) {
        System.out.println("CexStompSessionHandler.handleTransportError");
        System.err.println("Exception: " + throwable);
        throwable.printStackTrace();
        printSession(stompSession);
    }

    @Override
    public Type getPayloadType(StompHeaders stompHeaders) {
        return String.class;
    }

    @Override
    public void handleFrame(StompHeaders stompHeaders, Object o) {
        System.out.println("CexStompSessionHandler.handleFrame");
        System.err.println("Object: " + o);
        printHeaders(stompHeaders);
    }

    public void printSession(StompSession stompSession) {
        System.out.println("sessionId: " + stompSession.getSessionId());
        System.out.println("isConnected: " + stompSession.isConnected());
    }

    public void printHeaders(StompHeaders stompHeaders) {
        System.out.println("headers: " + stompHeaders.entrySet());
    }

    public void sendAuthRequest(StompSession stompSession, String apiKey, String secretKey) {
        long timestamp = System.currentTimeMillis() / 1000;

        String sign = calculateSignature(timestamp + apiKey, secretKey);

        AuthRequest request = new AuthRequest();
        request.setAuth(new AuthData(apiKey, timestamp, sign));

        try {
            String message = objectWriter.writeValueAsString(request);

            System.out.println("Sending authentication request [ timestamp = " + timestamp + " ]");
            System.out.println("Sending WS message: " + message);

            stompSession.send("", message);
        } catch (JsonProcessingException e) {
            System.err.println("CommonResponse while processing 'auth' request: " + e.toString());
        }
    }

    private String calculateSignature(String data, String apiSecret) {
        Mac hmac = null;

        try {
            hmac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiSecret.getBytes(StandardCharsets.UTF_8), "HmacSHA256");
            hmac.init(secret_key);
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
        }

        return String.format("%X", new BigInteger(1, hmac.doFinal(data.getBytes())));
    }

}
