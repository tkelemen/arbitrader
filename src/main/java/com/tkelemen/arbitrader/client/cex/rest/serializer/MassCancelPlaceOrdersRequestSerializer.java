package com.tkelemen.arbitrader.client.cex.rest.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.tkelemen.arbitrader.client.cex.rest.message.MassCancelPlaceOrdersRequest;

import java.io.IOException;

public class MassCancelPlaceOrdersRequestSerializer extends JsonSerializer<MassCancelPlaceOrdersRequest> {
    @Override
    public void serialize(MassCancelPlaceOrdersRequest massCancelPlaceOrdersRequest, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("key", massCancelPlaceOrdersRequest.getKey());
        jsonGenerator.writeStringField("signature", massCancelPlaceOrdersRequest.getSignature());
        jsonGenerator.writeNumberField("nonce", massCancelPlaceOrdersRequest.getNonce());
        jsonGenerator.writeArrayFieldStart("cancel-orders");
        for(String orderId : massCancelPlaceOrdersRequest.getCancelOrders()) {
            jsonGenerator.writeString(orderId);
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeArrayFieldStart("place-orders");
        for(MassCancelPlaceOrdersRequest.PlaceOrder placeOrder : massCancelPlaceOrdersRequest.getPlaceOrders()) {
            jsonGenerator.writeObject(placeOrder);
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeBooleanField("cancelPlacedOrdersIfPlaceFailed", massCancelPlaceOrdersRequest.isCancelPlacedOrdersIfPlaceFailed());
        jsonGenerator.close();
    }
}
