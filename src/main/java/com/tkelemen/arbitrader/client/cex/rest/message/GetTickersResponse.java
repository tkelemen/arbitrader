package com.tkelemen.arbitrader.client.cex.rest.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetTickersResponse {
    private String e;
    private String ok;
    private List<TickerDetail> data;

    public String getE() {
        return e;
    }

    public void setE(String e) {
        this.e = e;
    }

    public String getOk() {
        return ok;
    }

    public void setOk(String ok) {
        this.ok = ok;
    }

    public List<TickerDetail> getData() {
        return data;
    }

    public void setData(List<TickerDetail> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "GetTickersResponse{" +
                "e='" + e + '\'' +
                ", ok='" + ok + '\'' +
                ", data=" + data +
                '}';
    }
}
