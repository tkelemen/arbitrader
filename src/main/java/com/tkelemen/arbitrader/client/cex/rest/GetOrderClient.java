package com.tkelemen.arbitrader.client.cex.rest;

import com.tkelemen.arbitrader.client.cex.rest.message.GetOrderRequest;
import com.tkelemen.arbitrader.client.cex.rest.message.GetOrderResponse;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GetOrderClient extends PrivateCexRestQuery {
    @Autowired
    public GetOrderClient(ApplicationConfig config) {
        super(config);
    }

    public GetOrderResponse get(String id) {
        return privateQuery("get_order/", GetOrderResponse.class, new GetOrderRequest(id));
    }
}