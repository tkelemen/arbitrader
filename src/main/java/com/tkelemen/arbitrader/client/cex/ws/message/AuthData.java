package com.tkelemen.arbitrader.client.cex.ws.message;

public class AuthData {
    private String key;

    private long timestamp;

    private String signature;

    public AuthData(String key, long timestamp, String signature) {
        this.key = key;
        this.timestamp = timestamp;
        this.signature = signature;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String apiKey) {
        this.key = apiKey;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    @Override
    public String toString() {
        return "AuthData{" +
                "key='" + key + '\'' +
                ", timestamp=" + timestamp +
                ", signature='" + signature + '\'' +
                '}';
    }
}
