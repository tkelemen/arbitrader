package com.tkelemen.arbitrader.client.cex.ws;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.tkelemen.arbitrader.client.cex.ws.message.AuthData;
import com.tkelemen.arbitrader.client.cex.ws.message.AuthRequest;
import com.tkelemen.arbitrader.client.cex.ws.message.OrderBookSubscriptionRequest;
import com.tkelemen.arbitrader.client.cex.ws.message.SubscriptionData;
import com.tkelemen.arbitrader.client.kraken.ws.message.SubscribeRequest;
import com.tkelemen.arbitrader.client.ws.WsEventCallback;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.exception.WebsocketClientException;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Service
public class CexWsClient extends WebSocketClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(CexWsClient.class);

    private WsEventCallback eventCallback;

    private boolean connected = false;

    private final ObjectWriter objectWriter = new ObjectMapper().writer();

    private long timestampCorrection = 0;

    private final ApplicationConfig config;

    @Autowired
    public CexWsClient(ApplicationConfig config) throws URISyntaxException {
        super(new URI(config.getCexWebSocketUrl()));

        this.config = config;

        timestampCorrection = config.getCexWebSocketAuthTimestampCorrection();
    }

    public void connect(WsEventCallback callback) {
        this.eventCallback = callback;

        if(this.connected) {
            this.close();
        }

        connect();
    }

    public void sendAuthenticationRequest() {
        long timestamp = System.currentTimeMillis() / 1000;

        // TODO: timestamp correction, for some reason CEX returns error without this
        timestamp += timestampCorrection;
        String sign = calculateSignature(timestamp + config.getCexApiKey(), config.getCexApiSecret());

        AuthRequest request = new AuthRequest();
        request.setAuth(new AuthData(config.getCexApiKey(), timestamp, sign));

        try {
            String message = objectWriter.writeValueAsString(request);

            LOGGER.info("Sending authentication request [ timestamp = " + timestamp + " ]");
            LOGGER.debug("Sending WS message: " + message);

            send(message);
        } catch (JsonProcessingException e) {
            LOGGER.error("CommonResponse while processing 'auth' request: " + e.toString());
        }
    }

    public void subscribe(String pair) throws WebsocketClientException {
        subscriptionStateChangeRequest(pair, SubscribeRequest.EVENT_SUBSCRIBE);
    }

    public void unsubscribe(String pair) throws WebsocketClientException {
        subscriptionStateChangeRequest(pair, SubscribeRequest.EVENT_UNSUBSCRIBE);
    }

    private void subscriptionStateChangeRequest(String pair, String eventType) throws WebsocketClientException {
        if (!connected) {
            throw new WebsocketClientException("Connection not opened");
        }

        OrderBookSubscriptionRequest request = new OrderBookSubscriptionRequest();
        request.setData(new SubscriptionData(pair.split("/"), 15, true));

        try {
            String message = objectWriter.writeValueAsString(request);

            LOGGER.debug("Sending WS message: " + message);

            send(message);
        } catch (JsonProcessingException e) {
            LOGGER.error("CommonResponse while processing '" + eventType + "' request: " + e.toString());

            throw new WebsocketClientException("Unable to execute '" + eventType + "' for pair " + pair);
        }
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {
        connected = true;

        LOGGER.info("CEX WS connection opened");

        eventCallback.onOpen(handshakedata);
    }

    @Override
    public void onMessage(String message) {
        LOGGER.debug("CEX WS message received: " + message);

        eventCallback.onMessage(message);
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        connected = false;

        LOGGER.info("CEX WS connection closed by " + (remote ? "remote peer" : "us") + " Code: " + code + " Reason: "
                + reason);

        eventCallback.onClose(code, reason, remote);
    }

    @Override
    public void onError(Exception ex) {
        LOGGER.error("CEX WS error: " + ex.toString());
        ex.printStackTrace();

        eventCallback.onError(ex);
    }


    private String calculateSignature(String data, String apiSecret) {
        Mac hmac = null;

        try {
            hmac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiSecret.getBytes(StandardCharsets.UTF_8), "HmacSHA256");
            hmac.init(secret_key);
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
        }

        return String.format("%X", new BigInteger(1, hmac.doFinal(data.getBytes())));
    }

    public boolean isConnected() {
        return connected;
    }
}