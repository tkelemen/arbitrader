package com.tkelemen.arbitrader.client.cex.ws.message;

import java.util.Arrays;

public class SubscriptionData {
    private String[] pair;
    private int depth;
    private boolean subscribe;

    public SubscriptionData() {
    }

    public SubscriptionData(String[] pair, int depth, boolean subscribe) {
        this.pair = pair;
        this.depth = depth;
        this.subscribe = subscribe;
    }

    public String[] getPair() {
        return pair;
    }

    public void setPair(String[] pair) {
        this.pair = pair;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public boolean isSubscribe() {
        return subscribe;
    }

    public void setSubscribe(boolean subscribe) {
        this.subscribe = subscribe;
    }

    @Override
    public String toString() {
        return "SubscriptionData{" +
                "pair=" + Arrays.toString(pair) +
                ", depth=" + depth +
                ", subscribe=" + subscribe +
                '}';
    }
}
