package com.tkelemen.arbitrader.client.cex.rest.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CurrencyLimitDetail {
    private List<PairDetail> pairs;

    public List<PairDetail> getPairs() {
        return pairs;
    }

    public void setPairs(List<PairDetail> pairs) {
        this.pairs = pairs;
    }

    @Override
    public String toString() {
        return "CurrencyLimitDetail{" +
                "pairs=" + pairs +
                '}';
    }
}
