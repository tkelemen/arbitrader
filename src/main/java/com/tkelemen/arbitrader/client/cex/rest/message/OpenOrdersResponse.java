package com.tkelemen.arbitrader.client.cex.rest.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.tkelemen.arbitrader.client.cex.rest.deserializer.OpenOrdersDeserializer;

import java.util.ArrayList;
import java.util.List;


@JsonDeserialize(using = OpenOrdersDeserializer.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OpenOrdersResponse extends CommonResponse {
    private List<Order> openOrders = new ArrayList<>();

    public List<Order> getOpenOrders() {
        return openOrders;
    }

    public void setOpenOrders(List<Order> openOrders) {
        this.openOrders = openOrders;
    }

    @Override
    public String toString() {
        return "OpenOrdersResponse{" +
                "openOrders=" + openOrders +
                "} " + super.toString();
    }
}
