package com.tkelemen.arbitrader.client.cex.rest.message;

public class GetOrderRequest extends PrivateQueryRequest {
    private String id;

    public GetOrderRequest(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "GetOrderRequest{" +
                "id='" + id + '\'' +
                "} " + super.toString();
    }
}
