package com.tkelemen.arbitrader.client.cex.rest;

import com.tkelemen.arbitrader.client.cex.rest.message.GetBalanceResponse;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GetBalanceClient extends PrivateCexRestQuery {
    @Autowired
    public GetBalanceClient(ApplicationConfig config) {
        super(config);
    }

    public GetBalanceResponse get() {
        return privateQuery("balance/", GetBalanceResponse.class);
    }
}
