package com.tkelemen.arbitrader.client.cex.rest.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.tkelemen.arbitrader.client.cex.rest.message.CancelOrderResponse;

import java.io.IOException;

public class CancelOrderDeserializer extends JsonDeserializer<CancelOrderResponse> {
    @Override
    public CancelOrderResponse deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        final JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        final CancelOrderResponse response = new CancelOrderResponse();

        if (node.has("error")) {
            response.setError(node.get("error").asText());
        } else if(node.isBoolean()) {
            response.setSuccess(node.asBoolean());
        }

        return response;
    }
}