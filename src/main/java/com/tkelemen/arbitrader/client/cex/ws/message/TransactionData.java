package com.tkelemen.arbitrader.client.cex.ws.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionData {
    private Long id;
    private String d;
    private String c;
    private BigDecimal a;
    private String user;
    private String symbol;
    private String symbol2;
    private BigDecimal amount;
    private String order;
    private String type;
    private BigDecimal ds;
    private BigDecimal cs;
    private String time;
    private BigDecimal balance;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }

    public String getC() {
        return c;
    }

    public void setC(String c) {
        this.c = c;
    }

    public BigDecimal getA() {
        return a;
    }

    public void setA(BigDecimal a) {
        this.a = a;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol2() {
        return symbol2;
    }

    public void setSymbol2(String symbol2) {
        this.symbol2 = symbol2;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getDs() {
        return ds;
    }

    public void setDs(BigDecimal ds) {
        this.ds = ds;
    }

    public BigDecimal getCs() {
        return cs;
    }

    public void setCs(BigDecimal cs) {
        this.cs = cs;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "TransactionData{" +
                "id=" + id +
                ", d='" + d + '\'' +
                ", c='" + c + '\'' +
                ", a=" + a +
                ", user='" + user + '\'' +
                ", symbol='" + symbol + '\'' +
                ", symbol2='" + symbol2 + '\'' +
                ", amount=" + amount +
                ", order='" + order + '\'' +
                ", type='" + type + '\'' +
                ", ds=" + ds +
                ", cs=" + cs +
                ", time='" + time + '\'' +
                ", balance=" + balance +
                '}';
    }
}
