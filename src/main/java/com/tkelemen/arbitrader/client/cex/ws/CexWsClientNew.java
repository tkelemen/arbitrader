package com.tkelemen.arbitrader.client.cex.ws;

import com.tkelemen.arbitrader.config.ApplicationConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.jetty.JettyWebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import javax.net.ssl.SSLContext;
import java.net.http.HttpClient;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

@Service
public class CexWsClientNew {
    private final ApplicationConfig config;

    @Autowired
    public CexWsClientNew(ApplicationConfig config) {
        this.config = config;
    }

    public void connect() {
        SSLContext sslContext = null;
        try {
            sslContext = SSLContext.getInstance( "TLS" );
            sslContext.init( null, null, null ); // will use java's default key and trust store which is sufficient unless you deal with self-signed certificates
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

        //final SslContextFactory.Client factory = new SslContextFactory.Client();
        //factory.setSslContext(sslContext);
        WebSocketClient client = new JettyWebSocketClient();

        WebSocketStompClient stompClient = new WebSocketStompClient(client);
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
        stompClient.start();
        StompSessionHandler sessionHandler = new CexStompSessionHandler(config);
        stompClient.connect(config.getCexWebSocketUrl(), sessionHandler);

        new Scanner(System.in).nextLine(); // Don't close immediately.
    }
}
