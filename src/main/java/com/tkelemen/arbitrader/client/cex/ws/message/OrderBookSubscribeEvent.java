package com.tkelemen.arbitrader.client.cex.ws.message;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize
public class OrderBookSubscribeEvent extends OrderBookUpdateEvent {
    private String oid;

    private String ok;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getOk() {
        return ok;
    }

    public void setOk(String ok) {
        this.ok = ok;
    }
}
