package com.tkelemen.arbitrader.client.cex.rest.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CurrencyLimitsResponse {
    private String e;
    private String ok;
    private CurrencyLimitDetail data;

    public String getE() {
        return e;
    }

    public void setE(String e) {
        this.e = e;
    }

    public String getOk() {
        return ok;
    }

    public void setOk(String ok) {
        this.ok = ok;
    }

    public CurrencyLimitDetail getData() {
        return data;
    }

    public void setData(CurrencyLimitDetail data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "CurrencyLimitsResponse{" +
                "e='" + e + '\'' +
                ", ok='" + ok + '\'' +
                ", data=" + data +
                '}';
    }
}
