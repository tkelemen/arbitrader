package com.tkelemen.arbitrader.client.cex.rest;

import com.tkelemen.arbitrader.client.cex.rest.message.GetTickersResponse;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetTickersClient extends CexRestQuery {
    @Autowired
    public GetTickersClient(ApplicationConfig config) {
        super(config);
    }

    public GetTickersResponse get(List<String> tickers) {
        return get("tickers/" + String.join("/", tickers), GetTickersResponse.class);
    }
}
