package com.tkelemen.arbitrader.client.cex.rest.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PairDetail {
    private String symbol1;
    private String symbol2;
    private int pricePrecision;
    private BigDecimal minLotSize;
    private BigDecimal minLotSizeS2;
    private BigDecimal maxLotSize;
    private BigDecimal minPrice;
    private BigDecimal maxPrice;

    public String getSymbol1() {
        return symbol1;
    }

    public void setSymbol1(String symbol1) {
        this.symbol1 = symbol1;
    }

    public String getSymbol2() {
        return symbol2;
    }

    public void setSymbol2(String symbol2) {
        this.symbol2 = symbol2;
    }

    public int getPricePrecision() {
        return pricePrecision;
    }

    public void setPricePrecision(int pricePrecision) {
        this.pricePrecision = pricePrecision;
    }

    public BigDecimal getMinLotSize() {
        return minLotSize;
    }

    public void setMinLotSize(BigDecimal minLotSize) {
        this.minLotSize = minLotSize;
    }

    public BigDecimal getMinLotSizeS2() {
        return minLotSizeS2;
    }

    public void setMinLotSizeS2(BigDecimal minLotSizeS2) {
        this.minLotSizeS2 = minLotSizeS2;
    }

    public BigDecimal getMaxLotSize() {
        return maxLotSize;
    }

    public void setMaxLotSize(BigDecimal maxLotSize) {
        this.maxLotSize = maxLotSize;
    }

    public BigDecimal getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
    }

    public BigDecimal getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(BigDecimal maxPrice) {
        this.maxPrice = maxPrice;
    }

    @Override
    public String toString() {
        return "PairDetail{" +
                "symbol1='" + symbol1 + '\'' +
                ", symbol2='" + symbol2 + '\'' +
                ", pricePrecision=" + pricePrecision +
                ", minLotSize=" + minLotSize +
                ", minLotSizeS2=" + minLotSizeS2 +
                ", maxLotSize=" + maxLotSize +
                ", minPrice=" + minPrice +
                ", maxPrice=" + maxPrice +
                '}';
    }
}
