package com.tkelemen.arbitrader.client.cex.ws.message;

import java.io.Serializable;

public class AuthRequest implements Serializable {
    private String e = "auth";

    private AuthData auth;

    public String getE() {
        return e;
    }

    public void setE(String e) {
        this.e = e;
    }

    public AuthData getAuth() {
        return auth;
    }

    public void setAuth(AuthData auth) {
        this.auth = auth;
    }

    @Override
    public String toString() {
        return "AuthRequest{" +
                "e='" + e + '\'' +
                ", auth=" + auth +
                '}';
    }
}
