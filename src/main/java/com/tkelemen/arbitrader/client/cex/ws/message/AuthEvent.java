package com.tkelemen.arbitrader.client.cex.ws.message;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize
public class AuthEvent extends CexEvent {
    public static final String OK = "ok";

    private String ok;

    private Data data;

    private long timestamp;

    public AuthEvent() {
    }

    public String getOk() {
        return ok;
    }

    public void setOk(String ok) {
        this.ok = ok;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "AuthEvent{" +
                "ok='" + ok + '\'' +
                ", data=" + data +
                ", timestamp=" + timestamp +
                '}';
    }

    @JsonDeserialize
    class Data {
        private String ok;

        private String error;

        public Data() {
        }

        public String getOk() {
            return ok;
        }

        public void setOk(String ok) {
            this.ok = ok;
        }

        public String getError() {
            return error;
        }

        public void setError(String error) {
            this.error = error;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "ok='" + ok + '\'' +
                    ", error='" + error + '\'' +
                    '}';
        }
    }
}
