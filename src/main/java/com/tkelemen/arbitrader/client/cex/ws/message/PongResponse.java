package com.tkelemen.arbitrader.client.cex.ws.message;

import java.io.Serializable;

public class PongResponse implements Serializable {
    private String e = "pong";

    public String getE() {
        return e;
    }

    public void setE(String e) {
        this.e = e;
    }
}
