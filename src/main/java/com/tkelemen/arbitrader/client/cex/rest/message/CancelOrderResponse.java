package com.tkelemen.arbitrader.client.cex.rest.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.tkelemen.arbitrader.client.cex.rest.deserializer.CancelOrderDeserializer;

@JsonDeserialize(using = CancelOrderDeserializer.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CancelOrderResponse extends CommonResponse {
    private boolean isSuccess;

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    @Override
    public String toString() {
        return "CancelOrderResponse{" +
                "isSuccess=" + isSuccess +
                "} " + super.toString();
    }
}
