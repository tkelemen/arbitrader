package com.tkelemen.arbitrader.client.cex.rest.message;

import java.math.BigDecimal;

public class Balance {
    private BigDecimal available;
    private BigDecimal orders;

    public Balance() {
    }

    public Balance(BigDecimal available, BigDecimal orders) {
        this.available = available;
        this.orders = orders;
    }

    public BigDecimal getAvailable() {
        return available;
    }

    public void setAvailable(BigDecimal available) {
        this.available = available;
    }

    public BigDecimal getOrders() {
        return orders;
    }

    public void setOrders(BigDecimal orders) {
        this.orders = orders;
    }

    @Override
    public String toString() {
        return "Balance [available=" + available.toPlainString() + ", orders=" + orders.toPlainString() + "]";
    }

}
