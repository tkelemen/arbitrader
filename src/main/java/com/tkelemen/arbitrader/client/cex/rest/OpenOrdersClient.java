package com.tkelemen.arbitrader.client.cex.rest;

import com.tkelemen.arbitrader.client.cex.rest.message.OpenOrdersResponse;
import com.tkelemen.arbitrader.client.cex.rest.message.Order;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class OpenOrdersClient extends PrivateCexRestQuery {

    @Autowired
    public OpenOrdersClient(ApplicationConfig config) {
        super(config);
    }

    public String getString() {
        return privateQuery("open_orders/", String.class);
    }

    public OpenOrdersResponse get() {
        return privateQuery("open_orders/", OpenOrdersResponse.class);
    }
}
