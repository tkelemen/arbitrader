package com.tkelemen.arbitrader.client.cex.ws.message;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderBookData {
    @JsonAlias({"time"})
    private long timestamp;

    private String pair;

    private long id;

    private List<BigDecimal[]> bids;

    private List<BigDecimal[]> asks;

    private String error;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<BigDecimal[]> getBids() {
        return bids;
    }

    public void setBids(List<BigDecimal[]> bids) {
        this.bids = bids;
    }

    public List<BigDecimal[]> getAsks() {
        return asks;
    }

    public void setAsks(List<BigDecimal[]> asks) {
        this.asks = asks;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        StringBuilder bidsSb = new StringBuilder();

        for (BigDecimal[] record : bids) {
            bidsSb.append(record[0].toPlainString()).append(":").append(record[1].toPlainString()).append(",");
        }
        if (bidsSb.length() > 0) {
            bidsSb.deleteCharAt(bidsSb.length() - 1);
        }
        StringBuilder askSb = new StringBuilder();

        for (BigDecimal[] record : asks) {
            askSb.append(record[0].toPlainString()).append(":").append(record[1].toPlainString()).append(",");
        }
        if (askSb.length() > 0) {
            askSb.deleteCharAt(askSb.length() - 1);
        }

        return "OrderBookData{" +
                "timestamp=" + timestamp +
                ", pair='" + pair + '\'' +
                ", id=" + id +
                ", bids=" + bidsSb.toString() +
                ", asks=" + askSb.toString() +
                ", error='" + error + '\'' +
                '}';
    }
}
