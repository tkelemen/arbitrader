package com.tkelemen.arbitrader.client.cex.ws.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.tkelemen.arbitrader.client.cex.ws.deserializer.CexEventDeserializer;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(using = CexEventDeserializer.class)
public class CexEvent {
    public static final String EVENT_CONNECTED = "connected";
    public static final String EVENT_AUTH = "auth";
    public static final String EVENT_PING = "ping";
    public static final String EVENT_ORDER_BOOK_SUBSCRIBE = "order-book-subscribe";
    public static final String EVENT_ORDER_BOOK_UPDATE = "md_update";
    public static final String EVENT_TRANSACTION = "tx";

    @JsonProperty("e")
    private String eventType;

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }
}
