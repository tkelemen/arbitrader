package com.tkelemen.arbitrader.client.cex.rest;

import com.tkelemen.arbitrader.client.cex.rest.message.MassCancelPlaceOrdersRequest;
import com.tkelemen.arbitrader.client.cex.rest.message.MassCancelPlaceOrdersResponse;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MassCancelPlaceOrdersClient extends PrivateCexRestQuery {
    @Autowired
    public MassCancelPlaceOrdersClient(ApplicationConfig config) {
        super(config);
    }
    public MassCancelPlaceOrdersResponse cancelPlace(MassCancelPlaceOrdersRequest request) {
        return privateQuery("mass_cancel_place_orders/", MassCancelPlaceOrdersResponse.class, request);
    }
}
