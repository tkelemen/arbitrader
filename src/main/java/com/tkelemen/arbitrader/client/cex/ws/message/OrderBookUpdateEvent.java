package com.tkelemen.arbitrader.client.cex.ws.message;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize
public class OrderBookUpdateEvent extends CexEvent {
    private OrderBookData data;

    public OrderBookData getData() {
        return data;
    }

    public void setData(OrderBookData data) {
        this.data = data;
    }
}
