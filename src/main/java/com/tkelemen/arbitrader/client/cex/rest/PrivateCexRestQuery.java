package com.tkelemen.arbitrader.client.cex.rest;

import com.tkelemen.arbitrader.client.cex.rest.message.PrivateQueryRequest;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.BodyInserters;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

abstract public class PrivateCexRestQuery extends CexRestQuery {
    private static volatile long lastNonce = System.currentTimeMillis();

    private static final NavigableMap<Long, String> requestLog = new TreeMap<>();

    PrivateCexRestQuery(ApplicationConfig config) {
        super(config);
    }

    synchronized <T> T privateQuery(String path, Class<T> responseType) {
        return privateQuery(path, responseType, new PrivateQueryRequest());
    }

    synchronized <T> T privateQuery(String path, Class<T> responseType, PrivateQueryRequest request) {
        long nonce = getNextNonce();

        request.setNonce(nonce);
        request.setKey(config.getCexApiKey());
        request.setSignature(calculateSignature(nonce, config.getCexApiUsername(), config.getCexApiKey(),
                config.getCexApiSecret()));

        recordRequest(nonce, path);

        return client.post().uri(path)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(request))
                .retrieve()
                .bodyToMono(responseType).block();
    }

    private synchronized static void recordRequest(long nonce, String path) {
        if(requestLog.size() > 9999) {
            requestLog.remove(requestLog.firstKey());
        }
        requestLog.put(nonce, path);
    }

    private String calculateSignature(long nonce, String username, String apiKey, String apiSecret) {
        final String message = nonce + username + apiKey;

        try {
            final Mac hmac = Mac.getInstance("HmacSHA256");
            final SecretKeySpec secretKey = new SecretKeySpec(apiSecret.getBytes(StandardCharsets.UTF_8), "HmacSHA256");
            hmac.init(secretKey);

            return String.format("%064x", new BigInteger(1, hmac.doFinal(message.getBytes()))).toUpperCase();
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
        }

        // this should not happen
        return "";
    }

    private static synchronized long getNextNonce() {
        long nextNonce = System.currentTimeMillis();

        if(lastNonce == nextNonce) {
            nextNonce++;
        }

        lastNonce = nextNonce;

        return nextNonce;
    }

    public static synchronized Map<Long, String> getRequestLog() {
        return requestLog;
    }
}

