package com.tkelemen.arbitrader.client.cex.rest.message;

import java.math.BigDecimal;

public class Order {
    private String id;
    private Long time;
    private String type;
    private BigDecimal price;
    private BigDecimal amount;
    private BigDecimal pending;
    private String symbol1;
    private String symbol2;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getPending() {
        return pending;
    }

    public void setPending(BigDecimal pending) {
        this.pending = pending;
    }

    public String getSymbol1() {
        return symbol1;
    }

    public void setSymbol1(String symbol1) {
        this.symbol1 = symbol1;
    }

    public String getSymbol2() {
        return symbol2;
    }

    public void setSymbol2(String symbol2) {
        this.symbol2 = symbol2;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id='" + id + '\'' +
                ", time=" + time +
                ", type='" + type + '\'' +
                ", price=" + price +
                ", amount=" + amount +
                ", pending=" + pending +
                ", symbol1='" + symbol1 + '\'' +
                ", symbol2='" + symbol2 + '\'' +
                '}';
    }
}
