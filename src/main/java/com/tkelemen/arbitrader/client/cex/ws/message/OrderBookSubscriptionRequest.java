package com.tkelemen.arbitrader.client.cex.ws.message;

public class OrderBookSubscriptionRequest {
    private String e = "order-book-subscribe";

    private SubscriptionData data;

    private String oid;

    public String getE() {
        return e;
    }

    public void setE(String e) {
        this.e = e;
    }

    public SubscriptionData getData() {
        return data;
    }

    public void setData(SubscriptionData data) {
        this.data = data;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    @Override
    public String toString() {
        return "OrderBookSubscriptionRequest{" +
                "e='" + e + '\'' +
                ", data=" + data +
                ", oid='" + oid + '\'' +
                '}';
    }
}
