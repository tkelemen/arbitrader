package com.tkelemen.arbitrader.client.cex.rest.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.tkelemen.arbitrader.client.cex.rest.message.Balance;
import com.tkelemen.arbitrader.client.cex.rest.message.GetBalanceResponse;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Map;

public class GetBalanceDeserializer extends JsonDeserializer<GetBalanceResponse> {
    @Override
    public GetBalanceResponse deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        ObjectCodec oc = jsonParser.getCodec();
        JsonNode node = oc.readTree(jsonParser);

        GetBalanceResponse response = new GetBalanceResponse();
        Iterator<Map.Entry<String, JsonNode>> entryIterator = node.fields();

        if (node.has("error")) {
            response.setError(node.get("error").asText());
        } else {
            while (entryIterator.hasNext()) {
                Map.Entry<String, JsonNode> entry = entryIterator.next();

                if ("timestamp".equals(entry.getKey())) {
                    response.setTimestamp(entry.getValue().asText());
                } else if ("username".equals(entry.getKey())) {
                    response.setUsername(entry.getValue().asText());
                } else {
                    JsonNode available = entry.getValue().get("available");
                    JsonNode orders = entry.getValue().get("orders");

                    response.setBalance(entry.getKey(), new Balance(new BigDecimal(available.textValue()), orders != null ? new BigDecimal(orders.textValue()) : BigDecimal.ZERO));
                }
            }
        }

        return response;
    }
}