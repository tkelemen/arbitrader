package com.tkelemen.arbitrader.client.cex.rest.message;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MassCancelPlaceOrdersRequest extends PrivateQueryRequest {
    @JsonProperty("cancel-orders")
    private List<String> cancelOrders = new ArrayList<>();

    @JsonProperty("place-orders")
    private List<PlaceOrder> placeOrders = new ArrayList<>();

    private boolean cancelPlacedOrdersIfPlaceFailed;

    public void addCancelOrder(String orderId) {
        cancelOrders.add(orderId);
    }

    public void addPlaceOrder(String pair, PlaceOrderType type, BigDecimal price, BigDecimal amount) {
        final PlaceOrder placeOrder = new PlaceOrder();
        placeOrder.setType(type.name().toLowerCase());
        placeOrder.setPair(pair.split("/"));
        placeOrder.setPrice(price);
        placeOrder.setAmount(amount);

        placeOrders.add(placeOrder);
    }

    public List<String> getCancelOrders() {
        return cancelOrders;
    }

    public void setCancelOrders(List<String> cancelOrders) {
        this.cancelOrders = cancelOrders;
    }

    public List<PlaceOrder> getPlaceOrders() {
        return placeOrders;
    }

    public void setPlaceOrders(List<PlaceOrder> placeOrders) {
        this.placeOrders = placeOrders;
    }

    public boolean isCancelPlacedOrdersIfPlaceFailed() {
        return cancelPlacedOrdersIfPlaceFailed;
    }

    public void setCancelPlacedOrdersIfPlaceFailed(boolean cancelPlacedOrdersIfPlaceFailed) {
        this.cancelPlacedOrdersIfPlaceFailed = cancelPlacedOrdersIfPlaceFailed;
    }

    public enum PlaceOrderType {
        BUY, SELL
    }

    public static class PlaceOrder {
        private String[] pair;
        private String type;
        private BigDecimal amount;
        private BigDecimal price;

        @JsonProperty("order_type")
        private String orderType = "limit";

        public String[] getPair() {
            return pair;
        }

        public void setPair(String[] pair) {
            this.pair = pair;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public void setAmount(BigDecimal amount) {
            this.amount = amount;
        }

        public String getOrderType() {
            return orderType;
        }

        public void setOrderType(String orderType) {
            this.orderType = orderType;
        }

        public BigDecimal getPrice() {
            return price;
        }

        public void setPrice(BigDecimal price) {
            this.price = price;
        }

    }
}
