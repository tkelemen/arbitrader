package com.tkelemen.arbitrader.client.cex.rest.message;

import javax.xml.bind.annotation.XmlElement;
import java.math.BigDecimal;

public class PlaceMarketOrderRequest extends PrivateQueryRequest {
    public String type;
    public BigDecimal amount;

    @XmlElement(name = "order_type")
    public String order_type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getOrder_Type() {
        return order_type;
    }

    public void setOrder_Type(String orderType) {
        this.order_type = orderType;
    }

    @Override
    public String toString() {
        return "PlaceMarketOrderRequest [type=" + type + ", amount=" + amount + ", orderType=" + order_type + " ]";
    }
}
