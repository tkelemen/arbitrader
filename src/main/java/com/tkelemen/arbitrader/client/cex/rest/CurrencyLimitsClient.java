package com.tkelemen.arbitrader.client.cex.rest;

import com.tkelemen.arbitrader.client.cex.rest.message.CurrencyLimitsResponse;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CurrencyLimitsClient extends CexRestQuery {
    @Autowired
    public CurrencyLimitsClient(ApplicationConfig config) {
        super(config);
    }

    public CurrencyLimitsResponse get() {
        return get("currency_limits", CurrencyLimitsResponse.class);
    }
}
