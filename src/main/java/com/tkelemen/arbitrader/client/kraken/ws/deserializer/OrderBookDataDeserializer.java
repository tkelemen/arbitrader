package com.tkelemen.arbitrader.client.kraken.ws.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.tkelemen.arbitrader.client.kraken.ws.message.OrderBookData;
import com.tkelemen.arbitrader.client.kraken.ws.message.OrderBookEntry;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class OrderBookDataDeserializer extends JsonDeserializer<OrderBookData> {
    @Override
    public OrderBookData deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        ObjectCodec oc = jsonParser.getCodec();
        JsonNode node = oc.readTree(jsonParser);

        JsonNode asks = node.has("as") ? node.get("as") : node.get("a");
        List<OrderBookEntry> asList = new ArrayList<>();
        if (asks != null) {
            for (JsonNode entry : asks) {
                asList.add(new OrderBookEntry(entry.get(0).asText(), entry.get(1).asText(), entry.get(2).asText()));
            }
        }

        JsonNode bets = node.has("bs") ? node.get("bs") : node.get("b");
        List<OrderBookEntry> bsList = new ArrayList<>();
        if (bets != null) {
            for (JsonNode entry : bets) {
                bsList.add(new OrderBookEntry(entry.get(0).asText(), entry.get(1).asText(), entry.get(2).asText()));
            }
        }

        long checksum = 0L;
        if (node.has("c")) {
            checksum = node.get("c").asLong();
        }

        return new OrderBookData(asList, bsList, checksum);
    }
}
