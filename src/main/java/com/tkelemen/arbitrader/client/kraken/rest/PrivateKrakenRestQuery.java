package com.tkelemen.arbitrader.client.kraken.rest;

import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.exception.RestClientException;
import com.tkelemen.arbitrader.util.CryptoUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

abstract class PrivateKrakenRestQuery {
    protected ApplicationConfig config;

    private WebClient client;

    @Autowired
    PrivateKrakenRestQuery(ApplicationConfig config) {
        this.config = config;

        client = WebClient.builder()
                .baseUrl(config.getKrakenApiRestBaseUrl())
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .build();
    }

    <T> T query(String path, Class<T> responseType) throws RestClientException {
        return query(path, responseType, new HashMap<>());
    }

    <T> T query(String path, Class<T> responseType, Map<String, String> parameters) throws RestClientException {
        long nonce = System.currentTimeMillis();

        parameters.put("nonce", String.valueOf(nonce));

        StringBuilder signData = new StringBuilder();
        MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();

        try {
            for (Map.Entry<String, String> param : parameters.entrySet()) {
                formData.add(param.getKey(), param.getValue());

                if (signData.length() > 0) {
                    signData.append("&");
                }
                signData.append(param.getKey()).append("=").append(CryptoUtils.urlEncode(param.getValue()));
            }

            return client.post().uri(path).body(BodyInserters.fromFormData(formData)).headers(httpHeaders -> {
                httpHeaders.add("API-Key", config.getKrakenApiKey());
                try {
                    httpHeaders.add("API-Sign", calculateSign(signData.toString(), nonce, path, config.getKrakenApiPrivateKey()));
                } catch (InvalidKeyException | NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
            }).retrieve().bodyToMono(responseType).block();
        } catch (UnsupportedEncodingException e) {
            throw new RestClientException(e);
        }
    }

    private String calculateSign(String postData, long nonce, String url, String secret)
            throws InvalidKeyException, NoSuchAlgorithmException {
        // create SHA-256 hash of the nonce and the POST data
        byte[] sha256 = CryptoUtils.sha256(nonce + postData);

        // set the API method and retrieve the path
        byte[] path = CryptoUtils.stringToBytes(url);

        // decode the API secret, it's the HMAC key
        byte[] hmacKey = CryptoUtils.base64Decode(secret);

        // create the HMAC message from the path and the previous hash
        byte[] hmacMessage = CryptoUtils.concatArrays(path, sha256);

        // create the HMAC-SHA512 digest, encode it and set it as the request
        // signature
        String hmacDigest = CryptoUtils.base64Encode(CryptoUtils.hmacSha512(hmacKey, hmacMessage));

        return hmacDigest;
    }

}

