package com.tkelemen.arbitrader.client.kraken.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetOrderInfoResponse {
    private List<String> error;

    private Map<String, Order> result;

    public List<String> getError() {
        return error;
    }

    public void setError(List<String> error) {
        this.error = error;
    }

    public Map<String, Order> getResult() {
        return result;
    }

    public void setResult(Map<String, Order> result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "GetOrderInfoResponse{" +
                "error=" + error +
                ", result=" + result +
                '}';
    }

    public static class Order {
        public static final String STATUS_OPEN = "open";
        public static final String STATUS_CANCELLED = "cancelled";
        public static final String STATUS_CLOSED = "closed";

        private String refid;
        private String userref;
        private String status;
        private String reason;
        private BigDecimal opentm;
        private BigDecimal closetm;
        private BigDecimal starttm;
        private BigDecimal expiretm;
        private Description descr;
        private BigDecimal vol;
        private BigDecimal vol_exec;
        private BigDecimal cost;
        private BigDecimal fee;
        private BigDecimal price;
        private BigDecimal stopprice;
        private BigDecimal limitprice;
        private String misc;
        private String oflags;
        private List<String> trades;

        public String getRefid() {
            return refid;
        }

        public void setRefid(String refid) {
            this.refid = refid;
        }

        public String getUserref() {
            return userref;
        }

        public void setUserref(String userref) {
            this.userref = userref;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public BigDecimal getOpentm() {
            return opentm;
        }

        public void setOpentm(BigDecimal opentm) {
            this.opentm = opentm;
        }

        public BigDecimal getClosetm() {
            return closetm;
        }

        public void setClosetm(BigDecimal closetm) {
            this.closetm = closetm;
        }

        public BigDecimal getStarttm() {
            return starttm;
        }

        public void setStarttm(BigDecimal starttm) {
            this.starttm = starttm;
        }

        public BigDecimal getExpiretm() {
            return expiretm;
        }

        public void setExpiretm(BigDecimal expiretm) {
            this.expiretm = expiretm;
        }

        public Description getDescr() {
            return descr;
        }

        public void setDescr(Description descr) {
            this.descr = descr;
        }

        public BigDecimal getVol() {
            return vol;
        }

        public void setVol(BigDecimal vol) {
            this.vol = vol;
        }

        public BigDecimal getVol_exec() {
            return vol_exec;
        }

        public void setVol_exec(BigDecimal vol_exec) {
            this.vol_exec = vol_exec;
        }

        public BigDecimal getCost() {
            return cost;
        }

        public void setCost(BigDecimal cost) {
            this.cost = cost;
        }

        public BigDecimal getFee() {
            return fee;
        }

        public void setFee(BigDecimal fee) {
            this.fee = fee;
        }

        public BigDecimal getPrice() {
            return price;
        }

        public void setPrice(BigDecimal price) {
            this.price = price;
        }

        public BigDecimal getStopprice() {
            return stopprice;
        }

        public void setStopprice(BigDecimal stopprice) {
            this.stopprice = stopprice;
        }

        public BigDecimal getLimitprice() {
            return limitprice;
        }

        public void setLimitprice(BigDecimal limitprice) {
            this.limitprice = limitprice;
        }

        public String getMisc() {
            return misc;
        }

        public void setMisc(String misc) {
            this.misc = misc;
        }

        public String getOflags() {
            return oflags;
        }

        public void setOflags(String oflags) {
            this.oflags = oflags;
        }

        public List<String> getTrades() {
            return trades;
        }

        public void setTrades(List<String> trades) {
            this.trades = trades;
        }

        @Override
        public String toString() {
            return "Order{" +
                    "refid='" + refid + '\'' +
                    ", userref='" + userref + '\'' +
                    ", status='" + status + '\'' +
                    ", reason='" + reason + '\'' +
                    ", opentm=" + opentm +
                    ", closetm=" + closetm +
                    ", starttm=" + starttm +
                    ", expiretm=" + expiretm +
                    ", descr=" + descr +
                    ", vol=" + vol +
                    ", vol_exec=" + vol_exec +
                    ", cost=" + cost +
                    ", fee=" + fee +
                    ", price=" + price +
                    ", stopprice=" + stopprice +
                    ", limitprice=" + limitprice +
                    ", misc='" + misc + '\'' +
                    ", oflags='" + oflags + '\'' +
                    ", trades=" + trades +
                    '}';
        }
    }

    public static class Description {
        private String pair;
        private String type;
        private String ordertype;
        private BigDecimal price;
        private BigDecimal price2;
        private String leverage;
        private String order;
        private String close;

        public String getPair() {
            return pair;
        }

        public void setPair(String pair) {
            this.pair = pair;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getOrdertype() {
            return ordertype;
        }

        public void setOrdertype(String ordertype) {
            this.ordertype = ordertype;
        }

        public BigDecimal getPrice() {
            return price;
        }

        public void setPrice(BigDecimal price) {
            this.price = price;
        }

        public BigDecimal getPrice2() {
            return price2;
        }

        public void setPrice2(BigDecimal price2) {
            this.price2 = price2;
        }

        public String getLeverage() {
            return leverage;
        }

        public void setLeverage(String leverage) {
            this.leverage = leverage;
        }

        public String getOrder() {
            return order;
        }

        public void setOrder(String order) {
            this.order = order;
        }

        public String getClose() {
            return close;
        }

        public void setClose(String close) {
            this.close = close;
        }

        @Override
        public String toString() {
            return "Description{" +
                    "pair='" + pair + '\'' +
                    ", type='" + type + '\'' +
                    ", ordertype='" + ordertype + '\'' +
                    ", price=" + price +
                    ", price2=" + price2 +
                    ", leverage='" + leverage + '\'' +
                    ", order='" + order + '\'' +
                    ", close='" + close + '\'' +
                    '}';
        }
    }
}
