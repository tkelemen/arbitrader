package com.tkelemen.arbitrader.client.kraken.ws.message;

public class OrderBookSnapshot extends KrakenEvent {
    protected Long channelId;
    protected OrderBookData orderBookData;
    protected String bookType;
    protected String symbol;

    public OrderBookSnapshot(Long channelId, OrderBookData orderBookData, String bookType, String symbol) {
        this.channelId = channelId;
        this.orderBookData = orderBookData;
        this.bookType = bookType;
        this.symbol = symbol;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public OrderBookData getOrderBookData() {
        return orderBookData;
    }

    public void setOrderBookData(OrderBookData orderBookData) {
        this.orderBookData = orderBookData;
    }

    public String getBookType() {
        return bookType;
    }

    public void setBookType(String bookType) {
        this.bookType = bookType;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return "OrderBookSnapshot{" +
                "channelId=" + channelId +
                ", orderBookData=" + orderBookData +
                ", bookType='" + bookType + '\'' +
                ", symbol='" + symbol + '\'' +
                '}';
    }
}
