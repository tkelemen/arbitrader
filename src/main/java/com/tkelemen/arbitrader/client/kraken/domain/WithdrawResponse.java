package com.tkelemen.arbitrader.client.kraken.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WithdrawResponse {
    private WithdrawResult result;

    private List<String> error;

    public WithdrawResult getResult() {
        return result;
    }

    public void setResult(WithdrawResult result) {
        this.result = result;
    }

    public List<String> getError() {
        return error;
    }

    public void setError(List<String> error) {
        this.error = error;
    }

    public static class WithdrawResult {
        private String refid;

        public String getRefid() {
            return refid;
        }

        public void setRefid(String refid) {
            this.refid = refid;
        }

        @Override
        public String toString() {
            return "WithdrawResult{" +
                    "refid='" + refid + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "WithdrawResponse{" +
                "result=" + result +
                ", error=" + error +
                '}';
    }
}
