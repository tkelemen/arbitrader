package com.tkelemen.arbitrader.client.kraken.ws;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.tkelemen.arbitrader.client.kraken.ws.message.SubscribeRequest;
import com.tkelemen.arbitrader.client.kraken.ws.message.Subscription;
import com.tkelemen.arbitrader.client.ws.WsEventCallback;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.exception.WebsocketClientException;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;

@Service
public class KrakenWsClient extends WebSocketClient {
    private static final Logger logger = LoggerFactory.getLogger(KrakenWsClient.class);

    private WsEventCallback eventCallback;

    private boolean connected = false;

    private ObjectWriter objectWriter = new ObjectMapper().writer();

    @Autowired
    public KrakenWsClient(ApplicationConfig config) throws URISyntaxException {
        super(new URI(config.getKrakenWebSocketUrl()));
    }

    public KrakenWsClient(String serverUri) throws URISyntaxException {
        super(new URI(serverUri));
    }

    public void connect(WsEventCallback callback) {
        this.eventCallback = callback;

        connect();
    }

    public void subscribe(String pair) throws WebsocketClientException {
        subscriptionStateChangeRequest(pair, SubscribeRequest.EVENT_SUBSCRIBE);
    }

    public void unsubscribe(String pair) throws WebsocketClientException {
        subscriptionStateChangeRequest(pair, SubscribeRequest.EVENT_UNSUBSCRIBE);
    }

    private void subscriptionStateChangeRequest(String pair, String eventType) throws WebsocketClientException {
        if (!connected) {
            throw new WebsocketClientException("Connection not opened");
        }

        SubscribeRequest request = new SubscribeRequest();
        request.setEvent(eventType);
        request.setPair(Collections.singletonList(pair));
        request.setSubscription(new Subscription(Subscription.NAME_BOOK, 10));

        try {
            String message = objectWriter.writeValueAsString(request);

            logger.debug("Sending WS message: " + message);

            send(message);
        } catch (JsonProcessingException e) {
            logger.error("CommonResponse while processing '" + eventType + "' request: " + e.toString());

            throw new WebsocketClientException("Unable to execute '" + eventType + "' for pair " + pair);
        }
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {
        connected = true;

        logger.info("Kraken WS connection opened");

        eventCallback.onOpen(handshakedata);
    }

    @Override
    public void onMessage(String message) {
        logger.debug("Kraken WS message received: " + message);

        eventCallback.onMessage(message);
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        connected = false;

        logger.debug("Kraken WS connection closed by " + (remote ? "remote peer" : "us") + " Code: " + code + " Reason: "
                + reason);

        eventCallback.onClose(code, reason, remote);
    }

    @Override
    public void onError(Exception ex) {
        logger.error("Kraken WS error: " + ex.toString());

        eventCallback.onError(ex);
    }

    public boolean isConnected() {
        return connected;
    }
}