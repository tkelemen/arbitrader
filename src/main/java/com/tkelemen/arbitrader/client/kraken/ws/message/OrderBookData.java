package com.tkelemen.arbitrader.client.kraken.ws.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.tkelemen.arbitrader.client.kraken.ws.deserializer.OrderBookDataDeserializer;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(using = OrderBookDataDeserializer.class)
public class OrderBookData {
    private List<OrderBookEntry> askEntries;
    private List<OrderBookEntry> bidEntries;
    private long checksum;

    public OrderBookData(List<OrderBookEntry> askEntries, List<OrderBookEntry> bidEntries) {
        this.askEntries = askEntries;
        this.bidEntries = bidEntries;
    }

    public OrderBookData(List<OrderBookEntry> askEntries, List<OrderBookEntry> bidEntries, long checksum) {
        this.askEntries = askEntries;
        this.bidEntries = bidEntries;
        this.checksum = checksum;
    }

    public List<OrderBookEntry> getAskEntries() {
        return askEntries;
    }

    public void setAskEntries(List<OrderBookEntry> askEntries) {
        this.askEntries = askEntries;
    }

    public List<OrderBookEntry> getBidEntries() {
        return bidEntries;
    }

    public void setBidEntries(List<OrderBookEntry> bidEntries) {
        this.bidEntries = bidEntries;
    }

    public long getChecksum() {
        return checksum;
    }

    public void setChecksum(long checksum) {
        this.checksum = checksum;
    }

    @Override
    public String toString() {
        return "OrderBookData{" +
                "askEntries=" + askEntries +
                ", bidEntries=" + bidEntries +
                ", checksum=" + checksum +
                '}';
    }
}
