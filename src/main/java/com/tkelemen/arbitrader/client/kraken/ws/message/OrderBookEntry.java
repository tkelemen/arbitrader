package com.tkelemen.arbitrader.client.kraken.ws.message;

public class OrderBookEntry {
    private String price;
    private String quantity;
    private String timestamp;

    public OrderBookEntry(String price, String quantity, String timestamp) {
        this.price = price;
        this.quantity = quantity;
        this.timestamp = timestamp;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "OrderBookEntry{" +
                "price='" + price + '\'' +
                ", quantity='" + quantity + '\'' +
                ", timestamp='" + timestamp + '\'' +
                '}';
    }
}
