package com.tkelemen.arbitrader.client.kraken.ws.message;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.math.BigInteger;

@JsonDeserialize
public class SubscriptionStatus extends KrakenEvent {
    @JsonProperty("channelID")
    private BigInteger channelId;

    private String channelName;

    private String event;

    private String pair;

    private String status;

    private Subscription subscription;

    public BigInteger getChannelId() {
        return channelId;
    }

    public void setChannelId(BigInteger channelId) {
        this.channelId = channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }

    @Override
    public String toString() {
        return "SubscriptionStatus{" +
                "channelId=" + channelId +
                ", channelName='" + channelName + '\'' +
                ", event='" + event + '\'' +
                ", pair='" + pair + '\'' +
                ", status='" + status + '\'' +
                ", subscription=" + subscription +
                '}';
    }
}
