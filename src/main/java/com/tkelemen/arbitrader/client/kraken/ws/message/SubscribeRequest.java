package com.tkelemen.arbitrader.client.kraken.ws.message;

import java.io.Serializable;
import java.util.List;

public class SubscribeRequest implements Serializable {
    public static final String EVENT_SUBSCRIBE = "subscribe";
    public static final String EVENT_UNSUBSCRIBE = "unsubscribe";

    private String event;

    private List<String> pair;

    private Subscription subscription;

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public List<String> getPair() {
        return pair;
    }

    public void setPair(List<String> pair) {
        this.pair = pair;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }

    @Override
    public String toString() {
        return "SubscribeRequest{" +
                "event='" + event + '\'' +
                ", pair=" + pair +
                ", subscription=" + subscription +
                '}';
    }
}
