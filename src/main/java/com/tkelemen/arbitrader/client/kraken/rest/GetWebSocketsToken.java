package com.tkelemen.arbitrader.client.kraken.rest;

import com.tkelemen.arbitrader.client.kraken.domain.WebSocketsTokenResponse;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.exception.RestClientException;
import org.springframework.stereotype.Service;

@Service
public class GetWebSocketsToken extends PrivateKrakenRestQuery {
    public GetWebSocketsToken(ApplicationConfig config) {
        super(config);
    }

    public WebSocketsTokenResponse get() throws RestClientException {
        return query("/0/private/GetWebSocketsToken", WebSocketsTokenResponse.class);
    }
}
