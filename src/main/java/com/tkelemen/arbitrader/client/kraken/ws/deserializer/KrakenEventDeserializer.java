package com.tkelemen.arbitrader.client.kraken.ws.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.*;
import com.tkelemen.arbitrader.client.kraken.ws.message.*;

import java.io.IOException;
import java.util.Iterator;

public class KrakenEventDeserializer extends JsonDeserializer<KrakenEvent> {

    private ObjectMapper mapper;

    @Override
    public KrakenEvent deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        if (mapper == null) {
            mapper = new ObjectMapper();
        }

        ObjectCodec oc = jsonParser.getCodec();
        JsonNode node = oc.readTree(jsonParser);

        ObjectReader orderBookSnapshotReader = mapper.readerFor(OrderBookData.class);

        KrakenEvent result = new UnknownEvent();

        if (node.isArray()) {
            if(node.size() == 4) {
                Long channelId = node.get(0).asLong();

                JsonNode bookData = node.get(1);

                Iterator<String> it = bookData.fieldNames();
                if (it.hasNext()) {
                    String field = it.next();
                    if ("as".equals(field)) {
                        OrderBookData orderBookData = orderBookSnapshotReader.readValue(bookData);

                        result = new OrderBookSnapshot(channelId, orderBookData, node.get(2).asText(), node.get(3).asText());
                    } else if ("a".equals(field) || "b".equals(field)) {
                        OrderBookData orderBookData = orderBookSnapshotReader.readValue(bookData);

                        result = new OrderBookUpdate(channelId, orderBookData, node.get(2).asText(), node.get(3).asText());
                    }
                }
            } else if (node.size() == 5) {
                Long channelId = node.get(0).asLong();

                JsonNode askBookData = node.get(1);
                JsonNode bidBookData = node.get(2);

                OrderBookData askOrderBookData = orderBookSnapshotReader.readValue(askBookData);
                OrderBookData bidOrderBookData = orderBookSnapshotReader.readValue(bidBookData);
                askOrderBookData.setBidEntries(bidOrderBookData.getBidEntries());
                askOrderBookData.setChecksum(bidOrderBookData.getChecksum());

                result = new OrderBookUpdate(channelId, askOrderBookData, node.get(3).asText(), node.get(4).asText());
            }
        } else if (node.has("errorMessage")) {
            result = mapper.readerFor(ErrorEvent.class).readValue(node);
        } else if (node.has("event")) {
            String eventType = node.get("event").asText();
            switch (eventType) {
                case "systemStatus":
                    result = mapper.readerFor(SystemStatus.class).readValue(node);
                    break;
                case "subscriptionStatus":
                    result = mapper.readerFor(SubscriptionStatus.class).readValue(node);
                    break;
                case "heartbeat":
                    result = mapper.readerFor(Heartbeat.class).readValue(node);
                    break;
            }
        }

        return result;
    }
}
