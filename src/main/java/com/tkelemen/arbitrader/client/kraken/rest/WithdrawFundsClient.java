package com.tkelemen.arbitrader.client.kraken.rest;

import com.tkelemen.arbitrader.client.kraken.domain.WithdrawResponse;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.exception.RestClientException;
import com.tkelemen.arbitrader.util.CurrencyUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Service
public class WithdrawFundsClient extends PrivateKrakenRestQuery {
    public WithdrawFundsClient(ApplicationConfig config) {
        super(config);
    }

    public WithdrawResponse withdraw(String asset, String recipientAlias, BigDecimal amount) throws RestClientException {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("asset", asset);
        parameters.put("key", recipientAlias);
        parameters.put("amount", CurrencyUtils.formatCrypto(amount));

        return query("/0/private/Withdraw", WithdrawResponse.class, parameters);
    }
}
