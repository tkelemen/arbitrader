package com.tkelemen.arbitrader.client.kraken.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddOrderResponse {
    private OrderInfo result;

    private List<String> error;

    public OrderInfo getResult() {
        return result;
    }

    public void setResult(OrderInfo result) {
        this.result = result;
    }

    public List<String> getError() {
        return error;
    }

    public void setError(List<String> error) {
        this.error = error;
    }

    public static class OrderInfo {
        private Description descr;

        private List<String> txid;

        public Description getDescr() {
            return descr;
        }

        public void setDescr(Description descr) {
            this.descr = descr;
        }

        public List<String> getTxid() {
            return txid;
        }

        public void setTxid(List<String> txid) {
            this.txid = txid;
        }

        @Override
        public String toString() {
            return "OrderInfo{" +
                    "descr=" + descr +
                    ", txid=" + txid +
                    '}';
        }
    }

    static class Description {
        private String order;

        private String close;

        public String getOrder() {
            return order;
        }

        public void setOrder(String order) {
            this.order = order;
        }

        public String getClose() {
            return close;
        }

        public void setClose(String close) {
            this.close = close;
        }

        @Override
        public String toString() {
            return "Description{" +
                    "order='" + order + '\'' +
                    ", close='" + close + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "AddOrderResponse{" +
                "result=" + result +
                ", error=" + error +
                '}';
    }
}
