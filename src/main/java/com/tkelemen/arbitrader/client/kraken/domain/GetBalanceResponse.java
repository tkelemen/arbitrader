package com.tkelemen.arbitrader.client.kraken.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetBalanceResponse {
    private List<String> error;

    private Map<String, BigDecimal> result;

    public List<String> getError() {
        return error;
    }

    public void setError(List<String> error) {
        this.error = error;
    }

    public Map<String, BigDecimal> getResult() {
        return result;
    }

    public void setResult(Map<String, BigDecimal> result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "GetBalanceResponse{" +
                "error=" + error +
                ", result=" + result +
                '}';
    }
}
