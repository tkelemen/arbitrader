package com.tkelemen.arbitrader.client.kraken.ws.message;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize()
public class Heartbeat extends KrakenEvent {
}
