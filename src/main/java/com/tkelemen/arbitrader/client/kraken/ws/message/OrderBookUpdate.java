package com.tkelemen.arbitrader.client.kraken.ws.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderBookUpdate extends OrderBookSnapshot {
    public OrderBookUpdate(Long channelId, OrderBookData orderBookData, String bookType, String symbol) {
        super(channelId, orderBookData, bookType, symbol);
    }

    @Override
    public String toString() {
        return "OrderBookUpdate{" +
                "channelId=" + channelId +
                ", orderBookData=" + orderBookData +
                ", bookType='" + bookType + '\'' +
                ", symbol='" + symbol + '\'' +
                '}';
    }
}
