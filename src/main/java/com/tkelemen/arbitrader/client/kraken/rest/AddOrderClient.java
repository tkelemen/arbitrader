package com.tkelemen.arbitrader.client.kraken.rest;

import com.tkelemen.arbitrader.client.kraken.domain.AddOrderResponse;
import com.tkelemen.arbitrader.client.kraken.domain.OrderDirection;
import com.tkelemen.arbitrader.client.kraken.domain.OrderType;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.exception.RestClientException;
import com.tkelemen.arbitrader.util.CurrencyUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AddOrderClient extends PrivateKrakenRestQuery {
    public AddOrderClient(ApplicationConfig config) {
        super(config);
    }

    public AddOrderResponse buy(OrderType orderType, String pair, BigDecimal volume, boolean volumeInQuote) throws RestClientException {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("ordertype", orderType.getValue());
        parameters.put("type", OrderDirection.BUY.getValue());
        parameters.put("pair", pair);
        parameters.put("volume", CurrencyUtils.formatCrypto(volume));

        List<String> flags = new ArrayList<>();
        if (volumeInQuote) {
            flags.add("viqc");
        }
        if (config.isKrakenDisableMarketPriceProtection()) {
            flags.add("nompp");
        }
        if (!flags.isEmpty()) {
            parameters.put("oflags", String.join(",", flags));
        }

        return query("/0/private/AddOrder", AddOrderResponse.class, parameters);
    }

    public AddOrderResponse sell(OrderType orderType, String pair, BigDecimal volume) throws RestClientException {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("ordertype", orderType.getValue());
        parameters.put("type", OrderDirection.SELL.getValue());
        parameters.put("pair", pair);
        parameters.put("volume", CurrencyUtils.formatCrypto(volume));

        return query("/0/private/AddOrder", AddOrderResponse.class, parameters);
    }
}
