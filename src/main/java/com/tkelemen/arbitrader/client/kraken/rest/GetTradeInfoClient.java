package com.tkelemen.arbitrader.client.kraken.rest;

import com.tkelemen.arbitrader.client.kraken.domain.GetTradeInfoResponse;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.exception.RestClientException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class GetTradeInfoClient extends PrivateKrakenRestQuery {
    public GetTradeInfoClient(ApplicationConfig config) {
        super(config);
    }

    public GetTradeInfoResponse get(String... txIds) throws RestClientException {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("txid", String.join(",", txIds));

        return query("/0/private/QueryTrades", GetTradeInfoResponse.class, parameters);
    }
}
