package com.tkelemen.arbitrader.client.kraken.rest;

import com.tkelemen.arbitrader.client.kraken.domain.GetBalanceResponse;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.exception.RestClientException;
import org.springframework.stereotype.Service;

@Service
public class KrakenGetBalanceClient extends PrivateKrakenRestQuery {
    public KrakenGetBalanceClient(ApplicationConfig config) {
        super(config);
    }

    public GetBalanceResponse get() throws RestClientException {
        return query("/0/private/Balance", GetBalanceResponse.class);
    }
}
