package com.tkelemen.arbitrader.client.kraken.ws.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Subscription implements Serializable {
    public static final String NAME_BOOK = "book";

    private Integer depth;

    private String name;

    private Integer interval;

    private boolean ratecounter;

    private String token;

    public Subscription() {
    }

    public Subscription(String name, Integer depth) {
        this.name = name;
        this.depth = depth;
    }

    public Integer getDepth() {
        return depth;
    }

    public void setDepth(Integer depth) {
        this.depth = depth;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    public boolean isRatecounter() {
        return ratecounter;
    }

    public void setRatecounter(boolean ratecounter) {
        this.ratecounter = ratecounter;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "Subscription{" +
                "depth=" + depth +
                ", name='" + name + '\'' +
                ", interval=" + interval +
                ", ratecounter=" + ratecounter +
                ", token='" + token + '\'' +
                '}';
    }
}
