package com.tkelemen.arbitrader.client.kraken.ws.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorEvent extends KrakenEvent {
    private String errorMessage;

    private String event;

    private String status;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ErrorEvent{" +
                "errorMessage='" + errorMessage + '\'' +
                ", event='" + event + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
