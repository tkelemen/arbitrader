package com.tkelemen.arbitrader.client.kraken.ws.message;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.math.BigInteger;

@JsonDeserialize
public class SystemStatus extends KrakenEvent {
    @JsonProperty("connectionID")
    private BigInteger connectionId;

    private String event;

    private String status;

    private String version;

    public BigInteger getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(BigInteger connectionId) {
        this.connectionId = connectionId;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "SystemStatus{" +
                "connectionId=" + connectionId +
                ", event='" + event + '\'' +
                ", status='" + status + '\'' +
                ", version='" + version + '\'' +
                '}';
    }
}
