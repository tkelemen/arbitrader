package com.tkelemen.arbitrader.client.kraken.domain;

public enum OrderDirection {
    BUY("buy"), SELL("sell");

    private String value;

    OrderDirection(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
