package com.tkelemen.arbitrader.client.kraken.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetTradeInfoResponse {
    private List<String> error;

    private Map<String, Trade> result;

    static class Trade {
        private String ordertxid;
        private String pair;
        private BigDecimal time;
        private String type;
        private String ordertype;
        private BigDecimal price;
        private BigDecimal cost;
        private BigDecimal fee;
        private BigDecimal vol;
        private BigDecimal margin;
        private String misc;

        public String getOrdertxid() {
            return ordertxid;
        }

        public void setOrdertxid(String ordertxid) {
            this.ordertxid = ordertxid;
        }

        public String getPair() {
            return pair;
        }

        public void setPair(String pair) {
            this.pair = pair;
        }

        public BigDecimal getTime() {
            return time;
        }

        public void setTime(BigDecimal time) {
            this.time = time;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getOrdertype() {
            return ordertype;
        }

        public void setOrdertype(String ordertype) {
            this.ordertype = ordertype;
        }

        public BigDecimal getPrice() {
            return price;
        }

        public void setPrice(BigDecimal price) {
            this.price = price;
        }

        public BigDecimal getCost() {
            return cost;
        }

        public void setCost(BigDecimal cost) {
            this.cost = cost;
        }

        public BigDecimal getFee() {
            return fee;
        }

        public void setFee(BigDecimal fee) {
            this.fee = fee;
        }

        public BigDecimal getVol() {
            return vol;
        }

        public void setVol(BigDecimal vol) {
            this.vol = vol;
        }

        public BigDecimal getMargin() {
            return margin;
        }

        public void setMargin(BigDecimal margin) {
            this.margin = margin;
        }

        public String getMisc() {
            return misc;
        }

        public void setMisc(String misc) {
            this.misc = misc;
        }

        @Override
        public String toString() {
            return "Trade{" +
                    "ordertxid='" + ordertxid + '\'' +
                    ", pair='" + pair + '\'' +
                    ", time=" + time +
                    ", type='" + type + '\'' +
                    ", ordertype='" + ordertype + '\'' +
                    ", price=" + price +
                    ", cost=" + cost +
                    ", fee=" + fee +
                    ", vol=" + vol +
                    ", margin=" + margin +
                    ", misc='" + misc + '\'' +
                    '}';
        }
    }

    public List<String> getError() {
        return error;
    }

    public void setError(List<String> error) {
        this.error = error;
    }

    public Map<String, Trade> getResult() {
        return result;
    }

    public void setResult(Map<String, Trade> result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "GetTradeInfoResponse{" +
                "error=" + error +
                ", result=" + result +
                '}';
    }
}
