package com.tkelemen.arbitrader.client.kraken.domain;

public class WebSocketsTokenResult {
    private String token;

    private Long expires;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getExpires() {
        return expires;
    }

    public void setExpires(Long expires) {
        this.expires = expires;
    }

    @Override
    public String toString() {
        return "WebSocketsTokenResult{" +
                "token='" + token + '\'' +
                ", expires=" + expires +
                '}';
    }
}
