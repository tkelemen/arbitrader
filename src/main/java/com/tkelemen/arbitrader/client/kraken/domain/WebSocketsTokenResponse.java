package com.tkelemen.arbitrader.client.kraken.domain;

import java.util.List;

public class WebSocketsTokenResponse {
    private List<String> error;

    private WebSocketsTokenResult result;

    public List<String> getError() {
        return error;
    }

    public void setError(List<String> error) {
        this.error = error;
    }

    public WebSocketsTokenResult getResult() {
        return result;
    }

    public void setResult(WebSocketsTokenResult result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "WebSocketsTokenResponse{" +
                "error='" + error + '\'' +
                ", result=" + result +
                '}';
    }
}
