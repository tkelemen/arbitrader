package com.tkelemen.arbitrader.exception;

import com.tkelemen.arbitrader.model.Exchange;

public class AddressNotFoundException extends Exception {
    public AddressNotFoundException(String message) {
        super(message);
    }
}
