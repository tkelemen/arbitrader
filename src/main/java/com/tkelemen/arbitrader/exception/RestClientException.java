package com.tkelemen.arbitrader.exception;

public class RestClientException extends Exception {
    public RestClientException(String message) {
        super(message);
    }

    public RestClientException(Throwable cause) {
        super(cause);
    }
}
