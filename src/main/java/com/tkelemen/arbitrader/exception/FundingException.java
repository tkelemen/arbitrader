package com.tkelemen.arbitrader.exception;

public class FundingException extends Exception {
    public FundingException(String message) {
        super(message);
    }
}
