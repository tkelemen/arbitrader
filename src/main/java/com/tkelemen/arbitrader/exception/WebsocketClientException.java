package com.tkelemen.arbitrader.exception;

public class WebsocketClientException extends Exception {
    public WebsocketClientException(String message) {
        super(message);
    }
}
