package com.tkelemen.arbitrader.exception;

public class ExchangeApiException extends Exception {
    public ExchangeApiException(String message) {
        super(message);
    }
}
