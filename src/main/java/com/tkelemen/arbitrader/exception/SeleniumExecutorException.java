package com.tkelemen.arbitrader.exception;

public class SeleniumExecutorException extends Exception {
    public SeleniumExecutorException(String message) {
        super(message);
    }
}
