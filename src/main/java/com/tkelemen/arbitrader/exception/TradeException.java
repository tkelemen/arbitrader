package com.tkelemen.arbitrader.exception;

public class TradeException extends Exception {
    public TradeException(String message) {
        super(message);
    }
}
