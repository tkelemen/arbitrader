package com.tkelemen.arbitrader.exception;

public class OrderBookNotSyncedException extends Exception {
    public OrderBookNotSyncedException() {
    }

    public OrderBookNotSyncedException(String message) {
        super(message);
    }
}
