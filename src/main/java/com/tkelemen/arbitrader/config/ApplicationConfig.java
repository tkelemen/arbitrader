package com.tkelemen.arbitrader.config;

import com.tkelemen.arbitrader.model.Exchange;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

@Component
public class ApplicationConfig {
    @Value("${marketWatchEnabled}")
    private boolean marketWatchEnabled;

    @Value("${predictionsEnabled}")
    private boolean predictionsEnabled;

    @Value("${tradingEnabled}")
    private boolean tradingEnabled;

    @Value("${notificationEnabled}")
    private boolean notificationEnabled;

    @Value("${withdrawEnabled}")
    private boolean withdrawEnabled;

    @Value("${emailProcessingEnabled}")
    private boolean emailProcessingEnabled;

    @Value("${makerTradingEnabled}")
    private boolean makerTradingEnabled;

    @Value("#{'${allowedExchanges}'.split(',')}")
    private List<Exchange> allowedExchanges;

    @Value("#{'${withdrawEnabledExchanges}'.split(',')}")
    private List<Exchange> withdrawEnabledExchanges;

    @Value("${kraken.apiKey}")
    private String krakenApiKey;

    @Value("${kraken.apiPrivateKey}")
    private String krakenApiPrivateKey;

    @Value("${kraken.apiRestBaseUrl}")
    private String krakenApiRestBaseUrl;

    @Value("${kraken.wsUrl}")
    private String krakenWebSocketUrl;

    @Value("${kraken.disableMarketPriceProtection}")
    private boolean krakenDisableMarketPriceProtection;

    @Value("${kraken.orderBookChecksumCalculationFrequency}")
    private int krakenOrderBookChecksumCalculationFrequency;

    @Value("${cex.apiRestBaseUrl}")
    private String cexApiRestBaseUrl;

    @Value("${cex.wsUrl}")
    private String cexWsUrl;

    @Value("${cex.apiKey}")
    private String cexApiKey;

    @Value("${cex.apiSecret}")
    private String cexApiSecret;

    @Value("${cex.apiUsername}")
    private String cexApiUsername;

    @Value("${cex.cookie.userId}")
    private String cexCookieUserId;

    @Value("${cex.cookie.sessionId}")
    private String cexCookieSessionId;

    @Value("${cex.cookie.prodSessionId}")
    private String cexCookieProdSessionId;

    @Value("${cex.2faSecret}")
    private String cex2FaSecret;

    @Value("${binance.apiKey}")
    private String binanceApiKey;

    @Value("${binance.secretKey}")
    private String binanceSecretKey;

    @Value("${kucoin.baseUrl}")
    private String kucoinBaseUrl;

    @Value("${kucoin.apiKey}")
    private String kucoinApiKey;

    @Value("${kucoin.secretKey}")
    private String kucoinSecretKey;

    @Value("${kucoin.passphrase}")
    private String kucoinPassphrase;

    @Value("${telegram.botToken}")
    private String telegramBotToken;

    @Value("${telegram.chatId}")
    private Long telegramChatId;

    @Value("${executeThreshold}")
    private BigDecimal executeThreshold;

    @Value("${minWatchThreshold}")
    private BigDecimal minWatchThreshold;

    @Value("${fundingThreshold}")
    private BigDecimal fundingThreshold;

    @Value("#{'${fundingQuotes}'.split(',')}")
    private Set<String> fundingQuotes;

    @Value("${fundingMaxAmount}")
    private BigDecimal fundingMaxAmount;

    @Value("${afterFundingMinGain}")
    private BigDecimal afterFundingMinGain;

    @Value("${makerWatchSpread}")
    private BigDecimal makerWatchSpread;

    @Value("${makerWatchGain}")
    private BigDecimal makerWatchGain;

    @Value("${makerCreateTriggerThreshold}")
    private BigDecimal makerCreateTriggerThreshold;

    @Value("${makerCancelTriggerThreshold}")
    private BigDecimal makerCancelTriggerThreshold;

    @Value("${cooldownPeriodOnLossMs}")
    private BigInteger cooldownPeriodOnLossMs;

    @Value("${mail.host}")
    private String mailHost;

    @Value("${mail.user}")
    private String mailUser;

    @Value("${mail.password}")
    private String mailPassword;

    @Value("${mail.protocol}")
    private String mailProtocol;

    @Value("${mail.port}")
    private int mailPort;

    @Value("${mail.folder.CEX}")
    private String mailFolderCex;

    @Value("${mail.folder.BINANCE}")
    private String mailFolderBinance;

    @Value("#{'${allowedPairs.CEX}'.split(',')}")
    private List<String> cexAllowedPairs;

    @Value("#{'${allowedPairs.BINANCE}'.split(',')}")
    private List<String> binanceAllowedPairs;

    @Value("#{'${allowedPairs.KRAKEN}'.split(',')}")
    private List<String> krakenAllowedPairs;

    @Value("#{'${allowedQuotesToTrade}'.split(',')}")
    private List<String> allowedQuotesToTrade;

    @Value("#{'${internalArbitrageAllowedExchanges}'.split(',')}")
    private Set<Exchange> internalArbitrageAllowedExchanges;

    @Value("#{'${internalArbitrageAllowedPairs}'.split(',')}")
    private Set<String> internalArbitrageAllowedPairs;

    @Value("#{'${allowedMakerTriggerExchanges}'.split(',')}")
    private Set<Exchange> allowedMakerTriggerExchanges;

    @Value("${cex.ws.auth.timestampCorrection}")
    private long cexWebSocketAuthTimestampCorrection;

    @Value("${cooldownAfterSuccessMs}")
    private long cooldownAfterSuccessMs;
    @Value("${cooldownAfterDesyncMs}")
    private long cooldownAfterDesyncMs;
    @Value("${cooldownAfterFailureMs}")
    private long cooldownAfterFailureMs;
    @Value("${highestAbsoluteProfitMode}")
    private boolean highestAbsoluteProfitMode;
    @Value("${scheduler.executeMakerTradesRateMs}")
    private long executeMakerTradesRateMs;
    @Value("${quoteFundsReservedForTakerTrades}")
    private BigDecimal quoteFundsReservedForTakerTrades;

    // TODO: move to config file
    private Map<String, NavigableSet<BigDecimal>> allowedQuoteVolumes;

    public ApplicationConfig() {
        allowedQuoteVolumes = new HashMap<>();
        NavigableSet<BigDecimal> fiatVolumes = new TreeSet<>(Arrays.asList(new BigDecimal(100), new BigDecimal(200), new BigDecimal(300), new BigDecimal(500), new BigDecimal(1000), new BigDecimal(2000), new BigDecimal(3000), new BigDecimal(5000)));

        allowedQuoteVolumes.put("USD", fiatVolumes);
        allowedQuoteVolumes.put("EUR", fiatVolumes);
        allowedQuoteVolumes.put("GBP", fiatVolumes);
        allowedQuoteVolumes.put("USDT", fiatVolumes);
        allowedQuoteVolumes.put("BUSD", fiatVolumes);
        allowedQuoteVolumes.put("USDC", fiatVolumes);
        allowedQuoteVolumes.put("AUD", fiatVolumes);

        allowedQuoteVolumes.put("BTC", new TreeSet<>(Arrays.asList(new BigDecimal("0.002"), new BigDecimal("0.004"), new BigDecimal("0.008"), new BigDecimal("0.015"), new BigDecimal("0.03"), new BigDecimal("0.05"))));
        allowedQuoteVolumes.put("ETH", new TreeSet<>(Arrays.asList(new BigDecimal("0.05"), new BigDecimal("0.1"), new BigDecimal("0.3"), new BigDecimal("0.6"), new BigDecimal("1"), new BigDecimal("2"))));
    }

    public boolean isKrakenDisableMarketPriceProtection() {
        return krakenDisableMarketPriceProtection;
    }

    public List<Exchange> getAllowedExchanges() {
        return allowedExchanges;
    }
    public List<Exchange> getWithdrawEnabledExchanges() {
        return withdrawEnabledExchanges;
    }

    public String getKrakenApiKey() {
        return krakenApiKey;
    }

    public String getKrakenApiPrivateKey() {
        return krakenApiPrivateKey;
    }

    public String getKrakenApiRestBaseUrl() {
        return krakenApiRestBaseUrl;
    }

    public String getKrakenWebSocketUrl() {
        return krakenWebSocketUrl;
    }

    public String getCexWebSocketUrl() {
        return cexWsUrl;
    }

    public String getCexApiKey() {
        return cexApiKey;
    }

    public String getCexApiSecret() {
        return cexApiSecret;
    }

    public String getCexApiRestBaseUrl() {
        return cexApiRestBaseUrl;
    }

    public String getCexApiUsername() {
        return cexApiUsername;
    }

    public String getCexCookieUserId() {
        return cexCookieUserId;
    }

    public String getCexCookieProdSessionId() {
        return cexCookieProdSessionId;
    }

    public String getCex2FaSecret() {
        return cex2FaSecret;
    }

    public String getBinanceApiKey() {
        return binanceApiKey;
    }

    public String getBinanceSecretKey() {
        return binanceSecretKey;
    }

    public String getKucoinBaseUrl() {
        return kucoinBaseUrl;
    }

    public String getKucoinApiKey() {
        return kucoinApiKey;
    }

    public String getKucoinSecretKey() {
        return kucoinSecretKey;
    }

    public String getKucoinPassphrase() {
        return kucoinPassphrase;
    }

    public String getTelegramBotToken() {
        return telegramBotToken;
    }

    public BigDecimal getMinWatchThreshold() {
        return minWatchThreshold;
    }

    public BigDecimal getMakerWatchSpread() {
        return makerWatchSpread;
    }

    public BigDecimal getMakerWatchGain() {
        return makerWatchGain;
    }

    public BigDecimal getMakerCreateTriggerThreshold() {
        return makerCreateTriggerThreshold;
    }

    public BigDecimal getMakerCancelTriggerThreshold() {
        return makerCancelTriggerThreshold;
    }

    public void setCexApiRestBaseUrl(String cexApiRestBaseUrl) {
        this.cexApiRestBaseUrl = cexApiRestBaseUrl;
    }

    public void setCexWsUrl(String cexWsUrl) {
        this.cexWsUrl = cexWsUrl;
    }

    public void setCexApiKey(String cexApiKey) {
        this.cexApiKey = cexApiKey;
    }

    public void setCexApiSecret(String cexApiSecret) {
        this.cexApiSecret = cexApiSecret;
    }

    public void setCexApiUsername(String cexApiUsername) {
        this.cexApiUsername = cexApiUsername;
    }

    public void setKrakenApiKey(String krakenApiKey) {
        this.krakenApiKey = krakenApiKey;
    }

    public void setKrakenApiPrivateKey(String krakenApiPrivateKey) {
        this.krakenApiPrivateKey = krakenApiPrivateKey;
    }

    public void setKrakenApiRestBaseUrl(String krakenApiRestBaseUrl) {
        this.krakenApiRestBaseUrl = krakenApiRestBaseUrl;
    }

    public void setKrakenWebSocketUrl(String krakenWebSocketUrl) {
        this.krakenWebSocketUrl = krakenWebSocketUrl;
    }

    public String getMailHost() {
        return mailHost;
    }

    public String getMailUser() {
        return mailUser;
    }

    public String getMailPassword() {
        return mailPassword;
    }

    public String getMailProtocol() {
        return mailProtocol;
    }

    public int getMailPort() {
        return mailPort;
    }

    public String getMailFolderCex() {
        return mailFolderCex;
    }

    public String getMailFolderBinance() {
        return mailFolderBinance;
    }

    public boolean isTradingEnabled() {
        return tradingEnabled;
    }

    public boolean isPredictionsEnabled() {
        return predictionsEnabled;
    }

    public boolean isNotificationEnabled() {
        return notificationEnabled;
    }

    public boolean isWithdrawEnabled() {
        return withdrawEnabled;
    }

    public boolean isEmailProcessingEnabled() {
        return emailProcessingEnabled;
    }

    public boolean isMakerTradingEnabled() {
        return makerTradingEnabled;
    }

    public String getCexCookieSessionId() {
        return cexCookieSessionId;
    }

    public List<String> getCexAllowedPairs() {
        return cexAllowedPairs;
    }

    public List<String> getBinanceAllowedPairs() {
        return binanceAllowedPairs;
    }

    public List<String> getKrakenAllowedPairs() {
        return krakenAllowedPairs;
    }

    public BigDecimal getFundingThreshold() {
        return fundingThreshold;
    }

    public BigDecimal getAfterFundingMinGain() {
        return afterFundingMinGain;
    }

    public boolean isMarketWatchEnabled() {
        return marketWatchEnabled;
    }

    public List<String> getAllowedQuotesToTrade() {
        return allowedQuotesToTrade;
    }

    public Map<String, NavigableSet<BigDecimal>> getAllowedQuoteVolumes() {
        return allowedQuoteVolumes;
    }

    public int getKrakenOrderBookChecksumCalculationFrequency() {
        return krakenOrderBookChecksumCalculationFrequency;
    }

    public Set<String> getFundingQuotes() {
        return fundingQuotes;
    }

    public BigDecimal getFundingMaxAmount() {
        return fundingMaxAmount;
    }

    public Set<Exchange> getInternalArbitrageAllowedExchanges() {
        return internalArbitrageAllowedExchanges;
    }

    public Set<String> getInternalArbitrageAllowedPairs() {
        return internalArbitrageAllowedPairs;
    }

    public Set<Exchange> getAllowedMakerTriggerExchanges() {
        return allowedMakerTriggerExchanges;
    }

    public Long getTelegramChatId() {
        return telegramChatId;
    }

    public long getCexWebSocketAuthTimestampCorrection() {
        return cexWebSocketAuthTimestampCorrection;
    }

    public long getCooldownAfterSuccessMs() {
        return cooldownAfterSuccessMs;
    }

    public long getCooldownAfterDesyncMs() {
        return cooldownAfterDesyncMs;
    }

    public long getCooldownAfterFailureMs() {
        return cooldownAfterFailureMs;
    }

    public void setMakerTradingEnabled(boolean makerTradingEnabled) {
        this.makerTradingEnabled = makerTradingEnabled;
    }

    public boolean isHighestAbsoluteProfitMode() {
        return highestAbsoluteProfitMode;
    }

    public void setHighestAbsoluteProfitMode(boolean highestAbsoluteProfitMode) {
        this.highestAbsoluteProfitMode = highestAbsoluteProfitMode;
    }

    public long getExecuteMakerTradesRateMs() {
        return executeMakerTradesRateMs;
    }

    public BigDecimal getExecuteThreshold() {
        return executeThreshold;
    }

    public BigDecimal getQuoteFundsReservedForTakerTrades() {
        return quoteFundsReservedForTakerTrades;
    }

    public void setQuoteFundsReservedForTakerTrades(BigDecimal quoteFundsReservedForTakerTrades) {
        this.quoteFundsReservedForTakerTrades = quoteFundsReservedForTakerTrades;
    }

    @Override
    public String toString() {
        return "ApplicationConfig{" +
                "marketWatchEnabled=" + marketWatchEnabled +
                ", predictionsEnabled=" + predictionsEnabled +
                ", tradingEnabled=" + tradingEnabled +
                ", notificationEnabled=" + notificationEnabled +
                ", withdrawEnabled=" + withdrawEnabled +
                ", emailProcessingEnabled=" + emailProcessingEnabled +
                ", makerTradingEnabled=" + makerTradingEnabled +
                ", allowedExchanges=" + allowedExchanges +
                ", withdrawEnabledExchanges=" + withdrawEnabledExchanges +
                ", krakenApiRestBaseUrl='" + krakenApiRestBaseUrl + '\'' +
                ", krakenWebSocketUrl='" + krakenWebSocketUrl + '\'' +
                ", krakenDisableMarketPriceProtection=" + krakenDisableMarketPriceProtection +
                ", krakenOrderBookChecksumCalculationFrequency=" + krakenOrderBookChecksumCalculationFrequency +
                ", cexApiRestBaseUrl='" + cexApiRestBaseUrl + '\'' +
                ", cexWsUrl='" + cexWsUrl + '\'' +
                ", cexApiUsername='" + cexApiUsername + '\'' +
                ", cexCookieUserId='" + cexCookieUserId + '\'' +
                ", cexCookieSessionId='" + cexCookieSessionId + '\'' +
                ", cexCookieProdSessionId='" + cexCookieProdSessionId + '\'' +
                ", kucoinBaseUrl='" + kucoinBaseUrl + '\'' +
                ", telegramBotToken='" + telegramBotToken + '\'' +
                ", minWatchThreshold=" + minWatchThreshold +
                ", fundingThreshold=" + fundingThreshold +
                ", fundingQuotes=" + fundingQuotes +
                ", fundingMaxAmount=" + fundingMaxAmount +
                ", afterFundingMinGain=" + afterFundingMinGain +
                ", mailHost='" + mailHost + '\'' +
                ", mailUser='" + mailUser + '\'' +
                ", mailProtocol='" + mailProtocol + '\'' +
                ", mailPort=" + mailPort +
                ", mailFolderCex='" + mailFolderCex + '\'' +
                ", mailFolderBinance='" + mailFolderBinance + '\'' +
                ", cexAllowedPairs=" + cexAllowedPairs +
                ", binanceAllowedPairs=" + binanceAllowedPairs +
                ", krakenAllowedPairs=" + krakenAllowedPairs +
                ", allowedQuotesToTrade=" + allowedQuotesToTrade +
                ", allowedQuoteVolumes=" + allowedQuoteVolumes +
                ", internalArbitrageAllowedExchanges=" + internalArbitrageAllowedExchanges +
                ", allowedMakerTriggerExchanges=" + allowedMakerTriggerExchanges +
                '}';
    }
}
