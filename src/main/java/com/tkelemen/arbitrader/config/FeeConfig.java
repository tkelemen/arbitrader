package com.tkelemen.arbitrader.config;

import com.tkelemen.arbitrader.model.Exchange;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Map;

@Component
@ConfigurationProperties(prefix = "fee")
public class FeeConfig {
    private Map<Exchange, BigDecimal> tradingMaker;
    private Map<Exchange, BigDecimal> tradingTaker;
    private Map<Exchange, Map<String, BigDecimal>> withdraw;
    private Map<Exchange, Map<String, BigDecimal>> deposit;

    public void setTradingMaker(Map<Exchange, BigDecimal> tradingMaker) {
        this.tradingMaker = tradingMaker;
    }

    public void setTradingTaker(Map<Exchange, BigDecimal> tradingTaker) {
        this.tradingTaker = tradingTaker;
    }

    public void setWithdraw(Map<Exchange, Map<String, BigDecimal>> withdraw) {
        this.withdraw = withdraw;
    }

    public void setDeposit(Map<Exchange, Map<String, BigDecimal>> deposit) {
        this.deposit = deposit;
    }

    public Map<Exchange, BigDecimal> getTradingMaker() {
        return tradingMaker;
    }

    public Map<Exchange, BigDecimal> getTradingTaker() {
        return tradingTaker;
    }
    public Map<Exchange, Map<String, BigDecimal>> getWithdraw() {
        return withdraw;
    }

    public Map<Exchange, Map<String, BigDecimal>> getDeposit() {
        return deposit;
    }
}
