package com.tkelemen.arbitrader.config;

import com.tkelemen.arbitrader.model.Address;
import com.tkelemen.arbitrader.model.Exchange;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@ConfigurationProperties(ignoreInvalidFields = true)
public class WalletConfig {
    private Map<Exchange, Map<String, Address>> wallet;

    public Map<Exchange, Map<String, Address>> getWallet() {
        return wallet;
    }

    public Map<String, Address> getWallet(Exchange exchange) {
        return wallet.get(exchange);
    }

    public void setWallet(Map<Exchange, Map<String, Address>> wallet) {
        this.wallet = wallet;
    }
}
