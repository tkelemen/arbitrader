package com.tkelemen.arbitrader.model;

import java.math.BigDecimal;
import java.util.NavigableMap;

public interface OrderBook {
    void recordUpdate(Object update);

    void setAskLevel(String price, BigDecimal quantity);

    void setBidLevel(String price, BigDecimal quantity);

    BigDecimal removeAskLevel(String price);

    BigDecimal removeBidLevel(String price);

    NavigableMap<String, BigDecimal> getAskLevels();

    NavigableMap<String, BigDecimal> getBidLevels();

    BigDecimal getAskLevelQuantity(String level);

    BigDecimal getBidLevelQuantity(String level);

    NavigableMap<String, Long> getAskLevelTimes();

    NavigableMap<String, Long> getBidLevelTimes();

    Long getAskLevelTime(String level);

    Long getBidLevelTime(String level);

    long getLastUpdatedTimestamp();

    void setLastUpdatedTimestamp(long lastUpdatedTimestamp);

    Exchange getExchange();

    void setExchange(Exchange exchange);

    String getSymbol();

    void reset();
}
