package com.tkelemen.arbitrader.model;

public enum Currency {
    USD,
    EUR,
    GBP,
    AUD,
    USDT,
    BUSD,
    BTC,
    ETH,
    BCH,
    XRP,
    XLM
}
