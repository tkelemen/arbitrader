package com.tkelemen.arbitrader.model.enums;

public enum OrderType {
    BUY, SELL
}