package com.tkelemen.arbitrader.model;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.util.Date;

public class MakerOpportunityDTO {
    private long id;

    private Date datetime;

    private long iterationId;

    private MakerOpportunityType type;

    private Exchange triggerExchange;

    private Exchange partnerExchange;

    private String fromQuote;

    private String base;

    private String toQuote;

    private BigDecimal triggerPrice;

    private BigDecimal fromQuoteAmount;

    private BigDecimal baseAmount;

    private BigDecimal toQuoteAmount;

    private BigDecimal triggerHighestBid;

    private BigDecimal triggerLowestAsk;

    private BigDecimal spread;

    private BigDecimal partnerBestPrice;

    private BigDecimal liquidityScore;

    private BigDecimal buyFee;

    private BigDecimal sellFee;

    private BigDecimal withdrawFee;

    private BigDecimal depositFee;

    private Boolean hasInsufficientQuoteFunds;

    private Boolean hasInsufficientBaseFunds;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public long getIterationId() {
        return iterationId;
    }

    public void setIterationId(long iterationId) {
        this.iterationId = iterationId;
    }

    public MakerOpportunityType getType() {
        return type;
    }

    public void setType(MakerOpportunityType type) {
        this.type = type;
    }

    public Exchange getTriggerExchange() {
        return triggerExchange;
    }

    public void setTriggerExchange(Exchange triggerExchange) {
        this.triggerExchange = triggerExchange;
    }

    public Exchange getPartnerExchange() {
        return partnerExchange;
    }

    public void setPartnerExchange(Exchange partnerExchange) {
        this.partnerExchange = partnerExchange;
    }

    public String getFromQuote() {
        return fromQuote;
    }

    public void setFromQuote(String fromQuote) {
        this.fromQuote = fromQuote;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getToQuote() {
        return toQuote;
    }

    public void setToQuote(String toQuote) {
        this.toQuote = toQuote;
    }

    public BigDecimal getTriggerPrice() {
        return triggerPrice;
    }

    public void setTriggerPrice(BigDecimal triggerPrice) {
        this.triggerPrice = triggerPrice;
    }

    public BigDecimal getFromQuoteAmount() {
        return fromQuoteAmount;
    }

    public void setFromQuoteAmount(BigDecimal fromQuoteAmount) {
        this.fromQuoteAmount = fromQuoteAmount;
    }

    public BigDecimal getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(BigDecimal baseAmount) {
        this.baseAmount = baseAmount;
    }

    public BigDecimal getToQuoteAmount() {
        return toQuoteAmount;
    }

    public void setToQuoteAmount(BigDecimal toQuoteAmount) {
        this.toQuoteAmount = toQuoteAmount;
    }

    public BigDecimal getBuyFee() {
        return buyFee;
    }

    public void setBuyFee(BigDecimal buyFee) {
        this.buyFee = buyFee;
    }

    public BigDecimal getSellFee() {
        return sellFee;
    }

    public void setSellFee(BigDecimal sellFee) {
        this.sellFee = sellFee;
    }

    public BigDecimal getWithdrawFee() {
        return withdrawFee;
    }

    public void setWithdrawFee(BigDecimal withdrawFee) {
        this.withdrawFee = withdrawFee;
    }

    public BigDecimal getDepositFee() {
        return depositFee;
    }

    public void setDepositFee(BigDecimal depositFee) {
        this.depositFee = depositFee;
    }

    public Boolean getHasInsufficientQuoteFunds() {
        return hasInsufficientQuoteFunds;
    }

    public void setHasInsufficientQuoteFunds(Boolean hasInsufficientQuoteFunds) {
        this.hasInsufficientQuoteFunds = hasInsufficientQuoteFunds;
    }

    public Boolean getHasInsufficientBaseFunds() {
        return hasInsufficientBaseFunds;
    }

    public BigDecimal getTriggerHighestBid() {
        return triggerHighestBid;
    }

    public void setTriggerHighestBid(BigDecimal triggerHighestBid) {
        this.triggerHighestBid = triggerHighestBid;
    }

    public BigDecimal getTriggerLowestAsk() {
        return triggerLowestAsk;
    }

    public void setTriggerLowestAsk(BigDecimal triggerLowestAsk) {
        this.triggerLowestAsk = triggerLowestAsk;
    }

    public BigDecimal getSpread() {
        return spread;
    }

    public void setSpread(BigDecimal spread) {
        this.spread = spread;
    }

    public BigDecimal getPartnerBestPrice() {
        return partnerBestPrice;
    }

    public void setPartnerBestPrice(BigDecimal partnerBestPrice) {
        this.partnerBestPrice = partnerBestPrice;
    }

    public BigDecimal getLiquidityScore() {
        return liquidityScore;
    }

    public void setLiquidityScore(BigDecimal liquidityScore) {
        this.liquidityScore = liquidityScore;
    }

    @Override
    public String toString() {
        return "MakerOpportunityDTO{" +
                "id=" + id +
                ", datetime=" + datetime +
                ", iterationId=" + iterationId +
                ", type=" + type +
                ", triggerExchange=" + triggerExchange +
                ", partnerExchange=" + partnerExchange +
                ", fromQuote='" + fromQuote + '\'' +
                ", base='" + base + '\'' +
                ", toQuote='" + toQuote + '\'' +
                ", triggerPrice=" + triggerPrice +
                ", fromQuoteAmount=" + fromQuoteAmount +
                ", baseAmount=" + baseAmount +
                ", toQuoteAmount=" + toQuoteAmount +
                ", buyFee=" + buyFee +
                ", sellFee=" + sellFee +
                ", withdrawFee=" + withdrawFee +
                ", depositFee=" + depositFee +
                ", hasInsufficientQuoteFunds=" + hasInsufficientQuoteFunds +
                ", hasInsufficientBaseFunds=" + hasInsufficientBaseFunds +
                '}';
    }

    public void setHasInsufficientBaseFunds(Boolean hasInsufficientBaseFunds) {
        this.hasInsufficientBaseFunds = hasInsufficientBaseFunds;
    }
}
