package com.tkelemen.arbitrader.model;

public enum SeleniumResult {
    SUCCESS, FAILURE
}
