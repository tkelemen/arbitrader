package com.tkelemen.arbitrader.model;

public class Pair {
    private Currency base;
    private Currency quote;

    public Pair(Currency base, Currency quote) {
        this.base = base;
        this.quote = quote;
    }

    public Currency getBase() {
        return base;
    }

    public Currency getQuote() {
        return quote;
    }
}
