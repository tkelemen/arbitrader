package com.tkelemen.arbitrader.model;

public enum MakerOpportunityType {
    SH, BL
}
