package com.tkelemen.arbitrader.model;

import java.math.BigDecimal;

public class Conversion {
    private Exchange fromExchange;
    private Exchange toExchange;
    private String fromAsset;
    private String toAsset;
    private String conversionAsset;
    private BigDecimal fromAmount;
    private BigDecimal toAmount;

    public Exchange getFromExchange() {
        return fromExchange;
    }

    public Conversion setFromExchange(Exchange fromExchange) {
        this.fromExchange = fromExchange;
        return this;
    }

    public Exchange getToExchange() {
        return toExchange;
    }

    public Conversion setToExchange(Exchange toExchange) {
        this.toExchange = toExchange;
        return this;
    }

    public String getFromAsset() {
        return fromAsset;
    }

    public Conversion setFromAsset(String fromAsset) {
        this.fromAsset = fromAsset;
        return this;
    }

    public String getToAsset() {
        return toAsset;
    }

    public Conversion setToAsset(String toAsset) {
        this.toAsset = toAsset;
        return this;
    }

    public BigDecimal getFromAmount() {
        return fromAmount;
    }

    public Conversion setFromAmount(BigDecimal fromAmount) {
        this.fromAmount = fromAmount;
        return this;
    }

    public BigDecimal getToAmount() {
        return toAmount;
    }

    public Conversion setToAmount(BigDecimal toAmount) {
        this.toAmount = toAmount;
        return this;
    }

    public String getConversionAsset() {
        return conversionAsset;
    }

    public Conversion setConversionAsset(String conversionAsset) {
        this.conversionAsset = conversionAsset;
        return this;
    }

    @Override
    public String toString() {
        return "Conversion{" +
                "fromExchange=" + fromExchange +
                ", toExchange=" + toExchange +
                ", fromAsset='" + fromAsset + '\'' +
                ", toAsset='" + toAsset + '\'' +
                ", conversionAsset='" + conversionAsset + '\'' +
                ", fromAmount=" + fromAmount +
                ", toAmount=" + toAmount +
                '}';
    }
}
