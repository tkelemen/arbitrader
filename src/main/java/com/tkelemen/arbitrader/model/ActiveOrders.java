package com.tkelemen.arbitrader.model;

import com.tkelemen.arbitrader.model.enums.OrderType;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ActiveOrders {
    private final Map<Exchange, Map<String, List<Order>>> activeOrders = new ConcurrentHashMap<>();

    public synchronized void add(Exchange exchange, OrderType orderType, String pair, BigDecimal price, BigDecimal amount) {

        final Order order = Order.create(pair, orderType, amount, price);
        final String key = getOrderKey(order);

        final Map<String, List<Order>> exchangeOrders = activeOrders.computeIfAbsent(exchange, a -> new ConcurrentHashMap<>());
        final List<Order> pairAndTypeOrderList = exchangeOrders.computeIfAbsent(key, a -> new ArrayList<>());

        boolean updatedAmount = false;
        for (Order existingOrder : pairAndTypeOrderList) {
            if (existingOrder.getPrice().compareTo(price) == 0) {
                existingOrder.setAmount(existingOrder.getAmount().add(amount));
                updatedAmount = true;
                break;
            }
        }
        if(!updatedAmount) {
            pairAndTypeOrderList.add(order);
        }
    }

    public synchronized List<Order> get(Exchange exchange, String pair, OrderType orderType) {
        final Map<String, List<Order>> exchangeOrders = activeOrders.get(exchange);
        if(exchangeOrders == null) {
            return Collections.emptyList();
        }

        final List<Order> pairAndTypeOrderList = exchangeOrders.get(getOrderKey(pair, orderType));
        return Objects.requireNonNullElse(pairAndTypeOrderList, Collections.emptyList());
    }

    public synchronized boolean has(Exchange exchange, String pair, OrderType orderType) {
        return has(exchange, pair, orderType, null, null);
    }

    public synchronized boolean has(Exchange exchange, String pair, OrderType orderType, BigDecimal price) {
        return has(exchange, pair, orderType, price, null);
    }

    public synchronized boolean has(Exchange exchange, String pair, OrderType orderType, BigDecimal price, BigDecimal amount) {
        final Map<String, List<Order>> exchangeOrders = activeOrders.get(exchange);
        if(exchangeOrders == null) {
            return false;
        }

        final List<Order> pairAndTypeOrderList = exchangeOrders.get(getOrderKey(pair, orderType));
        if(pairAndTypeOrderList == null || pairAndTypeOrderList.isEmpty()) {
            return false;
        }

        if(amount == null && price == null) {
            return true;
        }

        for (Order order : pairAndTypeOrderList) {
            if ((price == null || order.getPrice().compareTo(price) == 0) && (amount == null || order.getAmount().compareTo(amount) == 0)) {
                return true;
            }
        }

        return false;
    }

    public synchronized void remove(Exchange exchange, String pair, OrderType orderType, BigDecimal price, BigDecimal amount) {
        final Map<String, List<Order>> exchangeOrders = activeOrders.get(exchange);

        if(exchangeOrders == null) {
            throw new IllegalArgumentException("Could not find any orders on exchange " + exchange + " [ remove request ]");
        }

        final List<Order> pairAndTypeOrderList = exchangeOrders.get(getOrderKey(pair, orderType));
        if(pairAndTypeOrderList == null) {
            throw new IllegalArgumentException("Could not find any " + orderType + " " + pair + " orders on exchange " + exchange + " [ remove request ]");
        }

        for (int i = 0; i < pairAndTypeOrderList.size(); i++) {
            final Order order = pairAndTypeOrderList.get(i);
            if(order.getPrice().compareTo(price) == 0) {
                if(order.getAmount().compareTo(amount) > 0) {
                    order.setAmount(order.getAmount().subtract(amount));
                } else if (order.getAmount().compareTo(amount) == 0) {
                    pairAndTypeOrderList.remove(i);
                } else {
                    throw new IllegalArgumentException("Could not remove higher amount than currently in orders [ "
                            + orderType + " " + pair + ", on exchange " + exchange + ", tried to remove "  + amount +
                            ", actual " + order.getAmount());
                }
            }
        }
    }

    public synchronized BigDecimal getTotalAmount(Exchange exchange, String pair, OrderType orderType) {
        List<Order> pairAndTypeOrderList = get(exchange, pair, orderType);
        BigDecimal total = BigDecimal.ZERO;
        for (Order order : pairAndTypeOrderList) {
            total = total.add(order.getAmount());
        }

        return total;
    }

    public synchronized BigDecimal getAmountForPrice(Exchange exchange, String pair, OrderType orderType, BigDecimal price) {
        List<Order> pairAndTypeOrderList = get(exchange, pair, orderType);
        BigDecimal total = BigDecimal.ZERO;
        for (Order order : pairAndTypeOrderList) {
            if(order.getPrice().equals(price)) {
                total = total.add(order.getAmount());
            }
        }

        return total;
    }

    public Map<Exchange, Map<String, List<Order>>> getActiveOrders() {
        return activeOrders;
    }

    private static String getOrderKey(String pair, OrderType orderType) {
        return pair + "-" + orderType;
    }

    private static String getOrderKey(Order order) {
        return getOrderKey(order.getPair(), order.getOrderType());
    }

    private static class Order {
        private BigDecimal amount;
        private BigDecimal price;
        private String pair;
        private OrderType orderType;

        public Order(String pair, OrderType orderType, BigDecimal amount, BigDecimal price) {
            this.amount = amount;
            this.price = price;
            this.pair = pair;
            this.orderType = orderType;
        }

        public static Order create(String pair, OrderType orderType, BigDecimal amount, BigDecimal price) {
            return new Order(pair, orderType, amount, price);
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public void setAmount(BigDecimal amount) {
            this.amount = amount;
        }

        public BigDecimal getPrice() {
            return price;
        }

        public void setPrice(BigDecimal price) {
            this.price = price;
        }

        public String getPair() {
            return pair;
        }

        public void setPair(String pair) {
            this.pair = pair;
        }

        public OrderType getOrderType() {
            return orderType;
        }

        public void setOrderType(OrderType orderType) {
            this.orderType = orderType;
        }

        @Override
        public String toString() {
            return "Order{" +
                    "amount=" + amount +
                    ", price=" + price +
                    ", pair='" + pair + '\'' +
                    ", orderType=" + orderType +
                    '}';
        }
    }
}
