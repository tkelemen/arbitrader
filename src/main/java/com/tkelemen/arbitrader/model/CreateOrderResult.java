package com.tkelemen.arbitrader.model;

import com.tkelemen.arbitrader.model.enums.OrderType;

import java.math.BigDecimal;

public class CreateOrderResult {
    private String pair;
    private String orderId;
    private BigDecimal price = BigDecimal.ZERO;
    private String error;
    private OrderType type;
    private BigDecimal amount = BigDecimal.ZERO;

    public CreateOrderResult(String pair, String orderId, BigDecimal price, BigDecimal amount) {
        this.pair = pair;
        this.orderId = orderId;
        this.price = price;
        this.amount = amount;
    }

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public OrderType getType() {
        return type;
    }

    public void setType(OrderType type) {
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "CreateOrderResult{" +
                "pair='" + pair + '\'' +
                ", orderId='" + orderId + '\'' +
                ", price=" + price +
                ", error='" + error + '\'' +
                ", type=" + type +
                ", amount=" + amount +
                '}';
    }
}
