package com.tkelemen.arbitrader.model;

public enum WithdrawStage {
    PENDING,
    REQUESTED,
    CONFIRMED,
    RECEIVED,
    ERROR
}
