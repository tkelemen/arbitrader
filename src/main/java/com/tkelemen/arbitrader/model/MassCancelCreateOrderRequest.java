package com.tkelemen.arbitrader.model;

import com.tkelemen.arbitrader.model.enums.OrderType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MassCancelCreateOrderRequest {
    private List<String> ordersToCancelIdList = new ArrayList<>();
    private List<CreateOrder> ordersToCreateList = new ArrayList<>();

    public MassCancelCreateOrderRequest addOrderToCancel(String orderId) {
        ordersToCancelIdList.add(orderId);

        return this;
    }

    public MassCancelCreateOrderRequest addOrderToCreate(OrderType type, String pair, BigDecimal price, BigDecimal amount) {
        ordersToCreateList.add(new CreateOrder(type, pair, price, amount));

        return this;
    }

    public List<String> getOrdersToCancelIdList() {
        return ordersToCancelIdList;
    }

    public void setOrdersToCancelIdList(List<String> ordersToCancelIdList) {
        this.ordersToCancelIdList = ordersToCancelIdList;
    }

    public List<CreateOrder> getOrdersToCreateList() {
        return ordersToCreateList;
    }

    public void setOrdersToCreateList(List<CreateOrder> ordersToCreateList) {
        this.ordersToCreateList = ordersToCreateList;
    }

    @Override
    public String toString() {
        return "MassCancelCreateOrderRequest{" +
                "ordersToCancelIdList=" + ordersToCancelIdList +
                ", ordersToCreateList=" + ordersToCreateList +
                '}';
    }
}
