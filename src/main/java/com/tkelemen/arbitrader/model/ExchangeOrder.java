package com.tkelemen.arbitrader.model;

import com.tkelemen.arbitrader.model.enums.OrderType;

import java.math.BigDecimal;

public class ExchangeOrder {
    private String id;
    private OrderType type;
    private BigDecimal price;
    private BigDecimal amount;
    private BigDecimal executedAmount;
    private String base;
    private String quote;
    private Status status;
    private BigDecimal executedFee;
    private String executedFeeCurrency;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public OrderType getType() {
        return type;
    }

    public void setType(OrderType type) {
        this.type = type;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getExecutedAmount() {
        return executedAmount;
    }

    public void setExecutedAmount(BigDecimal executedAmount) {
        this.executedAmount = executedAmount;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public BigDecimal getExecutedFee() {
        return executedFee;
    }

    public void setExecutedFee(BigDecimal executedFee) {
        this.executedFee = executedFee;
    }

    public String getExecutedFeeCurrency() {
        return executedFeeCurrency;
    }

    public void setExecutedFeeCurrency(String executedFeeCurrency) {
        this.executedFeeCurrency = executedFeeCurrency;
    }

    @Override
    public String toString() {
        return "ExchangeOrder{" +
                "id='" + id + '\'' +
                ", type=" + type +
                ", price=" + price +
                ", amount=" + amount +
                ", executedAmount=" + executedAmount +
                ", base='" + base + '\'' +
                ", quote='" + quote + '\'' +
                ", status=" + status +
                ", executedFee=" + executedFee +
                ", executedFeeCurrency='" + executedFeeCurrency + '\'' +
                '}';
    }

    public enum Status {
        CREATED, DONE, CANCELLED, PARTIALLY_EXECUTED, PARTIALLY_EXECUTED_CANCELLED
    }
}
