package com.tkelemen.arbitrader.model;

import com.tkelemen.arbitrader.db.entity.Withdraw;

public class WithdrawResult extends ProcessResult {
    private WithdrawStage stage;
    private String message;
    private boolean success;
    private String withdrawId;
    private Withdraw withdraw;

    public WithdrawResult(WithdrawStage stage) {
        this.stage = stage;
    }

    public WithdrawStage getStage() {
        return stage;
    }

    public WithdrawResult setStage(WithdrawStage stage) {
        this.stage = stage;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public WithdrawResult setMessage(String message) {
        this.message = message;
        return this;
    }

    public boolean isSuccess() {
        return success;
    }

    public WithdrawResult setSuccess(boolean success) {
        this.success = success;
        return this;
    }

    public String getWithdrawId() {
        return withdrawId;
    }

    public WithdrawResult setWithdrawId(String withdrawId) {
        this.withdrawId = withdrawId;
        return this;
    }

    public Withdraw getWithdraw() {
        return withdraw;
    }

    public WithdrawResult setWithdraw(Withdraw withdraw) {
        this.withdraw = withdraw;
        return this;
    }

    @Override
    public String toString() {
        return "WithdrawResult{" +
                "stage='" + stage + '\'' +
                ", message='" + message + '\'' +
                ", success=" + success +
                ", withdrawId='" + withdrawId + '\'' +
                '}';
    }
}
