package com.tkelemen.arbitrader.model;

import java.math.BigInteger;

public class Address {
    private String address;

    private BigInteger tag;

    private String network;

    private Exchange exchange;

    private Integer precision;

    private String alias;

    public Address() {
    }

    public Address(String address) {
        this.address = address;
    }

    public Address(String address, BigInteger tag) {
        this.address = address;
        this.tag = tag;
    }

    public Address(String address, BigInteger tag, String network) {
        this.address = address;
        this.tag = tag;
        this.network = network;
    }

    public Address(String address, BigInteger tag, String network, int precision) {
        this.address = address;
        this.tag = tag;
        this.network = network;
        this.precision = precision;
    }

    public static Address create(String address, BigInteger tag, String network) {
        return new Address(address, tag, network);
    }

    public static Address create(String address, BigInteger tag) {
        return new Address(address, tag);
    }

    public static Address create(String address) {
        return new Address(address, null);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigInteger getTag() {
        return tag;
    }

    public String getNetwork() {
        return network;
    }

    public Address setNetwork(String network) {
        this.network = network;
        return this;
    }

    public void setTag(BigInteger tag) {
        this.tag = tag;
    }

    public boolean hasTag() {
        return tag != null;
    }

    public Exchange getExchange() {
        return exchange;
    }

    public Address setExchange(Exchange exchange) {
        this.exchange = exchange;
        return this;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @Override
    public String toString() {
        return "Address{" +
                "address='" + address + '\'' +
                ", tag=" + tag +
                ", network='" + network + '\'' +
                ", exchange=" + exchange +
                ", precision=" + precision +
                ", alias='" + alias + '\'' +
                '}';
    }

    public Integer getPrecision() {
        return precision;
    }

    public Address setPrecision(Integer precision) {
        this.precision = precision;
        return this;
    }
}
