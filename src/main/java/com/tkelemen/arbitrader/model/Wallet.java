package com.tkelemen.arbitrader.model;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

public class Wallet {
    private final Map<String, BigDecimal> totalFunds = new ConcurrentHashMap<>();
    private final Map<String, BigDecimal> lockedFunds = new ConcurrentHashMap<>();

    public void setTotalAmount(String ticker, BigDecimal amount) {
        this.totalFunds.put(ticker, amount);
    }

    public void setLockedAmount(String ticker, BigDecimal amount) {
        this.lockedFunds.put(ticker, BigDecimal.ZERO.compareTo(amount) > 0 ? BigDecimal.ZERO : amount);
    }

    public synchronized void subtractAmount(String ticker, BigDecimal amount) {
        setTotalAmount(ticker, getTotalAmount(ticker).subtract(amount));
    }

    public synchronized void unlockAmount(String ticker, BigDecimal amount) {
        setLockedAmount(ticker, getLockedAmount(ticker).subtract(amount));
    }

    public synchronized void addAmount(String ticker, BigDecimal amount) {
        setTotalAmount(ticker, getTotalAmount(ticker).add(amount));
    }

    public synchronized void lockAmount(String ticker, BigDecimal amount) {
        setLockedAmount(ticker, getLockedAmount(ticker).add(amount));
    }

    public synchronized void resetLockedAmounts() {
        lockedFunds.clear();
    }

    public BigDecimal getFreeAmount(String ticker) {
        BigDecimal totalAmount = getTotalAmount(ticker);
        BigDecimal lockedAmount = getLockedAmount(ticker);

        return totalAmount.subtract(lockedAmount);
    }

    public BigDecimal getTotalAmount(String ticker) {
        return this.totalFunds.getOrDefault(ticker, BigDecimal.ZERO);
    }

    public BigDecimal getLockedAmount(String ticker) {
        return this.lockedFunds.getOrDefault(ticker, BigDecimal.ZERO);
    }

    public Map<String, BigDecimal> getFreeAmounts() {
        final Map<String, BigDecimal> freeAmountMap = new HashMap<>();
        for(String ticker : totalFunds.keySet()) {
            freeAmountMap.put(ticker, getFreeAmount(ticker));
        }

        return freeAmountMap;
    }

    public String asFormattedString() {
        final StringBuilder sb = new StringBuilder();

        for(Map.Entry<String, BigDecimal> entry : new TreeMap<>(totalFunds).entrySet()) {
            if(BigDecimal.ZERO.compareTo(entry.getValue()) == 0) {
                continue;
            }

            sb.append(entry.getKey()).append(": ").append(entry.getValue().toPlainString());

            final BigDecimal locked = lockedFunds.get(entry.getKey());
            if(locked != null && BigDecimal.ZERO.compareTo(locked) < 0) {
                sb.append("(").append(locked.toPlainString()).append(")");
            }

            sb.append("\n");
        }

        return sb.toString();
    }

    @Override
    public String toString() {
        return "Wallet{" +
                "totalFunds=" + totalFunds +
                ", lockedFunds=" + lockedFunds +
                '}';
    }
}
