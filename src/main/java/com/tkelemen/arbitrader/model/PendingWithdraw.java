package com.tkelemen.arbitrader.model;

import com.tkelemen.arbitrader.db.entity.Arbitrage;
import com.tkelemen.arbitrader.exception.WithdrawException;
import com.tkelemen.arbitrader.service.arbitrage.WithdrawService;
import com.tkelemen.arbitrader.util.TradeUtils;

import java.math.BigDecimal;
import java.util.*;

public class PendingWithdraw {
    private final String ticker;
    private final Map<Exchange, BigDecimal> amountToDestination = new HashMap<>();
    private final Map<Exchange, List<Arbitrage>> arbitragesToDestination = new HashMap<>();

    public PendingWithdraw(String ticker) {
        this.ticker = ticker;
    }

    public void add(Arbitrage arbitrage) throws WithdrawException {
        final Exchange destinationExchange = Exchange.valueOf(arbitrage.getPrediction().getFinalExchange());

        BigDecimal currentAmount = amountToDestination.get(destinationExchange);
        if (currentAmount == null) {
            currentAmount = BigDecimal.ZERO;
        }
        if(arbitrage.getBoughtAmount() == null) {
            throw new WithdrawException("Could not process arbitrage without bought amount value [ id = " + arbitrage.getId() + " ]");
        }
        amountToDestination.put(destinationExchange, currentAmount.add(TradeUtils.getAmountToWithdraw(arbitrage)));

        List<Arbitrage> arbitrageList = arbitragesToDestination.get(destinationExchange);
        if (arbitrageList == null) {
            arbitrageList = new ArrayList<>();
        }
        arbitrageList.add(arbitrage);
        arbitragesToDestination.put(destinationExchange, arbitrageList);
    }

    public String getTicker() {
        return ticker;
    }

    public Set<Exchange> getDestinations() {
        return amountToDestination.keySet();
    }

    public BigDecimal getAmount(Exchange destinationExchange) {
        return amountToDestination.get(destinationExchange);
    }

    public List<Arbitrage> getArbitrages(Exchange destinationExchange) {
        return arbitragesToDestination.get(destinationExchange);
    }
}
