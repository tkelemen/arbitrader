package com.tkelemen.arbitrader.model;

import java.math.BigDecimal;
import java.util.Date;

public class MakerTriggerOrderDTO {
    private long id;

    private Date createDatetime;

    private Date updateDatetime;

    private MakerOpportunityDTO opportunity;

    private String pair;

    private BigDecimal amount;

    private BigDecimal price;

    private BigDecimal executedAmount;

    private String exchangeOrderId;

    private ExchangeOrder.Status status;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreateDatetime() {
        return createDatetime;
    }

    public void setCreateDatetime(Date createDatetime) {
        this.createDatetime = createDatetime;
    }

    public Date getUpdateDatetime() {
        return updateDatetime;
    }

    public void setUpdateDatetime(Date updateDatetime) {
        this.updateDatetime = updateDatetime;
    }

    public MakerOpportunityDTO getOpportunity() {
        return opportunity;
    }

    public void setOpportunity(MakerOpportunityDTO opportunity) {
        this.opportunity = opportunity;
    }

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getExecutedAmount() {
        return executedAmount;
    }

    public void setExecutedAmount(BigDecimal executedAmount) {
        this.executedAmount = executedAmount;
    }

    public String getExchangeOrderId() {
        return exchangeOrderId;
    }

    public void setExchangeOrderId(String exchangeOrderId) {
        this.exchangeOrderId = exchangeOrderId;
    }

    public ExchangeOrder.Status getStatus() {
        return status;
    }

    public void setStatus(ExchangeOrder.Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "MakerTriggerOrderDTO{" +
                "id=" + id +
                ", createDatetime=" + createDatetime +
                ", updateDatetime=" + updateDatetime +
                ", opportunity=" + opportunity +
                ", pair='" + pair + '\'' +
                ", amount=" + amount +
                ", price=" + price +
                ", executedAmount=" + executedAmount +
                ", exchangeOrderId='" + exchangeOrderId + '\'' +
                ", status=" + status +
                '}';
    }
}