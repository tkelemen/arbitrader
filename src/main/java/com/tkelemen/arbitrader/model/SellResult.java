package com.tkelemen.arbitrader.model;

import java.math.BigDecimal;

public class SellResult {
    private String status;
    private BigDecimal receivedAmount;
    private BigDecimal soldAmount;
    private BigDecimal sellFee;
    private String sellFeeCurrency;
    private String orderId;
    private String additionalOrderId;
    private String sellFeeIncluded;
    private String errorMessage;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getReceivedAmount() {
        return receivedAmount;
    }

    public void setReceivedAmount(BigDecimal receivedAmount) {
        this.receivedAmount = receivedAmount;
    }

    public BigDecimal getSoldAmount() {
        return soldAmount;
    }

    public void setSoldAmount(BigDecimal soldAmount) {
        this.soldAmount = soldAmount;
    }


    public BigDecimal getSellFee() {
        return sellFee;
    }

    public void setSellFee(BigDecimal sellFee) {
        this.sellFee = sellFee;
    }

    public String getSellFeeCurrency() {
        return sellFeeCurrency;
    }

    public void setSellFeeCurrency(String sellFeeCurrency) {
        this.sellFeeCurrency = sellFeeCurrency;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getAdditionalOrderId() {
        return additionalOrderId;
    }

    public void setAdditionalOrderId(String additionalOrderId) {
        this.additionalOrderId = additionalOrderId;
    }

    public String getSellFeeIncluded() {
        return sellFeeIncluded;
    }

    public void setSellFeeIncluded(String sellFeeIncluded) {
        this.sellFeeIncluded = sellFeeIncluded;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return "SellResult{" +
                "status='" + status + '\'' +
                ", receivedAmount=" + receivedAmount +
                ", soldAmount=" + soldAmount +
                ", sellFee=" + sellFee +
                ", sellFeeCurrency='" + sellFeeCurrency + '\'' +
                ", orderId='" + orderId + '\'' +
                ", additionalOrderId='" + additionalOrderId + '\'' +
                ", sellFeeIncluded='" + sellFeeIncluded + '\'' +
                '}';
    }
}
