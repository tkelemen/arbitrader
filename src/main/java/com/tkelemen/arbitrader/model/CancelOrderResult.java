package com.tkelemen.arbitrader.model;

import java.math.BigDecimal;

public class CancelOrderResult {
    private String orderId;
    private BigDecimal remainingAmount;
    private boolean success;
    private String error;

    public CancelOrderResult() {
    }

    public CancelOrderResult(String orderId, boolean success, String error) {
        this.orderId = orderId;
        this.success = success;
        this.error = error;
    }

    public CancelOrderResult(String orderId, BigDecimal remainingAmount, boolean success, String error) {
        this.orderId = orderId;
        this.remainingAmount = remainingAmount;
        this.success = success;
        this.error = error;
    }

    public BigDecimal getRemainingAmount() {
        return remainingAmount;
    }

    public void setRemainingAmount(BigDecimal remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "CancelOrderResult{" +
                "orderId='" + orderId + '\'' +
                ", remainingAmount=" + remainingAmount +
                ", success=" + success +
                ", error='" + error + '\'' +
                '}';
    }
}
