package com.tkelemen.arbitrader.model;

import com.tkelemen.arbitrader.model.enums.OrderType;

import java.math.BigDecimal;

public class CreateOrder {
    private String pair;
    private BigDecimal price;
    private BigDecimal amount;
    private OrderType type;

    public CreateOrder(OrderType type, String pair, BigDecimal price, BigDecimal amount) {
        this.type = type;
        this.pair = pair;
        this.price = price;
        this.amount = amount;
    }

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public OrderType getType() {
        return type;
    }

    public void setType(OrderType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "CreateOrder{" +
                "pair='" + pair + '\'' +
                ", price=" + price +
                ", amount=" + amount +
                ", type=" + type +
                '}';
    }
}
