package com.tkelemen.arbitrader.model;

import java.math.BigDecimal;

public class Transfer {
    public static final String STATUS_INITIATED = "INITIATED";
    public static final String STATUS_PENDING = "PENDING";
    public static final String STATUS_SENT = "SENT";
    public static final String STATUS_RECEIVED = "RECEIVED";

    private Exchange initialExchange;

    private Exchange finalExchange;

    private BigDecimal amount;

    private BigDecimal fee;

    private String withdrawId;

    private String transactionId;

    private String status;

    public Exchange getInitialExchange() {
        return initialExchange;
    }

    public void setInitialExchange(Exchange initialExchange) {
        this.initialExchange = initialExchange;
    }

    public Exchange getFinalExchange() {
        return finalExchange;
    }

    public void setFinalExchange(Exchange finalExchange) {
        this.finalExchange = finalExchange;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public String getWithdrawId() {
        return withdrawId;
    }

    public void setWithdrawId(String withdrawId) {
        this.withdrawId = withdrawId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
