package com.tkelemen.arbitrader.model;

import java.math.BigDecimal;

public class BuyResult {
    private String orderId;
    private String buyStatus;
    private String pair;
    private BigDecimal initialAmount;
    private BigDecimal boughtAmount;
    private BigDecimal buyFee;
    private String buyFeeCurrency;
    private String additionalOrderId;
    private String buyFeeIncluded;
    private BigDecimal withdrawFee;
    private String errorMessage;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getBuyStatus() {
        return buyStatus;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public String getPair() {
        return pair;
    }

    public void setBuyStatus(String buyStatus) {
        this.buyStatus = buyStatus;
    }

    public BigDecimal getInitialAmount() {
        return initialAmount;
    }

    public void setInitialAmount(BigDecimal initialAmount) {
        this.initialAmount = initialAmount;
    }

    public BigDecimal getBoughtAmount() {
        return boughtAmount;
    }

    public void setBoughtAmount(BigDecimal boughtAmount) {
        this.boughtAmount = boughtAmount;
    }

    public BigDecimal getBuyFee() {
        return buyFee;
    }

    public void setBuyFee(BigDecimal buyFee) {
        this.buyFee = buyFee;
    }

    public String getBuyFeeCurrency() {
        return buyFeeCurrency;
    }

    public void setBuyFeeCurrency(String buyFeeCurrency) {
        this.buyFeeCurrency = buyFeeCurrency;
    }

    public String getAdditionalOrderId() {
        return additionalOrderId;
    }

    public void setAdditionalOrderId(String additionalOrderId) {
        this.additionalOrderId = additionalOrderId;
    }

    public String getBuyFeeIncluded() {
        return buyFeeIncluded;
    }

    public void setBuyFeeIncluded(String buyFeeIncluded) {
        this.buyFeeIncluded = buyFeeIncluded;
    }

    public BigDecimal getWithdrawFee() {
        return withdrawFee;
    }

    public void setWithdrawFee(BigDecimal withdrawFee) {
        this.withdrawFee = withdrawFee;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return "BuyResult{" +
                "orderId='" + orderId + '\'' +
                ", buyStatus='" + buyStatus + '\'' +
                ", initialAmount=" + initialAmount +
                ", boughtAmount=" + boughtAmount +
                ", buyFee=" + buyFee +
                ", buyFeeCurrency='" + buyFeeCurrency + '\'' +
                ", additionalOrderId='" + additionalOrderId + '\'' +
                ", buyFeeIncluded='" + buyFeeIncluded + '\'' +
                ", withdrawFee=" + withdrawFee +
                '}';
    }
}
