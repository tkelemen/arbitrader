package com.tkelemen.arbitrader.model;

import java.math.BigDecimal;
import java.util.AbstractQueue;
import java.util.Locale;
import java.util.NavigableMap;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentSkipListMap;

public class DefaultOrderBook implements OrderBook {
    private final String symbol;
    private Exchange exchange;
    private final NavigableMap<String, BigDecimal> askLevels = new ConcurrentSkipListMap<>();
    private final NavigableMap<String, BigDecimal> bidLevels = new ConcurrentSkipListMap<>();

    private final NavigableMap<String, Long> askLevelsTime = new ConcurrentSkipListMap<>();
    private final NavigableMap<String, Long> bidLevelsTime = new ConcurrentSkipListMap<>();

    private long lastUpdatedTimestamp;

    private final BlockingQueue<Object> lastUpdates = new ArrayBlockingQueue<>(5);

    public DefaultOrderBook(String symbol) {
        this.symbol = symbol;
        this.lastUpdatedTimestamp = System.currentTimeMillis();
        reset();
    }

    @Override
    public synchronized void recordUpdate(Object update) {
        if(lastUpdates.remainingCapacity() == 0) {
            lastUpdates.poll();
        }

        lastUpdates.offer(update);
    }

    public synchronized void setAskLevel(String price, BigDecimal quantity) {
        String pricePadded = String.format(Locale.US, "%014.8f", new BigDecimal(price));// StringUtils.leftPad(price, 15, "0");
        long now = System.currentTimeMillis();
        askLevels.put(pricePadded, quantity);
        askLevelsTime.put(pricePadded, now);

        setLastUpdatedTimestamp(now);
    }

    public synchronized void setBidLevel(String price, BigDecimal quantity) {
        String pricePadded = String.format(Locale.US, "%014.8f", new BigDecimal(price)); //StringUtils.leftPad(price, 15, "0");
        long now = System.currentTimeMillis();
        bidLevels.put(pricePadded, quantity);
        bidLevelsTime.put(pricePadded, now);

        setLastUpdatedTimestamp(now);
    }

    public synchronized BigDecimal removeAskLevel(String price) {
        String pricePadded = String.format(Locale.US, "%014.8f", new BigDecimal(price));
        askLevelsTime.remove(pricePadded);
        setLastUpdatedTimestamp(System.currentTimeMillis());

        return askLevels.remove(pricePadded); //StringUtils.leftPad(price, 15, "0"));
    }

    public synchronized BigDecimal removeBidLevel(String price) {
        String pricePadded = String.format(Locale.US, "%014.8f", new BigDecimal(price));
        bidLevelsTime.remove(pricePadded);
        setLastUpdatedTimestamp(System.currentTimeMillis());

        return bidLevels.remove(pricePadded);
    }

    public NavigableMap<String, BigDecimal> getAskLevels() {
        return askLevels;
    }

    public NavigableMap<String, BigDecimal> getBidLevels() {
        return bidLevels;
    }

    public BigDecimal getAskLevelQuantity(String level) {
        return askLevels.get(level);
    }

    public BigDecimal getBidLevelQuantity(String level) {
        return bidLevels.get(level);
    }

    public NavigableMap<String, Long> getAskLevelTimes() {
        return askLevelsTime;
    }

    public NavigableMap<String, Long> getBidLevelTimes() {
        return bidLevelsTime;
    }

    public Long getAskLevelTime(String level) {
        return askLevelsTime.get(level);
    }

    public Long getBidLevelTime(String level) {
        return bidLevelsTime.get(level);
    }

    public long getLastUpdatedTimestamp() {
        return lastUpdatedTimestamp;
    }

    public void setLastUpdatedTimestamp(long lastUpdatedTimestamp) {
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
    }

    public Exchange getExchange() {
        return exchange;
    }

    public void setExchange(Exchange exchange) {
        this.exchange = exchange;
    }

    public String getSymbol() {
        return symbol;
    }

    public synchronized void reset() {
        askLevels.clear();
        bidLevels.clear();
        askLevelsTime.clear();
        bidLevelsTime.clear();
        lastUpdates.clear();
    }

    @Override
    public String toString() {
        return "DefaultOrderBook{" +
                "symbol='" + symbol + '\'' +
                ", exchange=" + exchange +
                ", askLevels=" + askLevels +
                ", bidLevels=" + bidLevels +
                ", askLevelsTime=" + askLevelsTime +
                ", bidLevelsTime=" + bidLevelsTime +
                ", lastUpdatedTimestamp=" + lastUpdatedTimestamp +
                ", lastUpdates=" + lastUpdates +
                '}';
    }
}
