package com.tkelemen.arbitrader.model;

import java.util.ArrayList;
import java.util.List;

public class MassCancelCreateOrderResult {
    private List<CancelOrderResult> cancelOrderResultList = new ArrayList<>();
    private List<CreateOrderResult> createOrderResultList = new ArrayList<>();
    private String error;

    public void addCancelOrderResult(CancelOrderResult cancelOrderResult) {
        cancelOrderResultList.add(cancelOrderResult);
    }

    public void addCreateOrderResult(CreateOrderResult createOrderResult) {
        createOrderResultList.add(createOrderResult);
    }

    public List<CancelOrderResult> getCancelOrderResultList() {
        return cancelOrderResultList;
    }

    public void setCancelOrderResultList(List<CancelOrderResult> cancelOrderResultList) {
        this.cancelOrderResultList = cancelOrderResultList;
    }

    public List<CreateOrderResult> getCreateOrderResultList() {
        return createOrderResultList;
    }

    public void setCreateOrderResultList(List<CreateOrderResult> createOrderResultList) {
        this.createOrderResultList = createOrderResultList;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "MassCancelCreateOrderResult{" +
                "cancelOrderResultList=" + cancelOrderResultList +
                ", createOrderResultList=" + createOrderResultList +
                ", error='" + error + '\'' +
                '}';
    }
}
