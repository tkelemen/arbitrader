package com.tkelemen.arbitrader.model;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class Opportunity {
    public static String FLAG_BEST_GAIN = "BEST_GAIN";
    public static String FLAG_INSUFFICIENT_QUOTE_FUNDS = "INSUFFICIENT_QUOTE_FUNDS";
    public static String FLAG_INSUFFICIENT_BASE_FUNDS = "INSUFFICIENT_BASE_FUNDS";

    private String pair;

    private String finalPair;

    private BigDecimal initialAmount;

    private BigDecimal finalAmount;

    private BigDecimal transferAmount;

    private Exchange initialExchange;

    private Exchange finalExchange;

    private BigDecimal buyFee;

    private BigDecimal sellFee;

    private BigDecimal withdrawFee;

    private BigDecimal depositFee;

    private Set<String> flags = new HashSet<>();

    private OrderBook initialOrderBook;
    private OrderBook finalOrderBook;

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public String getFinalPair() {
        return finalPair;
    }

    public void setFinalPair(String finalPair) {
        this.finalPair = finalPair;
    }

    public BigDecimal getInitialAmount() {
        return initialAmount;
    }

    public void setInitialAmount(BigDecimal initialAmount) {
        this.initialAmount = initialAmount;
    }

    public BigDecimal getFinalAmount() {
        return finalAmount;
    }

    public void setFinalAmount(BigDecimal finalAmount) {
        this.finalAmount = finalAmount;
    }

    public BigDecimal getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(BigDecimal transferAmount) {
        this.transferAmount = transferAmount;
    }

    public Exchange getInitialExchange() {
        return initialExchange;
    }

    public void setInitialExchange(Exchange initialExchange) {
        this.initialExchange = initialExchange;
    }

    public Exchange getFinalExchange() {
        return finalExchange;
    }

    public void setFinalExchange(Exchange finalExchange) {
        this.finalExchange = finalExchange;
    }

    public BigDecimal getBuyFee() {
        return buyFee;
    }

    public void setBuyFee(BigDecimal buyFee) {
        this.buyFee = buyFee;
    }

    public BigDecimal getSellFee() {
        return sellFee;
    }

    public void setSellFee(BigDecimal sellFee) {
        this.sellFee = sellFee;
    }

    public BigDecimal getWithdrawFee() {
        return withdrawFee;
    }

    public void setWithdrawFee(BigDecimal withdrawFee) {
        this.withdrawFee = withdrawFee;
    }

    public BigDecimal getDepositFee() {
        return depositFee;
    }

    public void setDepositFee(BigDecimal depositFee) {
        this.depositFee = depositFee;
    }

    public Set<String> getFlags() {
        return flags;
    }

    public void addFlag(String flag) {
        this.flags.add(flag);
    }

    public void removeFlag(String flag) {
        this.flags.remove(flag);
    }

    public OrderBook getInitialOrderBook() {
        return initialOrderBook;
    }

    public void setInitialOrderBook(OrderBook initialOrderBook) {
        this.initialOrderBook = initialOrderBook;
    }

    public OrderBook getFinalOrderBook() {
        return finalOrderBook;
    }

    public void setFinalOrderBook(OrderBook finalOrderBook) {
        this.finalOrderBook = finalOrderBook;
    }

    @Override
    public String toString() {
        return "Opportunity{" +
                "pair='" + pair + '\'' +
                ", finalPair='" + finalPair + '\'' +
                ", initialAmount=" + initialAmount +
                ", finalAmount=" + finalAmount +
                ", transferAmount=" + transferAmount +
                ", initialExchange=" + initialExchange +
                ", finalExchange=" + finalExchange +
                ", buyFee=" + buyFee +
                ", sellFee=" + sellFee +
                ", withdrawFee=" + withdrawFee +
                ", depositFee=" + depositFee +
                ", flags='" + flags + '\'' +
                '}';
    }
}
