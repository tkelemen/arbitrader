package com.tkelemen.arbitrader.model;

import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentSkipListMap;

public class KrakenOrderBook implements OrderBook {
    private final String symbol;
    private Exchange exchange;
    private final NavigableMap<String, String> askLevels = new ConcurrentSkipListMap<>();
    private final NavigableMap<String, String> bidLevels = new ConcurrentSkipListMap<>();

    private final NavigableMap<String, Long> askLevelsTime = new ConcurrentSkipListMap<>();
    private final NavigableMap<String, Long> bidLevelsTime = new ConcurrentSkipListMap<>();

    private long lastUpdatedTimestamp;

    private final Queue<Object> lastUpdates = new ArrayBlockingQueue<>(5);

    public KrakenOrderBook(String symbol) {
        this.symbol = symbol;
        this.lastUpdatedTimestamp = System.currentTimeMillis();
        reset();
    }

    @Override
    public void recordUpdate(Object update) {
        lastUpdates.offer(update);
    }

    public void setAskLevel(String price, BigDecimal quantity) {
        setAskLevel(price, quantity.toPlainString());
    }

    public void setBidLevel(String price, BigDecimal quantity) {
        setBidLevel(price, quantity.toPlainString());
    }

    public void setAskLevel(String price, String quantity) {
        String pricePadded = StringUtils.leftPad(price, 15, "0");
        long now = System.currentTimeMillis();
        askLevels.put(pricePadded, quantity);
        askLevelsTime.put(pricePadded, now);

        setLastUpdatedTimestamp(now);
    }

    public void setBidLevel(String price, String quantity) {
        String pricePadded = StringUtils.leftPad(price, 15, "0");
        long now = System.currentTimeMillis();
        bidLevels.put(pricePadded, quantity);
        bidLevelsTime.put(pricePadded, now);
        setLastUpdatedTimestamp(now);
    }

    public BigDecimal removeAskLevel(String price) {
        String pricePadded = StringUtils.leftPad(price, 15, "0");
        askLevelsTime.remove(pricePadded);
        setLastUpdatedTimestamp(System.currentTimeMillis());

        String removedPrice = askLevels.remove(pricePadded);

        return removedPrice != null ? new BigDecimal(removedPrice) : null;
    }

    public BigDecimal removeBidLevel(String price) {
        String pricePadded = StringUtils.leftPad(price, 15, "0");
        bidLevelsTime.remove(pricePadded);
        setLastUpdatedTimestamp(System.currentTimeMillis());

        String removedPrice = bidLevels.remove(pricePadded);

        return removedPrice != null ? new BigDecimal(removedPrice) : null;
    }

    public NavigableMap<String, BigDecimal> getAskLevels() {
        NavigableMap<String, BigDecimal> tranformedMap = new ConcurrentSkipListMap<>();
        for (Map.Entry<String, String> entry : askLevels.entrySet()) {
            tranformedMap.put(entry.getKey(), new BigDecimal(entry.getValue()));
        }
        return tranformedMap;
    }

    public NavigableMap<String, BigDecimal> getBidLevels() {
        NavigableMap<String, BigDecimal> transformedMap = new ConcurrentSkipListMap<>();
        for (Map.Entry<String, String> entry : bidLevels.entrySet()) {
            transformedMap.put(entry.getKey(), new BigDecimal(entry.getValue()));
        }
        return transformedMap;
    }

    public BigDecimal getAskLevelQuantity(String level) {
        return new BigDecimal(askLevels.getOrDefault(level, "0"));
    }

    public BigDecimal getBidLevelQuantity(String level) {
        return new BigDecimal(bidLevels.getOrDefault(level, "0"));
    }

    public NavigableMap<String, Long> getAskLevelTimes() {
        return askLevelsTime;
    }

    public NavigableMap<String, Long> getBidLevelTimes() {
        return bidLevelsTime;
    }

    public Long getAskLevelTime(String level) {
        return askLevelsTime.get(level);
    }

    public Long getBidLevelTime(String level) {
        return bidLevelsTime.get(level);
    }

    public long getLastUpdatedTimestamp() {
        return lastUpdatedTimestamp;
    }

    public void setLastUpdatedTimestamp(long lastUpdatedTimestamp) {
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
    }

    public Exchange getExchange() {
        return exchange;
    }

    public void setExchange(Exchange exchange) {
        this.exchange = exchange;
    }

    public String getSymbol() {
        return symbol;
    }

    public synchronized void reset() {
        askLevels.clear();
        bidLevels.clear();
        askLevelsTime.clear();
        bidLevelsTime.clear();
        lastUpdates.clear();
    }

    @Override
    public String toString() {
        return "KrakenOrderBook{" +
                "symbol='" + symbol + '\'' +
                ", exchange=" + exchange +
                ", askLevels=" + askLevels +
                ", bidLevels=" + bidLevels +
                ", askLevelsTime=" + askLevelsTime +
                ", bidLevelsTime=" + bidLevelsTime +
                ", lastUpdatedTimestamp=" + lastUpdatedTimestamp +
                ", lastUpdates=" + lastUpdates +
                '}';
    }
}
