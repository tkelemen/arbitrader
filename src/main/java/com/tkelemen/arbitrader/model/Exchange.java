package com.tkelemen.arbitrader.model;

public enum Exchange {
    KRAKEN,
    CEX,
    BITSTAMP,
    COINBASE,
    BINANCE,
    BITFINEX,
    KUCOIN
}
