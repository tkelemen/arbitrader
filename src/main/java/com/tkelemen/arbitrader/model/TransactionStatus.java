package com.tkelemen.arbitrader.model;

public enum TransactionStatus {
    WAITING, INITIATED, DONE, ERROR
}
