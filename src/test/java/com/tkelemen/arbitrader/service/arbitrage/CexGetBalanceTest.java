package com.tkelemen.arbitrader.service.arbitrage;

import com.tkelemen.arbitrader.client.cex.rest.GetBalanceClient;
import com.tkelemen.arbitrader.config.ApplicationConfig;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class CexGetBalanceTest {
    public static void main(String[] args) {
        ApplicationConfig config = new ApplicationConfig();
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream("application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        config.setCexApiKey(prop.getProperty("cex.apiKey"));
        config.setCexApiRestBaseUrl(prop.getProperty("cex.apiRestBaseUrl"));
        config.setCexApiSecret(prop.getProperty("cex.apiSecret"));
        config.setCexApiUsername(prop.getProperty("cex.apiUsername"));
        GetBalanceClient getBalanceClient = new GetBalanceClient(config);
        System.out.println(getBalanceClient.get().toString());
    }
}
