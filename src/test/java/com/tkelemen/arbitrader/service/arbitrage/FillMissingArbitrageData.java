package com.tkelemen.arbitrader.service.arbitrage;

import com.tkelemen.arbitrader.client.cex.rest.GetOrderClient;
import com.tkelemen.arbitrader.client.cex.rest.message.GetOrderResponse;
import com.tkelemen.arbitrader.db.entity.Arbitrage;
import com.tkelemen.arbitrader.db.repository.ArbitrageRepository;
import com.tkelemen.arbitrader.model.Exchange;
import com.tkelemen.arbitrader.util.CurrencyUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class FillMissingArbitrageData {
    @Autowired
    private ArbitrageRepository arbitrageRepository;

    @Autowired
    private GetOrderClient getOrderClient;

    public void fillData() {
        List<Arbitrage> arbitrageList = arbitrageRepository.findWithMissingDetails(Exchange.CEX.name());

        for(Arbitrage arbitrage : arbitrageList) {
            if(arbitrage.getBuyFee() == null) {
                if(arbitrage.getBuyOrderId() == null) {
                    System.out.println("ERROR arbitrage " + arbitrage.getId() + " buy order id null");
                    continue;
                }
                System.out.println("Getting order data for buy order id " + arbitrage.getBuyOrderId());
                GetOrderResponse response = getOrderClient.get(arbitrage.getBuyOrderId());

                String pair = arbitrage.getPrediction().getPair();
                String quote = CurrencyUtils.getQuote(pair);

                if(response != null && response.getTakerFeeAmount(quote) != null) {
                    arbitrage.setBuyFee(response.getTakerFeeAmount(quote));
                    arbitrage.setBuyFeeCurrency(quote);

                    //arbitrageRepository.save(arbitrage);
                } else {
                    System.out.print("ERROR while getting order details for id " + arbitrage.getBuyOrderId());
                }
            } else if(arbitrage.getSellFee() == null) {
                if(arbitrage.getSellOrderId() == null) {
                    System.out.println("ERROR arbitrage " + arbitrage.getId() + " sell order id null");
                    continue;
                }
                System.out.println("Getting order data for sell order id " + arbitrage.getSellOrderId());
                GetOrderResponse response = getOrderClient.get(arbitrage.getSellOrderId());

                String quote = CurrencyUtils.getQuote(arbitrage.getPrediction().getFinalPair());

                if(response != null && response.getTakerFeeAmount(quote) != null) {
                    arbitrage.setSellFee(response.getTakerFeeAmount(quote));
                    arbitrage.setSellFeeCurrency(quote);

                    //arbitrageRepository.save(arbitrage);
                } else {
                    System.out.print("ERROR while getting order details for id " + arbitrage.getSellOrderId());
                }
            }
        }
    }
}
