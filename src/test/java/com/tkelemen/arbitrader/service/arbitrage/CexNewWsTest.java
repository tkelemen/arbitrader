package com.tkelemen.arbitrader.service.arbitrage;

import com.tkelemen.arbitrader.client.cex.ws.CexWsClientNew;
import com.tkelemen.arbitrader.config.ApplicationConfig;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class CexNewWsTest {
    public static void main(String[] args) {
        ApplicationConfig config = new ApplicationConfig();
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream("application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        config.setCexWsUrl(prop.getProperty("cex.wsUrl"));
        config.setCexApiKey(prop.getProperty("cex.apiKey"));
        config.setCexApiSecret(prop.getProperty("cex.apiSecret"));

        CexWsClientNew client = new CexWsClientNew(config);
        client.connect();
    }
}
