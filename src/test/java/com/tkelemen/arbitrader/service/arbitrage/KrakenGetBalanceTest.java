package com.tkelemen.arbitrader.service.arbitrage;

import com.tkelemen.arbitrader.client.kraken.rest.KrakenGetBalanceClient;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.exception.RestClientException;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class KrakenGetBalanceTest {
    public static void main(String[] args) throws RestClientException {
        ApplicationConfig config = new ApplicationConfig();
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream("application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        config.setKrakenApiKey(prop.getProperty("kraken.apiKey"));
        config.setKrakenApiPrivateKey(prop.getProperty("kraken.apiPrivateKey"));
        config.setKrakenApiRestBaseUrl(prop.getProperty("kraken.apiRestBaseUrl"));
        config.setKrakenWebSocketUrl(prop.getProperty("kraken.wsUrl"));

        KrakenGetBalanceClient client = new KrakenGetBalanceClient(config);
        System.out.println(client.get());
    }
}
