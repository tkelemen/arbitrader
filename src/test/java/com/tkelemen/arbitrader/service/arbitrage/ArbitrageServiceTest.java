package com.tkelemen.arbitrader.service.arbitrage;

import com.tkelemen.arbitrader.db.entity.Prediction;
import com.tkelemen.arbitrader.model.DefaultOrderBook;
import com.tkelemen.arbitrader.model.OrderBook;
import com.tkelemen.arbitrader.service.market.OrderBookHelper;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ArbitrageServiceTest {
    @Test
    public void testCalculateBaseQuantity() {
        OrderBook orderBook = new DefaultOrderBook("TEST");
        orderBook.setAskLevel("100", new BigDecimal("0.1"));
        orderBook.setAskLevel("150", new BigDecimal("0.1"));
        orderBook.setAskLevel("200", new BigDecimal("0.5"));
        orderBook.setAskLevel("300", new BigDecimal("1.0"));
        orderBook.setAskLevel("1000", new BigDecimal("10.0"));

        assertTrue(new BigDecimal("0.1").compareTo(OrderBookHelper.calculateBaseQuantity(orderBook, new BigDecimal(10))) == 0);
        assertTrue(new BigDecimal("0.325").compareTo(OrderBookHelper.calculateBaseQuantity(orderBook, new BigDecimal(50))) == 0);
        assertTrue(new BigDecimal("0.45").compareTo(OrderBookHelper.calculateBaseQuantity(orderBook, new BigDecimal(75))) == 0);
        assertTrue(new BigDecimal("0.95").compareTo(OrderBookHelper.calculateBaseQuantity(orderBook, new BigDecimal(200))) == 0);
        assertTrue(new BigDecimal("2.275").compareTo(OrderBookHelper.calculateBaseQuantity(orderBook, new BigDecimal(1000))) == 0);
    }


    @Test
    public void testCalculateQuoteQuantity() {
        OrderBook orderBook = new DefaultOrderBook("TEST");
        orderBook.setBidLevel("100", new BigDecimal("0.1"));
        orderBook.setBidLevel("90", new BigDecimal("0.1"));
        orderBook.setBidLevel("80", new BigDecimal("0.5"));
        orderBook.setBidLevel("50", new BigDecimal("1.0"));

        assertTrue(new BigDecimal("10").compareTo(OrderBookHelper.calculateQuoteQuantity(orderBook, new BigDecimal("0.1"))) == 0);
        assertTrue(new BigDecimal("19").compareTo(OrderBookHelper.calculateQuoteQuantity(orderBook, new BigDecimal("0.2"))) == 0);
        assertTrue(new BigDecimal("43").compareTo(OrderBookHelper.calculateQuoteQuantity(orderBook, new BigDecimal("0.5"))) == 0);
        assertTrue(new BigDecimal("99").compareTo(OrderBookHelper.calculateQuoteQuantity(orderBook, new BigDecimal("1.5"))) == 0);
    }
}
