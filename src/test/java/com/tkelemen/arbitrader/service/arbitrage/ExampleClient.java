package com.tkelemen.arbitrader.service.arbitrage;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.tkelemen.arbitrader.client.kraken.ws.message.*;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.handshake.ServerHandshake;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.zip.CRC32;

public class ExampleClient extends WebSocketClient {
    private Map<String, Map<String, BigDecimal>> askOrderBook = new ConcurrentSkipListMap<>();
    private Map<String, Map<String, BigDecimal>> bidOrderBook = new ConcurrentSkipListMap<>();

    private static final ObjectMapper mapper = new ObjectMapper();
    private final ObjectReader objectReader = mapper.readerFor(KrakenEvent.class);
    private int i = 0;
    private long startTime = System.currentTimeMillis();
    private BigDecimal ZERO = new BigDecimal(0);
    private Map<String, Long> resubscriptions = new ConcurrentHashMap<>();
    private Set<String> activeSubscriptins = new ConcurrentSkipListSet<>();

    public ExampleClient(URI serverUri, Draft draft) {
        super(serverUri, draft);
    }

    public ExampleClient(URI serverURI) {
        super(serverURI);
    }

    public ExampleClient(URI serverUri, Map<String, String> httpHeaders) {
        super(serverUri, httpHeaders);
    }

    public void subscribe(String pair) {
        send("{\n" +
                "  \"event\": \"subscribe\",\n" +
                "  \"pair\": [\n" +
                "    \"" + pair + "\"\n" +
                "  ],\n" +
                "  \"subscription\": {\n" +
                "    \"name\": \"book\", \"depth\": 10\n" +
                "  }\n" +
                "}");
    }

    public void unsubscribe(String pair) {
        send("{\n" +
                "  \"event\": \"unsubscribe\",\n" +
                "  \"pair\": [\n" +
                "    \"" + pair + "\"\n" +
                "  ],\n" +
                "  \"subscription\": {\n" +
                "    \"name\": \"book\", \"depth\": 10\n" +
                "  }\n" +
                "}");
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {
        subscribe("ETH/EUR");
        subscribe("XBT/EUR");
        subscribe("XBT/USD");
        subscribe("XBT/GBP");
        subscribe("ETH/USD");
        subscribe("ETH/GBP");
        subscribe("ETH/XBT");

        subscribe("XRP/EUR");
        subscribe("XRP/USD");
        subscribe("XRP/GBP");
        subscribe("XRP/XBT");

        subscribe("XLM/EUR");
        subscribe("XLM/USD");
        subscribe("XLM/GBP");
        subscribe("XLM/XBT");

        subscribe("ADA/EUR");
        subscribe("ADA/USD");
        subscribe("ADA/GBP");
        subscribe("ADA/XBT");

        subscribe("DOT/EUR");
        subscribe("DOT/USD");
        subscribe("DOT/GBP");
        subscribe("DOT/XBT");

        subscribe("LTC/EUR");
        subscribe("LTC/USD");
        subscribe("LTC/GBP");
        subscribe("LTC/XBT");

        subscribe("LINK/EUR");
        subscribe("LINK/USD");
        subscribe("LINK/GBP");
        subscribe("LINK/XBT");

        subscribe("BCH/EUR");
        subscribe("BCH/USD");
        subscribe("BCH/GBP");
        subscribe("BCH/XBT");

        subscribe("UNI/EUR");
        subscribe("UNI/USD");
        subscribe("UNI/GBP");
        subscribe("UNI/XBT");

        subscribe("USDT/EUR");
        subscribe("USDT/USD");
        subscribe("USDT/GBP");
        subscribe("USDT/XBT");
        System.out.println("opened connection");
        // if you plan to refuse connection based on ip or httpfields overload: onWebsocketHandshakeReceivedAsClient
    }

    @Override
    public void onMessage(String message) {
        try {
            KrakenEvent event = objectReader.readValue(message);
            //System.out.println(i + ": " + message);

            if (event instanceof OrderBookSnapshot) {
                i++;
                String pair = ((OrderBookSnapshot) event).getSymbol();

                OrderBookData data = ((OrderBookSnapshot) event).getOrderBookData();

                updateOrderBook(pair, data);

                if (activeSubscriptins.contains(pair)) {
                    long checksum = calculateChecksum(pair);

                    if (data.getChecksum() != 0 && checksum != data.getChecksum()) {
                        //System.out.println("Wrong checksum for pair " + pair + ": " + checksum + " [received " + data.getChecksum() + "]");
                        //System.out.println("Unsubscribing/resubscribing");

                        activeSubscriptins.remove(pair);
                        unsubscribe(pair);

                        Long pairResubscriptions = resubscriptions.get(pair);
                        if (pairResubscriptions == null) {
                            pairResubscriptions = 0L;
                        }
                        resubscriptions.put(pair, ++pairResubscriptions);
                    } else {
                        //System.out.println("Checksum OK for pair " + pair);
                    }
                }

                if (i % 1000 == 0) {
                    System.out.print(".");
                }

                if (i % 10000 == 0) {
                    System.out.println();
                    long elapsedSeconds = Math.round((System.currentTimeMillis() - startTime) / 1000f);
                    System.out.println("Updates received: " + i);
                    System.out.println("Seconds elapsed: " + elapsedSeconds);
                    System.out.println("Updates/s: " + Math.round(i / (float) elapsedSeconds));
                    System.out.println("Resubscribes: " + resubscriptions);
                    System.out.println("Active subscriptions: " + activeSubscriptins);
                    System.out.println(askOrderBook);
                    System.out.println(bidOrderBook);
                }
            } else if (event instanceof SubscriptionStatus) {
                //System.out.println(event);
                String pair = ((SubscriptionStatus) event).getPair();

                if ("unsubscribed".equals(((SubscriptionStatus) event).getStatus())) {
                    askOrderBook.put(pair, new ConcurrentSkipListMap<>());
                    bidOrderBook.put(pair, new ConcurrentSkipListMap<>());

                    subscribe(((SubscriptionStatus) event).getPair());
                } else if ("subscribed".equals(((SubscriptionStatus) event).getStatus())) {
                    activeSubscriptins.add(pair);
                }
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    private void updateOrderBook(String pair, OrderBookData data) {
        Map<String, BigDecimal> pairAskOrderBook = askOrderBook.get(pair);
        if (pairAskOrderBook == null) {
            pairAskOrderBook = new ConcurrentSkipListMap<>();
            askOrderBook.put(pair, pairAskOrderBook);
        }
        Map<String, BigDecimal> pairBidOrderBook = bidOrderBook.get(pair);
        if (pairBidOrderBook == null) {
            pairBidOrderBook = new ConcurrentSkipListMap<>();
            bidOrderBook.put(pair, pairBidOrderBook);
        }

        for (OrderBookEntry entry : data.getAskEntries()) {
            BigDecimal qty = new BigDecimal(entry.getQuantity());

            if (qty.compareTo(ZERO) == 0) {
                pairAskOrderBook.remove(entry.getPrice());
            } else {
                pairAskOrderBook.put(entry.getPrice(), qty);
            }
        }

        for (OrderBookEntry entry : data.getBidEntries()) {
            BigDecimal qty = new BigDecimal(entry.getQuantity());

            if (qty.compareTo(ZERO) == 0) {
                pairBidOrderBook.remove(entry.getPrice());
            } else {
                pairBidOrderBook.put(entry.getPrice(), qty);
            }
        }
    }

    private long calculateChecksum(String pair) {
        Map<String, BigDecimal> pairAskOrderBook = askOrderBook.get(pair);
        Map<String, BigDecimal> pairBidOrderBook = bidOrderBook.get(pair);

        NavigableSet<String> askPrices = ((ConcurrentSkipListMap) pairAskOrderBook).navigableKeySet();
        NavigableSet<String> bidPrices = ((ConcurrentSkipListMap) pairBidOrderBook).descendingKeySet();

        StringBuilder bidInput = new StringBuilder();
        StringBuilder askInput = new StringBuilder();

        if (askPrices.size() < 10 || bidPrices.size() < 10) {
            return 0;
        }

        Iterator<String> askIterator = askPrices.iterator();
        Iterator<String> bidIterator = bidPrices.iterator();

        //System.out.println(pairAskOrderBook);
        //System.out.println(pairBidOrderBook);

        for (int i = 0; i < 10; i++) {
            String askPrice = askIterator.next();
            String bidPrice = bidIterator.next();
            String askQty = pairAskOrderBook.get(askPrice).toPlainString();
            String bidQty = pairBidOrderBook.get(bidPrice).toPlainString();

            askInput.append(formatNumberForChecksum(askPrice));
            askInput.append(formatNumberForChecksum(askQty));
            bidInput.append(formatNumberForChecksum(bidPrice));
            bidInput.append(formatNumberForChecksum(bidQty));
        }

        String crcInput = askInput.append(bidInput).toString();

        //System.out.println("CRC input: " + crcInput);

        CRC32 crc32 = new CRC32();
        crc32.update(crcInput.getBytes());

        return crc32.getValue();
    }

    private String formatNumberForChecksum(String input) {
        return input.replaceAll("\\.", "").replaceFirst("^0*", "");
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        // The codecodes are documented in class org.java_websocket.framing.CloseFrame
        System.out.println(
                "Connection closed by " + (remote ? "remote peer" : "us") + " Code: " + code + " Reason: "
                        + reason);
    }

    @Override
    public void onError(Exception ex) {
        ex.printStackTrace();
        // if the error is fatal then onClose will be called additionally
    }

    public static void main(String[] args) throws URISyntaxException {
        ExampleClient c = new ExampleClient(new URI(
                "wss://ws.kraken.com"));
        c.connect();
    }

}