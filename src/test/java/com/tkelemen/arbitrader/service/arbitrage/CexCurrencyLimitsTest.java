package com.tkelemen.arbitrader.service.arbitrage;

import com.tkelemen.arbitrader.client.cex.rest.CurrencyLimitsClient;
import com.tkelemen.arbitrader.client.cex.rest.message.CurrencyLimitsResponse;
import com.tkelemen.arbitrader.config.ApplicationConfig;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class CexCurrencyLimitsTest {
    public static void main(String[] args) {
        ApplicationConfig config = new ApplicationConfig();
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream("application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        config.setCexApiRestBaseUrl(prop.getProperty("cex.apiRestBaseUrl"));

        CurrencyLimitsClient client = new CurrencyLimitsClient(config);
        CurrencyLimitsResponse response = client.get();
        System.out.println(response);
    }
}
