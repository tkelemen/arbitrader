package com.tkelemen.arbitrader.service.arbitrage;

import com.tkelemen.arbitrader.client.cex.rest.CancelOrderClient;
import com.tkelemen.arbitrader.client.cex.rest.OpenOrdersClient;
import com.tkelemen.arbitrader.config.ApplicationConfig;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class CexCancelOrderTest {
    public static void main(String[] args) {
        ApplicationConfig config = new ApplicationConfig();
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream("application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        config.setCexApiKey(prop.getProperty("cex.apiKey"));
        config.setCexApiRestBaseUrl(prop.getProperty("cex.apiRestBaseUrl"));
        config.setCexApiSecret(prop.getProperty("cex.apiSecret"));
        config.setCexApiUsername(prop.getProperty("cex.apiUsername"));

        CancelOrderClient client = new CancelOrderClient(config);
        System.out.println(client.cancel("65079649951"));
    }
}
