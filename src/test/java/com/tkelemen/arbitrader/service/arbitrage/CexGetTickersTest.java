package com.tkelemen.arbitrader.service.arbitrage;

import com.tkelemen.arbitrader.client.cex.rest.GetTickersClient;
import com.tkelemen.arbitrader.client.cex.rest.message.GetTickersResponse;
import com.tkelemen.arbitrader.config.ApplicationConfig;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

public class CexGetTickersTest {
    public static void main(String[] args) {
        ApplicationConfig config = new ApplicationConfig();
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream("application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        config.setCexApiRestBaseUrl(prop.getProperty("cex.apiRestBaseUrl"));

        GetTickersClient getTickersClient = new GetTickersClient(config);
        GetTickersResponse response = getTickersClient.get(new ArrayList<>(Arrays.asList("USD", "EUR", "GBP")));
        System.out.println(response);
    }
}
