package com.tkelemen.arbitrader.service.arbitrage;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class CexWithdrawTest {
    public static void main(String[] args) {
        SeleniumReader reader = new SeleniumReader(null);
        reader.run();
    }


    static class SeleniumReader implements Runnable {
        private String url;

        SeleniumReader(String url) {
            this.url = url;
        }

        @Override
        public void run() {
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--window-size=1920,1200", "--ignore-certificate-errors");
            WebDriver driver = new ChromeDriver(options);

            WebDriverWait wait = new WebDriverWait(driver, 60);
            try {
                driver.get("https://cex.io/auth/login");

                driver.findElement(By.cssSelector("input[name=email]")).sendKeys("tomaskel@gmail.com");
                driver.findElement(By.cssSelector("input[name=password]")).sendKeys("t86o04m01i");

                driver.findElement(By.id("sign-in-button")).click();

            } catch (WebDriverException e) {
                System.err.println("Selenium WebDriver error: " + e.toString());
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (!Thread.interrupted()) {
                    String screenshotPath = System.getProperty("java.io.tmpdir") + File.separator + "discord_" + Thread.currentThread().getId() + ".png";

                    System.out.println("Execution finished, saving screenshot to: " + screenshotPath);
                    takeScreenShot(driver, screenshotPath);
                }
                driver.quit();
            }
        }

        private void takeScreenShot(WebDriver webdriver, String destinationPath) {
            try {
                TakesScreenshot scrShot = ((TakesScreenshot) webdriver);

                File srcFile = scrShot.getScreenshotAs(OutputType.FILE);

                FileCopyUtils.copy(srcFile, new File(destinationPath));
            } catch (IOException e) {
                System.err.println("Could not save screenshot: " + e.toString());
            }
        }
    }
}
