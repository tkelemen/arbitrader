package com.tkelemen.arbitrader.service.arbitrage;

import com.tkelemen.arbitrader.model.Address;
import com.tkelemen.arbitrader.service.arbitrage.withdraw.CexWithdrawExecutorService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.math.BigInteger;

@SpringBootTest
public class CexWithdrawServiceTest {
    @Autowired
    private CexWithdrawExecutorService cexWithdrawExecutorService;

    @Test
    public void testWithdraw() {
        cexWithdrawExecutorService.initiateWithdraw("XLM", new BigDecimal(100), Address.create("GAHK7EEG2WWHVKDNT4CEQFZGKF2LGDSW2IVM4S5DP42RBW3K6BTODB4A", BigInteger.valueOf(1035616594)));
    }
}
