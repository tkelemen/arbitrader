package com.tkelemen.arbitrader.service.arbitrage;

import com.tkelemen.arbitrader.client.cex.rest.MassCancelPlaceOrdersClient;
import com.tkelemen.arbitrader.client.cex.rest.message.MassCancelPlaceOrdersRequest;
import com.tkelemen.arbitrader.config.ApplicationConfig;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Properties;

public class CexMassCancelPlaceTest {
    public static void main(String[] args) {
        ApplicationConfig config = new ApplicationConfig();
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream("application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        config.setCexApiKey(prop.getProperty("cex.apiKey"));
        config.setCexApiRestBaseUrl(prop.getProperty("cex.apiRestBaseUrl"));
        config.setCexApiSecret(prop.getProperty("cex.apiSecret"));
        config.setCexApiUsername(prop.getProperty("cex.apiUsername"));

        MassCancelPlaceOrdersClient client = new MassCancelPlaceOrdersClient(config);
        MassCancelPlaceOrdersRequest request = new MassCancelPlaceOrdersRequest();
        request.addCancelOrder("66243700909");
        request.addCancelOrder("66241392731");
        request.addCancelOrder("66243388593");
        request.addCancelOrder("66246392731");
        request.addCancelOrder("66246392731");
        request.addPlaceOrder("XRP/GBP", MassCancelPlaceOrdersRequest.PlaceOrderType.BUY, new BigDecimal("0.4"), new BigDecimal("1000"));
        request.addPlaceOrder("XRP/GBP", MassCancelPlaceOrdersRequest.PlaceOrderType.BUY, new BigDecimal("0.401"), new BigDecimal("100"));
        System.out.println(client.cancelPlace(request));
    }
}
