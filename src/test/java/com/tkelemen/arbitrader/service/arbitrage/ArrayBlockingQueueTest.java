package com.tkelemen.arbitrader.service.arbitrage;

import org.junit.jupiter.api.Test;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class ArrayBlockingQueueTest {

    @Test
    public void queueOfferTest() {
        Queue<Integer> queue = new ArrayBlockingQueue<>(3);
        queue.offer(1);
        queue.offer(2);
        queue.offer(3);
        queue.offer(4);
        queue.offer(5);

        System.out.println(queue);
    }
}
