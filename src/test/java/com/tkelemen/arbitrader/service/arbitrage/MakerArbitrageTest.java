package com.tkelemen.arbitrader.service.arbitrage;

import com.tkelemen.arbitrader.service.arbitrage.maker.MakerArbitrageExecutorService;
import com.tkelemen.arbitrader.service.arbitrage.maker.MakerArbitrageFinderService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MakerArbitrageTest {
    private final MakerArbitrageFinderService makerArbitrageFinderService;

    private final MakerArbitrageExecutorService makerArbitrageExecutorService;

    @Autowired
    public MakerArbitrageTest(ArbitrageManagerService arbitrageManagerService,
                              MakerArbitrageFinderService makerArbitrageFinderService,
                              MakerArbitrageExecutorService makerArbitrageExecutorService) {

        this.makerArbitrageFinderService = makerArbitrageFinderService;
        this.makerArbitrageExecutorService = makerArbitrageExecutorService;

        arbitrageManagerService.initTrading();
        arbitrageManagerService.subscribeForMarketData();
    }

    @Test
    public void checkPossibleProfits() throws InterruptedException {
        Thread.sleep(20000);

        System.out.println(makerArbitrageFinderService.getCurrentOpportunities());
    }

}
