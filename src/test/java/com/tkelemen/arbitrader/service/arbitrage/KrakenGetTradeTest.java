package com.tkelemen.arbitrader.service.arbitrage;

import com.tkelemen.arbitrader.client.kraken.domain.OrderDirection;
import com.tkelemen.arbitrader.client.kraken.domain.OrderType;
import com.tkelemen.arbitrader.client.kraken.rest.AddOrderClient;
import com.tkelemen.arbitrader.client.kraken.rest.GetTradeInfoClient;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.exception.RestClientException;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Properties;

public class KrakenGetTradeTest {
    public static void main(String[] args) throws RestClientException {
        ApplicationConfig config = new ApplicationConfig();
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream("application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        config.setKrakenApiKey(prop.getProperty("kraken.apiKey"));
        config.setKrakenApiPrivateKey(prop.getProperty("kraken.apiPrivateKey"));
        config.setKrakenApiRestBaseUrl(prop.getProperty("kraken.apiRestBaseUrl"));
        config.setKrakenWebSocketUrl(prop.getProperty("kraken.wsUrl"));

        GetTradeInfoClient client = new GetTradeInfoClient(config);
        System.out.println(client.get("T6V7Q6-LCISP-EZBWLL"));
    }
}
