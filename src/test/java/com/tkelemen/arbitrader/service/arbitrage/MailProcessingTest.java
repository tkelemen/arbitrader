package com.tkelemen.arbitrader.service.arbitrage;

import com.opencsv.CSVWriter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import javax.mail.*;
import javax.mail.search.SearchTerm;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MailProcessingTest {
    private static final int MESSAGE_COUNT_TO_PROCESS = 10;

    public static void main(String[] args) {
        Properties props = new Properties();

        props.setProperty("mail.store.protocol", "imap");
        props.setProperty("mail.mime.base64.ignoreerrors", "true");
        props.setProperty("mail.imap.partialfetch", "false");
        props.setProperty("mail.imaps.partialfetch", "false");

        Session session = Session.getDefaultInstance(props, null);
        Store store = null;

        Map<String, Integer> withdrawCounts = new HashMap<>();
        List<String> requestedAmountMissing = new ArrayList<>();
        List<String> commissionMissing = new ArrayList<>();
        List<String> transactionPresent = new ArrayList<>();

        try {
            store = session.getStore("imaps");
            store.connect("imap.gmail.com", 993, "tomaskel@gmail.com", "bjdyjsmgqzztcpft");

            String folder = "cex";
            Folder inbox = store.getFolder(folder);
            inbox.open(Folder.READ_ONLY);

            int totalCount = inbox.getMessageCount();
            int fromId = totalCount > MESSAGE_COUNT_TO_PROCESS ? totalCount - MESSAGE_COUNT_TO_PROCESS : 1;

            SearchTerm searchTerm = new SearchTerm() {
                @Override
                public boolean match(Message message) {
                    try {
                        return message.getSubject().contains("Crypto Withdrawal Confirmed") || message.getSubject().contains("Ledger Withdrawal Confirmed");
                    } catch (MessagingException e) {
                        System.err.println("Error while processing email: " + e);
                        return false;
                    }
                }
            };

            //Message[] messages = inbox.search(searchTerm);
            Message[] messages = inbox.getMessages(15250, totalCount);
            //Message[] messages = inbox.getMessages();

            if (messages.length == 0) {
                System.out.println("No e-mails found.");
            }

            final FetchProfile fp = new FetchProfile();
            fp.add(FetchProfile.Item.ENVELOPE);
            fp.add(FetchProfile.Item.CONTENT_INFO);

            inbox.fetch(messages, fp);

            System.out.println("Processing " + messages.length + " messages");

            CSVWriter writer = new CSVWriter(new FileWriter("cex_withdrawals.csv", true));
            writer.writeNext(new String[] {"Date", "Asset", "Withdraw amount", "Fee", "Transaction id"});

            int id = 15250;
            for (Message message : messages) {
                id++;
                if(!store.isConnected()) {
                    store.connect("imap.gmail.com", 993, "tomaskel@gmail.com", "ujxfeuwogfqamdnj");
                }
                if(!inbox.isOpen()) {
                    inbox.open(Folder.READ_WRITE);
                }
                /*if (message.getFlags().contains(Flags.Flag.SEEN)) {
                    System.out.println("Skipping seen message with subject: " + message.getSubject());
                    continue;
                }*/

                if (message.match(searchTerm)) {

                    System.out.println("Processing message with subject: " + message.getSubject() + " (id " + id);

                    Pattern subjectPattern = Pattern.compile("(Crypto|Ledger) Withdrawal Confirmed — (.*) \\(GMT\\)");
                    Matcher subjectMatcher = subjectPattern.matcher(message.getSubject());

                    if(!subjectMatcher.matches()) {
                        System.out.println("No subject match");
                    }
                    String date = subjectMatcher.group(2);

                    System.out.print("[Date: " + date);
                    /*if(!subjectMatcher.find() || !"10".equals(subjectMatcher.group(2))) {
                        System.out.println("Not in october");
                        continue;
                    }*/

                    Document mail = Jsoup.parse(message.getContent().toString());

                    //System.out.println("Processing email body: " + mail.text());

                    Pattern pattern = Pattern.compile("Requested amount: ([A-Z]{3,4}) (\\d*\\.\\d+|\\d+\\.\\d*|\\d*)");
                    Matcher matcher = pattern.matcher(mail.text());

                    String ticker = null;
                    String amount = null;
                    if(matcher.find() && matcher.group(1) != null && matcher.group(2) != null) {
                        ticker = matcher.group(1);
                        amount = matcher.group(2);

                        System.out.print(", Requested amount: " + amount + " " + ticker);

                        Integer count = withdrawCounts.get(ticker);
                        if (count == null) {
                            count = 1;
                        } else {
                            count++;
                        }

                        withdrawCounts.put(ticker, count);
                    } else {
                        requestedAmountMissing.add(message.getSubject());
                    }

                    Pattern commissionPattern = Pattern.compile("Commission: ([A-Z]{3,4}) (\\d*\\.\\d+|\\d+\\.\\d*|\\d*)");
                    Matcher commissionPatternMatcher = commissionPattern.matcher(mail.text());

                    String fee = null;
                    if(commissionPatternMatcher.find() && commissionPatternMatcher.group(1) != null && commissionPatternMatcher.group(2) != null) {
                        System.out.print(", Commission: " + commissionPatternMatcher.group(2) + " " + commissionPatternMatcher.group(1));
                        fee = commissionPatternMatcher.group(2);
                    } else {
                        commissionMissing.add(message.getSubject());
                    }

                    Pattern transactionHashPattern = Pattern.compile("Transaction hash: ([a-zA-Z0-9]+)");
                    Matcher transactionHashPatternMatcher = transactionHashPattern.matcher(mail.text());

                    String hash = "";
                    if(transactionHashPatternMatcher.find() && transactionHashPatternMatcher.group(1) != null) {
                        hash = transactionHashPatternMatcher.group(1);
                        System.out.print(", Transaction hash: " + hash);
                        transactionPresent.add(message.getSubject());
                    }

                    System.out.println("]");
                    writer.writeNext(new String[] { date, ticker, amount, fee, hash });
                    writer.flush();
                }
            }

            //writer.close();
            inbox.close(false);
            store.close();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println(withdrawCounts);
            System.out.println(requestedAmountMissing);
            System.out.println(commissionMissing);
            System.out.println(transactionPresent);
        }
    }
}
