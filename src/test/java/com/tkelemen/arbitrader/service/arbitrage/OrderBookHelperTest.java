package com.tkelemen.arbitrader.service.arbitrage;

import com.tkelemen.arbitrader.model.DefaultOrderBook;
import com.tkelemen.arbitrader.model.OrderBook;
import com.tkelemen.arbitrader.service.market.OrderBookHelper;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Map;

public class OrderBookHelperTest {
    @Test
    public void testQuoteCalculationSpeed() {
        OrderBook orderBook = new DefaultOrderBook("TEST");
        orderBook.setBidLevel("0.128012", new BigDecimal("100"));
        orderBook.setBidLevel("0.228821", new BigDecimal("111"));
        orderBook.setBidLevel("0.239212", new BigDecimal("234"));
        orderBook.setBidLevel("0.299321", new BigDecimal("546"));
        orderBook.setBidLevel("0.310110", new BigDecimal("788"));
        orderBook.setBidLevel("0.311111", new BigDecimal("1534"));
        orderBook.setBidLevel("0.321113", new BigDecimal("1934"));
        orderBook.setBidLevel("0.354892", new BigDecimal("2410"));
        orderBook.setBidLevel("0.388120", new BigDecimal("5015"));
        orderBook.setBidLevel("0.389130", new BigDecimal("1489"));
        orderBook.setBidLevel("0.391130", new BigDecimal("7895"));
        orderBook.setBidLevel("0.421113", new BigDecimal("4454"));
        orderBook.setBidLevel("0.441892", new BigDecimal("554"));
        orderBook.setBidLevel("0.488120", new BigDecimal("971"));
        orderBook.setBidLevel("0.569130", new BigDecimal("9871"));
        orderBook.setBidLevel("0.591130", new BigDecimal("10111"));
        orderBook.setBidLevel("0.621113", new BigDecimal("14112"));
        orderBook.setBidLevel("0.641892", new BigDecimal("25101"));
        orderBook.setBidLevel("0.788120", new BigDecimal("35471"));
        orderBook.setBidLevel("0.969130", new BigDecimal("79445"));
        orderBook.setBidLevel("1.591130", new BigDecimal("99811"));

        long currTime = System.currentTimeMillis();
        for(int i = 1; i <= 300000; i++) {
            OrderBookHelper.calculateQuoteQuantity(orderBook, new BigDecimal(i ));
        }
        System.out.println("BigDecimal took: " + (System.currentTimeMillis() - currTime) + " ms");

    }
}
