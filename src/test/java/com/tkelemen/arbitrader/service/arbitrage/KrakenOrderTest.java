package com.tkelemen.arbitrader.service.arbitrage;

import com.tkelemen.arbitrader.client.kraken.domain.OrderType;
import com.tkelemen.arbitrader.client.kraken.rest.AddOrderClient;
import com.tkelemen.arbitrader.config.ApplicationConfig;
import com.tkelemen.arbitrader.exception.RestClientException;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Properties;

public class KrakenOrderTest {
    public static void main(String[] args) throws RestClientException {
        ApplicationConfig config = new ApplicationConfig();
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream("application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        config.setKrakenApiKey(prop.getProperty("kraken.apiKey"));
        config.setKrakenApiPrivateKey(prop.getProperty("kraken.apiPrivateKey"));
        config.setKrakenApiRestBaseUrl(prop.getProperty("kraken.apiRestBaseUrl"));
        config.setKrakenWebSocketUrl(prop.getProperty("kraken.wsUrl"));

        AddOrderClient client = new AddOrderClient(config);
        System.out.println(client.sell(OrderType.MARKET, "LUNA/USD", new BigDecimal(50000)));
    }
}
