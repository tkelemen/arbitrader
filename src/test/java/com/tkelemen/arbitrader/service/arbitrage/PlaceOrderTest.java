package com.tkelemen.arbitrader.service.arbitrage;

import com.tkelemen.arbitrader.client.cex.rest.PlaceOrderClient;
import com.tkelemen.arbitrader.client.cex.rest.message.PlaceMarketOrderResponse;
import com.tkelemen.arbitrader.client.cex.rest.message.PlaceOrderResponse;
import com.tkelemen.arbitrader.config.ApplicationConfig;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Properties;

public class PlaceOrderTest {
    public static void main(String[] args) {
        ApplicationConfig config = new ApplicationConfig();
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream("application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        config.setCexApiKey(prop.getProperty("cex.apiKey"));
        config.setCexApiRestBaseUrl(prop.getProperty("cex.apiRestBaseUrl"));
        config.setCexApiSecret(prop.getProperty("cex.apiSecret"));
        config.setCexApiUsername(prop.getProperty("cex.apiUsername"));

        //PlaceOrderClient placeOrder = new PlaceOrderClient(config);
        //PlaceOrderResponse response = placeOrder.marketBuy("XTZ/USD", new BigDecimal(20));

        PlaceOrderClient placeOrder = new PlaceOrderClient(config);
        PlaceMarketOrderResponse response = placeOrder.marketSell("XRP/USDT", new BigDecimal(20));

        System.out.println(response.toString());
    }
}
